---
title: "Sobre mim"
author: Blumen Herzenschein
---

{{< image src="/2023/01/flowers_heartshine_no_bg-240x240-1.png" style="width:20%" position="center" >}}

Olá, seja bem vindo ao meu website. Ele foi feito com [Hugo](https://gohugo.io/) usando o [tema hello-friend-ng](https://themes.gohugo.io/themes/hugo-theme-hello-friend-ng/). Este blog serve pra espairar e relatar minhas descobertas.

Sou um furry brasileiro, austero mas de bom coração.

Estudei Letras com formação em Português e Alemão por cinco anos na Universidade de São Paulo e trabalhei como tradutor e revisor de artigos acadêmicos desde 2018. Meu par de idiomas principal é Português Brasileiro <-> Inglês Americano.

Atualmente trabalho com documentação técnica, algo que encaixa bem com o fato de eu gostar de ajudar os outros.

Meus interesses são:

* Software: Não ligo muito para hardware. Tenho apreciado a filosofia do código aberto faz um tempo e prefiro licenças copyleft. Brinquei um pouco com C, C#, JavaScript e Perl, mas C++ é o que peguei o jeito. Desenvolvimento com GUI é massa, e o Qt deixa o C++ bem mais agradável de usar. Não curto tanto algoritmos quanto criar um programa que eu posso efetivamente ver. KDE é a comunidade de software que escolhi.

* Fandoms: Já li muitos artigos sobre psicologia social, especialmente dinâmicas de grupo, e amo o trabalho acadêmico feito pelo [International Anthropomorphic Research Project](https://furscience.com/) de estudar o furry fandom e quebrar os preconceitos que as pessoas têm sobre ele.

* Saúde: Gosto de aprender sobre e fazer coisas que me ajudem a prevenir potenciais problemas de saúde a longo termo, como técnicas de exercício, alongamentos, hardware ergonômico, e me exercitar para ficar bem nas minhas "meias de programação". Eu sigo canais do YouTube como o Institute of Human Anatomy.

* Jogos: Não sou tão fã de jogos AAA quanto de jogos com dinâmicas, lore e design interessantes. Prefiro RPGs e Metroidvanias, alguns FPS. Músicas de videogame (VGM) são meu gosto principal por música, de tal forma que quando vou a um karaoke não sei de muitas letras para cantar. Eu acompanho alguns speedrunners and poderia até fazer o mesmo se tivesse hardware melhorzinho. Eu sigo canais do YouTube como Nitro Rad, Design Doc, Daryl Talks Games e 8-bit Music Theory.

* Narratologia: Tenho algum interesse em técnicas narrativas em mídias específicas, como livros, séries, filmes e jogos. O mesmo se aplica a cinematografia e como ângulos de câmera, trilhas sonoras e afins podem alterar o aspecto narrativo da obra. Eu sigo canais de YouTube como Savage Books e Hello Future Me.
