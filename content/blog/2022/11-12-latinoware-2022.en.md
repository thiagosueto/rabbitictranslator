---
title: KDE Network Brazil – Latinoware 2022
author: Blumen Herzenschein
date: 2022-11-12
url: latinoware-2022
aliases: /wordpress/index.php/2022/11/12/latinoware-2022/
tags:
  - Planet KDE
---

{{< figure src="/2022/11/latinowaresmol.jpg" >}}

From 01/11 until 05/11, I went from São Paulo to Foz do Iguaçu in Paraná to tend to the KDE booth at the biggest free software event of Latin America, Latinoware 2022, together with two great people who have been attending Latinoware for years, Pedro and Barbara, and who have been contributing to KDE longer than I am.

Immediately after arriving at the hotel close to the midnight of 01/11, I got a heartwarming greeting from other event participants who would present talks over the next few days. I did my check-in afterwards and went to my bedroom, where I just dozed off.

## The first day

The next day, 02/11, the KDE booth would be open. By the time I arrived I was late by about an hour or so and ended up missing the opening of the booth and the first morning photos, but it was not an issue.

The booth consisted of a small space between 2x3 or 2x4 meters, I'm not entirely sure, where no more than 6 people could be in at the same time, and it was the right size for a 3 people team to work, manageable. At the back we had a blue KDE banner, a glass table and three chairs, and at the front a booth table where dozens of stickers, bookmarkers and keychains were being distributed while either Barbara or Pedro were presenting KDE to the passersby.

Initially I watched the way they were presenting KDE, and started assisting Pedro with finishing the 3D printed keychains that were to be distributed over the next few days. Barbara was focusing on presenting Krita and Kdenlive, which are her specialty as a content creator and journalist, and Pedro was focusing on community oriented matters and presenting our groups and social media, especially the one on Telegram, the most used group we have in Brazil.

Anyone who knows me IRL is aware that I take a bit to get warmed up to social occasions - I imagine some of my readers might share this trait with me - and I'm quite the introvert, if a bit lacking in the social department, not necessarily shy. A bit terse sometimes. This was my first time with many things too: my first time flying by myself and staying at my own hotel room, my first time attending a free software event, my first time at Latinoware and the first time tending to a booth. You'd imagine a person such as myself would be way too quiet for the task, but that couldn't be farther from the truth.

I stayed at the booth mostly helping with keychains and the occasional talking, and was starting to get excited with showing KDE, Plasma and our applications to people. I brought my laptop to showcase things but I still needed to finish tweaking it, in addition to getting a proper internet connection, and when Barbara went out to lunch, I got the chance to be the main speaker for a while.

{{< figure src="/2022/11/pedrokeychainssmol.jpg" caption="Pedro preparing the keychains for distributing." >}}

Barbara's gaming laptop would be used to showcase her work and the animation she made in Kdenlive for the event. Pedro used his to showcase KDEConnect as well as other software. Both looked like expensive machines, they looked good and modern.

The laptop I brought to the event wasn't particularly good looking, as I bought it used from a friend at a time when I feared my main machine would die and I'd be unable to work on my translations, and this specific laptop got showered with isopropanol at a certain point, the reason for its dark stains and rough texture. However, it's a touch laptop. And I'm a heavy Plasma Wayland user who likes to use rolling release distros. You can see what I was going for, if you're familiar with the current state of touch in Linux. If you want a fully touch enabled experience on Linux, yeah, you can do this in X11, but the Wayland session will be ready for this by default.

So, touch laptops aren't really such a common thing in Brazil, at least not for people like me and the people living near me, people of low middle class or lower income classifications, touch laptops are very much a middle class+ thing. So I was sure people would get excited to have their first experiences with a touch laptop with Linux, especially since it is in a different OS.

This proved to be particularly handy to showcase the Plasma swipe gestures and demonstrating new tech, in addition to letting people draw on Krita.

I had a swipe gesture at the right border of the screen that would hide all applications and show a peek of the desktop. At the upper side I enabled the Overview, a fairly new desktop effect that is similar to GNOME and MacOS. I also had a bottom one for opening the main launcher and a left one to switch between windows, but those were scarcely used.

It was cool to show people that yes, Plasma on tablet mode adapts the size of its components to fit your fingers, that yes, our apps integrate well with a touch experience, and yes, it's that fluid.

After Barbara went back, I went to lunch with Pedro, and was delightfully surprised to see that the bakery/restaurant right to the left of the hotel was fairly inexpensive despite the delicious food, which is often not the case elsewhere. We talked about our experience with free software and programming.

After lunch things got hectic at the booth, as people were really loving the 3D printed keychains. Did I mention the keychains, stickers and bookmarkers were all made by Barbara using only free software at her print shop?

At a certain point, without noticing, I started talking nonstop at the booth. First a quick mention of what KDE (the community) is, and that we provide both a graphical desktop environment - I'd use that swipe gesture to hide all windows to say, "I mean this beautiful thing you can see over here" - that is for Linux and is called Plasma, but that we also provide a collection of applications - I'd use the menu, unhide all windows, show a busy overview - that is mostly crossplatform, running on Windows, Linux, Mac, Android, and even Linux for phones!

{{< figure src="/2022/11/meshowingsmol.jpg" >}}

{{< figure src="/2022/11/metalkingsmol.jpg" caption="Me showcasing Okular." >}}

I started talking about Dolphin and how it is multi-paned, Ghostwriter, our newest addition and the software I am using to draft this very blog post, Kate and how suitable it is for developers and casual notetakers alike, and of course Krita and Kdenlive. I'd end up talking about how configurable Plasma is - while mentioning that you **_really_** don't need to configure it if you're the sort who just wants to use it and do your tasks -, how the touchpad gestures follow your finger movement, and how naturally your theme choice applies to the system.

Whenever I needed a break, I started helping with the keychains again. While I was loud, excited and on automatic mode, repeating the same content for everyone who took a look at our booth, Pedro had a much calmer attitude than me, asking what people studied or worked with, where they were from, which operating system they used or had to use, and then proceeding to suggest certain KDE stuff that would interest people the most, in addition to making the free software and community spirit quite clear to the people passing by. I took note of this.

It might sound silly that I had no initial thought of personalizing my public speech to the needs of my target audience, but it is true. The reason why I started speaking the way I did was because whenever I get excited about things I like, I just start going on about it over and over, but really this mostly satisfies my own desire to talk about such things; when at a public setting like this, I should instead tailor my content to focus on people instead. My purpose in the event was to bring KDE to people, after all. Silly me learned this several years ago, but didn't have much practice.

Having understood this, I'd be sure to practice this a lot over the next two days.

Nearing the end of the day, I was exhausted, but felt accomplished. Presenters and people from other booths would come over to ours, as most of them already knew Barbara and Pedro. A nice Hungarian fellow who was interested in Cyber Security and spoke English started a conversation with Pedro, and he'd be the first English speaker I'd meet at the event. His main interest lead to talking about areas in KDE that are security related, of course: Plasma Vaults, the VPN widget, KGpg, etc. Neither I or Barbara are really familiar with this security stuff, and Pedro would end up providing the best technical explanations at the event in the next few days.

On the matter of language, neither of us three truly knew Spanish to account for the Spanish-speaking population, but while not ideal, this didn't pose such a big issue. We still understood what was being said, the main problem was speaking. However, as the event happened in Brazil and several talks were done in Portuguese, most of the passersby could understand the language when not spoken too fast, and often groups would have their own "interpreters" to clarify things to their companions.

At night, after the event, we went out to have dinner and have a few drinks with the other presenters. On the way back Pedro and I would stay at the hotel restaurant for a while, where we chatted with that Hungarian fellow for several hours, and on occasion other well known people who came to sit with us, like a forensics specialist and a shell master.

While I do interact quite a bit with the international community, it's exclusively through text and it's not often that I get the chance to speak in English. It's not an uncommon thing for translators like myself, mind you. I don't do interpreting, only document translation, so my spoken English was rather rusty. Fortunately my pronunciation is quite decent and I had been making voice calls with friends and giving English lessons to a friend recently, which helped.

I felt myself making progress with my public speaking skills and my spoken English, which made my motivation go up during the next few days.

## The second day - Promotion

On the second day, I woke up too early despite the drinks and, as usual, I had no hangover whatsoever. I went to the booth earlier and took a look at the surroundings.

Our booth was next to the Linux Professional Institute booth, where Jon "maddog" Hall resided with a Santa hat. A few booths to the side, a Debian booth. To our right and back, 3D printing booths.

I had prepared my laptop a bit better, now that I had an internet connection. It's a Dell Inspiron 7348 with an i7 5500 and respective HD 5500 graphics running openSUSE Tumbleweed and Plasma 5.26.2. Its hardware audio was broken, but that wouldn't be an issue.

I had eight virtual desktops, each to showcase one or two programs at a time: Firefox with links to the Krita, Kdenlive, Plasma Mobile, KDE websites; Dolphin and Index; Ghostwriter and Kate; Okular; Elisa; Haruna; Subtitle Composer; Discover and System Settings.

Now my promotional strategy was different; I had in my mind the content I'd showcase people (developed throughout the first day), and from that content list I'd fetch the relevant information that would catch the attention of the users based on what they do.

Most people loved Dolphin's dual panes, although not many were excited to see that it had tab support - just like a browser! - even after mentioning that yes, every tab can have dual panes. Its embedded terminal that follows the folder you're in was particularly attractive to the more technical folks.

Index did not get as much interest. It served more as an entrypoint to say that we have projects like Plasma Mobile, for phones, and certain apps run on Android too, and that would get people excited, if anything, because actual Linux on phones is still not commonplace.

Ghostwriter was interesting to showcase to students of any IT field. Most of the demographic consisted of university students, and I'd say about 70% were in some way related to IT, yet I had to explain what Markdown was to many of those people. "Oh, Github has that" was something I heard at least two times.

Kate is a project that has served me well, and I recently got used to its Session functionality. For the technically inclined, I'd mention its git support and terminal, as well as its tons of plugins for several languages, which lead to some eyebrows raising in interest. For those who didn't mention working in an IT-related field, I mentioned how I used Sessions to keep the notes I took on the English classes I was giving to my friend separate from other notes I take for other activities.

Okular was easy to promote, as most people visiting the booth were students, or had used PDFs at some point to study. It's well known for its power on Linux, but how many people actually use the Quality of Life features of PDF readers? I figured most just used navigation functionality, and I was apparently right.

I had downloaded a Science Direct paper on Plagiarism that I had used at my Academic English classes back at University, just because it was easiest to find and it was properly formatted. I'd start by showcasing annotations, which aren't particularly noteworthy by themselves. Using the touchscreen, of course, which is easier and looks lazier - and thus \*inviting\*.

The actually important part was the sidebar. If the PDF file is properly formatted - and I made this very clear -, there should be a list of chapters where navigation was possible with a single click/touch. The miniature preview was very handy to navigate, which made my demonstration very fast every time. The annotation sidebar wasn't particularly popular, but still handy to know. The bookmarks sidebar was more popular, as I'd reiterate, "whenever you reach that critical point in the PDF, you know, you notice that you're going to revisit it again and again and again, you can bookmark that page and voilà." These aren't actually such noteworthy features, but they make your life easier, and learning about them with Okular specifically means the passersby would be ready to use most of it just by looking at it. Despite it being really fast to navigate, I struggled a bit to use Okular's dense UI with my fingers while not in tablet mode, especially for chapter navigation.

Elisa proved to be hard to promote. Less than 10% - or heck, even 5% - of the people passing by had actual music files on their machines, and almost everyone used Spotify. I find Elisa gorgeous, but beauty cannot best people's usecases. However, the people who did mess with music files were very interested in it, and asked if there was software to manage metadata to show their music well in such music players, in which case I'd mention Kid3.

Haruna was a bit hard to promote too. It's gorgeous too and it uses a powerful backend, MPV, but its beautiful sidebar for managing videos only shows up whenever there is more than one video in the playlist, and I had set it up such that the Plasma 5.26 announcement would play alone in its own folder, so I'd have to switch folders in order to showcase it, every time. I wish the loop button was directly in the UI instead of the settings, as this would have made things much easier to manage.

I focused on its configurability instead, as it is where it shines. I'd show how you can hide the menubar and even the toolbar to keep it minimal and simple, and I'd scroll through the huge list of shortcuts to tailor the video player to the user's needs. In particular, the shortcut one can be set so as to make the video player act like YouTube, that is, a click/touch \_anywhere in the video\_ to pause/play. By then, I forgot entirely that there was already such a thing: right click to pause/play at any time. But I feel like left click would work best, especially on touchscreens.

I showcased Subtitle Composer because it is my favorite. As a translator I'm aware of the software available out there on the other side of the fence; for whatever reason, the most popular subtitling software on Windows are oldware or have bad UX. Subtitling is where open source shines, really, as Aegisub and Subtitle Composer are really the best of the kind. I created the basics for the Subtitle Composer website, sent a patch to it myself, and I once made the Brazilian Portuguese subtitles for the Veritasium video [Gravity is not a force][1].

On the day prior, Barbara had used the About section of Krita to show her own name as the main Brazilian translator of Krita, having done over 6000 segments of very high quality. I tried something similar, mentioning my contributions on the About section of Subtitle Composer. Such things need to be mentioned so as to demonstrate that contributing is not something that is distant from the user, but rather as close as can be, and especially that it can be done by non-programmers too.

Discover was a rather neutral experience to people, as by definition it serves as a proxy to discover more software; to showcase it meant to casually use it in front of them like it was just an expected feature of the system - which is true on all competing major platforms. It was rather unfortunate that I didn't get to showcase its [slightly more populated new homepage][2], but it fit its role. Like the browser pages I kept open, it was used to quickly showcase other software I did not have installed, which I'll be talking about during the third day.

System Settings was quite interesting to see. The look on people's faces was either "Oh wow I want this" or "I couldn't care less". First I'd show its trivial theme switching, which fortunately has been made faster and more fluid in recent versions. I'd end up going back to the light theme so as to make more of the interface clear from afar, but whenever I opened the Overview to show that all windows were following the theme consistently, I'd color someone impressed.

Next I'd show desktop effects, alternating between not-so-useful but impressive animations and the hidden Quality of Life gems. I emphasized several times how this is an optional feature, and how if people really did not care to customize their system and they really just wanted to work and do nothing else, they could. Easily.

One of Plasma's strengths is that the system is supposed to tailor to the user, not the opposite. This was my main motto during the event, and I wanted to break the stereotype that Plasma is made only for customizers.

So… almost everyone loved Wobbly Windows, _**obviously**_. The ones that didn't care much asked about its performance and battery consumption. Once a group of people was obviously into it, I'd mention how after installing Plasma at home they could increase the wobbliness to the max just for fun and see it going crazy on their screen. I increased my pitch such that it would make it clear that I was talking about a one time thing to do exclusively for fun.

Track Mouse had a very helpful usecase: "I'm sure you've been in this situation already: you're writing in Microsoft Word or LibreOffice and the page is just a huge white canvas filled with thin black lines, _**just like the cursor**_, in which case you'd lose it all the time, right? With this, you can avoid that."

After I enabled the Fall Apart effect where windows would fall into pieces, people would get smiles on their faces when I'd casually showcase the desktop without taking it much into account, which was pretty fun to me. People didn't really get tired of it.

Mouse Click Animation got mostly neutral reactions with a few positive ones: I'd mention its relevance in making YouTube tutorials or when doing livestreaming/streaming over Teams, Meet, Discord to make it clear when and where they clicked. It seems most of the audience did not have such a usecase.

Magic Lamp was 50/50, it either impressed or it did not, no in-between. It is a pleasant feature to have, but its purpose is to not stand out, so this sort of reaction is understandable.

Zoom was amusing and mostly a success. It's primarily an accessibility feature, but like the previous utility desktop effects, it can be used for things like making presentations online. It got more people interested than I was expecting, and that was _**before**_ I showed how limitless one could go with the Zoom effect, making even cursor pixels huge on the screen. So people either liked it because of how useful it was, or they found its max values impressive/funny.

While I'm aware that its lack of a zoom cap is probably useless in a general sense, I wouldn't have been able to entice as many people otherwise.

Sadly I was aware of an issue with the Magnifier where it requires a logout or a reboot or something to take effect after switching to it from Zoom, as both features utilize the same keyboard shortcuts, so I couldn't show both in the same go.

I'd have shown the Cube, but it was "under maintenance" for this release, quite the unfortunate timing. But there's always next time.

## The second day - Highlights

The morning was hectic, but the afternoon was "hecticerer".

At Latinoware, the booths had a mostly secondary role compared to the talks. Barbara herself would do two at the event: "How a print shop can work with free software", and "3D Printing at school". In both occasions only Pedro and I tended to the booth.

Three talks were of particular interest to KDE: "How to live without Photoshop" and "Showing how to use free software for graphics design", by Elias Silveira, and "Videography with free software", by Carlos Eduardo Cruz, alias Cadunico.

Elias Silveira uses Krita and Cadunico uses Kdenlive.

At a certain point during the day, Elias stopped by the booth to draw a Konqi using Barbara's Android tablet. This was a great opportunity to highlight how KDE software can run on multiple platforms, and those who doubted would see how true it was in person.

Here's the drawing he made:

{{< figure src="/2022/11/konqidrawingsmol.jpg" caption="Konqi drawing. Note the Android icons at the bottom." >}}

It must be noted that he is not used to drawing without a keyboard and without a dedicated drawing tablet, yet he managed to draw most of his Konqi foundation in about 30 minutes, taking the rest of the time to improve it further. Moreover, _**he used only one layer**_.

The spectators were mesmerized, especially since people who have never drawn using specialized software before might not be aware of the general process, which is different from what one would typically expect.

When drawing something that is to be colored early, one may paint a base color and put several transparent variants above so as to achieve the desired shade, with whites and blacks to lighten or darken certain areas and details. Elias made it clear with his hands, Barbara made it clear with her words.

{{< figure src="/2022/11/eliasdrawingsmol.jpg" caption="The public watching Elias draw." >}}

After drawing this Konqi, he left the booth and we focused on the keychains for the time being. It had been a few hours that the booth had been full of people passing by, it would continue to be full of people wanting a keychain. It was both libre and gratis, after all.

There was a brief period of rest as folks were attending several talks at once, but mostly the booth would have at least three people looking at all times. Some time afterwards, Elias came back and started drawing again, and this time his inspiration would be literally up front.

As mentioned, our booth was positioned right in front of the LPI booth.

I'll let the following video speak for itself:

{{< peertube tube.kockatoo.org k8eKWoFFhviWatciZcdHux >}}

{{< figure src="/2022/11/maddogsmol.jpg" caption="Elias and Jon \"maddog\" Hall, the chairman of the board of the LPI." >}}

The general structure of the drawing would take about half an hour to a full hour, the rest of the time being used to improve the drawing. As you can see, everyone wanted to watch it.

Seeing as me talking wouldn't really be as effective and since I was starting to lose my voice, I set my laptop so that people could draw directly on Krita with it.

Later that day, Cadunico would join the booth and help prepare and edit this very video you just watched using Blender + Kdenlive. His work is, as you can see, now shared over KDE's official YouTube and PeerTube.

{{< figure src="/2022/11/cadunicosmilesmol.jpg" caption="Cadunico working on Blender and Kdenlive with Barbara’s laptop." >}}

On the night of the second day, the event folks had a gathering at a nearby bar, Guns 'N Beer, where people holding their event badge would get a discount. The band that would play there was entirely composed of event folks, too. In that gathering I got to meet many attendees who gave talks at Latinoware every year.

## The third day

The third day was the slowest, but also the day I found more tiring, if anything, because I was starting to feel myself getting voiceless from so much speaking in the few days I've been there.

Despite that, I was still enjoying being the main speaker.

Fairly early on in the day, the keychains would already be depleted.

I feel like I should mention how much stuff we had to distribute. The list of things we brought to give away to the people visiting our booth was the following:

* 30 Konqi and Kate sticker sets (for kids)
* 120 K logo stickers
* 90 Konqi - white background stickers
* 90 Konqi - blue background stickers
* 140 Kdenlive stickers
* 324 K logo - white vinil stickers
* 324 K logo - blue vinil stickers
* 200 K logo - 3D keychains
* 800 KDE bookmarkers

Distributed entirely for free, and entirely made with free software.

{{< figure src="/2022/11/proudstickersmol.jpg" caption="Proudly using a KDE sticker!" >}}

{{< figure src="/2022/11/folkswithstickerssmol.jpg" caption="Students with their KDE stickers!" >}}

The keychains required one of the vinil stickers to be stickied to them, and part of the interactivity of our booth was that people would have to stick it themselves. Barbara and Pedro would demonstrate the procedure to folks and the visitors would do the same thing.

Even with a DYI approach to this, all 200 keychains were given away in a matter of two days and a half.

Despite the fact that each person would grab multiple stickers and bookmarkers together, we can safely say that this three-day venture was a huge success, with several hundred people attending our booth, the majority of which would leave knowing about KDE and the software we provide, with bookmarkers linking to our Brazilian KDE website.

Sometime in the middle of the day, Elias stopped by to draw once again, this time making an impressive GNU warrior holding a drawing tablet as shield and a pen as sword.

{{< figure src="/2022/11/gnu.jpg" >}}

{{< figure src="/2022/11/eliasgnusmol.jpg" caption="GNU made with a single layer in an Android tablet running Krita." >}}

Nearing 5 p.m. we'd be down to three bookmarkers and a dozen stickers left. By the end of the day, we had none left.

But I have to mention five other interesting things about this day before I finish this blog post.

First is that, now, I would remember to mention Konsole for the more technically inclined passersby. It didn't really came to mind in the previous two days because it isn't a prototypical GUI window like the others I was showcasing. Its new plugin infrastructure was made by Tomaz Canabrava, a well known Brazilian contributor, so I felt like it needed special mention to make it clear that the Brazilian community has strong influence in development.

The second is that on that day we'd see multiple educators passing by, which was a great opportunity to showcase GCompris, you know, the software pronounced like French "j'ai compris", "I understand", for teaching stuff to children. I would also mention KStars, and the entire Education category in apps.kde.org.

I had already mentioned it before in the previous days, as there were educators passing by too, but I didn't get the chance to elaborate much on it given the large volume of people. The third day was relatively chill for that.

So based on my perception of people's reactions, they loved it, and were mostly unaware that actual free software existed for education purposes. Moreover, the idea that KDE provided an entire suite of educational software was like icing on the cake.

A father and his 7 year old daughter visited the booth around the time Elias was there. Pedro and Barbara had been talking to them quite a lot in the previous days, especially since Barbara taught her how to make 3D printed keychains. I was resting by then, but I left my laptop on the front so she could draw on Krita, this was the result:

{{< figure src="/2022/11/sofiaprint.png" caption="Sofia’s doodle drawn with her own fingers on the touchscreen." >}}

Two of the last few people visiting our booth were some Danish folks who spoke English, where I got to shine for a bit explaining about our software. I mentioned before that I was giving English classes to a friend, so I was now the "English spokesperson" of the group, and I enjoyed it.

While originally they went there to hear more about Kdenlive, they were primary school teachers, which made the subject more dynamic. Video editing was their hobby, and from so much explaining done by Barbara in the previous days, I managed to learn how to cut videos using the Selection tool, which played well with touch as I could simply drag from the preview to the main timeline. I mentioned GCompris and the rest of the software suite related to education; so far, all educators I'd met at the event taught either adolescents, young adults or adults, but these two were the perfect target audience for GCompris.

{{< figure src="/2022/11/danishfolksmol.jpg" caption="The tall Danish guys, Ole, a regular presenter at the event who is also Danish, and me." >}}

Also, while presenting the main website to them, we noted that it had Svenska, but no Dansk. This was a great opportunity to mention the contribution aspect of our software community, and that in practice, helping translate our stuff is mostly a matter of joining a translation group, getting them to send you the files, translate, then send them back.

This got them rather excited. I figure something as central as the main website being translatable by anyone interested in doing this is a rather appealing idea, both from a personal perspective "I get to translate the main page for KDE, wow", and from a community perspective "My translation will make things more welcoming to other Danish speakers".

The last visitor was the only person to come to talk about the state of touch on Linux, and thankfully this is a topic I'm excited about and know fairly well, so I instructed her on the criteria needed to have a good experience with touchscreens. Namely that of the touchscreen drivers existing for her device in the first place, iio-sensor-proxy (whose main developer is very chill and respectful), Wayland (including Plasma, GNOME and Sway and the fact that the more up-to-date, the better in Plasma's case), and Maliit. I mentioned that yes, one can set touch support on X11 too, but for that she'd need to consult the Arch wiki and set things up herself.

Nearing the end of the event, my laptop was up front with either Haruna playing the Plasma 5.26 announcement video or with Krita to let people draw, while Barbara's laptop would showcase Cadunico's video.

## Results

A huge success.

Over 1100 stickers distributed.

800 bookmarkers distributed.

200 keychains distributed.

3 drawings done in front of people.

1 video for KDE's official YouTube/PeerTube channel.

Socializing with other event participants and getting to know influential people in the free software scene.

I didn't know I had it in me to be such a spokesperson type, and I feel like I could make talks and presentations for KDE often if I had the backing for such.

Understanding the procedure and getting first hand experience of traveling by myself and by plane.

Practicing spoken English, which made me more confident in my skills.

Motivation to contribute more.

—

I hope you liked reading this 10 page blog post, don't forget to check out:

[KDE Brasil on Telegram][3]

[The main KDE website][4]

[The main Brazilian KDE website][5]

[All talks during Latinoware 2022][6]

[Barbara's blog][7]

[Cadunico's website][8]

[Elias' Instagram][9]

 [1]: https://youtu.be/XRr1kaXKBsU
 [2]: https://invent.kde.org/plasma/discover/-/merge_requests/398#note_550637
 [3]: https://t.me/kdebrasil
 [4]: https://kde.org/
 [5]: https://br.kde.org/
 [6]: https://www.youtube.com/c/LatinowarePlay/playlists
 [7]: https://www.barbara.blog.br/blog/
 [8]: https://cadunico.art.br/
 [9]: https://www.instagram.com/elias_silveira_arte/
