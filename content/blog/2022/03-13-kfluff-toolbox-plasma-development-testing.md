---
title: KFluff – Podman + toolbox for Plasma development/testing?
author: Blumen Herzenschein
date: 2022-03-13
url: toolbox-plasma-development-testing
aliases: /wordpress/index.php/2022/03/13/kfluff-toolbox-plasma-development-testing/
tags:
  - Planet KDE
  - Fluff
---
I've revisited toolbox recently, and I had forgotten how much I liked the tool the last time I tried it.

[Podman][1] is a technology developed by Red Hat for managing containers, its most used commands are identical to docker's, it's daemonless (so you don't have a systemd service running all the time, your containers simply run on demand and you can't have a single service borking down all of your containers) and rootless (no root required to manage them). Docker recently got rootless support, but it's [kind of a pain to setup][2]. Podman integrates with things like Cockpit, and one of its most famous niceties is its [ability to generate systemd services][3] for your containers.

[Toolbox][4] is a tool based on podman which hasn't gotten much recognition outside of immutable operating systems like [MicroOS][5], [Silverblue][6]/[Kinoite][7] and [EndlessOS][7]. Its has just four commands and manages podman containers in such a way that the container integrates with your home. This means that, if you enter a podman container using toolbox, you can see and edit things in your home folder. Moreover, you can run GUI applications with it.

But the truly cool thing about it is the implied opposite of this home integration: its root dissociation. It means anything you install to root from within the container will be installed in the container root. So things like packages or, really, any root operation.

In practice, you can:

  * Enter the container
  * Go to your project folder
  * git clone a C++ project with a GUI
  * Install all the build dependencies as root
  * Compile the project
  * Run `sudo cmake --install build` or `sudo ninja install` or `sudo make install`
  * Exit the container
  * Run your GUI project while loading only the libraries installed in the container's root

All while having absolutely zero libraries and development software installed on your system.

This is very handy. But of course, container limitations also apply here. For instance, for user-facing KDE software, unless you're willing to install the entire desktop environment in your container, you'll probably face the issue of programs not running 100% well because of runtime software that we take for granted; you as the container creator have to ensure things work. This is the case for pretty much every DE-related software out there, as far as I've seen. This is usually not an issue with terminal utilities, though.

I'll exemplify what sort of caveats you might face with using toolbox for GUI applications. As a plus, you'll get to know most software used for desktop integration, so you don't have to search for them when playing with toolbox.

## A toolbox compilation in practice

I'm on openSUSE Tumbleweed, which preconfigures toolbox appropriately with its own container registry. I'll be compiling just Kate and nothing else.

To create a container, I just run:

```bash
toolbox create
```

Then a toolbox called toolbox-yourusername-user is created. The container itself runs openSUSE Tumbleweed too.

Let's enter it:

```bash
toolbox enter
```

Install a few necessities:

```bash
sudo zypper install git clang ninja
```

And the essentials for Kate:

```bash
## First enable the source package repository
sudo zypper modifyrepo --enable repo-source
## Then install Kate's build dependencies
sudo zypper source-install --build-deps-only kate
```

Let's make a folder in our home and download the code for Kate:

```bash
mkdir gitkate
cd gitkate
git clone https://invent.kde.org/utilities/kate.git
```

Let's run cmake:

```bash
cmake -B build/
```

And build it:

```bash
cmake --build build/
```

Lastly, let's install it to the container's root:

```bash
sudo cmake --install build
```

And run Kate:

```bash
kate
```

{{< figure src="/2022/03/Screenshot_20220313_003702.png" >}}

This looks awful, but let's look at this by steps.

It's missing text, so we need to install fonts. Hence:

```bash
sudo zypper install noto-sans-fonts hack-fonts
```

{{< figure src="/2022/03/Screenshot_20220313_003817.png" >}}

The fonts look weird, but at least there's text now.

It's also a tad blurry. If we open KRunner, type &#8220;KWin&#8221; and open the KWin debug console, we can see that this window is actually running on XWayland. To fix that, we need QtWayland.

```bash
sudo zypper install libqt5-qtwayland
```

{{< figure src="/2022/03/Screenshot_20220313_003919.png" >}}

Nice, it's sharper now, even if the fonts are still a bit weird, to fix that we need plasma-integration.

```bash
sudo zypper install plasma5-integration-plugin
```

{{< figure src="/2022/03/Screenshot_20220313_004807.png" >}}

You might not see it in the above example, but the cursor being shown over this window is actually Adwaita. Moreover, when we run `kate`, we see: `Icon theme "breeze" not found.`

```bash
sudo zypper install breeze
```

{{< figure src="/2022/03/Screenshot_20220313_005924.png" >}}

Installing Breeze is not really enough apparently.

What we're missing is actually an environment variable, XCURSOR_THEME. If we run the following:

```bash
XCURSOR_THEME=breeze_cursors kate
```

{{< figure src="/2022/03/Screenshot_20220313_010351.png" >}}

Nice. If we want this change to be permanent, we can set this variable in /etc/profile.d inside our container. So:

```bash
echo 'XCURSOR_THEME=breeze_cursors' | sudo tee --append /etc/profile.d/cursortheme.sh
```

## A podman application in practice

So we've seen that, while toolbox lets us have a quite clean development environment that we can edit from the comfort of our home, it poses some challenges because it is pretty much like any other podman or docker container still: very manual.

To minimize those issues, we can use podman's image creation functionality to set up our own multiple sets of development environments.

Let's imagine I am on openSUSE Leap instead of Tumbleweed, and I want a Krypton container to develop only with libraries from git. Moreover, I want to prepare for most of those theming/integration issues.

I'd create a toolbox, enter it, install everything I want, set the cursor theme, anything really.

```bash
toolbox create
toolbox enter

## Add all Krypton repositories
sudo zypper addrepo --priority 90 https://download.opensuse.org/repositories/KDE:/Unstable:/Qt/openSUSE_Tumbleweed/ kdeu-qt5
sudo zypper addrepo --priority 90 https://download.opensuse.org/repositories/KDE:/Unstable:/Frameworks/openSUSE_Factory/ kdeu-kf5
sudo zypper addrepo --priority 90 https://download.opensuse.org/repositories/KDE:/Unstable:/Applications/KDE_Unstable_Frameworks_openSUSE_Factory/ kdeu-apps
sudo zypper addrepo --priority 90 https://download.opensuse.org/repositories/KDE:/Unstable:/Extra/KDE_Unstable_Frameworks_openSUSE_Factory/ kdeu-extra

## Enable the source repo for source-install
sudo zypper modifyrepo --enable repo-source

## Update the package cache
sudo zypper refresh

## Install most general development metapackages
sudo zypper install --type pattern devel_C_C++ devel_kde_frameworks devel_qt5 devel_qt6

## The general necessities
sudo zypper install git clang ninja

## The huge amount of libraries needed for building Plasma
sudo zypper source-install --build-deps-only plasma5-desktop

## Install most runtime software needed for integration
## QtQuick Controls styles are needed for QML, plasma-integration for QtWidgets
sudo zypper install noto-sans-fonts hack-fonts qqc2-breeze-style qqc2-desktop-style plasma5-integration-plugin libqt5-qtwayland breeze xdg-desktop-portal-kde

## Add the cursor theme environment variable
echo 'XCURSOR_THEME=breeze_cursors' | sudo tee --append /etc/profile.d/cursortheme.sh
```

Being done with that, we can create a new podman image based on this existing container. We can exit the container and run a simple command to do so:

```bash
podman commit toolbox-youruserhere-user
```

Podman will then take some time to copy the current state of your container, depending on how t h i c c it is. When it is done, it will spout a numerical ID that references our new image. Copy that, as we will be giving it a new name:

```bash
podman tag <IDHERE> krypton
```

If we then run podman images, we'll see our newly created `localhost/krypton` container available.

If we wish to do so, we can remove our toolbox containers entirely and replace them with the new ones:

```bash
toolbox reset
podman rm toolbox-yourusername toolbox-yourusername-user
toolbox create --reg localhost --img krypton
```

Likewise, we can create as many toolboxes as we want using this image:

```bash
toolbox create --reg localhost --img krypton --container <namehere>
```

## Possible use cases

Toolbox could be used for general development in stable distributions, as its root environment is completely separate from the user's own. So a Debian Stable or Kubuntu LTS user might use a KDE Neon Unstable or an openSUSE Krypton system to compile specific KDE software using git libraries.

It could be used for people interested in packaging their software in a different distro than their own. After all, their packaging tools will be available from the container's package manager. No need for VMs. Moreover, I think it should be possible to use Ubuntu-based images for creating snaps and appimages, though I haven't tried this.

It could also be used for software that is only available in one distro and not another.

KCMs are notorious for their annoying development setup. Either you use kdesrc-build and create an entire Plasma session from git to test it, or install it as root, anything else can be a tad more complicated. So toolbox could be used for KCMs that do not rely on certain services that are inaccessible to the container; when I tried this, most KCMs would actually show up fine. Some unfortunately do not work, like the one I actually wanted to work on, default applications.

It is a good alternative for the current [KDE Neon Docker setup][8]. I might be misremembering, but last I recall, the Neon docker setup works well with desktop integration. However, it does require much more setup than a simple toolbox container (once you know what to install).

I assume it might be useful for the Qt5 to Qt6 port. After all, you can have two containers, one only with Qt5 and KF5 libraries, one only with Qt6 and KF6, make images out of those, and go bonkers with it, comparing the results side by side.

So there you have it; toolbox was fun to learn and test, I hope you'll have fun with it too.

 [1]: https://podman.io/
 [2]: https://docs.docker.com/engine/security/rootless/
 [3]: https://docs.podman.io/en/latest/markdown/podman-generate-systemd.1.html
 [4]: https://docs.fedoraproject.org/en-US/fedora-kinoite/toolbox/
 [5]: https://microos.opensuse.org/
 [6]: https://silverblue.fedoraproject.org/
 [7]: https://kinoite.fedoraproject.org/
 [8]: https://community.kde.org/Neon/Docker
