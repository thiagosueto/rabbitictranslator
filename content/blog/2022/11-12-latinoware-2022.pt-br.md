---
title: Rede KDE Brasil – Latinoware 2022
author: Blumen Herzenschein
date: 2022-11-25
url: latinoware-2022
aliases: /pt-br/wordpress/index.php/2022/11/25/latinoware-2022-br/
tags:
  - Planet KDE PTBR
---

{{< figure src="/2022/11/latinowaresmol.jpg" >}}

Eu viajei de São Paulo para Foz do Iguaçu no Paraná nos dias 01/11 até 05/11 para participar como expositor no estande da KDE no maior evento de software livre da América Latina, o Latinoware 2022, junto de dois grandes contribuidores da KDE, o Pedro e a Barbara, que já são frequentadores do evento faz tempo.

Logo após chegar no hotel por volta da meia noite do dia 01/11, fui cumprimentado calorosamente por participantes e palestrantes do evento, vários de renome. Fiz o check-in logo após e fui pro meu quarto, onde capotei.

## Primeiro dia

O estande da KDE abriria no dia seguinte, 02/11, mas acabei me atrasando por mais ou menos uma hora e acabei perdendo a abertura do estande e as primeiras fotos da manhã, mas isso acabou não sendo nenhum problema no final.

O estande era um espaço pequeno de 2x3 ou 2x4, não sei dizer exatamente. Nele, cabiam umas 6 pessoas ao mesmo tempo, então o suficiente para uma equipe de 3 pessoas poder trabalhar de maneira ok. No fundo do estande tínhamos um banner azul da KDE e uma mesa de vidro junta de três cadeiras, e na frente tínhamos um balcão/armário onde dezenas de adesivos, marca-páginas e chaveiros eram distribuídos enquanto ou a Barbara ou o Pedro estavam apresentando a KDE para os transeuntes.

No começo fiquei observando o modo como estavam apresentando a KDE, e depois comecei a ajudar o Pedro a preparar os chaveiros feitos com impressão 3D que seriam distribuídos nos próximos dias. A Barbara estava apresentando especialmente o Krita e o Kdenlive, que são sua especialidade; ela é criadora de conteúdo e jornalista. O Pedro por sua vez focava em assuntos relacionados à comunidade, apresentando nossos grupos e redes sociais (em particular as do Telegram, que é o principal chat que usamos no Brasil).

As pessoas que me conhecem pessoalmente sabem que eu levo um tempo para pegar no tranco quando se trata de socializar (e imagino que alguns leitores compartilhem deste traço comigo). Sou bem introvertido, tenho até um tanto de dificuldade em entender sugestões sociais às vezes, embora não me considere efetivamente tímido. Ocasionalmente posso soar meio seco, sem a intenção. A ocasião era minha "primeira vez" em vários aspectos também: minha primeira vez voando de avião sozinho e tendo meu próprio quarto de hotel, minha primeira vez participando de um evento de software livre, minha primeira vez no Latinoware, e minha primeira vez sendo expositor num estande. Dizendo isso você leitor poderia achar que eu não teria jeito de apresentador, mas você estaria redondamente enganado.

Pro começo, fiquei na maior parte ajudando com os chaveiros e conversas ocasionais, e estava subindo a animação em mostrar a KDE, o Plasma e nossos aplicativos pras pessoas. Eu trouxe meu laptop para as demonstrações, mas eu ainda precisava terminar de configurá-lo, conseguir uma conexão de internet decente, mas quando a Barbara foi almoçar, tive a chance de ser o principal apresentador por um tempo.

{{< figure src="/2022/11/pedrokeychainssmol.jpg" caption="Pedro preparando os chaveiros para distribuição." >}}


O laptop gamer da Barbara acabou sendo usado principalmente para exibir o seu próprio trabalho com software livre, além da animação que ela fez no Kdenlive para o evento. O Pedro usou o seu laptop para demonstrações do KDEConnect e outros programas. As duas máquinas pareciam top de linha, bonitas e modernas.

O laptop que eu tinha trago para o evento não estava efetivamente bonito. Eu comprei ele usado de um amigo quando meu laptop principal tinha dado pau e eu fiquei com medo de não poder trabalhar nas minhas traduções, e esse laptop específico tem história: literalmente tomou um banho de isopropanol. Por isso suas manchas escuras e textura áspera.

No entanto, era um laptop com tela de toque. E eu sou um desses usuários de Plasma Wayland que prefere usar distros rolling release. Você pode imaginar qual era minha intenção em levar aquele laptop em particular se você estiver familiarizado com a situação atual do uso de tela de toque no Linux. Se você quiser uma experiência padrão e completa de tela de toque no Linux, você pode muito bem fazer isso no X11 (configurando), mas a sessão Wayland já vem com tudo prontinho por padrão.

Além disso, laptops de tela de toque não são uma coisa comum no Brasil, ou pelo menos não são para as pessoas como eu ou que vivem perto da periferia onde moro, gente de classe média baixa ou de classificação menor. Laptops de tela de toque são coisa de gente de classe média pra cima. Eu estava convencido de que as pessoas ficariam animadas em ter suas primeiras experiências de tela de toque num laptop com Linux, especialmente devido ao fator novidade.

Minhas escolhas provaram ser convenientes para demonstrar os gestos de toque e para demonstrar tecnologias mais recentes, assim como permitir que as pessoas desenhassem no Krita.

Eu tinha gestos de deslizar na tela para abrir programas. Um na margem direita da tela para esconder todos os aplicativos e exibir a área de trabalho. Na parte de cima habilitei o Overview, ou Visão Geral, um efeito de área de trabalho similar ao do GNOME e MacOS. Eu também configurei um na parte de baixo para abrir o lançador de aplicativos/menu e um na esquerda para trocar de janelas, mas usei estes dois últimos bem pouco.

Foi divertido mostrar para as pessoas que sim, o Plasma adapta o tamanho dos seus componentes para você usar com seus dedos no modo tablet, que sim, nossos apps integram bem com a experiência de tela de toque, e que sim, é tão fluido como você pode ver.

Quando a Barbara voltou, eu fui almoçar com o Pedro, e foi uma surpresa bem agradável ver que a padaria/restaurante logo ao lado do hotel não era tão cara a despeito da comida ser ótima, coisa que não costuma ocorrer em pontos turísticos assim. Ficamos conversando sobre nossas experiências com software livre e programação.

Após o almoço as coisas ficaram meio caóticas no estande porque todo mundo simplesmente amou os chaveiros feitos em impressora 3D. Acho que devo ressaltar que os chaveiros, adesivos e marca-páginas foram todos feitos pela Barbara e usando apenas software livre em sua gráfica.

Num determinado ponto, sem perceber, comecei a falar sem parar no estande. Primeiro uma menção breve de o quê a KDE é (uma comunidade internacional), e explicar que fornecemos um ambiente gráfico de desktop pra Linux (momento em que eu usava o gesto de toque para esconder todas as janelas e dizer "quero dizer, essa coisa bonita que vocês podem ver aqui"), e que também temos uma seleção imensa de aplicativos (momento em que eu mostrava o menu, mostrava as janelas novamente, exibia uma Visão Geral cheia de coisas) que na sua maior parte roda em múltiplas plataformas, Windows, Linux, Mac, Android, e até Linux para celulares!

{{< figure src="/2022/11/meshowingsmol.jpg" >}}

{{< figure src="/2022/11/metalkingsmol.jpg" caption="Eu demonstrando o Okular." >}}

Eu comecei a falar sobre o Dolphin e como ele tem múltiplos painéis, do Ghostwriter, a nossa mais recente adição e o software que usei para rascunhar este mesmo post, o Kate e seu uso para desenvolvedores e anotadores casuais, e é claro, o Krita e o Kdenlive. Eu fiquei falando sobre o quão configurável o Plasma é (porém deixando claro que isso é **_completamente opcional_** e você pode simplesmente usar o padrão e focar apenas nas suas tarefas), sobre como os gestos de touchpad seguem o movimento dos seus dedos, e sobre quão fluida é a troca de tema no sistema.

Toda vez que eu precisava fazer uma pausa eu voltava a ajudar com os chaveiros. Enquanto eu era barulhento, animado e estava no modo automático repetindo o mesmo conteúdo para todos que passavam por nosso estande, o Pedro seguia com uma atitude muito mais sossegada do que a minha, perguntando sobre a área de estudo ou de trabalho das pessoas, de onde elas eram, qual sistema operacional elas estavam usando ou precisavam usar, e prosseguia com sugestões sobre as coisas da KDE que poderiam mais interessar aos ouvintes, além de tornar o espírito comunitário e de software livre exorbitantemente claro para os transeuntes.

Pode parecer meio tosco que eu de início não pensei em adequar meu discurso às necessidades do meu público alvo, mas aconteceu. A razão de eu ter exposto os assuntos da maneira que fiz foi porque sempre que fico animado com as coisas de que gosto, começo a falar sem freio algum, mas com o objetivo de satisfazer meu próprio desejo de falar sobre esses assuntos. Em situações como a do estande, no entanto, o que eu devia fazer era ajustar meu conteúdo para focar nas pessoas; afinal, meu propósito no evento era _**trazer a KDE às pessoas**_. Isso foi coisa que aprendi já anos atrás mas não tive muita oportunidade de fazer na prática até então.

Tendo percebido isso, decidi me corrigir e aproveitar a situação para praticar nos dois dias seguintes.

Próximo do final do dia eu já estava exausto, embora bem satisfeito. Apresentadores e expositores apareciam no nosso estande, e vários já conheciam a Barbara e o Pedro. Um húngaro bastante amigável interessado em segurança de computadores tinha abordado o Pedro e conversado em inglês, e ele foi o primeiro falante de inglês que eu conheci no evento. Devido a seus interesses, naturalmente acabamos conversando sobre áreas da KDE que eram relacionadas a segurança: o Cofre do Plasma (Plasma Vaults), o widget de VPN, o KGpg, etc. Nem eu nem a Barbara estávamos familiarizados com esses assuntos de segurança, e o Pedro seguiria fornecendo as melhores explicações técnicas de nós três nos dias seguintes.

Voltando ao assunto de idiomas, nenhum de nós três sabia espanhol de fato para lidar com os falantes que vinham ao estande, mas isso acabou não sendo um problema sério. Todos nós entendíamos o que diziam, o problema principal era nós falarmos. Mas como o evento acontece no Brasil e várias palestras tinham sido dadas em português, a maioria dos transeuntes podia entender o que dizíamos se não falássemos rápido demais, e por vezes os grupos que passavam tinham seu próprios "intérpretes" para esclarecer as coisas pros seus colegas.

À noite, depois do evento, nós saímos para jantar e tomar umas com os outros apresentadores do evento. Na volta, o Pedro e eu ficamos no restaurante do hotel por um tempo, onde conversamos com aquele húngaro amigável por várias horas, e onde ocasionalmente outras pessoas vinham se juntar a nós, notavelmente um especialista forense papiloscopista (que lida com impressões digitais) e um mestre de shell.

Embora eu interaja bastante com a comunidade internacional, isso se dá exclusivamente por texto e não é sempre que tenho a chance de _**falar**_ em inglês. Isso não é algo incomum para tradutores como eu, aliás. Não lido com interpretação, apenas tradução de documentos, então meu inglês falado estava bem enferrujado. Felizmente minha pronúncia estava bem decente e eu tinha feito algumas chamadas de voz com amigos, assim como dado aulas de inglês para um amigo. Isso ajudou.

Minha impressão a respeito do progresso com minhas habilidades de discurso público e inglês falado me deram motivação extra para os próximos dias.

## Segundo dia - Promovendo a KDE

No segundo dia eu acordei cedo demais apesar dos drinques e, como costuma acontecer comigo, não tive ressaca. Fui para o estande mais cedo e dei uma olhada no lugar.

Nosso estande estava de frente ao estande da Linux Professional Institute, onde o Jon "maddog" Hall estava sentado de chapéu de Papai Noel. Alguns estandes ao lado, um estande do Debian. À nossa direita e costas, estandes de impressão 3D.

Eu preparei meu laptop um pouco melhor agora que tinha conexão com a internet direito. É um Dell Inspiron 7348 com um i7 5500 e placa HD 5500 rodando openSUSE Tumbleweed e Plasma 5.26.2. A caixa de som estava quebrada, mas isso não seria problema para o propósito do evento.

Eu tinha oito áreas de trabalho virtuais (VD), cada uma servindo para exibir um ou dois programas ao mesmo tempo: Firefox com links pros sites do Krita, Kdenlive, Plasma Mobile e KDE em geral; Dolphin e Index; Ghostwriter e Kate; Okular; Elisa; Haruna; Criador de Legendas (Subtitle Composer); Discover e Configurações do Sistema.

Agora minha estratégia de promoção era diferente: eu já tinha em mente o conteúdo do dia anterior na minha cabeça, e dessa lista de conteúdo eu selecionava as informações relevantes que chamariam a atenção dos usuários baseado no que eles fazem. 

A maioria das pessoas **_amou_** os painéis duplos do Dolphin, embora não muitos tenham ficado animados ao ver o suporte a abas (igualzinho ao navegador!) mesmo após mencionar que sim, cada aba pode ter dois painéis. Seu terminal embutido que segue a pasta em que você está foi particularmente atraente para as pessoas de cunho mais técnico.

O Index não recebeu muita atenção. Ele servia mais como ponto de partida para mencionar que temos projetos como Plasma Mobile para celulares, e que certos apps rodam no Android também, e isso sim é que por sua vez deixava as pessoas entusiasmadas, no mínimo, porque Linux de verdade em celulares não é comum ainda.

O Ghostwriter foi interessante de mostrar para estudantes de qualquer área de TI. A maioria da população de transeuntes consistia de estudantes de universidade, e eu diria que por volta de 70% desses tinha alguma relação com TI, e mesmo assim precisei explicar o que Markdown era para várias dessas pessoas. "Ah, o Github tem isso daí" foi algo que ouvi pelo menos duas vezes.

O Kate é um projeto que me atende muito bem, e recentemente eu aprendi a usar sua funcionalidade de Sessões. Para os de histórico mais técnico, eu mencionava o suporte a git e o terminal, assim como seus inúmeros plugins para várias linguagens, fazendo algumas sobrancelhas se erguerem de curiosidade. Para os que não mencionavam trabalhar em áreas relacionadas a TI, mencionei como eu usava Sessões para manter as notas das aulas de inglês que estava dando ao meu amigo e como eu as mantia separadas de outras notas de outras atividades.

O Okular foi fácil de promover, já que a maioria das pessoas visitando o estande eram estudantes ou tinham usado PDFs em algum momento para estudar. Ele é bem conhecido como sendo poderoso no Linux, mas quantas pessoas de fato usam funcionalidades de leitor de PDF que facilitam sua vida? Era minha impressão que a maioria apenas usa funcionalidade de navegação, e aparentemente eu estava certo. 

Eu tinha baixado um artigo acadêmico da Science Direct que tratava de plagiarismo, foi um artigo que eu tinha lido nas minhas aulas de Inglês Acadêmico na universidade. Escolhi ele simplesmente porque estava fácil e estava formatado direito. Eu comecei explicando sobre anotações, nada particularmente notável. Usando a tela de toque o tempo todo, é claro, já que é mais fácil e parece de pouco esforço (e portanto convidativo).

A parte que era realmente importante era a barra lateral. Quando o arquivo PDF está bem formatado (e deixei isso bem claro), aparece uma lista de capítulos onde a navigação se torna possível com apenas um toque ou clique. A visualização de miniatura foi bem útil para navegar também, deixando minha demonstração rápida toda vez. A barra de anotações não foi muito popular, mas é conhecimento útil. A barra de favoritos foi mais popular; como eu repetia, "quando você chega naquele ponto crítico de um PDF, sabe, quando você percebe que vai revisitar ele de novo e de novo e de novo, você pode só favoritar essa página e pronto".

Essas funcionalidades não eram particularmente notáveis, mas elas tornam a vida mais fácil, e aprender sobre elas **_com o Okular especificamente_** significa que as pessoas já sairiam preparadas para usá-lo só de ver. Apesar de ser extremamente rápido de navegar, eu penei um pouco usando a interface densa e compacta do Okular com meus dedos quando não estava em modo tablet, especialmente para navegar de um capítulo a outro.

O Elisa foi difícil de promover. Menos de 10% (ou mesmo 5%) da população tinha arquivos de música nas suas máquinas, e quase todo mundo usava Spotify. Eu acho o Elisa lindo, mas beleza não supera o fato dos usuários não terem necessidade de mp3. No entanto, as pessoas que de fato mexiam com arquivos de música ficaram bem interessadas, e até perguntavam se havia software para gerenciar metadados e fazer a música aparecer direito nos reprodutores de música, e nessas horas eu mencionava o Kid3.

O Haruna também foi um desafio promover. É lindo também e usa um backend poderoso, o MPV, mas sua barra lateral elegante para gerenciar vídeos só aparece quando há mais de um vídeo na lista de reprodução, e eu tinha configurado ele de maneira tal que apenas o vídeo de anúncio do Plasma 5.26 tocaria em uma pasta separada, então toda vez que eu queria mostrar a barra lateral eu tinha que trocar de pastas. As coisas teriam sido bem mais fáceis se o botão de loop estivesse direto na interface ao invés das configurações.

No lugar, eu acabei focando nas suas configurações, já que é onde ele se destaca. Eu mostrei como era possível esconder a barra de menu e até a barra de ferramentas para deixar a interface mínima e simples, e eu descia pela gigantesca lista de atalhos que servia para deixar o reprodutor de vídeo ao gosto do usuário. Em particular, mencionei o atalho que permite que o app funcione que nem no YouTube, um clique/toque **_em qualquer lugar do vídeo_** para pausar/reproduzir. Naquela hora, eu tinha esquecido completamente que isso já existia com clique direito, então não demonstrei isso, mas ainda acho que clique esquerdo funcionaria melhor, especialmente em telas de toque.

Eu também queria mostrar o Criador de Legendas (Subtitle Composer) porque é meu favorito. Como tradutor estou ciente do software disponível do outro lado da cerca, e a grama não é verde lá; por alguma razão que não entendo, os programas de legendagem mais populares no Windows ou são oldware ou tem UX ruim. Legendagem é onde programas de código aberto se destacam, na verdade: o Aegisub e o Criador de Legendas são os melhores que já vi.

Além disso, eu também fiz a base do site do Criador de Legendas, mandei patch, e fiz as legendas brasileiras do vídeo do Veritasium [A gravidade não é uma força][1]. No dia anterior, a Barbara tinha usado a seção Sobre do Krita para mostrar o seu próprio nome como principal tradutora brasileira do Krita, tendo feito mais de 6000 segmentos da mais alta qualidade. Eu tentei algo similar mencionando minhas contribuições na seção Sobre do Criador de Legendas porque mencionar esse tipo de coisa acaba servindo como mensagem de que contribuir não é algo distante do usuário, e sim algo extremamente próximo, além de ressaltar que contribuições podem ser feitas por não-programadores também.

Já o Discover foi uma experiência relativamente neutra para os ouvintes, o que é ok, porque ele serve como mero intermediário para descobrir mais software; expô-lo efetivamente significa usá-lo casualmente na frente das pessoas como se fosse funcionalidade esperada do sistema (que é o caso em todas as plataformas competidoras principais). Foi um tanto triste que não pude mostrar sua [página inicial um pouco mais cheinha][2], mas ele cumpriu seu papel. De modo similar às páginas da web que mantive abertas, ele foi usado para mostrar rapidamente outros programas que eu não tinha instalado e que vou mencionar quando for tratar do terceiro dia.

As Configurações do Sistema foram interessantes de falar sobre. As pessoas que viam tinham rostos gritando ou "Meu deus, quero" ou "Afe, por quê estou ouvindo isso". De início mostrei a troca de temas rápida e eficaz (que felizmente foi algo melhorado nas últimas versões), mas acabava sempre voltando para o tema claro para deixar a interface mais visível de longe. Em ambos os casos, quando eu abria a Visão Geral para demonstrar que todos os aplicativos agora estavam seguindo o tema de maneira consistente, sempre impressionava alguém.

Em seguida eu mostrei os efeitos da área de trabalho, as famosas animações do Plasma, alternando sempre entre as animações bonitas e impressionantes mas de pouco uso e aquelas pérolas que melhoram a vida de todo mundo. Eu ressaltei várias vezes o quanto essas funcionalidades todas eram opcionais, e que se as pessoas realmente não ligam de customizar seus sistemas e querem só trabalhar, elas podem. Facilmente.

A implicação aqui é que uma das melhores coisas no Plasma é que o sistema se adequa ao usuário, não o contrário. Esse foi meu "slogan" durante o evento, e eu queria muito quebrar o estereótipo de que o Plasma é pensado apenas para quem adora customizar.

Enfim… quase todos amaram as Janelas Instáveis, **_óbvio_**. Aqueles que não ligavam muito perguntavam sobre o desempenho e consumo de bateria. Assim que um grupo de pessoas claramente demonstrava ter curtido, eu mencionava que, após instalarem o Plasma em casa, eles podiam aumentar a "gelatinosidade" do efeito até o máximo só por diversão, ver as janelas darem a louca na tela. Eu deliberadamente afinei minha voz nesses momentos para indicar que o que estava sugerindo era exclusivamente para brincar, e não para usar no dia-a-dia.

O efeito Seguir o Mouse tinha um uso particularmente útil: "Vocês provavelmente já se depararam com isso: vocês estão escrevendo no Word ou LibreOffice e a página é basicamente um canvas branco gigante cheio de linhas finas pretas **_exatamente como o cursor_**, e daí você acaba perdendo ele o tempo todo. Com isso daqui vocês podem evitar isso".

Depois que habilitei o efeito de Despedaçar janelas, as pessoas constantemente sorriam impressionadas ou riam quando eu mostrava o ambiente gráfico casualmente, agindo como se aquilo não fosse nada, o que foi bem divertido. As pessoas não cansavam desse efeito.

A Animação de Clique do Mouse obteve na maior parte reações neutras, com algumas positivas: eu mencionei sua relevância em fazer tutoriais de YouTube ou compartilhar a tela em programas como Teams, Meet, Discord, para deixar claro quando e onde algo era clicado. A maioria dos ouvintes não pareceu ter esse uso.

A Lâmpada Mágica foi oito oitenta, ou impressionava ou não impressionava, nenhum meio termo. É uma funcionalidade legal de ter, mas **_seu propósito é não chamar a atenção_**, então este tipo de reação é compreensível.

O efeito de Zoom foi interessante e na maior parte um sucesso. Ele é principalmente uma funcionalidade de acessibilidade, mas como os efeitos mencionados acima, ele pode ser usado para coisas como apresentações online. Ele atraiu mais gente interessada do que eu esperava, e isso foi **_antes_** de eu mostrar quão sem limites se podia aumentar o zoom, deixando até os pixels do cursor gigantes na tela. Acabou que as pessoas ou gostavam dele por ser útil ou achavam seus valores máximos impressionantes/engraçados.

Estou bastante ciente de que esta falta de limites de zoom é inútil em termos práticos, mas sem isso eu não teria cativado um número tão grande de pessoas.

Infelizmente eu também estava ciente de que o efeito de Lupa requer logout ou reinício do sistema, algo assim, para tomar efeito após ter tirado do Zoom, porque ambos os efeitos usam o mesmo atalho de teclado, então não pude mostrar ambos pros mesmos grupos de pessoas.

Eu teria exibido o Cubo da Área de Trabalho, mas ele "estava em manutenção" durante o lançamento atual, infelizmente. Mas sempre tem a próxima vez.

## Segundo dia - Destaques

A manhã foi caótica, mas a tarde foi mais ainda.

No Latinoware, os estandes tinham um papel secundário comparado às palestras. A própria Barbara fez duas durante o evento: "Como funciona uma gráfica com softwares livres" e "Impressão 3D na escola". Em ambas as ocasiões apenas o Pedro e eu ficamos no estande.

Três outras palestras eram particularmente interessantes para a KDE: "Como viver sem o Photoshop" e "Demonstração do uso dos softwares livres gráficos", por Elias Silveira, e "Videografismo com software livre", por Carlos Eduardo Cruz, apelidado Cadunico.

O Elias Silveira usa o Krita e o Cadunico usa o Kdenlive.

Num determinado ponto do dia, o Elias apareceu no nosso estande para desenhar o Konqi usando o tablet Android da Barbara. Essa foi uma excelente oportunidade para destacar como software da KDE pode rodar em múltiplas plataformas, e aqueles que ainda estivessem dúvidas poderiam ver isso pessoalmente.

Aqui o desenho que ele fez:

{{< figure src="/2022/11/konqidrawingsmol.jpg" caption="Desenho do Konqi. Note os ícones de Android na parte de baixo." >}}

Devo mencionar que ele não estava acostumado a desenhar sem teclado e sem uma mesa digitalizadora dedicada, e mesmo assim ele desenhou a base deste Konqi em mais ou menos meia hora, usando o resto do tempo para melhorar mais o desenho. Além disso, **_ele usou só uma camada_**, porque sim.

As pessoas assistindo ficavam só fazendo isso, assistindo, especialmente porque pessoas que nunca desenharam com software especializado assim podem não estar cientes do processo geral, algo que pode ser bem diferente do imaginado.

Quando se desenha algo que vai ser colorido cedo como neste caso, uma possibilidade é pintar com uma cor base e colocar várias variantes da mesma cor com transparências de níveis diferentes para conseguir o tom certo, com brancos e pretos para clarear ou escurecer certas áreas e pormenores. O Elias deixou isso claro com as mãos, e a Barbara deixou isso claro com suas palavras.

{{< figure src="/2022/11/eliasdrawingsmol.jpg" caption="O público assistindo ao Elias desenhar." >}}

Depois de desenhar esse Konqi o Elias saiu do estande e voltamos a focar nos chaveiros por enquanto. O estande passou o tempo todo cheio nas horas anteriores, e ele continuaria o tempo todo cheio, agora com gente querendo chaveiros. Os chaveiros eram completamente livres e gratuitos, afinal.

Houve um breve momento de descanso conforme as pessoas participavam de várias palestras ao mesmo tempo, mas o estande continuava sempre tendo pelo menos três pessoas visitando. Um tempo depois, o Elias voltou e começou a desenhar novamente, e desta vez sua inspiração estaria literalmente na frente dele.

Eu mencionei já: nosso estande estava posicionado de frente para o estande do LPI. Acho mais fácil deixar o vídeo falar por si mesmo:

{{< peertube tube.kockatoo.org k8eKWoFFhviWatciZcdHux >}}

{{< figure src="/2022/11/maddogsmol.jpg" caption="Elias e Jon \"maddog\" Hall, diretor do Conselho do Linux Professional Institute." >}}

A estrutura geral do desenho levou de meia hora a uma hora, mais ou menos, o resto do tempo novamente foi usado para melhoras. Como você pode ver, todos queriam assistir.

Dado que eu continuar falando não seria tão efetivo enquanto ele desenhava e que eu já estava começando a perder a minha voz, eu deixei meu laptop preparadinho no balcão para que os transeuntes pudessem desenhar direto no Krita com ele.

Mais tarde, o Cadunico se juntou ao nosso estande e ajudou a preparar e editar o próprio vídeo que você acabou de ver acima, usando Blender e Kdenlive. O trabalho dele, como você pode ver, foi compartilhado no canal oficial da KDE no YouTube e no PeerTube.

{{< figure src="/2022/11/cadunicosmilesmol.jpg" caption="Cadunico trabalhando no Blender e no Kdenlive com o laptop da Barbara." >}}

Na noite do segundo dia, o pessoal do evento se reuniu num bar perto do hotel, o Guns 'N Beer, onde quem estivesse com seu crachá do evento recebia desconto. A banda que tocou naquela noite era inteiramente composta de gente do evento, também. Naquele amontoado tive a chance de conhecer vários apresentadores frequentes do Latinoware.

## Terceiro dia

O terceiro dia foi o mais calmo, mas também o mais cansativo, no mínimo, porque eu estava fortemente sentindo perder a voz de tanto falar nos poucos dias que estive lá.

Mesmo assim, estava adorando ser o principal falante expondo no estande.

Os chaveiros acabaram bem cedo naquele dia.

Acho que é o momento de mencionar a quantidade de coisas que nós distribuímos. A lista de coisas foi a seguinte:

* 30 conjuntos de adesivos Konqi e Kate (para as crianças)
* 120 adesivos de logo letra K
* 90 Konqi - adesivos de fundo branco
* 90 Konqi - adesivos de fundo azul
* 140 adesivos do Kdenlive
* 324 logo letra K - adesivos de vinil brancos
* 324 logo letra K - adesivos de vinil azuis
* 200 logo letra K - chaveiros 3D
* 800 marca-páginas da KDE

Tudo distribuído completamente de graça, e feito exclusivamente feito com software livre.

{{< figure src="/2022/11/proudstickersmol.jpg" caption="Usando adesivo da KDE com orgulho!" >}}

{{< figure src="/2022/11/folkswithstickerssmol.jpg" caption="Estudantes com seus brindes da KDE!" >}}

Os chaveiros precisavam de adesivos de vinil para colar, e parte da interatividade do nosso estande foi que as pessoas que vinham tinham que colar os adesivos elas mesmas. A Barbara e o Pedro demonstraram o procedimento, os visitantes repetiam.

Mesmo com uma abordagem DIY assim, todos os 200 chaveiros se foram em apenas dois dias e meio.

Apesar de cada pessoa pegar múltiplos adesivos e marca-páginas em montes, podemos dizer com segurança que nossa estadia de três dias foi um grande sucesso, com centenas de pessoas passando no nosso estande, das quais a maioria iria embora sabendo sobre a KDE e o software que fornecemos, com marca-páginas linkando para o nosso site brasileiro da KDE.

No meio do dia, o Elias apareceu mais uma vez para desenhar, desta vez fazendo um guerreiro GNU segurando uma mesa digitalizadora como escudo e uma caneta como espada.

{{< figure src="/2022/11/gnu.jpg" >}}

{{< figure src="/2022/11/eliasgnusmol.jpg" caption="GNU feito com uma só camada num tablet Android rodando Krita." >}}

Por volta das 5 da tarde nós só tínhamos três marca-páginas e uma dezena de adesivos sobrando. No final do dia, não tínhamos nenhum.

Mas eu preciso mencionar ainda cinco coisas interessantes sobre esse dia antes de terminar esta postagem de blog imensa.

O primeiro de tudo é que, desta vez, eu lembrei de mencionar o Konsole para os transeuntes mais técnicos. Ele não tinha me passado pela cabeça nos dois primeiros dias porque ele não é uma janela GUI prototípica como as outras que eu fiquei exibindo. Sua nova infraestrutura de plugins foi feita pelo Tomaz Canabrava, um contribuidor brasileiro conhecidíssimo, então eu precisava fazer questão de mencioná-lo pelas mesmas razões de antes e para deixar claro que a comunidade brasileira tem forte influência no desenvolvimento.

A segunda coisa é que naquele dia vimos vários educadores passando no estande, o que foi uma excelente oportunidade para mostrar o GCompris, aquele software pronunciado como o francês "j'ai compris", "entendi", que é para ensinar crianças. Eu também mencionei o KStars e toda a categoria de Educação no apps.kde.org.

Eu já havia mencionado os programas de educação nos dias anteriores (já que nesses dias também alguns educadores também passaram no estande), mas não tive a chance de elaborar mais sobre eles devido ao grande volume de pessoas. O terceiro dia foi relativamente sossegado e pude fazer isso.

Baseado na minha percepção das reações que obtive, eles adoraram isso, e na maioria nem sabiam que havia software livre focado em educação. No mais, a ideia de que a KDE fornece toda uma suíte de software educacional foi como a cereja do bolo.

Um pai com sua filha de 7 anos visitaram o estande por volta do mesmo tempo que o Elias esteve lá. O Pedro e a Barbara já tinham conversado bastante com eles nos dias anteriores, especialmente já que a Barbara ensinou a menina a fazer chaveiros em impressora 3D. Eu estava descansando na hora, mas eu deixei meu laptop no balcão para que ela pudesse desenhar no Krita, e ela fez o seguinte:

{{< figure src="/2022/11/sofiaprint.png" caption="O desenho da Sofia desenhado com seus próprios dedos na tela de toque." >}}

Duas das últimas pessoas visitando nosso estande foram dois dinamarqueses que falavam inglês. Eu tive meu pequeno momento de glória explicando sobre nosso software em inglês com eles. Eu tinha mencionado antes que eu estava dando aulas de inglês para um amigo, então eu acabei virando o "palestrador do inglês" do grupo, e eu gostei assim.

Embora os dois dinamarqueses tivessem vindo ao estande para ouvir mais sobre o Kdenlive originalmente, fiquei sabendo que eram professores de escola primária, o que tornou a conversa mais dinâmica. Edição de vídeo era o hobby deles, e de tanto que a Barbara explicou nos dias anteriores, eu cheguei a aprender como cortar vídeos com a ferramenta de seleção, e com tela de toque ainda por cima, arrastando do preview para a linha do tempo. Naturalmente, eu mencionei o GCompris e o resto da suíte de software de educação; até então, todos os educadores que eu tinha visto no evento davam aulas para adolescentes, jovens adultos ou adultos, só estes dois eram o público alvo perfeito do GCompris.

{{< figure src="/2022/11/danishfolksmol.jpg" caption="Os dois dinamarqueses altões, o Ole, apresentador regular do evento também dinamarquês, e eu." >}}

Além disso, enquanto apresentava o site principal da KDE a eles, nós descobrimos que o site tinha tradução para Svenska (sueco) mas nada de Dansk (dinamarquês). Essa foi uma boa oportunidade para mencionar a questão das contribuições na nossa comunidade de software, e que na prática, ajudar a traduzir nossas coisas é basicamente questão de entrar num grupo de tradução, pedir pra te enviarem os arquivos certos, traduzir, depois mandar pro grupo de volta.

Isso deixou eles bastante entusiasmados. Eu imagino que algo tão central quanto o site principal de um grupo grande assim sendo traduzível por qualquer interessado no projeto é uma ideia excitante, tanto do ponto de vista pessoal de "Eu vou ser quem traduziu o site principal da KDE, uau" quanto do ponto de vista comunitário "Minha tradução vai tornar o site mais convidativo e vai ajudar outros falantes de dinamarquês".

A última visitante foi a única pessoa que veio conversar especificamente sobre o estado atual do uso de tela de toque no Linux, e felizmente este é um assunto que me anima muito e que sei extensivamente, então eu a instruí quanto aos critérios necessários para ter uma boa experiência com telas de toque. Isto é, que precisa haver drivers para o dispositivo dela, que é necessário o iio-sensor-proxy (cujo desenvolvedor é super sossegado e agradável de colaborar), Wayland (incluindo o Plasma, o GNOME e o Sway assim como o fato de que quanto mais atualizado, melhor, no caso do Plasma), e o Maliit. Eu mencionei também que sim, dá para configurar o suporte a toque no X11, mas para isso ela precisaria consultar a wiki do Arch e configurar as coisas ela mesma.

Próximo de acabar o evento, meu laptop ficou na frente ou com o Haruna rodando o vídeo do Plasma 5.26 ou com o Krita aberto para as pessoas desenharem, enquanto o laptop da Barbara mostrava o vídeo do Cadunico.

## Resultados

Um baita sucesso.

Mais de 1100 adesivos distribuídos.

800 marca-páginas distribuídos.

200 chaveiros distribuídos.

3 desenhos feitos pra todo mundo ver.

1 vídeo para o canal oficial da KDE no YouTube/PeerTube.

Socializei com outros participantes do evento e conheci pessoas influentes do cenário atual do software livre.

Eu não sabia que eu era do tipo que poderia ser tão falante, e eu estou convencido de que eu poderia fazer palestras e apresentações para a KDE com frequência caso eu tenha o apoio para tal, como tive neste evento.

Pude entender a logística e ter experiência de primeira mão de viajar sozinho e de avião.

Pude praticar inglês falado e fiquei mais confiante quanto às minhas habilidades.

Adquiri motivação para contribuir mais.

—

Espero que tenha gostado de ler esta postagem de blog de 10 páginas, não se esqueça de dar uma olhada no:

[KDE Brasil no Telegram][3]

[Site principal da KDE][4]

[Site principal brasileiro da KDE][5]

[Todas as palestras do Latinoware 2022][6]

[Blog da Barbara][7]

[Website do Cadunico][8]

[Instagram do Elias][9]

 [1]: https://youtu.be/XRr1kaXKBsU
 [2]: https://invent.kde.org/plasma/discover/-/merge_requests/398#note_550637
 [3]: https://t.me/kdebrasil
 [4]: https://kde.org/
 [5]: https://br.kde.org/
 [6]: https://www.youtube.com/c/LatinowarePlay/playlists
 [7]: https://www.barbara.blog.br/blog/
 [8]: https://cadunico.art.br/
 [9]: https://www.instagram.com/elias_silveira_arte/
