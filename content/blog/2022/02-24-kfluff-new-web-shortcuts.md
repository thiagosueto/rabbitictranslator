---
title: KFluff – New KRunner web shortcuts for development and user support
author: Blumen Herzenschein
date: 2022-02-24
url: kfluff-web-shortcuts
aliases: /wordpress/index.php/2022/02/24/kfluff-new-web-shortcuts/
tags:
  - Planet KDE
  - Fluff
---
I added some KRunner web shortcuts recently to KIO ([MR600][1], [MR631][2]), and I'd like to tell you about them since they made my life significantly easier, and they might make yours easier too if you contribute to KDE. The inclusions from MR600, namely Invent and Invent Repo, were added in **KDE Frameworks 5.88**, whereas the **new ones from MR 631 I expect will land in 5.92**.

KRunner's web shortcuts, or web runners, are very convenient once you get used to them. After enabling them, you can type something in the format `keyword: queryhere`, and your query will be immediately sent and the result shown in your browser. That is to say, if you open KRunner and type `dd: kde`, your browser will open and directly show the result of the search for `kde` in DuckDuckGo. This provides a faster workflow as you no longer need to open your browser, type the address of the search provider, wait for it to load and then search; you open krunner, type and search, loading the page once. Web shortcuts like this can be applied to virtually any website which provides a search query URL, and you can decide whether they're separated by : or by spaces. I prefer spaces.

Note that these web shortcuts can be used in the Kickoff menu and in the new Overview as well, you are not limited to KRunner in order to use them.

## Searching Invent

The most notable addition to these web shortcuts was the four new shortcuts for our Gitlab instance, Invent.

You see, Gitlab, as lovely as it is for development, can be rather slow and unintuitive/unpredictable in terms of UX at times. It has two search bars with distinct behavior and pros and cons, and they can change further depending on where you are, which settings you have and whether you're logged in or not. This means that, depending on the task you want to do, you'll have differing or even multiple routes to achieve your goal. This is not a bad thing, and you get used to it eventually, but it's certainly not optimal for speed.

To avoid this cognitive overhead and general loading slowness you can simply use the shortcuts I added.


**The first is general search via the keywords `invent` and `inv`.**

This is essentially the same as:

  * Opening invent.kde.org
  * Waiting to load
  * Clicking the top search bar
  * Typing your query
  * Pressing Enter

This is the simplest use case, and it can be useful for searching pings, users or really anything else, but is essentially a _fallback_. This can be used just in case you don't need to use the other three. With this web shortcut, the required actions become:

  * Opening KRunner
  * Typing e.g. `inv wayland`
  * Pressing Enter


**The second is repository search via the keywords `inventrepo`, `invrepo`, `repo`.**


This is essentially the same as:

  * Opening invent.kde.org
  * Waiting to load
  * Clicking the top search bar
  * Typing your query
  * Pressing Enter
  * Waiting to load
  * Clicking any of the results
  * Waiting to load
  * Clicking in Original Project

Which, might I add, is the fastest way to do this via the web interface. I know of other two ways of achieving the same, but they are slower still.

Needless to say, this is not a generic search for a specific project, this is a _search for the original project_. The latter is, as I see it, a much more common use case than the former when it comes to development.

This shortcut takes advantage of the filter of Most Stars, as the original project is predictably going to be the project contributors favorite the most.

With this shortcut, it becomes:

  * Opening KRunner
  * Typing e.g. `repo kio`
  * Pressing Enter
  * Waiting to load
  * Clicking the first result


**The third is merge request search via the keywords `inventmr`, `invmr` and `mr`.**


This is essentially the same as:

  * Opening invent.kde.org
  * Waiting to load
  * Clicking the top search bar
  * Typing your query
  * Pressing Enter
  * Waiting to load
  * Clicking the Merge Requests filter

I was rather conflicted with how to implement this shortcut, as I'd additionally prefer to add the Open Merge Requests Only filter, but this was my bias due to my current status as a novice contributor: I don't need to reference closed MRs because I'm not at a level where they assist me yet.

Again, this is faster with the shortcut:

  * Opening KRunner
  * Typing e.g. `mr web search`
  * Pressing Enter


**The fourth is issue search via the keywords `inventissues`, `invissues`, `issues` and `issue`.**


This is essentially the same as:

  * Opening invent.kde.org
  * Waiting to load
  * Clicking the top search bar
  * Typing your query
  * Pressing Enter
  * Waiting to load
  * Clicking the Issues filter

The same logic used in the MR shortcut applies here: it is not filtered by open issues because it's a broader use case than mine. The shortcut is equally as fast:

  * Opening KRunner
  * Typing e.g. `issue padding`
  * Pressing Enter


**However, if you do want those last two web shortcuts to be filtered by Open MRs and Open Issues, it's very simple to customize them: copy their files (`invent_mr.desktop` and `invent_issues.desktop`) from `/usr/share/kservices5/searchproviders/` to `~/.local/share/kservices5/searchproviders/`, make the long steps specified above to grab the correct URL, then add `\\{@}` where your actual query is supposed to be in the URL.**


This way, your custom web shortcuts will override the system web shortcuts.

By the way, as a little piece of trivia, when you attempt to delete a web shortcut through the KCM, you're actually copying the respective system files to that home config folder and adding Hidden=true.

## Searching development documentation

A few web shortcuts used for development were old and needing updates too, I added a few new ones that convenienced me, and hopefully you too. I feel like we should make the experience top notch for contributors to develop in Plasma, so this was a priority too.


**The existing KDE API documentation search was updated to use the new version of api.kde.org, and it can now be used with the keywords `kdeapi`, `kde`, `api`.**


This means that typing `api formlayout` will search for formlayout in api.kde.org.


**Now the deprecated Qt4 API search was removed, the Qt5 API is accessed via the keyword `qt5`, and the Qt6 API is accessed via the keywords `qt6` and `qt`.**


This means that typing `qt json` will search for json in the Qt6 documentation. It makes sense to me that the generic keyword `qt`, the easiest and shortest to type, belongs here and not in Qt5.

If you're a C++ developer, you probably know of cppreference, cplusplus, Microsoft's C++ docs, and Learn CPP.


**Because cppreference is well known as THE reference in C++ second only to the actual standard document (or so it seems to me), it earns the `cpp` keyword, in addition to `cppreference` and `cppref`.**


Mind you, cppreference and a few other shortcuts have direct redirection, meaning that if your search exactly matches an entry in the database, the page is loaded directly instead of getting a search list.


**cplusplus can be easily searched with, well, the `cplusplus` keyword.**



**Microsoft's C++ documentation can be accessed with `microsoftcpp` or `mscpp`.**



**The keyword for Learn CPP is, you guessed it, `learncpp`.**



**For those interested in Rust, you can now search the official Rust standard via the unsurprising keyword `rust`.**



**And if your development involves building Flatpaks (which are essentially build scripts, not that different from pkgbuilds or ebuilds), you'll be glad to know that you can search the documentation with `flatpak`.**


## Specific communities

I feel like some web shortcuts had to be updated or created for the sake of certain communities that are very fond of KDE.

The ricing community is well fed with Get Hot New Stuff for most KCMs and several ways to install customizations, but the existing web shortcut was pointing to the old website, KDE Look, and would get unnecessary redirection time to KDE Store.


**So the web shortcut for the KDE Store was updated to the new URL and a new keyword was added, `kdestore`, without removing the old ones `kdelook`, `klook`, `kdel` , in case someone had a weird case of muscle memory.**


With the advent of the Steam Deck and the fact Plasma has had a very complete Joystick KCM for a long time (and, well, developers who play games!), it makes sense to add something for gamers as well. I am myself a Metroidvania speedrunner wannabe in my spare time.


**Now you can search the WineHQ Database directly via the keywords `winehq` and `wine`, and you can search for games in ProtonDB via the keywords `protondb` and `proton`.**


Krita has been one of the most popular KDE software applications out there and it was what brought me to the Linux world, so I have a special place in my heart for it. I've added one for it then.


**You can now directly search the Krita Manual with the keyword `krita`.**


Now, a bit of self indulgement doesn't hurt. Since I'm an avid user of Reddit and mostly restrict myself to r/kde (and have been doing so for a few years) and I'm a not-so-avid moderator, and I see myself using the search so often, and I'd like to see it gain a bit more of official status, and [&#8230;]


**Now there's a keyword `reddit` for searching Reddit in general and a keyword `kreddit` for searching r/kde specifically.**


## Package search / User support

One thing I see myself using very often when providing user support is checking which packages are provided by specific distros via their web interfaces. For instance, Ubuntu and all its flavors have packages.ubuntu.com, and so does Debian with packages.debian.org; Arch has archlinux.org/packages, openSUSE has software.opensuse.org.


**There are already exists one for Debian, `deb`, but I had to add one for Ubuntu, unsurprisingly with the keyword `ubuntu`. OpenSUSE was also lacking one, the easy naming choice was of course `opensuse`. Arch already has a keyword called `arch`, but this one goes to the wiki; I instead added `archpkg` for package search in the official repositories.**


Now what's the use case for these?

Well, different distributions follow different naming schemes for their packages. You're never sure of how it's called unless you actually know the naming patterns used. If you're trying to assist someone who is missing a package, be it for their general use or for development, it makes sense to tell them to search themselves based on a hint you provide, but it's nicer to go the extra mile and either teach them how to search or to provide the package name yourself. Moreover, there are cases where the user is unable to find the package despite their search, or they got confused about which package to install.

Less selflessly, you might be interested in the distro packaging specifics or you'd like to search details of a package build. Personally, it is not uncommon for me to check this sort of information. Very often I search for plasma workspace just to see which Plasma version is being used in which distro and in which version, or I search to see if a certain distro version provides a certain package which mine provides.

As a last and short trivia before I conclude this post, did you know there is already a web shortcut for searching bug reports?

This was not updated or added with my MRs, but it deserves special mention _simply because it exists_.


**Bug reporters and triagers will like to know they can make quick searches on bugs.kde.org using the keyword `bug`. This means that by typing e.g. `bug 12345`, you will be immediately redirected to bug report number 12345, and that by typing e.g. `bug kwin_wayland` you will be using Bugzilla's quick search feature to list kwin_wayland bug reports.**


 [1]: https://invent.kde.org/frameworks/kio/-/merge_requests/600
 [2]: https://invent.kde.org/frameworks/kio/-/merge_requests/631
