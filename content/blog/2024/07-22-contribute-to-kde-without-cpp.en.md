---
title: "You can contribute to KDE with non-C++ code"
date: 2024-07-22
url: contribute-to-kde-without-cpp
tags:
  - Planet KDE
---

Not everything made by KDE uses C++. This is probably obvious to some people, but it's worth mentioning nevertheless.

And I don't mean this as just "well duh, KDE uses QtQuick which is written with C++ *and QML*". I also don't mean this as "well duh, Qt has a lot of bindings to other languages". I mean explicitly "KDE has tools written *primarily* in certain languages and specialized formats".

Note that I said “specialized formats”. I don’t *want* to restrict this blog post to only programming languages.

I'll be straight to the point. You can contribute to KDE with:

* [Python](#python)
* [Ruby](#ruby)
* [Perl](#perl)
* [Containerfile / Docker / Podman](#containers)
* [HTML / SCSS / JavaScript](#websites)
* [Web Assembly](#wasm)
* [Flatpak / Snap](#flatpak-snap)
* [CMake](#cmake)
* [Java](#java)
* [Rust](#rust)

Here's how.

## How to find projects by language

You'll need to [create an account](https://community.kde.org/Infrastructure/GitLab) on [KDE Invent](https://invent.kde.org/). After logging in, you can click on the "Explore projects" button in the corner and click on the huge search field in the middle of the screen: it will list the option "Language".

Click on it and you'll be prompted with a select list of languages used at KDE. Then press Enter.

## Python

While it is possible to use Kirigami with Python (either PyQt or PySide) and [we have a tutorial for that](https://develop.kde.org/docs/getting-started/python/), it is not as useful as can be: we don't current have bindings for KDE Frameworks to be used in Python code. It's planned, though.

No, the cool tool we use everywhere is [Appium tests](https://develop.kde.org/docs/apps/tests/). These are automated tests done to GUI applications by interacting directly with them as though the tests were done by a human, that is to say, by clicking on GUI elements and by typing on text fields automatically.

In recent years KDE has implemented Appium tests in a lot of KDE software as part of the [Automatization Goal](https://community.kde.org/Goals/Automate_and_systematize_internal_processes). To achieve this, KDE made a [Selenium Webdriver](https://invent.kde.org/sdk/selenium-webdriver-at-spi) that leverages the de facto accessibility standard on Linux, [AT-SPI2](https://www.freedesktop.org/wiki/Accessibility/AT-SPI2/). If a KDE project that has a GUI doesn't yet have Appium tests, go for it. By the way, the webdriver also has a few Python scripts!

We do have a few projects that are written in C++ but make heavy use of Python, like [KDevelop](https://invent.kde.org/kdevelop/kdevelop), [Kdenlive](https://invent.kde.org/multimedia/kdenlive/-/tree/master/data/scripts), [Lokalize](https://invent.kde.org/sdk/lokalize/-/tree/master/scripts) and [KTimeTracker](https://invent.kde.org/pim/ktimetracker/-/tree/master/test).

[KDevelop's plugin for Python](https://invent.kde.org/kdevelop/kdev-python) especially has been in maintenance for years now! It would be great if it could be revived so people would use KDE's IDE more.

Another cool project to contribute to is [kde-builder](https://invent.kde.org/sdk/kde-builder), the replacement to our main [Perl-written](#perl) development tool [kdesrc-build](https://develop.kde.org/docs/getting-started/building/kdesrc-build-setup/).

One that we sorely need for generating our QML API is [Doxyqml](https://invent.kde.org/sdk/doxyqml), which uses [Doxygen](https://www.doxygen.nl/) under the hood.

And another project that's intimately related to Linux translations, [pology](https://invent.kde.org/sdk/pology), which is a collection of tools for processing [PO files](https://www.gnu.org/software/gettext/). It’s used in Lokalize.

[Craft](https://invent.kde.org/packaging/craft) is a tool that lets you build, install, and package software for Windows, MacOS, Android and Linux. It's pretty cool, like kdesrc-build but without requiring to build everything yourself (you can install the latest cached prebuilt package instead). It has [documentation](https://community.kde.org/Craft) in the Community wiki.

It works by writing straightforward recipes that are used to build KDE software, named [Craft Blueprints](https://invent.kde.org/packaging/craft-blueprints-kde), that let you set a program's dependencies, metadata, or how it's going to be packaged. It’s a full blown solution that is used even outside KDE. It has [documentation](https://community.kde.org/Craft/Blueprints) in the Community wiki as well. There are dozens of blueprints you can help with!

Python is also used for KDE websites and systems administration.

Some of [our websites](https://invent.kde.org/websites) that use [Hugo](#hugo) have a file called `custom_generation.py` that is used to write the specifics of how to deploy a website with the KDE CI in case a project needs something special.

A simple example is used by [Kate](https://invent.kde.org/websites/kate-editor-org/-/blob/master/scripts/custom_generation.py), but a more complex case is the [KDE Developer Website](https://invent.kde.org/documentation/develop-kde-org/-/tree/master/scripts).

The [KDE Gitlab CI infrastructure](https://community.kde.org/Infrastructure/Continuous_Integration_System) uses a lot of Python. You can see that especially with [ci-utilities](https://invent.kde.org/sysadmin/ci-utilities), which uses several Python scripts to automate tasks in its Gitlab templates.

Another sysadmin Python project that you can contribute to is the [CI Notary Service](https://invent.kde.org/sysadmin/ci-notary-service), used to sign packages for deployment, like Windows, MacOS or Flatpak. Naturally, this requires some platform-specific knowledge.

There are many other projects where you can contribute with Python, and it’s most definitely the top used language in KDE other than C++, QML and CMake.

To summarize:

* [Appium tests](https://develop.kde.org/docs/apps/tests/)
* [Selenium Webdriver](https://invent.kde.org/sdk/selenium-webdriver-at-spi)
* apps like [KDevelop](https://invent.kde.org/kdevelop/kdevelop) / [KDevelop's plugin for Python](https://invent.kde.org/kdevelop/kdev-python) / [Kdenlive](https://invent.kde.org/multimedia/kdenlive/-/tree/master/data/scripts) / [Lokalize](https://invent.kde.org/sdk/lokalize/-/tree/master/scripts) / [KTimeTracker](https://invent.kde.org/pim/ktimetracker/-/tree/master/test)
* [kde-builder](https://invent.kde.org/sdk/kde-builder)
* [Doxyqml](https://invent.kde.org/sdk/doxyqml)
* [pology](https://invent.kde.org/sdk/pology)
* [Craft](https://invent.kde.org/packaging/craft)
* [Craft Blueprints](https://invent.kde.org/packaging/craft-blueprints-kde)
* [KDE websites](https://invent.kde.org/websites)
* [ci-utilities](https://invent.kde.org/sysadmin/ci-utilities)
* [CI Notary Service](https://invent.kde.org/sysadmin/ci-notary-service)

My personal recommendations are:

* [Doxyqml](https://invent.kde.org/sdk/doxyqml): we sorely need to improve our QML API generation!
* [Craft Blueprints](https://invent.kde.org/packaging/craft-blueprints-kde): the sheer quantity of them!
* [Appium tests](https://develop.kde.org/docs/apps/tests/): we need to make sure all our software is tested and working!
* [KDE websites](https://invent.kde.org/websites)
* [pology](https://invent.kde.org/sdk/pology): we have barely any experts on GNU gettext and Python!
* [Selenium Webdriver](https://invent.kde.org/sdk/selenium-webdriver-at-spi)
* [KDevelop's plugin for Python](https://invent.kde.org/kdevelop/kdev-python)

You should also take a look at this Akademy 2024 talk when it comes out:

* [Pythonizing Qt](https://conf.kde.org/event/6/contributions/208/), by PySide’s main Qt developer Cristián Maureira-Fredes

## Ruby

We use Ruby mainly for two things: Jekyll-based websites and some specialized tools.

Jekyll is a tool written in Ruby to create static websites, so we don’t really need much help with its Ruby code. If you do have experience with Jekyll though, you can take a look at the [KDE Jekyll Website guide](https://community.kde.org/KDE.org/Jekyll) and [KDE’s Aether Theme for Jekyll](https://invent.kde.org/websites/jekyll-kde-theme) and search for some websites yourself.

Now the interesting part is the specialized tools.

I’ve mentioned Appium just above in the Python section, but [Appium](https://appium.io/docs/en/2.2/ecosystem/#clients) and [Selenium](https://www.selenium.dev/documentation/) can actually be used with several languages, including Ruby. That means KDE’s Selenium AT-SPI Webdriver [can be used with Ruby](https://invent.kde.org/sdk/selenium-webdriver-at-spi/-/blob/master/examples/kinfocentertest.rb) to write Appium tests too.

KDE’s [Gitlab Triaging](https://invent.kde.org/sysadmin/gitlab-triaging) tool used by the [Gardening Team](https://community.kde.org/Gardening) is written entirely in Ruby (Gitlab itself uses a lot of Ruby too, so it makes sense).

The [Bugzilla Bot](https://invent.kde.org/sysadmin/bugzilla-bot), the one that automatically manages bug reports on https://bugs.kde.org, is also a tool that could use some love. As a tool that affects a huge amount of bug reports, any issues related to it will be significant.

The set of tools we use to make it easier to publish software releases, [Releaseme](https://invent.kde.org/sdk/releaseme), is very solid and only really known by KDE’s software maintainers.

Lastly we have an experimental [Homebrew KDE repository](https://invent.kde.org/packaging/homebrew-kde). Homebrew after all is a macOS package manager written in Ruby and whose recipes are written in Ruby.

To summarize:

* [KDE’s Aether Theme for Jekyll](https://invent.kde.org/websites/jekyll-kde-theme)
* [Appium tests](https://invent.kde.org/sdk/selenium-webdriver-at-spi/-/blob/master/examples/kinfocentertest.rb)
* [Gitlab Triaging tool](https://invent.kde.org/sysadmin/gitlab-triaging)
* [Bugzilla Bot](https://invent.kde.org/sysadmin/bugzilla-bot)
* [Releaseme](https://invent.kde.org/sdk/releaseme)
* [Homebrew KDE repository](https://invent.kde.org/packaging/homebrew-kde)

## Perl

We don’t really have many projects written in Perl. There’s one we do have that is major: [kdesrc-build](https://invent.kde.org/sdk/kdesrc-build), the main tool we have used for a long time to easily build KDE software from the master branch (or any branch really) together with its dependencies. It is basically our main contribution entrypoint for new developers.

It is currently lacking a maintainer and enough activity, and there’s a new tool around the block called [kde-builder](https://invent.kde.org/sdk/kde-builder) written in Python which is supposed to supersede it, but realistically the transition period from one tool to the next (as well as a migration plan) and the fact the tool is incredibly mature and stable means kdesrc-build will still be present for a long time.

To summarize:

* [kdesrc-build](https://invent.kde.org/sdk/kdesrc-build)

## Containerfile / Docker / Podman {#containers}

While this isn’t a language, it’s a specialized format with a lot of use in KDE infrastructure.

The two most notable examples of this are the sysadmin repositories [ci-images](https://invent.kde.org/sysadmin/ci-images) and [ci-utilities](https://invent.kde.org/sysadmin/ci-utilities). The former consists of container images and the latter includes, among other things, [Gitlab templates](https://docs.gitlab.com/ee/topics/build_your_application.html) using said images for [KDE’s continuous integration and delivery](https://community.kde.org/Infrastructure/Continuous_Integration_System).

We actually have very few system administrators and their work basically keeps KDE running. I’m sure they would appreciate the help.

If you have never dealt with Gitlab templates and you are not yet confident to mess around with KDE templates, you can do what I did before I got to write the above continuous integration wiki page: selfhost Gitlab on your own machine to learn the tools. I have a [podman quadlet](https://codeberg.org/herzenschein/herz-quadlet/src/branch/main/gitlab) just for selfhosting Gitlab that makes it easy. Gitlab does use over 6 GiB of RAM by default, though!

To summarize:

* [ci-images](https://invent.kde.org/sysadmin/ci-images)
* [ci-utilities](https://invent.kde.org/sysadmin/ci-utilities)

See also the section on [Web Assembly](#wasm), as it mentions a specialized tool that uses a container image under the hood.

You should also take a look at this Akademy 2024 talk when it comes out:

* [KDE’s CI and CD infrastructure](https://conf.kde.org/event/6/contributions/218/), by four of our KDE Sysadmins

## HTML / SCSS / JavaScript {#websites}

We have a sore need for web developers! We have an insane number of websites and few people to go through them.

But we need specifically web developers who are willing to work with [Hugo](https://gohugo.io/). Maybe a bit of Jekyll as well, as mentioned before in the [Ruby section](#ruby) we do have a few websites written in it.

Hugo itself is written in Go, like Jekyll is written in Ruby. Unlike Jekyll, however, the website portion of Hugo does make use of a specialized [template system](https://gohugo.io/templates/) that is based on Golang’s [text/template](https://pkg.go.dev/text/template) and [html/template](https://pkg.go.dev/html/template). Still, it’s not a difficult template system to learn, and most of the necessary knowledge will still be web technologies. It’s fairly easy to get it going.

Our main web developers already have their hands full with high-demand web stuff like [KDE’s Extensions for Hugo](https://invent.kde.org/websites/hugo-kde) and the main [kde.org website](https://invent.kde.org/websites/kde-org), and they still have to maintain many others.

In particular, the [KDE Developer website](https://develop.kde.org/) has many issues that can only be addressed by web folks, one of which is a serious accessibility issue: [low contrast in text](https://invent.kde.org/documentation/develop-kde-org/-/issues/141). I have marked most issues that only web folks can help with with the [“Needs web review/assistance” label](https://invent.kde.org/documentation/develop-kde-org/-/issues/?label_name%5B%5D=Needs%20Web%20review%2Fassistance).

In addition to Hugo and Jekyll, I believe we still have a few remaining websites that use PHP, Wordpress, Drupal and need porting away from those technologies, and of course several websites that are just plain unmaintained or poorly maintained. You can see a list of all websites available in the [Websites Group](https://invent.kde.org/websites).

To summarize:

* [list of all websites](https://invent.kde.org/websites)
* [KDE Developer’s website](https://invent.kde.org/documentation/develop-kde-org/-/issues/?label_name%5B%5D=Needs%20Web%20review%2Fassistance)

## Web Assembly {#wasm}

We have one tool written in web assembly that is unmaintained, decommissioned, and we’d very much love to get working again!

The tool is QML Online. It’s a website that lets you write QML code (and even Kirigami code) directly and generate the GUI on the website itself. It was excellent for prototyping and sharing code, but it unfortunately was not ported to Qt6. So far I have seen no other similar project (there’s like 5 out there) that managed to surpass it.

I have updated the [project’s readme](https://invent.kde.org/webapps/qmlonline) with better build instructions, and it works locally with Qt5, but the main container image it is based on needs to be ported to use Qt6 instead so we can get it running again. We need both someone who knows their way around containers *and* Qt6 for Web Assembly.

To summarize:

* Revive [QML Online](https://invent.kde.org/webapps/qmlonline)

## Flatpak / Snap {#flatpak-snap}

Both packaging technologies are very well regarded at KDE, but they have different culture and environments, even if they are very technically similar.

The relationship between KDE software and Flatpak is more like KDE being upstream: you will see Flatpak manifests directly on many KDE projects, and we do have a [flatpak tutorial on our KDE Developer platform](https://develop.kde.org/docs/packaging/flatpak/). It is also completely distribution agnostic, as you can build it on any distribution.

The relationship between KDE software and Snaps is more like Snap being upstream: it’s Canonical, Ubuntu and Ubuntu-based distros that do the packaging work. That is already more or less the case even outside KDE: Snap on most non-Ubuntu distributions is actually maintained by Canonical, and Snap issues on such distros is redirected to them. You will not really see Snap manifests in KDE software repositories, and the main tutorial for it is made by Canonical.

In practice, this means you can contribute to Flatpak manifests for KDE software directly on KDE’s repos (including for Flathub), and you can contribute to Snap manifests for KDE software by getting in contact with the folks that do Snap outside KDE (including for the Snap store), which includes the KDE neon people.

In Flatpak’s case the work is coordinated in a [KDE Flatpak Matrix group](https://community.kde.org/Matrix#Development/Contributors), while in Snap’s case the work is mostly done by the [KDE neon maintainers](https://community.kde.org/Guidelines_and_HOWTOs/Snap#Help) and Snap developers over the [Snapcraft Forum](https://forum.snapcraft.io/).

Both are actually very easy to onboard: if you are generally acquainted with how to build software, you can get started very easily with either.

To summarize:

* flatpak: most [KDE projects on Invent](https://invent.kde.org/), including those that don’t have a manifest yet
* flatpak: all [KDE projects on Flathub](https://flathub.org/), including making new ones with help from the [KDE Flatpak Matrix group](https://community.kde.org/Matrix#Development/Contributors)
* snap: on the [Snap forums](https://forum.snapcraft.io/) and with help from the [KDE neon maintainers](https://community.kde.org/Guidelines_and_HOWTOs/Snap#Help)

## CMake

This is a bit of specialized knowledge that needs mention.

I find modern CMake itself to be rather pleasant *to use* once you know what to do, and KDE’s [extra-cmake-modules](https://api.kde.org/frameworks/extra-cmake-modules/html/index.html) (ECM) makes it much more convenient for C++ and Qt projects in general (not just KDE). I’m in the middle of reading [Professional CMake](https://crascit.com/professional-cmake/) and loving it.

But actual CMake code in custom modules used for real life projects can be as complicated a beast as any Turing-complete code. For this reason, having more CMake experts looking into ECM code wouldn’t hurt.

It is otherwise possible to contribute to most KDE projects with CMake easily as almost everything uses CMake. What are the possible venues?

* Knowing standard CMake: there is an [introductory tutorial](https://cmake.org/cmake/help/latest/guide/tutorial/index.html) on the CMake website itself. Standard CMake is the same everywhere, and modern CMake especially is always considered an improvement over old CMake. For the main differences between old and modern CMake you can take a look at the [Effective CMake talk](https://youtu.be/bsXLMQ6WgIk) by Daniel Pfeiffer on YouTube.
* Knowing Qt CMake: Qt has some [specialized API](https://doc.qt.io/qt-6/cmake-manual.html) that is used for QML apps or to deploy applications. KDE uses the former but not the latter. It requires knowing standard CMake, and its knowledge is pretty valuable.
* Knowing KDE’s ECM: ECM improves on top of standard CMake and Qt CMake, reducing a lot of boilerplate. It requires knowing standard CMake and Qt CMake.

Basically what I’m saying is: if you know *some* CMake, you can contribute to any KDE project, and if you know *a lot* of CMake, you can contribute to ECM.

To summarize:

* most KDE projects using CMake
* [extra-cmake-modules](https://api.kde.org/frameworks/extra-cmake-modules/html/index.html)

## Java

We actually do have a few projects using Java. The most notable is probably [KDE Connect Android](https://invent.kde.org/network/kdeconnect-android), which is written exclusively in Java to be entirely native for the platform rather than use Qt’s crossplatform facilities.

Other KDE apps that *do* use Qt’s crossplatform facilities unavoidably also need to touch Java, as Qt does integrate with Java/Kotlin and needs some Java files to allow for deployment. [Qt’s Android documentation](https://doc.qt.io/qt-6/android-how-it-works.html) is probably best to explain this, but we also have some docs on [how we automate building KDE software for Android using Craft](https://develop.kde.org/docs/packaging/android/building_applications/) which might be relevant if you want to look into the whole Android process as is done by KDE.

To summarize:

* [KDE Connect Android](https://invent.kde.org/network/kdeconnect-android)
* most KDE projects that build on Android

## Rust

I was originally refraining from writing a section just for Rust because I myself don’t know the right places where you can contribute to KDE with Rust code, and I personally think we should focus on investing on our Python ecosystem.

The most I know is that KDE has a [Rust-to-Qt bindings generator](https://invent.kde.org/sdk/rust-qt-binding-generator), and we have a [KDE Rust wiki page](https://community.kde.org/Rust) detailing some things.

But then I saw that [redstrate](https://redstrate.com/) is making an Akademy 2024 talk whose name is [C++, Rust and Qt: Easier than you think](https://conf.kde.org/event/6/contributions/203/), and just this very talk should be enough to warrant its own section.

Perhaps this should help with the current situation involving Rust adoption: lots of people outside KDE hype for Rust, but few outside people actually help make it happen. It’s a bit of a chicken-and-egg situation, naturally, but still: we need people making it happen, not so much people hyping it.

In any case, I do also know _non-KDE_ projects you can contribute to with Rust, which ultimately would help improve the Rust ecosystem for KDE.

In case you didn’t know, I was a moderator for the [KDE Subreddit](https://www.reddit.com/r/kde/), and I’m the one who made that very long onboarding sidebar on the new Reddit version of the website and kept it up-to-date for years. I didn’t (and still don’t quite) particularly care about Rust myself, but I did make a honorable mention to it on the sidebar regardless, and I’ll use it as base here, with what I now know, in order of priority:

* [cxx](https://cxx.rs/) lets you use C++ in Rust projects and use Rust in C++ projects.
* [cxx-qt](https://github.com/KDAB/cxx-qt) lets you use Qt in Rust projects and use Rust in Qt projects.
* [Rust-Qt or Ritual](https://rust-qt.github.io/) includes bindings for Rust to use C++, and thus also Qt.
* [QMetaObject-rs](https://github.com/woboq/qmetaobject-rs) includes bindings for Rust to use QML, and thus also QtQuick.

The reason for the ordering is that cxx and cxx-qt currently seem to be the most promising projects. After all, cxx is bidirectional, and cxx-qt is made by KDAB, and we have (and have had) several people from KDAB contributing to KDE, even as contractors.

Furthermore, from what I’ve heard it seems likely that, if someone is going to make bindings for KDE Frameworks in Rust, they will probably need to be done using cxx.

Based on my own personal experience, both cxx and cxx-qt were very nice to use when writing Rust-only code, just so you know.

In another turn of events, however, if we were to see a boon in Ritual and QMetaObject-rs, it’s also quite possible that those could help improve the KDE Rust ecosystem.

To summarize:

* [KDE Rust wiki page](https://community.kde.org/Rust)
* [Rust-to-Qt bindings generator](https://invent.kde.org/sdk/rust-qt-binding-generator)
* [cxx](https://cxx.rs/)
* [cxx-qt](https://github.com/KDAB/cxx-qt)
* [Ritual](https://rust-qt.github.io/)
* [QMetaObject-rs](https://github.com/woboq/qmetaobject-rs)

You should also take a look at this Akademy 2024 talk when it comes out:

* [C++, Rust and Qt: Easier than you think](https://conf.kde.org/event/6/contributions/203/)
