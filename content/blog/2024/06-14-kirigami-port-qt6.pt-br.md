---
title: "O tutorial de Kirigami foi portado para o Qt6"
date: 2024-06-14
url: kirigami-port-qt6
tags:
  - Planet KDE PTBR
---

Após três meses de trabalho, o [tutorial de Kirigami da KDE](https://develop.kde.org/docs/getting-started/kirigami/) foi portado para o Qt6.

Caso você não saiba o que é o Kirigami:

* O Qt fornece duas tecnologias GUI para criar aplicativos de desktop: o [QtWidgets](https://doc.qt.io/qt-6/qtwidgets-index.html) e o [QtQuick](https://doc.qt.io/qt-6/qtquick-index.html)
* O QtWidgets usa somente C++ enquanto o QtQuick usa QML (e opcionalmente C++ e JavaScript)
* [Kirigami](https://develop.kde.org/frameworks/kirigami/) é uma biblioteca feita pela KDE que extende o QtQuick fornecendo vários componentes vantajosos e que facilitam a vida

Tecnicamente não houveram muitas mudanças de API no Kirigami. As mudanças principais foram:

* trocar do Kirigami.Overlaysheet pro Kirigami.Dialog
* trocar de actions.{main,left,right} para apenas actions
* trocar do Kirigami.BasicListItem para o Controls.ItemDelegate ou o Kirigami.TitleSubtitle e afins

Todas são APIs fáceis de entender, então desenvolvedores de apps não deverão ter problemas portando para elas. Mesmo que seja o caso, temos uma [página da wiki com instruções sobre como portar para Kirigami com Qt6](https://invent.kde.org/frameworks/kirigami/-/wikis/Porting-applications-to-KF6-version-of-Kirigami).

O maior desafio na verdade veio de outro lugar: o novo uso de registrar módulos QML de modo declarativo (declarative registration).

O jeito antigo de registrar novos tipos do C++ pro lado do QML involvia um contexto global ou chamadas de API específicas, o que geralmente se traduz em instanciar o objeto no código C++, podendo ser inconveniente. A depender do jeito que você registra e como a API foi chamada, também pode vir a ser necessário implementar uma factory.

O novo jeito envolve criar módulos QML diretamente pelo CMake e adicionar uma ou duas macros na classe que você quer expor, de modo a não precisar instanciar nada do lado do C++. Como bônus, também dá para adicionar recursos QML diretamente pelo CMake ao invés de escrever código XML.

A API usada para o novo jeito de registrar de modo declarativo é o qt_add_qml_module(), mas para nossas coisas da KDE usamos o ecm_add_qml_module(), que remove um monte do boilerplate.

Uma consequência do novo método de registrar módulos QML é que tipicamente espera-se que URIs usem o esquema de DNS reverso, rDNS (por exemplo, org.kde.kirigami.delegates) e também que o resource path se torna longo demais:

```
<prefixo_do_recurso><URI_de_import><arquivo>
```

Por exemplo, “qrc:/qt/qml/org/kde/example/SomeModule.qml”.

Isso leva a uma mudança de API: do engine.load(“qrc:/qt/qml/org/kde/example/SomeModule.qml”) para o engine.loadFromModule(“org.kde.example”, “SomeModule”), que é muito mais curto.

O [loadFromModule()](https://doc.qt.io/qt-6/qqmlapplicationengine.html#loadFromModule) no entanto requer um módulo QML (evidentemente), que precisa começar com uma letra maiúscula. Isso significa mudar toda instância de main.qml em Main.qml, e em todo lugar no tutorial. É uma tarefa cansativa, um chore task.

Um benefício legal de portar pro Qt6 foi que não é mais necessário especificar versões de import nos arquivos QML. Para usuários da API isso basicamente significa não ligar para o versionamento do app, e para escritores de tutorial isso significa não precisar caçar a versão mínima de import. Mas esta também é uma tarefa abrangente e cansativa.

Eu comecei a portar por este ponto por ser a parte que visualmente melhor indica que o conteúdo é em Qt6.

## Melhorias opcionais

Um problema antigo com o tutorial de Kirigami sempre foi as capturas de tela não atualizadas. Especificamente:

* imagens que não refletiam o código
* imagens que tinham ícones de janela X11
* imagens que tinham ícones de janela Wayland
* imagens que simplesmente não tavam bonitas
* imagens mal-posicionadas

Uma vez que o produto só pode ser tão bom quanto sua documentação, em outras palavras, o produto *é* a documentação, capturas de tela tem suma importância. Se o produto é exposto de maneira não profissional, ele se torna pouco atrativo.

A mudança de API de Overlaysheet para Dialog era uma mudança drástica na interface. Já que eu já precisava atualizar as capturas de tela pro Dialog mesmo, eu podia usar o [novo redesign do Dialog do Carl Schwann](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1519), que tem aparência ótima.

{{< figure src="/2024/06/kirigami-dialog.webp" >}}

A mudança de API nas Kirigami actions também levou a mudanças de interface, mas na interface para telefones e tablets. O Plasma Mobile também era pouco representado no tutorial, o que exigia atualizações.

{{< figure src="/2024/06/kirigami-actions.png" >}}

Então eu resolvi atualizar a maioria das capturas de tela, o que levou a algumas outras coisas em seguida:

* fazer as imagens representarem o código de exemplo
* fazer o código de exemplo representar as imagens
* arrumar o espaçamento entre imagens comparativas
* arrumar o posicionamento das imagens
* adicionar um arquivo desktop pros exemplos para que ícones de janela aparecessem no Wayland
* explicar como instalar o aplicativo para que ícones de janela aparecessem no Wayland

As duas primeiras implicavam em mudanças no design do app, usando as mesmas questões que vou falar na próxima seção desta postagem de blog. As duas últimas na verdade foram consequência das minhas atualizações incompletas de anos atrás.

Originalmente, o tutorial de Kirigami tinha vários problemas óbvios como código faltando, poucas imagens, quase nenhum link, nenhuma instrução de compilação ou de como rodar, etc. Quando eu atualizei ele anos atrás, eu não fui até o fim e também não tinha a expertise necessária.

Para que ícones de janela apareçam no Wayland, o aplicativo precisa ser instalado no sistema e fornecer um arquivo desktop. No X11 é possível ignorar o requerimento do arquivo desktop, mas o aplicativo ainda precisa ser instalado. Já que antes eu não sabia como instalar os aplicativos corretamente, eu apenas forneci instruções de como compilar e como consequência não pude fornecer capturas de tela com ícones de janelas. O atual tutorial finalmente tem isso.

Além disso, agora, já que o código está correto e fornece um arquivo desktop e as instruções de instalação certas, qualquer um que venha a testar o tutorial no futuro (eu inclusive) vai achar muito mais fácil de testar. Agora é possível copiar e colar o código diretamente com confiança de o projeto vai estar como deve. Quem teria imaginado que fornecer código reproduzível e instruções ajuda no QA e economiza tempo? /s

No estado atual, a não ser que hajam mudanças drásticas na API ou no tutorial, a maioria das partes em que eu mexi deverão durar pelo menos até o Plasma 7.

## Desafios de documentação

Para quem mexe com documentação, atualizar um tutorial de aplicativos desktop não é tarefa pequena ou simples. Tem várias coisas para se ter em mente:

* que design de app devo recomendar para os usuários?
* que design de app é de fato usado pelos desenvolvedores?
* que design de app é esperado de upstream?
* que API merece ser mencionada?
* há palavras chave para o leitor saber o que deve pesquisar para achar mais informações?
* como tornar os exemplos de código mais didáticos?
* como criar exemplos de código que ficam bonitos nas capturas de tela?
* que nível de explicação devo usar?
* o que devo remover?
* quando os exemplos de código devem ser resumidos ou completos?
* foram apresentados todos os conceitos necessários para o leitor terminar o tutorial?
* o que deve ser parte do tutorial e o que deve ser apenas uma nota?
* que práticas de documentação devo seguir ou ignorar?
* devo incluir workarounds para problemas comuns?
* devo incluir menções de possíveis problemas?
* e onde?
* como as capturas de tela devem estar posicionadas na página?
* que seções devo usar para esta página?
* qual ordem lógica deve ser usada para as seções?
* isso está consistente em todas as partes do tutorial que toquei?
* ainda faltam capturas de tela?
* ainda faltam links?
* o que eu devo deixar para fazer depois?

É necessário ter essas questões em mente e tomar decisões a respeito delas para poder lidar com a tarefa. Naturalmente, isso não significa que você deve lidar com todas ao mesmo tempo, apenas que você deve tê-las em mente.

As questões de design de aplicativos foram umas das mais difíceis de tratar, não tanto em relação aos exemplos de código usados, mas mais devido à estrutura de arquivos do projeto, que pode ser extremamente maleável em C++. Mesmo olhando apenas nos projetos da KDE eu encontrei uns 5 tipos diferentes de estrutura de projeto! E na maioria das vezes indo contra recomendações do Qt. Por exemplo, se seguirmos o Qt à letra, eles recomendariam usar uma estrutura de arquivos similar ao URI (como src/org/kde/example/AlgumModulo.qml) enquanto a KDE quase nunca faz isso (src/AlgumModulo.qml or src/qml/AlgumModulo.qml or src/algumsubdir/AlgumModulo.qml).

Decidi ir com uma abordagem híbrida parecida com a [solução 2 desta postagem do blog do Qt](https://www.qt.io/blog/implicit-imports-vs.-qml-modules-in-qt-6) com o Main.qml na pasta src/ e outros módulos QML numa subpasta (ou em múltiplas subpastas), já que é mais didático e escala melhor.

Outro aspecto para o qual você não deve se render é a desmotivação causada por tarefas cansativas ou entediantes! Pensar no design do aplicativo ou tentar descobrir jeitos didáticos de resolve problemas costuma ser desafiador, interessante ou divertido, mas tarefas entediantes sugam sua alma.

Para mim a parte mais exaustiva foi atualizar as capturas de tela. Eu precisei criar por volta de 200 dessas, seja por conta de erros ou de mudanças de design/didática. Acabei atualizando aproximadamente 51 imagens do tutorial, e isso ainda nem é tudo. Faz sentido, afinal, já que o tutorial do Kirigami tem 30 páginas de conteúdo (e eu mexi em 19 delas).

De maneira geral, lidar com documentação e tutoriais de API requer que o escritor passe por todo esse trabalho para que o leitor não passe por nenhum trabalho a mais e possa focuar só no que é necessário.

## Como aprender mais sobre

Para aprender sobre mais questões de documentação como esta e lidar com elas, dois excelentes materiais que eu recomendo que são gratuitos são o [Guia de Estilo de Documentação para desenvolvedores da Google (em inglês)](https://developers.google.com/style) e o [Curso de Escrita Técnica da Google (em inglês)](https://developers.google.com/tech-writing). Não seguimos estritamente essas diretrizes, mas elas são diretas e bons pontos de entrada.

Também temos o [Guia de Estilo do KDE Develop (em inglês)](https://develop.kde.org/docs/contribute/style/) que trata de certas coisas que fazemos apenas no KDE Develop. Em particular quero mencionar a seção final: [Siga práticas de documentação onde fizer sentido](https://develop.kde.org/docs/contribute/style/#standard-documentation-practices), que lista vários outros recursos para aprender sobre escrita técnica, tendo conteúdos curados especialmente por mim.

A KDE tem uma [página da wiki para documentação](https://community.kde.org/Get_Involved/documentation) explicando as diferentes áreas de documentação em que você pode contribuir na KDE, mencionando também as tecnologias que usamos para isso.

Além de coisas de coisas de documentação, temos também uma [página com recursos dedicados para aprender desenvolvimento com Qt](https://develop.kde.org/docs/getting-started/building/help-learn/), incluindo uma [lista curada de materiais de estudo](https://community.kde.org/Get_Involved/development/Learn).

E eu tenho um repositório pessoal chamado [minimal-qml6-examples](https://codeberg.org/herzenschein/minimal-qml6-examples) que está ainda incompleto mas fornece vários exemplos de código em Qt puro explicando o essencial de como registrar módulos QML usando a nova API do CMake.

Exclusivamente para os leitores brasileiros, também tenho um repositório chamado [Materiais de aprendizado Qt](https://github.com/herzenschein/materiais-de-aprendizado-qt) que explica o básico do que é o Qt e fornece uma quantidade gigantesca de links para você aprender Qt.
