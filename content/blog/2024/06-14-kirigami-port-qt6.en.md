---
title: "Kirigami tutorial now ported to Qt6"
date: 2024-06-14
url: kirigami-port-qt6
tags:
  - Planet KDE
---

After three months, KDE's [Kirigami tutorial](https://develop.kde.org/docs/getting-started/kirigami/) has been ported to Qt6.

In case you are unaware of what Kirigami is:

* Qt provides two GUI technologies to create desktop apps: [QtWidgets](https://doc.qt.io/qt-6/qtwidgets-index.html) and [QtQuick](https://doc.qt.io/qt-6/qtquick-index.html)
* QtWidgets uses only C++ while QtQuick uses QML (plus optional C++ and JavaScript)
* [Kirigami](https://develop.kde.org/frameworks/kirigami/) is a library made by KDE that extends QtQuick and provides a lot of niceties and quality-of-life components

Strictly speaking there weren't that many API changes to Kirigami. The most notable changes were:

* switching from Kirigami.Overlaysheet to Kirigami.Dialog
* switching from actions.{main,left,right} to raw actions
* switching from Kirigami.BasicListItem to Controls.ItemDelegate/Kirigami.TitleSubtitle et al.

Which are all easy-to-understand API, so developers shouldn't have issues porting to those. In any case, there is a [wiki page with some instructions on porting to Kirigami with Qt6](https://invent.kde.org/frameworks/kirigami/-/wikis/Porting-applications-to-KF6-version-of-Kirigami).

No, the actual challenge came from a different front: moving to declarative registration.

The old method of registering types from C++ to QML involved using a global context or specific API calls, which usually meant instantiating the object in C++ code, which can be annoying. Depending on the registration type and how it was called, it would also be necessary to implement a factory.

The new method involves creating QML modules directly from CMake and adding one or two macros in the class you want to expose, so you don't really have to instantiate anything in your C++ code. As an additional bonus, you can add QML resources directly from CMake instead of writing XML code.

The relevant Qt API for the new declarative registration is qt_add_qml_module(), but for our KDE stuff we use ecm_add_qml_module(), which removes the upstream boilerplate.

One consequence of the new declarative method of registering QML modules is that URIs are expected to use reverse DNS naming schemes (for example, org.kde.kirigami.delegates) and their resource path becomes way too long:

```
<resource_prefix><import_URI><file>
```

An example would be “qrc:/qt/qml/org/kde/example/SomeModule.qml”.

Which leads to an API change: from engine.load(“qrc:/qt/qml/org/kde/example/SomeModule.qml”) to engine.loadFromModule(“org.kde.example”, “SomeModule”), which is much shorter.

[loadFromModule()](https://doc.qt.io/qt-6/qqmlapplicationengine.html#loadFromModule) however requires, as the name implies, a QML module, which needs to start with an uppercase character. This means every main.qml needs to turn into Main.qml. This needs to be done everywhere in the tutorial, it's a chore task.

A nice-to-have benefit of the port to Qt6 is that we no longer need to specify import versions in QML files. For API users this means less concerns with version numbering and for tutorial writers this means not needing to hunt for a specific import version. But this also meant a chore task of removing imports from everywhere.

Since this is the more evident change that indicates the new content is using Qt6, that’s what I started the tutorial port with.

## Quality of life changes

A long-standing issue with the Kirigami tutorial had been the lack of up-to-date screenshots. In particular:

* screenshots that don't reflect the code
* screenshots that had X11 window icons
* screenshots that had Wayland window icons
* screenshots that looked plain bad
* screenshots that were badly positioned

Because a product is only as good as its documentation, that is to say, when the product *is* docs, this had paramount importance. If the product is showcased in a non-professional way, it ends up becoming unattractive.

The API change from Overlaysheet to Dialog meant a drastic UI change. Since I needed to update those screenshots, I could use the [new Dialog redesign by Carl Schwann](https://invent.kde.org/frameworks/kirigami/-/merge_requests/1519), which looks great.

{{< figure src="/2024/06/kirigami-dialog.webp" >}}

The API change to Kirigami actions also reflected in UI changes, but mostly on mobile. And Plasma Mobile was under-represented in the tutorial, so it needed updates too.

{{< figure src="/2024/06/kirigami-actions.png" >}}

So I went on about updating most screenshots, which entails a few things:

* make the screenshots represent the actual code
* make the actual code represent the screenshots
* fix spacing between comparison screenshots
* fix screenshot positioning
* add a desktop file for the examples so window icons show up on Wayland
* explain how to install the application so window icons show up on Wayland

The first two meant changes in app design with the same questions posed in the next section of this blog post. The last two were in fact the result of my lackluster updates from years ago.

Historically the original Kirigami tutorial had many blatant issues such as missing code, few screenshots, barely any links, no build or run instructions, etc. When I updated it years ago, I did not go full length with it and I didn’t have the expertise necessary.

In order to have window icons on Wayland, the application needs to be installed on the system and provide a desktop file. On X11 it is possible to disregard the desktop file requirement, but the application still needs to be installed. Because I didn’t know how to install applications properly, I only provided build instructions and as a consequence wasn’t able to get screenshots with window icons. The updated tutorial has those.

In addition to that, because the code is more accurate and provides its own desktop file and correct install instructions, the life of any tutorial tester gets much easier in the future, myself included. You can blindly copy+paste the code and the project will be as it should. Who would have thought that providing reproducible code and instructions helps QA and saves time in the end? /s

With the current update, unless there are drastic API or tutorial changes, most of the bits I touched should last well into Plasma 7 at least.

## Documentation challenges

For someone dealing with documentation, updating a tutorial on writing desktop applications isn't a small or simple task. There are a few things one needs to think about:

* what app design should I recommend to users?
* what app design is actually in use?
* what app design does upstream expect of API users?
* what API should get the spotlight?
* have I mentioned a keyword so the reader knows what to search for?
* how to design code examples to be didactic?
* how to design code examples that look good in screenshots?
* what level of explanation should I provide?
* what can I remove?
* when should code examples be short or complete?
* have I presented all concepts necessary for the user to finish the tutorial?
* what should be a part of the tutorial and what should be a note?
* what documentation practices should I follow and not follow?
* should I include workarounds to common issues?
* should I include notes about possible issues?
* if so where?
* how should screenshots be organized in the page?
* what sections should I use for this page?
* in which order should the sections be read?
* have I made this consistent across all text I touched?
* what screenshots are missing?
* what links are missing?
* what should I leave for later?

These questions need to be in your mind and you need to make lasting decisions on most of them in order to progress. This naturally does not mean you should address all of them at the same time, just that you need to keep those in mind.

The app design questions were the most difficult to think about, not so much for the code examples we use, but because of project file structure, which can be extremely malleable in C++. Even just searching through KDE projects, I managed to find 5 different project structures! Most of the time going counter to upstream Qt’s recommendations. For example, followed to the letter, Qt would recommend a file structure like the URI (src/org/kde/example/SomeModule.qml) while KDE almost never does that (src/SomeModule.qml or src/qml/SomeModule.qml or src/somedirname/SomeModule.qml).

I went with a sort of hybrid approach similar to [solution 2 of this Qt blog post](https://www.qt.io/blog/implicit-imports-vs.-qml-modules-in-qt-6) with Main.qml in the src/ directory and other QML modules in a separate subdirectory (or multiple subdirs), which is more didactic and scales better.

Another aspect one must not succumb to is demotivation caused by chore tasks! Things like thinking about app design or figuring out didactic ways to address issues can be challenging and fun, but chore tasks can drain your soul.

For me the most draining was updating screenshots, because I ended up having to do about 200 of those, either due to mistakes or due to didactic/design changes. In the end I had updated 51 tutorial screenshots or so, and the tutorial has more than that. The Kirigami tutorial alone has 30 pages of content after all (I touched only 19 of them).

Generally, when dealing with API documentation and tutorials, the writer needs to go through all of this trouble so that the reader does not go through any trouble at all and can focus on what they need.

## Learning more

To learn about more questions like these and how to address them, two good resources I recommend that are free are the [Google Developer Documentation Style Guide](https://developers.google.com/style) and the [Google Technical Writing Course](https://developers.google.com/tech-writing). We don’t follow them to the letter, but it is to-the-point and useful as a starting point.

We also have the [KDE Develop Style Guidelines](https://develop.kde.org/docs/contribute/style/) which addresses some of the more specific KDE Develop instructions. I’d like to point out the last section: [Follow standard documentation practices where it makes sense](https://develop.kde.org/docs/contribute/style/#standard-documentation-practices), which contains a list of other resources to learn about technical writing, including some content personally curated by me.

KDE has a good [entrypoint wiki page for documentation](https://community.kde.org/Get_Involved/documentation) explaining the various areas of documentation you can work with at KDE and the technologies used.

Documentation aside, we also have a [dedicated page full of resources to learn Qt development](https://develop.kde.org/docs/getting-started/building/help-learn/), including a [curated list of study materials](https://community.kde.org/Get_Involved/development/Learn).

I have a personal repository called [minimal-qml6-examples](https://codeberg.org/herzenschein/minimal-qml6-examples) which, while unfinished/unpolished, provides lots of code examples with raw Qt to understand the essentials of QML registration using the new CMake API.

