---
title: Long KFluff – Connecting to WireGuard VPN from Plasma NetworkManager (plasma-nm)
author: Blumen Herzenschein
date: 2020-05-31
url: wireguard-plasmanm
aliases: /wordpress/index.php/2020/05/31/kfluff-wireguard-from-plasma-nm/
tags:
  - Fluff
---
Well, that was interesting. I learned the basics on how to setup a Wireguard VPN and it was easy enough that I took less than a day to learn. I'm a really slow learner, but it was fun.

The extras I mention in this blog post I took another several hours to understand.

I noticed I couldn't find any guides showing how to configure NetworkManager to connect to a WireGuard VPN, so I'm writing it.

Let's take a look at how to create a common WireGuard VPN setup, and later we will be checking how to connect to it using the Plasma NetworkManager widget. This will be geared towards users inexperienced with WireGuard, not veterans. I am simply sharing my findings.

Sources: 

**WireGuard quickstart:** <https://www.wireguard.com/quickstart/>  
**Unofficial WireGuard documentation**: <https://github.com/pirate/wireguard-docs> (seriously, huge kudos to the maintainer, it's pretty awesome and honestly this should be the official documentation)
**WireGuard in NetworkManager (nmcli in fact)**: <https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/>

**DISCLAIMER: I'm not that tech-savvy, so I might not be using the correct terminology since I simply search, test and deduce. I am not an expert and this is not a proper WireGuard tutorial, it just briefly explains WireGuard elements in a specific common setup so we understand how to put things on NetworkManager.**

As usual, if you **actually** just want a tl;dr on how to create a WireGuard connection via the Plasma NetworkManager widget and don't care about the rest, [click here][1].

## A simple WireGuard setup

First let's think of a fairly typical use-case for VPNs: one server handles connections between multiple client devices. When thinking of VPNs, some people may think of private connections or accessing international content, but its primary use is in its name, to create a private network where your devices are accessible.

A nice setup for that and which is convenient for people who can't pay for more than a VPS is having a bouncer server that connects several clients together within a single network.

The first thing you must do is install WireGuard on the machines where you want to use it, this includes both server and clients. The second thing is creating a private and a public key for each machine where we will connect WireGuard. As the Quick Start documentation mentions, you can do this with a bash oneliner:

```bash
wg genkey | tee privatekey | wg pubkey > publickey
```

You'll also need to add a few lines to your bouncer's /etc/sysctl.conf file by running the following commands:

```bash
echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
echo "net.ipv4.conf.all.proxy_arp = 1" >> /etc/sysctl.conf
```

Now let's take a look at the WireGuard config file of an example bouncer server like that:

```ini
[Interface]
Address = 10.0.0.1/24
PrivateKey = bouncerprivatekey
ListenPort = 123456
PostUp = iptables -A FORWARD -i %i -j ACCEPT; iptables -A FORWARD -o %i -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
PostDown = iptables -D FORWARD -i %i -j ACCEPT; iptables -D FORWARD -o %i -j ACCEPT; iptables -t nat -D POSTROUTING -o eth0 -j MASQUERADE
[Peer]
PublicKey = nodepublickey
AllowedIPs = 10.0.0.2/32
[Peer]
PublicKey = nodepublickey
AllowedIPs = 10.0.0.3/32
```

Now let's take a look at one client WireGuard config file:

```ini
[Interface]
Address = 10.0.0.2/32
PrivateKey = nodeprivatekey
[Peer]
PublicKey = bouncerpublickey
Endpoint = 123.45.67.89:12345
AllowedIPs = 10.0.0.1/24,10.0.0.3/32
PersistentKeepalive = 60
```

With WireGuard, the `[Interface]` section is relevant to the device being configured, regardless of it being a bouncer, node, or both. `[Peer]` always refers to other devices you might want to connect to. In other words: `[Interface]` has to do with everything _****local****_, and `[Peer]` has to do with everything _****remote****_. In the case of a bouncer, `[Interface]` refers to the bouncer itself, and `[Peer]` to any nodes. In the case of a client, `[Interface]` refers to the node being configured, and `[Peer]` refers to both bouncers and nodes to which it connects.

Except for `Endpoint`, all other IPs in this example are created by you. In this example we have `10.0.0.1`, `10.0.0.2`, `10.0.0.3`, all individual IPs belonging to a subnet `10.0.0.0`. As you can check [here][2], when you want to denote only a single IP, you should add `/32` to the front, so `10.0.0.1/32` is assuredly a single IP. If you however refer to `10.0.0.1/24`, you'd be implicitly referring to `10.0.0.1`, `10.0.0.2`, `10.0.0.3` … `10.0.0.254`, `10.0.0.255`. I learned this with the linked documentation in fact, it's well explained.

So the main point is, you should choose any subnet you'd like for your VPN. If your current machine's subnet is `192.168.0.0/24` and your machine IP is something like `192.168.0.2/32`, you can use a `192.168.1.0/24` subnet for your VPN and have the individual nodes `192.168.1.2/32`, `192.168.1.3/32`, `192.168.1.4/32`, etc.

Make sure to create only subnets for use in LANs, that is, addresses that do not correspond to a public IP assigned to anyone else. See [this][3] for reference. As long as your subnet is within this range, you should be safe. So, for instance, if your current LAN's subnet is 10.0.0.x (where x is any number) like in this example, your VPN needs to have a different range, something like 10.0.3.x for instance.

Since the bouncer is the first to be configured for this setup, the example uses `10.0.0.1` in the `[Interface]` section of the bouncer. The `[Interface]` section in each node should include the same IPs assigned in the `[Peer]` section of the bouncer.

Continuing with our example: `Address` refers to the IP you'll be assigning to the machine inside the VPN subnet. The bouncer has `/24` instead of `/32` because it will handle the connections of the entire subnet, which in this case includes only bouncer `10.0.0.1`, node `10.0.0.2` and node `10.0.0.3`.

`PrivateKey` and `PublicKey` are what allows one machine to trust another. Again quoting the [Quick Start][4], a public key can be derived from a private key. The private key stays with the device, it's managed by Wireguard, and should never be published.

It will sound silly, but one of the issues I encountered when setting things up for the first time was mere human error: since a WireGuard connection between two devices includes four keys, you should be sure of where each key comes from, which is why my example mentions `bouncer` and `node`. This is easy to notice when looking at the config file since the private key is always located inside `[Interface]` and the public key is always located inside `[Peer]`.

When using a setup like the one proposed here, `ListenPort` should be setup for the bouncer only, since the clients will require a specific port to connect to, but the server only needs for the clients to be within its subnet to connect. See the [already linked unofficial documentation][5]. As all things port-related, never use the default ports for your server. If you don't set `ListenPort` for the client nodes, they will set it automatically and dynamically, which is a nice security touch.

`PostUp` and `PostDown` run commands after getting the network up and when the network gets down. The present `iptables` commands include and remove rules so that forwarding a NAT connection between our public interface (in this example it's `eth0`, find this with `ip a`) and our VPN is only allowed if the VPN is up. It's quite convenient and you can put multiple `PostUp`s and `PostDown`s in the same config file for running multiple things both after getting the network up or down, but both are optional. There's also `PreUp` and `PreDown` for commands before getting the network up or down.

Let's talk a bit about `PersistentKeepalive` before proceeding. It's real easy to understand: if one of your machines is behind a NAT—and it likely is—WireGuard connections might time out occasionally. Assigning a value like `25` to it makes WireGuard send a packet every 25 seconds, but it really depends on your ISP and router configuration. Check the [unofficial documentation][6] on that. I prefer leaving it at 60 instead, but that's because I didn't experience connection issues with this value.

**DISCLAIMER: the following explanation on <code>AllowedIPs</code> was deduced based on my readings, but I can't guarantee its truth factor as I'm not that knowledgeable in this respect, so take it not only with a grain of salt, but with a full pack.**

The most important and perhaps most confusing section of all of this is `AllowedIPs`, and it's something you'll be handling quite a bit depending on your setup, especially since we'll be checking how to create such a connection on NetworkManager soon.

`AllowedIPs` determines which subnets and IPs you shall connect to. Sounds simple, but it works a bit differently for the clients. If on a bouncer you simply specify the IP of the nodes that will be connecting to it, on a client you need to specify the subnet and other clients, depending on your configuration. In this case, if the `Address` of the client is `10.0.0.2/32`, then `AllowedIPs` should contain `10.0.0.1/24` and `10.0.0.3/32`. A more strict connection would contain `10.0.0.1/32` and `10.0.0.3/32`, and as such it does not work when peers are behind different NATs unless you define a specific route, but that's explained later.

Other combinations render different results that are unsuitable for our use-case, but may be useful for other use-cases, but that's what I found most fun about it. Let's take a brief look:

Case 1: AllowedIPs = `10.0.0.1/24,10.0.0.3/32`. Two peers behind two different NATs can see each other because the subnet has been specified. Pinging `10.0.0.1` works, pinging `10.0.0.3` works. The use-case here is something like having a home computer behind a NAT and a work computer at your company's NAT, or visiting a friend and using their internet. This is a more comprehensive use-case.

Case 2: AllowedIPs = `10.0.0.1/32,10.0.0.3/32`. Two peers behind two different NATs cannot see each other, but two peers behind the same NAT can see each other. In the first case, if `10.0.0.3` is behind a NAT different from `10.0.0.2`, pinging `10.0.0.1` and `10.0.0.3` results in only connecting to `10.0.0.1`. In the second case, if `10.0.0.3` is behind the same NAT as `10.0.0.2`, pinging `10.0.0.1` and `10.0.0.3` works. The use-case here is when all of your machines are behind your home NAT, and they likely would never get behind another NAT. This is a more restricted use-case.

Case 3: AllowedIPs = `10.0.0.anynumber/24`. You can ping any 255 peers within the `10.0.0.0/24` subnet, including your bouncer server at `10.0.0.1`. The use-case here is when you have a huge list of machines in the same VPN and you just need access to all peers. This is the most comprehensive use-case.

Now that we understand most elements of this example WireGuard config file, we can connect to it.

## The Plasma NetworkManager interface {#plasma-gui}

First things first, let's create a public and a private key for our client device by running the following:

```bash
wg genkey | tee privatekey | wg pubkey > publickey
```

Now, let's open the NetworkManager widget and click the + button to create a new network connection on your client device.

{{< figure src="/2020/05/Choose-a-Connection-Type-0531_0142.png" caption="Figure 1 - Choose WireGuard." >}}

After clicking on Create, we'll see the following screen:

{{< figure src="/2020/05/New-Connection-wireguard-0531_0144.png" caption="Figure 2 - New WireGuard connection." >}}

In this step you need to add the private key of your client.

Leave the option `Store password for this user only` on, it's a good default, especially if you don't have multiple users on your machine.

If you have a firewall configured, you might want to set a `Listen port` to allow this WireGuard connection, but otherwise it's preferable to keep it blank since the port will then dynamically change.

The option `Autoroute peers` is enabled by default and allows for automatic discovery of routes after specifying the subnet, which is what we usually want.

{{< figure src="/2020/06/New-Connection-wireguard-0601_0052.png" caption="Figure 3 - IPv4 tab." >}}

In the IPv4 tab, the immediately exposed interface essentially corresponds to the `Address` section in `[Interface]`. It's nothing complicated, just add the corresponding IP `10.0.0.2/32`, that is, destination `10.0.0.2`, netmask `255.255.255.255`.

Let's now click on the button `Peers...` of the `WireGuard Interface` tab.

{{< figure src="/2020/06/WireGuard-peers-properties-0601_0053-1.png" caption="Figure 4 - Peer options." >}}

It's important not to mistake things here: put the bouncer's public key here, not the key you generated on this machine. I know it's silly, but unlike WireGuard, you're prone to human error, y'know.

Because `Autoroute peers` was on, we only need to specify the subnet and we should be fine. So anything like `10.0.0.0/24` or `10.0.0.1/24` or `10.0.0.3/24` or `10.0.0.4/24`, whatever you want, as long as its netmask is `255.255.255.0`.

`Endpoint address` and `Endpoint port` are pretty self explanatory: it's the actual IP of the bouncer and its port. `Persistent keepalive` is also the same as in the config file. The usual recommendation for use behind NATs is a value of `25`, but you can make it last more if you don't experience disconnects caused by your NAT.

That's it. Unlike when configuring WireGuard through a config file, the `Autoroute peers` option in the GUI ([the equivalent to wireguard.peer-routes in nmcli][7]) allows us to be lazy a bit. If you'd like, you can read the following deductions I made on how autoroute, AllowedIPs and manually-defined routes work.

## A little bit of experimenting

So I was testing how the widget behaves in different configurations but with the same setup as this example, and I learned quite a bit. Let's take a look at my tests before trying to unravel them. I must note that `10.0.0.2` and `10.0.0.3` would be behind different NATs. Let's also assume that we're connecting from `10.0.0.2`.

I messed a bit with the `Routes...` button within the IPv4 tab of the widget paired with `AllowedIPs` and `Autoroute peers`, and discovered some interesting things.

**DISCLAIMER: these are deductions and explanations made with my own words, do not assume them to be true, despite me assuming they are correct. If I'm incorrect I'd be glad to hear you.**

```
If autoroute peers on:
And AllowedIPs 10.0.0.0/24
And no routes
Connected to 1 and 3.
```

This is the example we used in our GUI configuration, the laziest to set up. This essentially means that after WireGuard identifies the correct subnet to connect to, it automatically creates routes to the bouncer `10.0.0.1` and to the client `10.0.0.3`, so we can connect to it.

```
If autoroute peers off:
And AllowedIPs 10.0.0.0/24
And no routes
Can't connect.
```

Now this is the easiest to understand. If I don't manually any routes, automatic discovery is not active to create any routes, and `AllowedIPs` does not contain the `/32` IP of any connection, no routes exist to find the bouncer or any nodes.

```
If autoroute peers off:
And AllowedIPs 10.0.0.0/24
And route 10.0.0.1/32
Connected to 1.
```

This here implies that `AllowedIPs` only allows to see the subnet, and that after seeing the subnet and defining a route, I can connect to a specific node. Hence why not specifying `10.0.0.3/32` prohibits you from connecting to it.

```
If autoroute peers off:
And AllowedIPs 10.0.0.0/24
And route 10.0.0.1/32 and 10.0.0.3/32
Connected to 1 and 3.
```

{{< figure src="/2020/06/Edit-IPv4-Routes-0601_1031.png" caption="Figure 5 – Routes dialog." >}}

This effectively proves the theory shown in the previous scenario.

```
If autoroute peers off:
And AllowedIPs 10.0.0.1/32
And route 10.0.0.1/32
Connected to 1.
```

This is a bit confusing to understand, but if by the previous theory we denote a hierarchy where Autoroute is less specific than AllowedIPs, which in turn is less specific than Manually-defined Routes, then this must mean that AllowedIPs set as `10.0.0.1/32` is actually referring to a subnet `10.0.0.1` containing only one individual IP, namely `10.0.0.1/32`. Hence why it is able to connect to the bouncer, but does not discover other clients; it's because client `10.0.0.3` does not exist within a `10.0.0.1/32` subnet.

```
If autoroute peers off:
And AllowedIPs 10.0.0.1/32
And route 10.0.0.1/24
Connected to 1, keys unavailable for 3.
```

A level further in this rationale, within a subnet `10.0.0.1/32` there exists no `10.0.0.3`. This also means that `10.0.0.3/32` belongs to a different subnet, namely one within `/24`. Because `10.0.0.1/32` and `10.0.0.3/32` are recognized as belonging to different subnets, they cannot access the keys from one another.

Additionally, in this case, accessing a subnet `10.0.0.1/24` inside a subnet `10.0.0.1/32` that contains only one element means that the specified `10.0.0.1/24` subnet also contains one element, which is `10.0.0.1/32`. Kinda weird to think about it, but that's what I can deduce from it.

The ability to recognize specific routes as belonging to specific subnets and not allowing them to communicate with each other unless they have the correct keys is one of the reasons why WireGuard works; you can have billions of subnets with the same range on the internet, and none of their members can connect to each other without actually belonging to the respective subnet.

```
If autoroute peers off:
And AllowedIPs 10.0.0.1/32
And route 10.0.0.1/32 and 10.0.0.3/32
Connected to 1, keys unavailable for 3.
```

This is actually the same scenario as before. Within a subnet `10.0.0.1/32` containing only one element, namely `10.0.0.1`, there doesn't exist a node `10.0.0.3`. Hence the assumption by WireGuard that it must belong to a different subnet.

As for:

```
If autoroute peers on:
And AllowedIPs 10.0.0.1/32 and 10.0.0.3/32
And no routes
Connected to 1.
```

This was the most confusing for me to understand. I assume it is so because clients `10.0.0.2` and `10.0.0.3` are behind different NATs but `10.0.0.1` is public (a.k.a. not behind a NAT).

Remember Case 2 which I mentioned before?

Case 2: AllowedIPs = 10.0.0.1/32,10.0.0.3/32. Two peers behind two different NATs cannot see each other, but two peers behind the same NAT can see each other. In the first case, if 10.0.0.3 is behind a NAT different from 10.0.0.2, pinging 10.0.0.1 and 10.0.0.3 can ping 10.0.0.1 only. In the second case, if 10.0.0.3 is behind the same NAT as 10.0.0.2, pinging 10.0.0.1 and 10.0.0.3 works. The use-case here is when all of your machines are behind your home NAT, and they likely would never get behind another NAT. This is a more restricted use-case.

Let's also recall something mentioned in the [unofficial documentation][8]:

Nodes that are behind separate NATs should <em>not</em> be defined as peers outside of the public server config, **as no direct route is available between separate NATs**. Instead, nodes behind NATs should only define the public relay servers and other public clients as their peers, and should specify AllowedIPs = 192.0.2.1/24 on the public server that accept routes and bounce traffic for the VPN subnet to the remote NAT-ed peers.

However, this just means there is no route to `10.0.0.3` despite there being a one-element subnet `10.0.0.3/32` being specified in `AllowedIPs`. So if we add it:

```
If autoroute peers on:
And AllowedIPs 10.0.0.1/32 and 10.0.0.3/32
And route 10.0.0.3/32
Connected to 1 and 3.
```

Now we can connect to both.

And if we disable autoroute, we get this:

```
If autoroute peers off:
And AllowedIPs 10.0.0.1/32 and 10.0.0.3/32
And route 10.0.0.3/32
Connected to 3.
```

Which implies that the aforementioned reason for `10.0.0.1/32` being accessible despite having no manually-configured route, a.k.a. not behind a NAT, is only true when `Autoroute peers` is on. Isn't this autoroute functionality real handy?

## I don't know about you…

…but I find it particularly fun to gather bits and pieces of information and assembling them to deduce how things work, and then explaining that to other people. I don't want to be that kind of person that deduces things incorrectly and states it's the truth, though, so I wrote three disclaimers here.

I also know there are several tutorials out there explaining such things, but as mentioned, I could not find any including the Plasma NetworkManager widget. I hope this clarifies things for you somehow.

Additionally, this wasn't a tutorial geared towards people quite experienced with WireGuard: I'm sure those people would find out how to use WireGuard in the NetworkManager widget, and that they very likely already experimented with how WireGuard works, or at the very least know what the different configurations actually do. No, this was geared towards people like myself, who had some issues understanding some aspects of WireGuard. This also means I made this post for myself, you know? This is, after all, a personal blog.

 [1]: #plasma-gui
 [2]: https://github.com/pirate/wireguard-docs#CIDR-Notation
 [3]: https://www.arin.net/reference/research/statistics/address_filters/
 [4]: https://www.wireguard.com/quickstart/
 [5]: https://github.com/pirate/wireguard-docs#listenport
 [6]: https://github.com/pirate/wireguard-docs#persistentkeepalive
 [7]: https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/
 [8]: https://github.com/pirate/wireguard-docs#peer
