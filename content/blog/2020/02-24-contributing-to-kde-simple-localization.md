---
title: Contributing to KDE is easier than you think — Localization plain and simple
author: Blumen Herzenschein
date: 2020-02-24
url: kde-localization-simple
aliases: /wordpress/index.php/2020/02/24/contributing-to-kde-simple-localization/
tags:
  - Planet KDE
  - Translation
---
Sup beloved readers!

I'm kinda sick this week as well, but this time it's not so bad as to hinder me from writing this post. Moreover, this post is way more simple, straightforward and less tiring to write.

This is a series of blog posts explaining different ways to contribute to KDE in an easy-to-digest manner. This series is supposed to run parallel to my [keyboard shortcuts analysis][1] so that there can be content being published (hopefully) every week.

The purpose of this series originated from how I feel about asking users to contribute back to KDE. I firmly believe that showing users how contributing is easier than they think is more effective than simply calling them out and directing them to the correct resources; especially if, like me, said user suffers from anxiety or does not believe they are up to the task, in spite of their desire to help back.

Last time I explained the [whole process of contributing with Localization to KDE][2]. I've also posted [a little article I made some time ago on how to create a live-test environment to translate Scribus more easily][3], given that [Scribus might become a KDE application][4] in the future. It's a major decision for a project to do, so please respect the project's choices regardless of the outcome, yes? It's currently one of the main projects I work on, and I'll continue to work on it regardless of such a decision anyways, especially now that I've become a language coordinator for Brazilian Portuguese for Scribus (yay!).

By the way, if you'd like to contribute to the Scribus project with translations, you can contact them over #scribus on IRC (Freenode). If you're interested in helping with its Brazilian Portuguese translation, you may contact me directly through Telegram, I'm usually in the [KDE Brazilian Portuguese Localization group][5]; this way you'll also get in contact with the localization team. In addition, you may also directly apply to join the team over [Transifex][6].

Today's post will essentially describe how quick and easy it is to work with localization for KDE software. My latest post might have sounded intimidating or people might have gotten tired from reading it in the middle, which is a shame; hence the reason for this post.

Oh, existing translators should also have a reason to read this post, as I'll be showing brand new functionality in Lokalize too.

As a brief note, I'm currently using openSUSE Krypton with Plasma from master, meaning it's as updated as possible. I'm also using the XWayland session, because it's dope af. It doesn't affect my workflow at all, either.

But well, let's keep it short and begin.

## Preparations

Let's first go to our localization folder. For those who didn't read my previous post, it's the folder containing the stable and trunk branches with strings to translate. You can find them through here:

  * <https://websvn.kde.org/trunk/l10n-support/>
  * <https://websvn.kde.org/trunk/l10n-kf5/>
  * <https://websvn.kde.org/branches/stable/l10n-kf5/>

In the case of Brazilian Portuguese, we have [a small script][7] that sets everything up for you, regardless of whether you have developer rights or not.


{{< figure src="/2020/02/lok1.png" caption="Figure 1 – My main localization folder, with terminal about to execute script." >}}

After executing the script and since I have developer rights, I can simply type my password and download everything I need, with all files perfectly set up in their right place. For those who don't have such rights, it will just download everything for you without requesting a password.

It takes a while since there are a lot of files to download.

(For those interested, this essentially does the same as the following script:

```bash
#!/bin/bash
svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/pt_BR/
svn co svn://anonsvn.kde.org/home/kde/branches/stable/l10n-kf5/pt_BR/
```

Just replace the pt_BR with your respective language code.

If you already have developer rights to upload your translations, you should follow your localization team's instructions to set up your system. After all, by now you should already have contacted them, since you need the approval of an experienced translator to request such rights. This post is to be succinct, and as such I'll not explain the full procedure here.

Another way we can download everything and keep stuff up-to-date is by using `kdesvn`, and it's my preferred method when I just want to translate one file. You can install it from your repository. I love this tool. Take a look on this functionality that is provided by kdesvn:


{{< figure src="/2020/02/lok2-1.png" caption="Figure 2 – Install kdesvn and get two new options in your context menu: Atualizar = Update, Enviar = Send." >}}

Once you get to the folder containing the files you'd like to update, you can simply right-click the folder and select Update (kdesvn). This will update everything for you. The new dialog that appears should look similar to this:


{{< figure src="/2020/02/lok3.png" caption="Figure 3 – Update complete." >}}

After that, just open the file you're gonna work with. In my case, I'll be using Lokalize to translate www_www.po, the file containing the strings for all [KDE announcements][8].

## Translate!

And now, to work. If you're acquainted with Computer Assisted Translation (CAT) tools, you'll handle Lokalize just fine. Left pane shows the strings (or segments) to translate, the upper right pane shows the source text, the one immediately below is the field to input the translated text, yadda yadda. Pretty simple.

Now, one feature to be released soon with Lokalize is the ability to search according to filenames. Let's take a look:


{{< figure src="/2020/02/lok4.png" caption="Figure 4 – Filter per filename." >}}

This is particularly useful with this exact file, www_www.po. Since each release announcement is written in a different file, it follows that searching for all strings present in a specific file should render all strings pertaining to a specific announcement, and so it is very easy to verify whether you're missing any strings. Here, I'm searching for plasma-5.17, since I want to translate all missing strings for all Plasma 5.17 announcements, namely versions [5.17.0][9], [5.17.1][10], [5.17.2][11], [5.17.3][12], [5.17.4][13], [5.17.5][14] and [5.17.90][15]. Notice how their links are written; all of them contain the string of text "plasma-5.17".

This is vital because, currently, Plasma announcements **require that 100% of the announcement be translated** before they show up on the website.

Next I'll show you a bit on my workflow.


{{< figure src="/2020/02/lok5.png" caption="Figure 5 – Needs review." >}}

Another recently added feature is the ability to zoom on of either translation pane and have the other zoom at the same rate. It's awesome! I do tend to prefer big things, usually the bigger the better, and this makes it easier for you to read too.

As mentioned in my previous post, one interesting feature of .po file localization (Gettext files) is the use of _fuzzy strings_. These are strings that are pending review by the translator, either because a previously approved string was changed or because it bears a very similar structure to another string.

As can be seen above, it's the latter's case; also notice how the translation pane has the same color as the source text. The only differences between this and a previous string are shown in the bottom left pane, Alternate Translations; that is, the presence of `\n`, which should be removed, and the fact it's `month` instead of `week`.


{{< figure src="/2020/02/lok6.png" caption="Figure 6 – After translation." >}}

We can see here an issue with the current translation. This string begins with "This release adds […]", the previous one too, but I decided to use "inclui" instead of "adiciona" as a translation for "adds".

Since I saw this inconsistency, I couldn't ignore it! You shouldn't ignore inconsistencies in translation either. I took note of it and, after having translated all the remaining segments for 5.17, I turned the filter back to default and searched for "This release adds". Then I fixed all strings containing this inconsistency issue.

And, since I now had the same word repeated, namely the verb "incluir", I changed its second occurrence to something expressing a similar idea.


{{< figure src="/2020/02/lok7.png" >}}

Figure 7 – All strings fixed. Way to go for [consistency's sake][16]!

Now, with all the strings I'm willing to work on for today fixed, and having translated all 5.17 strings, it's time send my translations!

Don't forget to save your changes, of course. 🙂

## Sending your translation!

Now for the section I wanted to show you the most.

If you don't have developer rights, you won't be able to use kdesvn to send your translations. It's quite easy regardless, though: just send the file you just edited to your localization team, and they'll upload it in your stead, keeping the information that you are the one that authored the commit.

Just open kdesvn using the menu/launcher and either open the `folder containing the file(s) you changed`, or the main `messages` folder; it should show all folders in a list afterwards. If you've worked with kdesvn before, you can just use Open recent URL:


{{< figure src="/2020/02/lok8.png" caption="Figure 8 – Opening the folder containing the translated file." >}}

It's quite straightforward: kdesvn will identify that you have downloaded the files for translation, will recognize your ssh key and, with your password, it will be able to send the translations for you.

It's quite the handy tool with a minimal and simple interface.

The next thing we see is this:


{{< figure src="/2020/02/lok9.png" caption="Figure 9 – Listing all changed files." >}}

Again, this is quite straightforward. I chose to open the www folder directly. It shows an up arrow since there's something there you can upload, namely the red file on the right. You can safely ignore most of the toolbar buttons. With the folder selected, we can click on the Send button (just above the mouse in the screenshot).

It renders this screen:


{{< figure src="/2020/02/lok10.png" caption="Figure 10 – The changelog dialog." >}}

This is also quite the easy-to-use interface. The upper left pane, "Review affected items", contains all files that have been changed and can be uploaded. In this case it's just one, but were you to have dozens of files that you wish to upload with different commits, you can easily choose which files to upload.

The bottom pane, "Define a commit message", lets you write your commit message, unsurprisingly. This is just a message that will describe the upload of the file. I usually write my commit messages with the structure "Updates to file.po for pt_BR", but I sometimes forget how I wrote my previous commit messages, and thus the "Or use a previous one" come very useful. Aside from those three things, you can safely ignore the rest and press OK.


{{< figure src="/2020/02/lok11.png" caption="Figure 11 – Translated files uploaded! Hurray!" >}}

If you see a message saying "Revision #number was sent", then you're done!

If your upload fails, then it's because you've missed something in your translation, perhaps you forgot to include a tag or you missed a plural. You just need to fix those issues and try again, and it should upload successfully.

## That's it!

No, really, that's it. It wasn't that complicated either; this post just seems longer than it really is because of the number of screenshots, really. And because I like to keep things a bit more verbose than your typical tutorial, I suppose.

To summarize, the general workflow for a translator with developer rights is as follows:

  * Go to folder with file to translate — 10 seconds;
  * Right-click and update your files — 2 seconds;
  * Wait for update to finish — 60~90 seconds;
  * Translate — varies, could take hours;
  * Open kdesvn — 2 seconds;
  * Send files — 30~50 seconds.

And all of that done through the GUI, with windows, dialogs and whatnot.

Here's [the commit referenced in this post][17].

For the last bit of this post, I’ll add a few links for those willing to learn more or contribute.

Interesting links include the [Get Involved][18] wiki page for general contributing and the [Get Involved Translation][19] wiki page for a brief explanation on where to get more information; you may find the mailing list for your language in the [links available here][20] under kde-i18n or kde-l10n; you may also contact the [general i18n team on IRC][21]; if you’re Brazilian or speak Brazilian Portuguese, we have a [Telegram group][5] for you, and you can also check out [br.kde.org][22] and the [KDE-Brasil][23] group; if you’d like to translate widgets instead, you might wanna have a look at [store.kde.org][24] and search for the github page for the widget; most things related to KDE localization can be found on [the official page][25], for instance the [Translation HOWTO][26], which is an even more comprehensive guide than this blog post; and [this online tool][27] allows you to search already translated strings in KDE software, serving as an interesting translation memory that should even help in other fields. Some localization teams might be more approachable if they have a dedicated website for your country, such as [fr.kde.org][28] for France or [kde.ru][29] for Russia. If you’d like more information about translation on Linux, you may also be interested in checking [TranslateOnLinux][30] or a specialized distribution focused on translation called [tuxtrans][31], made by an Austrian professor at the University of Innsbrück. If you’d like to know about other professional CAT tools, you may want to check the [Proz comparison tool][32]; some of the tools displayed there also run on Linux, either natively, using Java (like [OmegaT][33], an open-source CAT tool) or through the browser.

 [1]: https://rabbitictranslator.com/wordpress/index.php/category/keyboard-shortcuts-analysis/
 [2]: https://rabbitictranslator.com/wordpress/index.php/2020/01/19/contributing-to-kde-localization/
 [3]: https://rabbitictranslator.com/wordpress/index.php/2020/02/17/live-test-for-translating-scribus/
 [4]: https://phabricator.kde.org/T10034
 [5]: https://t.me/kdel10nptbr
 [6]: https://www.transifex.com/scribus/public/
 [7]: https://websvn.kde.org/trunk/l10n-support/pt_BR/kde-pt_br-5.5.sh?view=log
 [8]: https://kde.org/announcements/
 [9]: https://kde.org/announcements/plasma-5.17.0.php
 [10]: https://kde.org/announcements/plasma-5.17.1.php
 [11]: https://kde.org/announcements/plasma-5.17.2.php
 [12]: https://kde.org/announcements/plasma-5.17.3.php
 [13]: https://kde.org/announcements/plasma-5.17.4.php
 [14]: https://kde.org/announcements/plasma-5.17.5.php
 [15]: https://kde.org/announcements/plasma-5.17.90.php
 [16]: https://phabricator.kde.org/T11093
 [17]: https://phabricator.kde.org/R883:1562817
 [18]: https://community.kde.org/Get_Involved
 [19]: https://community.kde.org/Get_Involved/translation
 [20]: https://mail.kde.org/mailman/listinfo/
 [21]: irc://irc.kde.org/kde-i18n
 [22]: https://br.kde.org/
 [23]: https://t.me/kdebrasil
 [24]: http://store.kde.org
 [25]: https://l10n.kde.org/
 [26]: https://l10n.kde.org/docs/translation-howto/
 [27]: https://l10n.kde.org/dictionary/search-translations.php
 [28]: http://kde.fr
 [29]: http://kde.ru
 [30]: https://translateonlinux.org/
 [31]: https://www.uibk.ac.at/tuxtrans/
 [32]: https://www.proz.com/software-comparison-tool/cat/cat_tools/2
 [33]: https://omegat.org/
