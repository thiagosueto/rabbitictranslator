---
title: How to translate LaTeX files by conversion to ODT/DOCX
author: Blumen Herzenschein
date: 2020-04-05
url: translate-latex
aliases: /wordpress/index.php/2020/04/05/translate-latex-by-conversion/
tags:
  - Translation
---
Today I'll be doing a translation-related post instead of a traditional linux-related post as I was faced with an interesting challenge recently.

In addition to translating academic articles from Brazilian Portuguese into English, I proofread academic articles in English. On occasion, I receive articles written in [LaTeX][1], a tool used to typeset documents in an automated manner such that the author gets to be in full control of the document layout. Generally, such a tool allows the author to bypass the need to pay for an editor/publisher to make the document compliant with a specific journal and gives the author complete power on how the document should look like. Most notably, articles written in LaTeX are done by engineers and linguists, although I've received articles concerning social sciences, chemistry and software development.

LaTeX is extremely convenient for authors and journals, but not so much for third parties such as translators. Firstly because the resulting document is compiled as PDF, which is a file format specifically designed **without editing in mind**. Meaning that, by design, editing a PDF file goes against its original purpose, and precisely that is the purpose of proofreading/translating it. Secondly because the source files, .tex, include a kind of "code" that must both be taken into account by the language specialist as well as be largely ignored. Lastly, because no LaTeX typesetting editors include translation/proofreading tools.

I'll talk a bit about those three matters, but for those who just want to learn the procedure I found out for translating LaTeX files, you can skip [by clicking here][2].

## The matter of PDF

PDF files may come in two different ways: either they are proper PDF files, or they are not.

Proper PDF files made from the export of an editable file can have their text selected in a PDF reader, for instance. In such cases, text and images are elements different from the page itself, which is why they are selectable and, depending on the tool used, exportable.

In the case of files created in unconventional ways, typically by scanners, things are not so pretty. Any visual elements in the PDF are embedded to the page; they are not aligned; they are not selectable nor exportable. It is, in essence, an image with no layers. This is the most annoying kind of PDF.

One possible way of proofreading a PDF file is to use a PDF reader that has annotation capabilities, such as Adobe Acrobat or [Okular][3]. It can be insufficient, however, as it inherently requires a lot of mouse interaction to do so, and as is well-known, switching between the mouse and the keyboard too often is slow and inefficient. So much so that proofreading a .tex file may be more convenient still.

On the other hand, there are some ways in which translation agencies may provide translation services of PDF files.

One of them is, well, requesting an editable file to the client. In other words, not handling PDF files at all, as they are a pain.

The second is using a tool (or service) able to export the text of a PDF file, have that text translated, and then reimported into the PDF. This is specially annoying because it is a highly specialized technique, hard to develop and maintain. Currently, only two tools allow for such: [Infix PDF][4], which is developed by Iceni, and [FlexiPDF][5], developed by Softmaker.

Iceni did this way before Softmaker as far as I know. As such, after having sold their product for several years and being essentially the only ones to offer such a product, they changed their sales model to be Software as a Service ([SaaS][6]) **only**. Meaning that their previous functionality of exporting PDF text to translation was turned into a service, named [TransPDF][7]. While Infix continues to be sold individually and continues to have this functionality, it is sold only under a license model now. [Memsource][8], a professional CAT tool, offers TransPDF as a service to allow the translator to handle PDF files (with a cost, of course), so I imagine other tools/services do the same.

Softmaker did things a bit differently: in addition to providing a SaaS license for FlexiPDF, they also sell it as a tool, meaning you can just buy it once and the product is effectively yours, without future removal of such translation functionality. This is, as I see it, much more effective for acquiring loyal customers, and between both business practices, I prefer it.

I've read somewhere that InfixPDF runs fine on WINE, and I tested FlexiPDF on WINE myself, it runs fine too, including the translation functionality. Softmaker at least did provide a [sensible reason][9] as to why there isn't a linux native version of FlexiPDF (yet).

The third way a translation company may provide the translation of PDF files is by converting PDF files into a more manageable format, such as .docx. This typically renders a poorer layout than the original, but it costs the less. It is also what [Matecat][10], another CAT tool, typically does; upload a PDF document and the final result of your translation should be a DOCX file. Converted PDF files do, usually, result in weird formatting, which translates into a ton of tags during translation and weird line segments, which can be a terrible hassle.

Neither this nor using an external tool are 100% effective, although the second method may render a better result. And neither is effective for non-proper PDFs; those typically require Optical Character Recognition ([OCR][11]) technology. For OCR there are numerous third-party tools with differing results, but generally speaking, none will be close to ideal. Non-proper PDF files should be avoided wherever possible by all means, as any other method is preferable.

## The matter of LaTeX "source code"

This is a really simple matter in fact. Let's take a look at a [sample LaTeX file][12] provided by Clark University (one of the first results on DDG):

```latex
\section{Introduction}
\TeX\ looks more difficult than it is. It is
almost as easy as $\pi$. See how easy it is to make special
symbols such as $\alpha$,
$\beta$, $\gamma$,
$\delta$, $\sin x$, $\hbar$, $\lambda$, $\ldots$ We also can make
subscripts
$A_{x}$, $A_{xy}$ and superscripts, $e^x$, $e^{x^2}$, and
$e^{a^b}$. We will use \LaTeX, which is based on \TeX\ and has
many higher-level commands (macros) for formatting, making
tables, etc. More information can be found in Ref.~\cite{latex}.
```

The first thing to be addressed is how the text is rather polluted. There are much more elements that should be paid attention to compared to a typical document translation. The same is valid for proofreading, which is typically addressed by using a proper LaTeX editor, such as [Kile][13], for instance, as such editors include proper text highlighting and the use of different colors to distinguish commands from text.

The command `\section{}` is used to name an academic article section, which is used to automatically generate the table of contents. As this is a command that follows `\`, it must not be translated, otherwise the command will not be recognized. This is the same as `$\alpha$`, for instance. However, the text within brackets `{}` should be translated, as well as all text not involving any commands. The translator should also be wary of the position of commands. I'll take a piece of this sample and change it a bit just to exemplify:

```latex
In order for this document to be compliant with
\href{Elsevier guidelines}{https://www.elsevier.com/authors/author-schemas/latex-instructions},
we used \LaTeX, which is based on \TeX\ and has many higher-level commands (macros) for formatting, making tables, etc.
```

Which in Brazilian Portuguese could be:

```latex
O \LaTeX, que é baseado no \TeX\ e inclui vários comandos de alto nível (macros) para formatação,
criação de tabelas etc. foi usado para deixar este documento em conformidade às
\href{diretrizes da Elsevier}{https://www.elsevier.com/authors/author-schemas/latex-instructions}.
```

Note how the position of the `\href` command was shifted from the latest position to the first. It could have been translated in another order, of course, but this is just an example. The matter of positioning is rather similar to how it is done with localization, but this is not something that every translator is aware of, hence why I mentioned this here.

## The matter of tooling

Another major issue is the aforementioned lack of proofreading/translation tools. For instance, for proofreading, there is no Track Changes, no Word Counting, no Split, no Highlight, and for translation, only a few CAT tools can translate such files, like [OmegaT][14].

The translation assignment that originated this blog post was rather urgent, and so I wasn't able to learn its workflow in time (the last time I've used OmegaT was a few years ago). I strongly recommend OmegaT however if you'd like an open source, free CAT tool that ensures that a strict professional-client confidentiality clause is possible.

For comparison, typically CAT tools handle ODT or DOCX/DOC, rendering minimum layout issues. This is the most common use case, which explains why it is so rare to find a CAT tool for handling LaTeX files, and also explains why I found the motivation to do such a post.

There isn't a "native" way of translating LaTeX files as far as I know, but one major advantage that LaTeX files have over PDF files is the fact they are compiled. Meaning that one can change what output format can be different from PDF, which also means other, more manageable formats, can be converted.

## The method {#method}

I saw three possible formats for managing LaTeX files such that I could translate them without using a specialized third-party tool like InfixPDF or FlexiPDF: plain text (TXT), RTF and HTML.

The first is easy to do with a straightforward command: `detex file > file.txt`.

This is particularly useful for counting words, however, it may not be as suitable for the task of translation. For instance, the document I was translating included several equations and references to previous equations, in addition to there being several commands used to represent symbols. In plain text, this effectively means an empty space is left where there once was a symbol or graphical depiction of an equation. This is not desirable since this may be a hassle for including the text back to the LaTeX file as there is no context whatsoever of which equation/symbol goes where.

The second method is done with an easy command too: `latex2rtf file`.

This was quite close to what I wanted for translation, but not quite yet. Both RTF and TXT display empty spaces for equations, but RTF will keep any mentions to bibliography by using its short name, it will try to use hyperlinks for any in-text references, but whenever a command is not recognized it will display a message such as `[Sorry. Ignored \begin{aligned} ... \end{aligned}]`. Unicode-enabled characters such as `Ω` do appear, though, which is nice.

The third method and the one which I recommend involves the use of `latex2html`.

Converting a LaTeX file to HTML with just this command would render an index.html file and one HTML file for each section, which is not what we want, hence we need to add the `-split 0` option. Thus, the command would be `latex2html -split 0`.

It still looks like a webpage instead of a document, however. There's also a section named "About this document" at the end of the resulting file. As such, for removing any HTML-explicit elements from it, let's add the following flag: `-nonavigation -noinfo`.

So the final command should be `latex2html -split 0 -nonavigation -noinfo file.tex`. Instead of doing this from the command line, however, you can simply add these parameters to your LaTeX editor of choice. What I did was simply add those flags to the "LaTeX to Web" option from Kile and everytime I set it to compile with LaTeX to Web, it ended up with an HTML file properly configured as I wanted.

After this, the resulting file can then be opened with an office suite utility such as [LibreOffice][15] and saved as ODT, for instance, which can then be used for your usual translation in any CAT tool.

The most astonishing thing I noticed about this method was that after uploading it to a CAT tool, it was, essentially, perfect. Tags were present only whenever there was a symbol or an image and when there was any sort of formatting, which made translation a breeze. Never were there excessive tags such as when translating a PDF converted to DOCX/ODT. I attribute this to the fact that both are open standards based on XML, namely [OOXML][16] and [ODF][17], and so their similarity allows LibreOffice to properly convert HTML formatting into those two. While PDF is also an open standard, it was designed not to be changed, as mentioned before, so it's harder to convert.

The only issue I faced with `latex2html` is that it ended up turning equations into quite huge images. When viewing the HTML file from the browser, it renders them huge; when viewing from LibreOffice, it renders big occasionally.

While doing some extra bit of research for this blog post, I found out about `hevea`, a tool similar to `latex2html`. It did not result in huge pictures and equations when viewing it from the browser, but that's because it actually rendered equations as text, and properly! However perfect this was for the browser, it was an issue when viewing from other tools. LibreOffice didn't manage to read all of those characters (although adding the flag `-textsymbols` did help), and after uploading the file to a CAT tool it was incredibly full of tags, in addition to the character gibberish found where an equation was supposed to be.

This is an interesting case where a tool which didn't render the best for browser viewing (`latex2html`) had the best results for translation, whereas a tool which provided the best results for browser viewing was worse to translate (`hevea`).

There are also [some other tools for conversion displayed on the official LaTeX wikibook][18], but I haven't tested those yet. Regardless, I believe the use of `latex2html` presented here should be a sufficient, reproducible method for translating documents written using LaTeX, and that such information should be useful to translators who work with this kind of file. This method should also be useful for proofreading, as this allows the use of word processors with Track Changes functionality.

 [1]: https://www.latex-project.org/about/
 [2]: #method
 [3]: https://kde.org/applications/graphics/org.kde.okular
 [4]: https://www.iceni.com/infix.htm
 [5]: https://www.softmaker.com/en/flexipdf
 [6]: https://en.wikipedia.org/wiki/Software_as_a_service
 [7]: https://www.iceni.com/transpdf.htm
 [8]: https://www.proz.com/software-comparison-tool/tool/memsource/40
 [9]: https://forum.softmaker.com/viewtopic.php?f=330&t=18828&p=62179&hilit=flexipdf+linux#p62179
 [10]: https://www.proz.com/software-comparison-tool/tool/matecat/134
 [11]: https://en.wikipedia.org/wiki/Optical_character_recognition
 [12]: http://physics.clarku.edu/sip/tutorials/TeX/intro.html
 [13]: https://kde.org/applications/office/org.kde.kile
 [14]: https://omegat.org/
 [15]: https://www.libreoffice.org/
 [16]: https://en.wikipedia.org/wiki/Office_Open_XML
 [17]: https://en.wikipedia.org/wiki/OpenDocument
 [18]: https://en.wikibooks.org/wiki/LaTeX/Export_To_Other_Formats
