---
title: 'Fluff: Linux sessions are neat'
author: Blumen Herzenschein
date: 2020-07-04
url: /index.php/2020/07/04/fluff-linux-sessions/
tags:
  - Fluff
---
For some time I've known about the ability to create login sessions on Linux, but hadn't meddled with it until recently. It's fairly easy to create them too. It basically consists of creating an executable script and putting it inside `/usr/bin` or another place within `$PATH`, then creating a `.desktop` file for executing that script inside `/usr/share/xsessions/`, or if you're willing to bind a compositor to the application, `/usr/share/wayland-sessions/`.

For instance, running Windows on a VirtualBox VM by means of an xsession is as easy as creating a script named `win10-session` like this:

```bash
#!/bin/bash
nmcli connection up MyNetworkName
/usr/lib/virtualbox/VirtualBoxVM --startvm 'Name of my Windows VM'
```

Then creating a minimal `win10-session.desktop` file like this:

```ini
[Desktop Entry]
Name=Windows 10
Exec=/usr/bin/win10-session
```

The `nmcli` command assumes that you've already connected to a network connection before and it is necessary for the VM to be able to connect to the internet, and new VirtualBox versions recommend you to use their `VirtualBoxVM` executable to start VMs. And by recommend, I mean they hinder you from using the usual command `VirtualBox`. Additionally, this is for a previously configured Windows VM which includes a shared folder, VirtualBox Guest Additions, and a preconfigured resolution for each screen so that it uses the number of monitors you have accordingly and seamlessly.

Naturally, you're free to use any other networking utility, like `ip` or even `ifconfig`, or another virtual machine management software like VMware or virt-manager.

This is such a convenient thing, but I barely ever see this being mentioned over the internet. With such a configuration you can simply logout of your Linux session and login directly to a VM without loading the WM/DE, which reduces overhead significantly. You also enjoy advantages of VMs such as snapshots, so if e.g. your Windows VM breaks, you can immediately restore it, and unlike dualbooting scenarios, Windows will be unable to break your boot by writing things to the MBR. If you shutdown your VM, you will automatically logout of your session, which makes the entire experience quite natural while not being tedious like that of dualbooting. And of course, you can assign some more RAM to your VM since you're deliberately not using anything other than the VM in your session.

Creating a login session is also effective for setting mini-kiosks: for instance, you may ~~create a Steam session running on Big Picture for those times you want to play games with your controller without distractions,~~ {{< super 1 >}} run a minimalistic WM like `openbox` with a preconfigured set of applications and environment, or open Firefox in app mode with the kinda recent `--ssb` option (you also need `browser.ssb.enabled` in your `about:config` to be set as `true`). Additionally, xsessions/wayland-sessions are created for your user if you've previously configured KDE's main utility for easily installing KDE apps from source, [kdesrc-build][1]. It's an interesting tool and checking how it works teaches a lot about building software in general.

{{< super "\[1\]" >}} I tested running Steam standalone and things are more complicated than that. You need a WM/compositor to handle input properly, similar to what has been tried with [steamos-ubuntu][2] or by running SteamOS itself.

I specifically mentioned the use of a Windows VM for three reasons: it's convenient for small to medium companies where machines are lacking in power and Windows-exclusive or difficult-to-set-up tools are required; I am going to need Windows and Microsoft Office to learn (and maybe find a way to improve) the typical workflow for applying the [JATS][3]-compliant [Scielo Publishing Schema][4] to Brazilian academic articles and see how it integrates to [OJS][5] and [SEER][6], and I intend to document it; I've been meaning to continue my [Keyboard Shortcuts Analysis][7], which I've put on hold for a long time for lack of motivation (it's quite tiring to check all keyboard shortcuts in a system and figure out why they make or do not make sense). The respective blog posts for those are already being drafted, though I do not know exactly when I'll publish them. So you can at least expect the following three posts: my next [Contributing to KDE is easier than you think][8] which should be about fixing the wikis; a post on my experience learning academic publishing in XML; and an analysis of Windows 10 keyboard shortcuts. Not necessarily in that order.

 [1]: https://community.kde.org/Get_Involved/development#Plasma_Desktop
 [2]: https://github.com/ShadowApex/steamos-ubuntu
 [3]: https://jats.nlm.nih.gov/
 [4]: https://scielo.readthedocs.io/projects/scielo-publishing-schema/pt_BR/latest/
 [5]: https://openjournalsystems.com/
 [6]: http://www.ibict.br/tecnologias-para-informacao/seer
 [7]: https://rabbitictranslator.com/wordpress/index.php/category/keyboard-shortcuts-analysis/
 [8]: https://rabbitictranslator.com/wordpress/index.php/category/contribute-to-kde/
