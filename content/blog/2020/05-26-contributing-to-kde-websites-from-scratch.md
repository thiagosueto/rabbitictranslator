---
title: Contributing to KDE is easier than you think – Websites from scratch
author: Blumen Herzenschein
date: 2020-05-26
url: kde-websites
aliases: /wordpress/index.php/2020/05/26/contributing-to-kde-websites-from-scratch/
tags:
  - Planet KDE
---
Sup y'all.

This is a series of blog posts explaining different ways to contribute to KDE in an easy-to-digest manner. The purpose of this series originated from how I feel about asking users to contribute back to KDE. I firmly believe that showing users how contributing is easier than they think is more effective than simply calling them out and directing them to the correct resources; especially if, like me, said user suffers from anxiety or does not believe they are up to the task, in spite of their desire to help back.

Last time I talked about websites, I taught how to [port current KDE websites to Markdown][1], and this led to a considerable influx of contributors, since it required very little technical knowledge. This blog post however is directed to people who are minimally acquainted with git, html/css, and Markdown. We will be learning a bit of how Jekyll and scss work too.

This post has now been copied over to <https://community.kde.org/KDE.org/Jekyll>! Which means the content available on the wiki can serve as a full tutorial explaining how to prepare KDE application websites from scratch using Jekyll. This should in theory enable you to at the very least set up a Jekyll environment, apply content to it, and insert some css in it for a bit of customization!

Please be wary that not every project necessarily wants a website of their own, and so you need to contact the website maintainers either directly or by contacting the KDE Web team over [Telegram][2], [Matrix][3] or [IRC][4]!

As usual, if you want to skip the explanations and start immediately, you can click [here][5].

## Preparations

The first thing we'll need to do is install the programs required to create the website. Just install: `ruby-dev bundler git` using your distribution package manager, and we're set to go!

Let's download the KDE repository containing the original KDE Jekyll theme. This way, we can both explore the repo and obtain the examples folder. Run the following command:

```bash
git clone https://invent.kde.org/websites/jekyll-kde-theme.git
```

Inside, there are mainly three folders we should take a look at: `/css`, `/_sass`, and `/examples`.

Inside `/_sass`, there are several `/.scss` files which should be the foundation of the KDE theme. If you already have some experience with these files, you should know that they can be imported to any website that uses the theme by adding an `@import` inside a `/main.scss`.

Inside `/css`, the file `/main.scss` is located. Here you can see that the various `.scss` files you previously saw in the `/_sass` folder are being imported into the project. Any extra CSS shoud be contained either within `main.scss` or any `.scss` file you create within this folder. 

If you check the `/examples` folder, you can see two folders: `/application` and `/application-components`. The first is a general example for building any application website; the latter is useful for bundled applications, as is the case of [kontact.kde.org][6]. For the purpose of this post, your main concern should be copying the `/application` folder to another place, more fitting for work.

In my case, for instance, I was planning to create the website for Subtitle Composer, so I put the folder inside my home folder. I then renamed it to `/subtitlecomposer-kde-org`, so as to follow the pattern found over KDE's Gitlab instance, [Invent][7].

## How stuff works

With this, we now have an example website to improve. Let's take a look at what you will change to make it a proper website for a KDE application:

{{< figure src="/2020/05/Screenshots-0522_0043.png" >}}

First of all, there are the folders `/assets/img` and `/css`. `/assets/img` is pretty straightforward, it's where you'll put any media you want to display on the website that's not displayed by the theme. The already present `app_icon.png` is what will be displayed on the upper left of the website, `screenshot.png` will be used to display the application screenshot inside the main example carousel.

As already mentioned, the `/css` folder is where you should store any `.scss` files you'd like to create, in addition to `main.scss`. Inside it, you should already see `home.scss` and `download.scss`. The absolute minimum required should be `home.scss`, though.

Your example website should also have a `.gitignore`. This is important since Jekyll generates extra folders and files when compiled, and we don't want them to be unnecessarily uploaded to our future repository when we push it to Invent.

The two main configuration files you should pay attention to are `Gemfile` and `_config.yml`. `Gemfile` is what determines the use of Jekyll and the KDE Jekyll theme, it should have something like:

```
source "https://rubygems.org"
ruby RUBY_VERSION
gem "jekyll", "3.8"
gem "jekyll-kde-theme", path: '../../'
```

You should always change the path of the jekyll-kde-theme to git, it's the main way the theme is distributed for all KDE application websites to receive fixes as soon as possible. You also need it if you want to use a dark theme on the website. You need to switch that line to include the following:

`gem "jekyll-kde-theme", :git => 'https://invent.kde.org/websites/jekyll-kde-theme.git'`

As for the `_config.yaml`, it's where most metadata for the website will be stored, and it's also what determines the website's structure:

{{< figure src="/2020/05/Screenshots-0518_0104.png" caption="Figure 1 - Default _config.yaml example." >}}

You should change the appropriate information there by checking the previous website for the application you're designing it for, or by contacting the devs directly if they don't already have a website.

I'll elaborate a bit more on the role of this file on the structure of the website later.

After reading the config files, the first file we should take a look at should be `index.html`.

{{< figure src="/2020/05/Screenshots-0519_2142.png" caption="Figure 2 - Default index.html example." >}}

The section within `---` is a bit of metadata determining what should be included on the website, it is called **Front Matter**. It should be present regardless of whether you'll be working on an HTML or Markdown file.

Then, you can see a section containing a carousel with only one image. You may have noticed that `kHeader` was also present within the KDE Jekyll `/_sass/home.scss` file. 

That's what's convenient in this kind of setup: you can use resources already available by the theme by just calling them. If you're already acquainted with those resources, it becomes quite easy to create multiple reproducible builds with them. We will be using a few of those later on.

This is how the `index.html` looks like by default; Konsole is used as a template website in this case.

{{< figure src="/2020/05/Screenshots-0519_1650.png" caption="Figure 3 - Default appearance of example website." >}}

Carousels are [generally a bad idea][8] unless they are properly implemented, thus the carousel doesn't come with more than one image by default. However, it is the website maintainer's choice as to whether it should be used or not.

The line we see there containing `{% include blog.html %}` serves to insert `blog.html` in the contents of `index.html`. `blog.html`, in turn, pulls the Markdown files inside your `/_posts` folder. Thus, the result:

{{< figure src="/2020/05/Screenshots-0519_1659.png" caption="Figure 4 - Default blog element. Shown using the dark theme so as to not f***ing blind you in my dark theme blog. The following images should also follow a dark theme for readability." >}}

Now, let's take a look at the `get-involved.md` file now, which should be easier to understand.

{{< figure src="/2020/05/Screenshots-0519_1647.png" caption="Figure 5 - Default get-involved.md example." >}}

Writing in Markdown is particularly easy, and Jekyll interprets each part of Markdown as HTML code, so if you want to add a specific style to something written in Markdown, you'd use its HTML equivalent, so in `## Text for a header`, `##` would be considered an `<h2>` tag.

{{< figure src="/2020/05/Screenshots-0519_1701.png" caption="Figure 6 - How default get-involved.md looks like." >}}

The `konqi: /assets/img/konqi-dev.png` does not pull from the `/assets/img` folder from your local project, but rather from the [Jekyll KDE theme itself][9]. The theme also provides for its layout on the page, so adding this simple line in your Front Matter will show this adorable Konqi icon properly. If any extra Konqi icon is required, they can be added manually to your local installation and called through a `konqi:` line.

One particular thing that is shown twice in this image is the use of variables. With `{{ site.somedataIwanttoshow }}` you can call any information present in your `_config.yaml`. Image XXXX shows two examples: `{{ site.title}}` and `{{ site.email }}`, which display on Image YYYY as `Konsole` and `konsole-devel@kde.org`, respectively.

## Get on with it! {#getonwithit}

[Alright, alright][10]. Let's build the website already.

So what you previously did if you read the previous section was installing ruby-dev, bundler and git, right? It should be noted that you need Ruby 2.6 as of now. If you only have Ruby 2.7, you'll face an error in which a gem named eventmachine won't install. I'll provide you with the respective commands for Ubuntu/Debian, openSUSE and Arch/Manjaro:

```
Ubuntu: sudo apt install ruby-dev bundler git
openSUSE: sudo zypper install ruby-devel ruby2.6-devel bundler git
Arch: sudo pacman -S ruby ruby2.6 ruby-bundler git
```

After that, you likely already cloned the KDE Jekyll repository by using the command `git clone https://invent.kde.org/websites/jekyll-kde-theme.git` and copied the `/examples/application` folder elsewhere, renaming the folder to something like `/myproject-kde-org`, right?

Next you'll want to change your directory (`cd`) to the project's top directory. If it is located in your home folder, you should `cd myproject-kde-org`.

Already inside, run `gem install jekyll --user-install`. This will run Ruby's default dependency manager, `gem`, which will install the `jekyll` gem with user privileges, that is, inside your home directory. This is convenient because then you don't fill your system with gems, and you can install different gems according to the project (not like you'll need to if you're just planning on creating one website).

After the command finishes downloading the gems, run `bundle config set path 'vendor/bundle'` and afterwards `bundle install`. The first command determines where to store the gems defined in your `Gemfile`, namely `jekyll` and `jekyll-kde-theme`. The command `bundle install` subsequently installs them. The later takes a bit to finish.

That's basically it. With that set up, we can generate our website with `bundle exec jekyll serve`. You'll see output like this:

{{< figure src="/2020/05/Screenshots-0519_1742.png" caption="Figure 7 - Serving Jekyll website on localhost at default port 4000." >}}

As you can see, the website should be available by opening a browser and typing `127.0.0.1:4000`. If you're working on a server without a GUI interface, get your ip with `ip a` and use it with the command:

`bundle exec jekyll serve --host my.ip.shouldbe.here --port anyportIwant`

Then access it through another machine on your LAN—this includes your phone, which is convenient when you're testing your website on mobile.

One nicety about Jekyll is the functionality of `auto-regeneration`. With this properly running, every change you do to a file on your website will show up automatically on it without requiring a "server restart", so to speak. With this, you can keep the website running on your side monitor (if you have any) while you change files on your main monitor.

If you get an error about [Duplicate Directories][11], just ignore it. This occurs because of a symlink in `/examples/application-components` which is quite convenient, and this error does not hinder you from generating your website. If you reeeeeeally don't want your beautiful terminal output to be plagued by an error message, though, you can just remove the `/vendor/bundle/ruby2.6/blablabla/_changelogs` and `/vendor/bundle/ruby2.6/blablabla/_posts` folders which are shown in the error output.

## A practical example: Subtitle Composer

Now I'll show you some ways I customized and worked on the Subtitle Composer website, the website that allowed me to learn how to handle Jekyll.

The first thing I noticed was the following line used to add a screenshot to the carousel:

```html
<div class="slide-background" style="background-image:url(/assets/img/screenshot.png)"></div>
```

This means I need to add `background-image:url(/path/to/image/either/local/or/from/theme.png)` whenever I want to have an element link to an image. I prefer to do so in scss since pure Markdown doesn't allow to apply styling directly, unlike HTML, so everything stays in the same place.

I could naturally switch the `screenshot.png` file in my `/assets/img` folder in order to switch the screenshot, which is what I did; but what about the wallpaper? I have nothing against Kokkini; however, I think keeping a more up-to-date wallpaper makes more sense for a new website. Well, in all honesty, I also wanted to do this because I particularly like the newest wallpapers. 😀

Apparently the wallpaper was showing up despite not existing in my local `/assets/img` folder, therefore it was being provided by the theme by default.

Right clicking on the wallpaper and selecting Inspect element immediately shows the css element `div.carousel-item-content`. However, after some testing, the actual element that needs to be changed is `#kHeader`. Therefore, on my `main.scss` file, I added `#kHeader {background-image:url(/assets/img/wallpaper.png)`. This managed to override the wallpaper provided by the KDE Jekyll theme, switching Kokkini for Next.

The next thing I wanted to do was to add a subtitle effect on the name of Subtitle Composer; it seemed like an awesome thing to do, but I had no idea how to do that (I'm not very experienced with web design at all! I actually took way too much time to learn all of this), and so I searched for it in the internet.

The guess that got me in the right direction was thinking that I'd need a thin border around text to convey an effect of subtitle, thus I searched for "css text 1px border", which led me to this StackOverflow question: [CSS Font Border?][12], with the following code: `h1 {color:yellow; text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;}`.

So I included a class named `subtitleheader` to my original `<h1>Subtitle Composer</h1>`, allowing me to add `.subtitleheader {font-size:30px;color:yellow; text-shadow:-3px 0 #4d4d4d, 0 3px #4d4d4d, 3px 0 #4d4d4d, 0 -3px #4d4d4d;font-weight:bold;min-width:130%}` to `main.scss` after a ton of experimenting until I achieved the desired result. I added similar code to the paragraph describing Subtitle Composer, namely `.subtitleparagraph {font-size:20px;color:#eff0f1; text-shadow:-2px 0 #4d4d4d, 0 2px #4d4d4d, 2px 0 #4d4d4d, 0 -2px #4d4d4d;font-weight:bold;min-width:130%}`, and so I arrived at this:

{{< figure src="/2020/05/Screenshots-0519_2347.png" caption="Figure 8 - Subtitle effect." >}}

So now I need to change the top navigation bar to fit what I needed.

{{< figure src="/2020/05/Screenshots-0520_0006.png" caption="Figure 9 - Top navigation bar on browser." >}}

For that, I need to learn how to change some stuff on `_config.yaml`. Let's take a look at the code:

{{< figure src="/2020/05/Screenshots-0520_0012.png" caption="Figure 10 - Top navigation bar on _config.yaml." >}}

This is quite straightforward. A navigation bar can be on the top or bottom section, and each `- title:` represents an element in the navigation bar, and each element can work as a list if you add a `subnav`. Despite the `url:` pointing to an html file, markdown files can be used too without specifying the .md file format.

I changed it to look like so:

{{< figure src="/2020/05/Screenshots-0520_0115.png" caption="Figure 11 - Changed top navigation bar." >}}

Which is way simpler than the default and fits the content I could find about Subtitle Composer that was available both on the [Invent project][13] and its [old Github][14].

I also copy-pasted the bottom navigation bar from the [KDE Connect website][15] so it gets quite complete on the bottom of the website too.

{{< figure src="/2020/05/Screenshots-0520_0147.png" caption="Figure 12 - Complete bottom navigation bar." >}}

After that I could work on inserting the content inside the respective .md files.

Let's take a look at `features.md` and `get-involved.md` now.

{{< figure src="/2020/05/Screenshots-0520_0125.png" caption="Figure 13 - Subtitle Composer features.md." >}}

{{< figure src="/2020/05/Screenshots-0520_0123.png" caption="Figure 14 - Subtitle Composer get-involved.md." >}}

Anyone acquainted with HTML and Markdown is likely to know that `---` is the equivalent to `<hr>`, it's an horizontal ruler, that is, it creates a straight horizontal line, which in this case acts sort of like a separator. I used this together with an HTML line break `
` so the header gets more noticeable.

Also note that using `---` after the Front Matter will be interpreted by Jekyll as normal Markdown.

The reason I wanted to show this was to exemplify my previous comment about Markdown being interpreted as HTML: if I add a style to the element `hr` in `main.scss`, it will work for both the HTML and Markdown notations. So, by adding `hr {border-color:#7f8c8d}` when using a dark theme, the result should be something like this:

{{< figure src="/2020/05/Screenshots-0520_0135-1.png" caption="Figure 15 - Horizontal ruler with light border." >}}

Well, talking about dark themes, what the heck do I need to do to add a dark theme? I already knew `prefers-color-scheme` was used for that, but I had no idea how it worked. The [Mozilla page][16] explains it very well though, you just need to add any style to:

`@media (prefers-color-scheme: dark) { any.style {should-be:here;} }`

Since the `hr` element needed to be light only when using a dark theme, and should be standard black everywhere else, I used it inside `prefers-color-scheme: dark`. So the result was this:

`@media (prefers-color-scheme: dark) { hr {border-color:#7f8c8d} }`

If you want to test `prefers-color-scheme` on your browser without changing your local theme, on Firefox you can go to `about:config` and create a config named `ui.systemUsesDarkTheme` with a number value of `0` or `1`, and on Chrome you can open the Developer console, click on the top right hamburger menu, go to More tools, Rendering, and change `Emulate CSS media feature prefers-color-scheme`.

I made a few more customizations to the website, as you can check out [here][17]. However, the examples I showed are not only enough for anyone to start hacking, but they are also proof of how I managed to learn stuff on my own, search for what I wanted to change on the website, and how I managed to learn all of that in a few months despite working full-time and not having that much free time in general. You can be sure you're able to do that too!

Additionally, Jekyll seems to be quite performant. It doesn't use JavaScript and most KDE websites done with it render over 90% performance on Google's PageSpeed Insights, y'know!

If you're interested in building a website from scratch using Jekyll for a KDE application, please contact the KDE Web team over [Telegram][2], [Matrix][3] or [IRC][4] first, yeah? This post was also copied over to <https://community.kde.org/KDE.org/Jekyll> if you'd rather to check the wiki for these instructions.

 [1]: https://rabbitictranslator.com/wordpress/index.php/2019/12/08/contributing-to-kde-porting-websites-to-markdown/
 [2]: https://t.me/KDEWeb
 [3]: https://webchat.kde.org/#/room/#freenode_#kde-www:matrix.org
 [4]: irc://chat.freenode.net/kde-www
 [5]: #getonwithit
 [6]: http://kontact.kde.org
 [7]: https://invent.kde.org/websites
 [8]: http://shouldiuseacarousel.com/
 [9]: https://invent.kde.org/websites/jekyll-kde-theme/-/tree/master/assets/img
 [10]: https://youtu.be/sXE8LdXzeHM
 [11]: https://github.com/guard/listen/wiki/Duplicate-directory-errors
 [12]: https://stackoverflow.com/questions/2570972/css-font-border
 [13]: https://invent.kde.org/multimedia/subtitlecomposer
 [14]: https://github.com/maxrd2/subtitlecomposer
 [15]: https://invent.kde.org/websites/kdeconnect-kde-org/-/blob/master/_config.yml
 [16]: https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme
 [17]: https://invent.kde.org/thiagosueto/subtitlecomposer-kde-org
