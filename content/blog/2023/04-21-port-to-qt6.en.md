---
title: "Porting to Qt6 in practice"
date: 2023-04-21
url: port-to-qt6
toc: true
tags:
  - Planet KDE
---

Recently I started using KTimeTracker to record the time I spent working, and I really like it. So far it's the only productivity app that meets my needs. So I began playing with porting it to Qt6.

I'm weak with algorithms, but I know my way with building programs. I made a [long fluff post about compilation]({{< ref "12-12-long-kfluff-compiling-with-stuff" >}}), even. So I was feeling confident that I would make at least *some* progress.

Here's how it went.

Too long, didn't read: simple porting tasks get you a long way.

## Comparing changes

KDE started branching projects this year, meaning that now we'd have a branch for everything that stayed on Qt5 (`kf5`) and for the stuff that would be made with Qt6 (`master`).

After branching, the next natural step is to see how KDE apps were finding and linking Qt and KDE libraries. By the time I started working on KTimeTracker, there were already some projects using KDE Frameworks, so I looked at those.

This port would clearly have to start from the `CMakeLists.txt` files, because that's what you use to search for dependencies and whatnot.

So I knew I had to compare the `kf5` and `master` branches, looking for how to find and link essential libraries in the `CMakeLists.txt` files. Having some sort of direction was all I needed.

The most obvious change was that some CMake stuff that had a 5 appended to it got changed to either not having the 5 or to having a 6. I knew why, because I was already acquainted with Qt6 CMake API like [qt_standard_project_setup()](https://doc.qt.io/qt-6/qt-standard-project-setup.html), which has a small note:

> If versionless commands are disabled, use qt6_standard_project_setup() instead.

This is referring to https://doc.qt.io/qt-6/cmake-qt5-and-qt6-compatibility.html#versionless-commands. So versionless = no 6, and with version = with 6. Okay. I went with versionless just because. I am not entirely sure of whether KDE will be using versionless commands or not.

We can see an example of "no 6" in this change I made for KTimeTracker:

```diff
- qt5_add_resources(icons_SRCS icons.qrc)
+ qt_add_resources(icons_SRCS icons.qrc)
```

`qt5_add_resources()` only works with Qt5, but `qt_add_resources()` works with both.

Now, things that changed from 5 to 6 were usually found inside CMake functions rather than in the functions themselves, though. For example, to search for Qt6/KF6 libraries:

```diff
- find_package(Qt5Test CONFIG REQUIRED)
+ find_package(Qt6Test CONFIG REQUIRED)
```

And to link those Qt6/KF6 libraries to the application:

```diff
- target_link_libraries(helpers libktimetracker Qt5::Test)
+ target_link_libraries(helpers libktimetracker Qt6::Test)
```

Nothing particularly surprising so far. The only thing that was novel to me was this sort of change that I saw in every project I looked at:

```diff
- set(KF5_MIN_VERSION "5.93.0")
+ set(KF6_MIN_VERSION "5.240.0")
```

That's provided by [extra-cmake-modules](https://api.kde.org/ecm/), KDE's CMake extensions to make compiling KDE projects easier. It was always "5.240.0". This was being used as a sort of placeholder or intermediate target for things that were targetting Qt6 while no official Qt6 releases happen. It seems reasonable to assume that once the change to Qt6 is fully done, the new minimum version would be "6.0.0".

Just looking at these very superficial changes to `CMakeLists.txt` files would already be pretty significant. CMake has, generally speaking, three build steps: project configuration, compilation, installation. With just these few changes, we would finish most of the first step, project configuration, which handles finding and linking libraries.

Now we actually try to compile the project.

## Actually compiling

Now, to compile KTimeTracker with Qt6, I needed to compile KDE Frameworks 6. Thankfully KDE has a special tool to easily compile KDE projects, and it has a [wiki page detailing how to compile with Qt6 using kdesrc-build](https://community.kde.org/Get_Involved/development/More#kdesrc-build,_Qt6_and_KDE_Frameworks_6).

I had this set up already, but I did something different than what you'd expect.

After having compiled the necessary libraries, I ran:

```bash
source ~/kde6/build/frameworks/kxmlgui/prefix.sh
cd to/my/cloned/repository/of/KTimeTracker
cmake -B build -G Ninja
```

And bam, all KDE Frameworks 6 libraries were found.

This is because the contents of that `prefix.sh` file are the following:

```bash
export PATH=/home/blumen/kde6/usr/bin:$PATH
export XDG_DATA_DIRS=/home/blumen/kde6/usr/share:${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}
export XDG_CONFIG_DIRS=/home/blumen/kde6/usr/etc/xdg:${XDG_CONFIG_DIRS:-/etc/xdg}
export QT_PLUGIN_PATH=/home/blumen/kde6/usr/lib64/plugins:$QT_PLUGIN_PATH
export QML2_IMPORT_PATH=/home/blumen/kde6/usr/lib64/qml:$QML2_IMPORT_PATH
export QT_QUICK_CONTROLS_STYLE_PATH=/home/blumen/kde6/usr/lib64/qml/QtQuick/Controls.2/:$QT_QUICK_CONTROLS_STYLE_PATH
```

And this adds kdesrc-build compiled projects to my PATHs. Handiest thing in the world.

Why would I do this if I could have made kdesrc-build compile KTimeTracker with a simple `kdesrc-build ktimetracker` instead of having me manually go to another cloned repository and do things manually? In reality I could just as easily have used kdesrc-build for that.

Well, because I was already doing that with something else, namely Kirigami. I had been playing with making my own Kirigami 6 apps this way: compile Kirigami with `kdesrc-build kirigami`, source prefix.sh, compile my own apps using this Kirigami 6 library. All of that from within a distrobox, even. So I thought, why not do the same with KTimeTracker? Turned out to be an ok option, and a fun experiment.

The assumptions I made about the changes I'd have to do to the `CMakeLists.txt` files were correct. I went past the "configure" step quickly.

This is the result you want to see after you are done:

```
-- Configuring done (0.3s)
-- Generating done (0.0s)
-- Build files have been written to: /home/blumen/gitstuff/ktimetracker/build
```

And this is the relevant [commit](https://invent.kde.org/pim/ktimetracker/-/commit/dd84c74eb057c57f76dd689536bffc599bbd8854?merge_request_iid=16) with my first CMake changes.

## The compilation step

After having done the configuration step by running `cmake -B build -G Ninja`, the compilation step was performed with `cmake --build build`.

Here are the issues I encountered and the solutions I used to start porting the project.

### KCalendarCore

The output I'd see upon compilation would be something similar to this:

```bash
[26/86] Building CXX object src/CMakeFiles/libktimetracker.dir/file/icalformatkio.cpp.o
FAILED: src/CMakeFiles/libktimetracker.dir/file/icalformatkio.cpp.o 
<huge line>
In file included from /home/blumen/gitstuff/ktimetracker/src/file/icalformatkio.cpp:21:
/home/blumen/gitstuff/ktimetracker/src/file/icalformatkio.h:24:10: fatal error: KCalCore/ICalFormat: No such file or directory
   24 | #include <KCalCore/ICalFormat>
      |          ^~~~~~~~~~~~~~~~~~~~~
compilation terminated.
```

If you can see something like `[number/totalnumber]`, then you're guaranteed to be in the compilation step.

This error was happening to every single `KCalCore` include.

I took a look at my kdesrc-build directory for build files to check that I had built everything for KCalendarCore, and indeed `~/kde6/build/frameworks/kcalendarcore/` and `~/kde6/usr/lib64/libKF6CalendarCore.so` were a thing.

I took a look at the [KCalendarCore::ICalFormat API](https://api.kde.org/frameworks/kcalendarcore/html/classKCalendarCore_1_1ICalFormat.html) and weirdly enough: `#include <icalformat.h>`.

Okay, that's weird. Shouldn't it be `KCalCore/ICalFormat`? I tried a few different techniques I knew from experience that came to mind, with no success.

Oh wait. The "Use with CMake" section on the right sidebar is showing `find_package(KF5CalendarCore)`. The API website shows KF5 stuff for KCalendarCore then, this isn't the best source for the new KF6 include, then.

It took me a while to realize I should be searching the KCalendarCore repository for answers.

I searched for MRs containing the keyword "kcalendarcore" and while peeking at a few changes done to some other files, I noticed something different:

```cpp
#include <KCalendarCore/CalendarPlugin>
```

So the include signature had changed at some point. No longer is it KCalCore, but KCalendarCore. Kinda obvious in retrospect.

I search for MRs containing "include" and find nothing, but searching for commits reveals [Rename the KCalCoreNamespace to KCalendarCore](https://invent.kde.org/frameworks/kcalendarcore/-/commit/3890f8e9d48a9edb87739689e15fbc925b8699f2) and [Rename the header install locations to KCalendarCore](https://invent.kde.org/frameworks/kcalendarcore/-/commit/b9439ef68ef2efcfcbbc2022cc087b051991dd62).

So this is confirmed, I make the necessary changes in the code, both in include and function signatures. It works!

The relevant commits are [this](https://invent.kde.org/pim/ktimetracker/-/commit/7974a0cd7be558f67d8fac7daf42332d53df5b28?merge_request_iid=16) and [this](https://invent.kde.org/pim/ktimetracker/-/commit/bec1539eefce08690154d0051d355038702ba37c?merge_request_iid=16).

### KWindowSystem

The next compilation issue I see is:

```bash
/home/blumen/gitstuff/ktimetracker/src/dialogs/taskpropertiesdialog.cpp:82:44: error: ‘numberOfDesktops’ is not a member of ‘KWindowSystem’
   82 |     const int numDesktops = KWindowSystem::numberOfDesktops();
      |                                            ^~~~~~~~~~~~~~~~
/home/blumen/gitstuff/ktimetracker/src/dialogs/taskpropertiesdialog.cpp:92:55: error: ‘desktopName’ is not a member of ‘KWindowSystem’
   92 |         auto *checkbox = new QCheckBox(KWindowSystem::desktopName(i + 1));
      |                                                       ^~~~~~~~~~~
```

Because this is happening in more than one function, my suspicion falls to KWindowSystem.

I take a look at the [KWindowSystem API page](https://api.kde.org/frameworks/kwindowsystem/html/classKWindowSystem.html): I immediately see [KWindowSystem::desktopName](https://api.kde.org/frameworks/kwindowsystem/html/classKWindowSystem.html#ab8b7b5a354bfa9673ad3ed74546f424a) has a deprecation note saying "Deprecated: since 5.101, use KX11Extras::desktopName() instead". Same thing for [numberOfDesktops()](https://api.kde.org/frameworks/kwindowsystem/html/classKWindowSystem.html#aed809b7c1d2beeae7bb6ff3e63563ef5).

Well, that was simple. The relevant commits are [this](https://invent.kde.org/pim/ktimetracker/-/commit/ff6733261517db3a8664655b8eb74b4008acde02?merge_request_iid=16) and [this](https://invent.kde.org/pim/ktimetracker/-/commit/84546143155ce446117b687e08c153653588bac9?merge_request_iid=16).

### QChar and QString

The next issue is:

```bash
/home/blumen/gitstuff/ktimetracker/src/dialogs/exportdialog.cpp: In constructor ‘ExportDialog::ExportDialog(QWidget*, TaskView*)’:
/home/blumen/gitstuff/ktimetracker/src/dialogs/exportdialog.cpp:48:37: error: conversion from ‘QString’ to non-scalar type ‘QChar’ requested
   48 |     QChar d = QLocale().decimalPoint();
      |               ~~~~~~~~~~~~~~~~~~~~~~^~
```

I take a look, and the relevant code is:

```cpp
// If decimal symbol is a comma, then default field separator to semi-colon.
// In France and Germany, one-and-a-half is written as 1,5 not 1.5
QChar d = QLocale().decimalPoint();
if (QChar::fromLatin1(',') == d) {
    ui.radioSemicolon->setChecked(true);
} else {
    ui.radioComma->setChecked(true);
}
```

There actually seems to be nothing wrong? decimalPoint() returns a QString, in the next few lines this is shown to be a single character, so I would have thought it would implicitly convert things fine.

I'm confused, so I ask about this in the KDE Brazil Development chat to see if anyone has any insight on this.

The folks there rightfully point out that the QLocale documentation has this note:

> Note: This function shall change to return a QString instead of QChar in Qt6. Callers are encouraged to exploit the QString(QChar) constructor to convert early in preparation for this.

Which I did not see, *because I was looking at the Qt6 documentation for QLocale, not the Qt5 documentation*. Indeed, for whatever reason it didn't cross my mind that *only* the Qt5 docs would mention functions that have changed or been deprecated.

So this is a simple change:

```diff
- QChar d = QLocale().decimalPoint();
+ QString d = QLocale().decimalPoint();
```

The relevant commit is [this](https://invent.kde.org/pim/ktimetracker/-/commit/aea8a3f1d03d053d6a8df74aa233c54ec3be7b99?merge_request_iid=16).

### QXmlDefaultHandler

The next issue was:

```bash
In file included from /home/blumen/gitstuff/ktimetracker/src/model/projectmodel.cpp:25:
/home/blumen/gitstuff/ktimetracker/src/import/plannerparser.h:26:10: fatal error: QXmlDefaultHandler: No such file or directory
   26 | #include <QXmlDefaultHandler>
      |          ^~~~~~~~~~~~~~~~~~~~
```

I look at the [Qt5 API docs for it](https://doc.qt.io/qt-5/qxmldefaulthandler.html). It has a note saying:

> This class is obsolete. It is provided to keep old source code working. We strongly advise against using it in new code.

I then look at the [Qt6 API docs](https://doc.qt.io/qt-6/qxmldefaulthandler.html), and it's in the Qt5 Core Compatibility component. I don't want to port its code just yet, so what I do is include the Qt5CoreCompat component in the `CMakeLists.txt` so it still compiles. The actual code port can be taken care of by either future me or someone else who knows more than me.

The relevant commit is [this](https://invent.kde.org/pim/ktimetracker/-/commit/0878a6431e9b84ea14e8baad262cf4601c1ef641?merge_request_iid=16).

### Compiler settings

I realize I can make some errors disappear, at least temporarily, making my job easier for now.

You see, the [extra-cmake-modules KDECompilerSettings module](https://api.kde.org/ecm/kde-module/KDECompilerSettings.html) blocks several implicit conversions, among other things. Generally this is a best practice, but for porting this just increases noise when there are better issues to solve. Furthermore, I'm not very familiar with how to solve these sorts of issues. So I removed a few definitions from those compiler settings as a temporary measure. After the project successfully compiles, *then* I can start thinking about best practices.

And indeed, this decreased the number of errors I was getting.

### fromRawString

The next issue was interesting:

```bash
/home/blumen/kde6/usr/include/KF6/KCalendarCore/kcalendarcore/icalformat.h:116:28: note: candidate: ‘virtual bool KCalendarCore::ICalFormat::fromRawString(const KCalendarCore::Calendar::Ptr&, const QByteArray&, const QString&)’
  116 |     Q_REQUIRED_RESULT bool fromRawString(const Calendar::Ptr &calendar, const QByteArray &string, const QString &notebook = QString()) override;
      |                            ^~~~~~~~~~~~~
/home/blumen/kde6/usr/include/KF6/KCalendarCore/kcalendarcore/icalformat.h:116:28: note:   candidate expects 3 arguments, 4 provided
/home/blumen/gitstuff/ktimetracker/src/file/icalformatkio.cpp:75:33: error: no matching function for call to ‘ICalFormatKIO::fromRawString(const KCalendarCore::Calendar::Ptr&, const QByteArray&, bool, const QString&)’
   75 |             return fromRawString(calendar, text, false, urlString);
      |                    ~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

If I take a look at the [kf5 version of icalformat.h](https://invent.kde.org/frameworks/kcalendarcore/-/blob/kf5/src/icalformat.cpp?ref_type=heads#L184) and the [master version of this file](https://invent.kde.org/frameworks/kcalendarcore/-/blob/master/src/icalformat.cpp#L174), I see two different signatures:

```cpp
// kf5
bool ICalFormat::fromRawString(const Calendar::Ptr &cal, const QByteArray &string, bool deleted, const QString &notebook)
```

```cpp
// master
bool ICalFormat::fromRawString(const Calendar::Ptr &cal, const QByteArray &string)
```

There are two arguments less. Removed, and the compilation error is gone:

```diff
- return fromRawString(calendar, text, false, urlString);
+ return fromRawString(calendar, text);
```

This probably requires some extra changes in code, but for now this works.

The relevant commit is [this](https://invent.kde.org/pim/ktimetracker/-/commit/8a5ed59413d3f85f2303a51a78678d2d24dc2d6b?merge_request_iid=16).

### setMargin

This issue seemed clear:

```bash
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp: In constructor ‘TimeTrackerWidget::TimeTrackerWidget(QWidget*)’:
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp:62:13: error: ‘class QLayout’ has no member named ‘setMargin’
   62 |     layout->setMargin(0);
      |             ^~~~~~~~~
```

But neither the Qt5 or Qt6 API mention this function *directly*. It took me a while to notice the "Obsolete Members" link that only exists in the Qt5 docs for it, and in fact I found that page through a search engine.

Even then, that page doesn't *quite* mention what to use instead. Based on a [StackOverflow post](https://stackoverflow.com/questions/60416290/setmargin-method-not-worked-for-qvboxlayout-in-qt-python), this is replaceable by 
[setContentsMargins(0,0,0,0)](https://doc.qt.io/qt-6/qlayout.html#setContentsMargins).

The change worked:

```diff
- layout->setMargin(0);
+ layout->setContentsMargins(0,0,0,0);
```

The relevant commit is [this](https://invent.kde.org/pim/ktimetracker/-/commit/b8ba60a9ef024503454c2d9d1b5ef216b203b7bd?merge_request_iid=16).

### | and +

The next issue is simple too:

```bash
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp: In member function ‘void TimeTrackerWidget::setupActions(KActionCollection*)’:
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp:253:82: warning: ‘constexpr QKeyCombination operator+(Qt::Modifier, Qt::Key)’ is deprecated: Use operator| instead [-Wdeprecated-declarations]
  253 |         actionCollection->setDefaultShortcut(action, QKeySequence(Qt::CTRL + Qt::Key_T));
      |                                                                                  ^~~~~
```

This occurred in several `QKeySequence` instances. Like the warning says, changing the `+` to `|` worked.

The relevant commit is [this](https://invent.kde.org/pim/ktimetracker/-/commit/968b564464d8a057fddd0034d640e49ca51dd4be?merge_request_iid=16).

### YesNo

This one I actually knew about before:

```bash
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp: In member function ‘void IdleTimeDetector::timeoutReached(int, int)’:
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp:87:22: error: ‘questionYesNo’ is not a member of ‘KMessageBox’
   87 |         KMessageBox::questionYesNo(nullptr,
      |                      ^~~~~~~~~~~~~
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp:93:23: error: ‘Yes’ is not a member of ‘KMessageBox’
   93 |     case KMessageBox::Yes:
      |                       ^~~
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp:99:23: error: ‘No’ is not a member of ‘KMessageBox’
   99 |     case KMessageBox::No:
      |                       ^~
```

This was mentioned before in ["No Yes / No, Yes?"](https://frinring.wordpress.com/2022/09/13/no-yes-no-yes/), a post made by another KDE contributor. This came around the time I was [updating one of the tutorial pages for KXmlGui](https://develop.kde.org/docs/getting-started/kxmlgui/hello_world/).

Essentially, the API change consists of replacing KMessageBox::questionYesNo with KMessageBox::questionTwoActions. The latter requires enum values, so it was slightly more involved:

```diff
- KMessageBox::questionYesNo(nullptr,
+ KMessageBox::questionTwoActions(nullptr,
```

```diff
- case KMessageBox::Yes:
+ case KMessageBox::ButtonCode::Ok:
```

## Conclusion

These porting tasks were not particularly hard. I managed to get very close to actually compiling KTimeTracker in Qt6 by resolving simple issues.

So far it seems there are only two issues left, QDesktopWidget and QRegExp, which were obsoleted, have no trivial replacements, and are not in Qt5CoreCompat. They are a bit beyond what I'm currently willing to work on at the moment.

Despite the simple nature of this blog post, hopefully this gives you some insight on the basic changes and thought processes you'd need to port your favorite application to Qt6.
