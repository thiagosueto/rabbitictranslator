---
title: "The Checklist"
date: 2023-10-10
url: checklist
tags:
  - Planet KDE
---

I've been quiet since Akademy. Before that, the silence was mostly due to
studying multiple books about technical writing for my talk.

After that, I had certain tasks left to do: Kirigami Addons and KConfig.
The former requires a proper revision on my part to be finished, and KConfig
is almost ready, but I struggled quite a bit with the solution to a problem I
was having.

I moved on to focus temporarily on other matters that I'm going to handle
in the near future. This blog post doesn't provide the solutions to these
matters, but it mentions my plans, which might interest you or give you a clear
perspective on what I've been doing. The post will feel incomplete because it
is a checklist. If you wish, you can skip to the end summary.

## The wikis

The [KDE wikis](https://wiki.kde.org/) are instances of
[MediaWiki](https://www.mediawiki.org/wiki/MediaWiki).
They were in the process of being redesigned, and
[Techbase](https://techbase.kde.org/Welcome_to_KDE_TechBase) is going away.

A few ideas occurred to me:

* What about a Markdown wiki? Is it worth it?
* What other wikis are there?
* How to replicate the KDE wikis?
* How do I extend MediaWiki?

To answer these I needed to deploy wikis, so I revisited
[Podman containers](https://podman.io/) for a while. Not raw Podman,
naturally: I started with
[podman-compose](https://github.com/containers/podman-compose).

Additionally, the [Get Involved Development pages](https://community.kde.org/Get_Involved/development)
for beginner contributors need a redesign too. That aspiring developers
continued to struggle with the instructions so often was a symptom of a
documentation issue, after all.

For this one, other questions arise:

* What should be the order of things for the Development pages?
* What information is missing from them or is unnecessary?
* How to visualize the content to analyze it?
* How many nesting levels should they have?

This also had some light relation to containers, as I found them to be really
effective at getting compilation done quickly. With
[distrobox](https://github.com/89luca89/distrobox), you don't really
need to be on an up-to-date distro to compile KDE software anymore, you can run
GUI apps just fine, you don't dirty your host machine with development
libraries and you don't need to compile all program dependencies. I've used it
as a replacement for
[kdesrc-build](https://community.kde.org/Get_Involved/development#Set_up_kdesrc-build)
for quite a few months on [MicroOS Plasma](https://microos.opensuse.org/)
(now called Kalpa) and it works really well, so naturally I was biased towards
having containers as a viable alternative development process on the Development
pages.

Given my infatuation, I focused some time on podman-compose to be able to test
new ways of doing things. Once I was comfortable with it, I started testing
different wiki solutions to see their limitations. Lastly, I started running
MediaWiki itself so I could learn the ins and outs of how to write wiki pages
the MediaWiki way. And oh boy, did it take a lot of reading...

I arrived at [MediaWiki templates](https://www.mediawiki.org/wiki/Help:Templates),
which are fairly reminiscent of [Hugo templates](https://gohugo.io/templates/)
in a way, but I stopped at custom templates made in Lua, as I don't know Lua.
Regardless, templates seemed like the missing link to make better presentable
content, so it was worth trying.

Because I ran my own MediaWiki instance, I could test anything I wanted without
affecting anyone else, I didn't have to wait for cache updates, and I felt much
more comfortable at making stupid mistakes, since there was no public record of
them. :P

I've also learned quite a bit about the typical MediaWiki way of doing things.

## The fediverse

Some time ago,
[Reddit enshittificated](https://www.reddit.com/r/kde/comments/14n1k04/rkde_is_now_public_once_again/),
so after [KDE made their own Lemmy instance](https://lemmy.kde.social/)
I decided to take a look at it. I liked it despite being a prominent user of
New Reddit, and it was fast, and I didn't particularly care about the
controversy surrounding its creators, so I tried selfhosting it myself, as well
as its sort-of competitor, [Kbin](https://kbin.pub/en).
Both containers are relatively complex to understand, despite being easy to
deploy, so they proved to be good learning instruments.

I also decided to take a look at the other service provided by the Lemmy
instance I was in, the service being [Mastodon](https://joinmastodon.org/).
I remembered seeing that Carl has a
[way to integrate Hugo to Mastodon](https://carlschwan.eu/2020/12/29/adding-comments-to-your-static-blog-with-mastodon/)
such that Mastodon can serve as a comments section, too.

I liked it so much that I started using Lemmy way less. Because of my
infatuation with it, I looked into other services, and the one that interested
me was [WriteFreely](https://writefreely.org/).
Could it serve as a replacement for Hugo?

I was also partially motivated to help the project, because it targeted writers.
In the furry fandom it is a rather small niche compared to visual arts and
tech, and it is literally what I studied at University (literature), so I
was naturally biased.

It was the key for me to truly learn containers, because it had quite a lot of
container documentation issues and because I was still inexperient with
Podman. It lead me to create my
[herz-podman repository](https://github.com/herzenschein/herz-podman),
which although rather barrent got me motivated to share my findings on Mastodon
for others to see, too.

By extension, I started getting more comfortable with
[YAML files](https://www.redhat.com/en/topics/automation/what-is-yaml).

## The translation tutorial

A project that I've postponed for way too long is the [new tutorial for contributors wanting to translate KDE software](https://invent.kde.org/yurchor/kde-translation-howto).

I'm not very fond of [Docbook](https://en.wikipedia.org/wiki/DocBook),
which is used for the current [KDE documentation](https://community.kde.org/Get_Involved/documentation),
so to make my life easier, I resorted to some other method of writing docs in
Markdown, namely [Mkdocs](https://www.mkdocs.org/), the simplest project I
could find. That was a long time ago, and I struggled a bit back then because
of YAML. It's always YAML. Ugh.

I revisited the thing and with my new experience with YAML, I understood it
much better, so I fixed my previous attempts and reorganized the thing in a
logical manner for myself and others to work on the tutorial.

So, one thing that I've seen in the technical writing books I've read is that
while Markdown as a syntax is really easy to learn, Markdown as a means to
document can be extremely limited. It could never satisfy the needs of formal
documentation--that is, without extensions.

MediaWiki provides for the needs of professional documentation via both
generated UI and templates, effectively serving as a
[content management system (CMS)](https://en.wikipedia.org/wiki/Content_management_system).
Hugo extends Markdown by letting you use HTML layouts and templates, so with
enough of them, it gets professional. Mkdocs uses
[python-markdown](https://python-markdown.github.io/) to extend Markdown to a
similar level to Hugo and Mediawiki.

This reassures my idea that maybe Markdown with extensions could take the place
of Docbook someday, whichever underlying software it is that would maybe
satisfy our documentation requirements in the future.

## The code architecture

My lack of palpable results to display to others during the period after Akademy,
paired with some personal issues at home, started to affect me. One thing that
helps in such times is to organize or clean your environment to improve your
mental state, which is what I did.

A huge part of the lack of organization at home was due to too many papers and
too many physical books. I could sort all papers in a single multifolder, but I
needed a solution for organizing my books. That solution ended up being
[Tellico](https://tellico-project.org/).

Tellico is an underrated piece of KDE software: it lets you catalogue entire
collections, especially books. Even better, it is able to fetch information
about books online by just inputting the book's ISBN.

Without it, I wouldn't have managed to catalogue my 148 books in a timely
manner. If I had a barcode scanner it could have gone even faster, in fact.

Tellico served the purpose of a database so well that I had an idea: could I
catalogue the issues with KDE documentation with it? Could it address the
systemic issue of readmes that don't explain what the project is for?

The answer was yes. But where to start?

KDE's most valued product, as far as I can tell, is
[Plasma](https://kde.org/plasma-desktop/). So I started cataloguing all
programs under the Plasma group that were not unmaintained and that had
documentation issues.

The result is now selfhosted in https://plasmadox.rabbitictranslator.com.
With such a list it is easy to address those issues later on.

Once we have those issues sorted for the readmes, then we can start thinking
about documenting broad code architecture for the Plasma group, that is to say,
how each project connects to each other, because then there should be a clearer
understanding of the required pieces.

## The Gitlab CI

At some point, playing with Podman containers, you end up needing to have them
start together with your system, and you do that with [systemd](https://systemd.io/).

Naturally I went looking into
[podman-generate-systemd](https://docs.podman.io/en/latest/markdown/podman-generate-systemd.1.html),
which as the name implies automatically generates systemd services to run those
containers.

That's when I read its manpage saying that it was deprecated. What the...?
What is the replacement? [Quadlets](https://docs.podman.io/en/latest/markdown/podman-systemd.unit.5.html).

It is not a drop-in replacement for podman-generate-systemd, actually. It was
in fact rather hard to learn about it because it's such a new technology that it
only now is getting properly promoted by the Podman folks. It replaces Compose
files with systemd unit files, which in principle sounds like a great idea, but
it requires rewriting Compose files to the new format, which can be unpractical
for large selfhosted systems already using Compose.

Eventually I read all of the existing documentation for it, including the
entire manpage, and started replacing everything on my server to use it instead.
It is incredibly sturdy tech despite its novelty, and I enjoyed learning about it.

The result can be seen in [herz-quadlet](https://github.com/herzenschein/herz-quadlet).

This made selfhosting any service way easier. What this means is that working on
MediaWiki, WikiJS, DokuWiki, BookStack and Trilium, all knowledge databases,
became trivial. But most importantly, it enables me to run software that is
very useful for KDE infrastructure, namely Gitlab and Bugzilla.

I have yet to try Bugzilla, but I already managed to selfhost Gitlab. The thing
is massive and by default it uses 6 GB of RAM, so I can only run it on my laptop,
but it's a full fledged solution.

KDE moved from Jenkins to Gitlab CI for quite a long time by now, yet it lacks
proper documentation for it, not much aside from a few mailing list posts
(if you know where to look for them), and a few superficial blog posts. So far,
projects have mostly been copying from other projects, in makeshift fashion.

This surely works for project developers in some capacity, but what about those
without their own projects and who have never tried CI before (like me)? I had
a few major questions:

* Can I do this in a fork?
* If not, can I do this without being a project maintainer?
* What do I have to request sysadmin before I can do this?
* What parts of the CI were really KDE specific?
* How much load am I really putting on KDE's infrastructure for testing CI
over and over to study?
* What's the expected UI for an unprivileged user compared to that of a sysadmin
or project maintainer?
* What is each party allowed to do in CI?
* Is there any useful information to be had in studying the sysadmin side of
things?

I tried at some point to give a test drive of CI using Github and Gitea, but
that's when I realized that *they were both different from Gitlab's*.
Furthermore, Gitea actually lets you install different underlying software for
CI. In the end, I did actually need to learn Gitlab's CI specifically.

With the ability to run Gitlab with Podman and having these questions in my head,
I decided to set Gitlab up for CI from scratch. It was very likely that it
could be set up using containers, and I was right. By doing so I'd get a broader
understanding of how CI works in Gitlab.

Setting up Gitlab for CI from scratch meant reading most of their documentation
and having to set up [Gitlab Runner](https://docs.gitlab.com/runner/),
which is what is actually used under the hood. What I learned there was way more
valuable than I was expecting.

I accomplished this already, updated my Gitlab Quadlets to include it, and I'm
even halfway close to understanding how CI is done on KDE's side.

## Summary

All of this is only part of what I have been doing in the last few months.
The studying and preparation will let me work non-stop on the following
things *and actually finish them*:

- [ ] Reorganizing the Development wiki pages
- [ ] Writing about KDE development with containers
- [ ] Finishing the Translation How To
- [ ] Fixing all product documentation for Plasma
- [ ] Devising a code overview for Plasma components
- [ ] Writing a full tutorial on KDE CI
- [ ] Writing down most of the automation done by sysadmin

Some of those are things that should be easier for others to work on too.

I have [a few mockups](https://invent.kde.org/teams/web/wiki-sites/-/issues/3#note_731050)
done partially showing how I envision the new structure for the Development
wiki pages. I also wrote a template for a
[Clickable Button](https://community.kde.org/Template_talk:ClickableButton)
and a [Centered Button](https://community.kde.org/Template_talk:CenteredButton),
both with nice rounded corners to make links more visually appealing.

I reorganized the [Translation How To draft](https://invent.kde.org/yurchor/kde-translation-howto)
to make it easier to visualize what needs to be done and where, in addition to
adding a script that should work to automatically install what is needed to run it.
Given that I'm not super well experienced with SVN, it would be very useful if
someone else could chime in with that. The rest I can manage.

I selfhosted [all Plasma product docs issues on my blog](https://plamadox.rabbitictranslator.com)
so it's accessible to anyone willing to work on that. They follow certain
documentation expectations that I've learned either from experience or from
reading technical writing books.
