---
title: "Using Kirigami built with Qt6 without kdesrc-build"
date: 2023-06-06
url: kirigami6
tags:
  - Planet KDE
---

I have a few blog posts planned, but the one I wanted to post involving KDE color schemes isn't finished yet (it's enormous and tedious). So instead, today I'm showing you how simple it is to compile Kirigami with Qt6 so you can start playing with it ahead of time.

Kirigami, KDE's library that extends QtQuick, is a [Tier 1 KDE Framework](https://api.kde.org/frameworks/). The cool thing about it is that it has effectively no dependency on any KDE libraries. It depends only on two things: Qt and [extra-cmake-modules](https://api.kde.org/ecm/manual/ecm.7.html) (ECM).

While [kdesrc-build](https://community.kde.org/Get_Involved/development) is amazing for core KDE projects like plasma-desktop, KIO and KWin, I find it sometimes overkill just to start messing with standalone projects or libraries with few dependencies. I usually just install the needed dependencies from the repositories and build the project.

In this case I can still install Qt from the repositories, but Kirigami built with Qt6 requires ECM built for Qt6. It's simple nonetheless. This is the whole process:

```bash
git clone https://invent.kde.org/frameworks/extra-cmake-modules.git
cd extra-cmake-modules
cmake -B build/ -DCMAKE_INSTALL_PREFIX=$HOME/ecm6
cmake --build build/
cmake --install build/
fish_add_path $HOME/ecm6
cd ..
git clone https://invent.kde.org/frameworks/kirigami.git
cd kirigami
cmake -B build/ -DCMAKE_INSTALL_PREFIX=$HOME/kirigami6
cmake --build build/
cmake --install build/
source build/prefix.sh.fish
```

I clone the ECM repository and build it. The only required dependency is Qt6. I make sure to have it be installed to a custom folder in my home.

After it gets installed to `~/ecm6`, I can just add that folder to my PATH. Since I use fish, there's a convenient function to add this path for me easily, `fish_add_path`. This is the same thing as appending a folder to PATH in bash, like `export PATH=$PATH:$HOME/ecm6`.

To use ECM in a project, the only thing we needed was to add it to PATH, nothing more.

Then onwards to the cloned Kirigami repository: compiling it after adding ECM to the PATH works fine. The only required dependency is Qt6.

When it's finished, I can just source the prefix file. If you are using sh, bash or zsh, you just `source prefix.sh` instead of `prefix.sh.fish`.

After the file is sourced, *you can use the same shell in which you sourced the file to compile your Kirigami projects*. Here's an example program:

`CMakeLists.txt`:

```cmake
cmake_minimum_required(VERSION 3.25)

project(kirigami6test)

find_package(Qt6 REQUIRED COMPONENTS Widgets Quick)
find_package(KF6Kirigami2 REQUIRED)

qt_policy(SET QTP0001 OLD) # You might or might not need this

qt_standard_project_setup()

qt_add_executable(kirigami6test)

target_sources(kirigami6test PRIVATE
    main.cpp
)

qt_add_qml_module(kirigami6test
    URI         example
    QML_FILES   main.qml
)

target_link_libraries(kirigami6test PRIVATE
    Qt6::Widgets
    Qt6::Quick
    Qt6::DBus
    KF6::Kirigami2
)
```

`main.cpp`:

```cpp
#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.load("qrc:/example/main.qml");
    app.exec();
}
```

`main.qml`:

```qml
import QtQml
import QtQuick.Layouts
import QtQuick.Controls as Controls

import org.kde.kirigami 2.20 as Kirigami

Kirigami.ApplicationWindow {
    width: 400
    height: 300
    pageStack.initialPage: Kirigami.Page {
        title: "Hello Worl6!"
        Kirigami.FormLayout {
            anchors.fill: parent
            Kirigami.Separator {
                Kirigami.FormData.isSection: true
                Kirigami.FormData.label: "Study Qt and Kirigami"
            }
            ColumnLayout {
                Kirigami.FormData.label: "I'm learning:"
                Controls.Button {
                    text: "QtQuick"
                    onClicked: pageStack.push(qtquicklearn)
                }
                Controls.Button {
                    text: "Kirigami"
                    onClicked: pageStack.push(kirigamilearn)
                }
            }
        }
    }
    Component {
        id: qtquicklearn
        Kirigami.Page {
            title: "Cool QtQuick resources"
            ColumnLayout {
                anchors.fill: parent
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "QML Book"
                    onClicked: Qt.openUrlExternally("https://www.qt.io/product/qt6/qml-book")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "KDAB QML Youtube Course"
                    onClicked: Qt.openUrlExternally("https://www.youtube.com/playlist?list=PL6CJYn40gN6hdNC1IGQZfVI707dh9DPRc")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Minimal QML6 Examples"
                    onClicked: Qt.openUrlExternally("https://github.com/herzenschein/minimal-qml6-examples")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "QML Online"
                    onClicked: Qt.openUrlExternally("https://qmlonline.kde.org/")
                }
            }
        }
    }
    Component {
        id: kirigamilearn
        Kirigami.Page {
            title: "Cool Kirigami resources"
            ColumnLayout {
                anchors.fill: parent
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Kirigami Tutorial"
                    onCLicked: Qt.openUrlExternally("https://develop.kde.org/docs/getting-started/kirigami/")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Kirigami Examples"
                    onCLicked: Qt.openUrlExternally("https://invent.kde.org/frameworks/kirigami/-/tree/master/examples")
                }
            }
        }
    }
}
```

As mentioned before, use the same shell where you sourced the prefix.sh file to compile. Run:

```bash
cmake -B build/
cmake --build build/
./build/kirigami6test
```

And your project should run using Kirigami built from master with Qt6. Isn't that cool?

Be sure to take a look at the links above.

Have fun!
