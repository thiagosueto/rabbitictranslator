---
title: "Como criar projetos usando Kirigami compilado com Qt6 e sem kdesrc-build"
date: 2023-06-06
url: kirigami6
tags:
  - Planet KDE PTBR
---

Eu tenho algumas postagens planejadas pro meu blog, mas a que eu queria mostrar sobre esquemas de cores no Plasma ainda não terminei (é um trabalho gigante e tedioso). Ao invés disso, vou mostrar quão simples é compilar o Kirigami com Qt6 para você já começar a brincar com ele.

O Kirigami, a biblioteca da KDE que estende o QtQuick, é um [KDE Framework de Tier 1](https://api.kde.org/frameworks/). O legal disso é que ele basicamente não tem dependência alguma com outras bibliotecas da KDE. Ele depende só de duas coisas: o Qt e o [extra-cmake-modules](https://api.kde.org/ecm/manual/ecm.7.html) (ECM).

Embora o [kdesrc-build](https://community.kde.org/Get_Involved/development) seja uma maravilha para projetos centrais para a KDE como o plasma-desktop, o KIO e o KWin, penso que talvez seja pedir demais usar ele só para poder mexer com projetos individuais ou bibliotecas com poucas dependências. Ao invés disso, costumo simplesmente instalar as dependências necessárias pelo repositório e compilar o projeto diretamente.

Com o que vamos fazer aqui, eu ainda instalo o Qt pelo repositório, no entanto o Kirigami compilado com Qt6 exige ECM compilado com Qt6. Não muda muito, ainda é simples. O processo é o seguinte:

```bash
git clone https://invent.kde.org/frameworks/extra-cmake-modules.git
cd extra-cmake-modules
cmake -B build/ -DCMAKE_INSTALL_PREFIX=$HOME/ecm6
cmake --build build/
cmake --install build/
fish_add_path $HOME/ecm6
cd ..
git clone https://invent.kde.org/frameworks/kirigami.git
cd kirigami
cmake -B build/ -DCMAKE_INSTALL_PREFIX=$HOME/kirigami6
cmake --build build/
cmake --install build/
source build/prefix.sh.fish
```

Eu clono o repositório do ECM e compilo ele. A única dependência que preciso é o Qt6. Eu confirmo que mandei o CMake instalar numa pasta específica na minha home.

Depois que ele é instalado em `~/ecm6`, eu simplesmente adiciono a pasta no meu PATH. Meu shell é o fish, que vem com uma função super conveniente para adicionar o caminho certo, o `fish_add_path`. É a mesma coisa que acrescentar a pasta ao PATH no bash usando `export PATH=$PATH:$HOME/ecm6`.

Para usar o ECM em um projeto, a única coisa que precisamos é adicionar ele ao PATH, só.

Prosseguindo pro repositório do Kirigami que foi clonado: compilar ele após ter adicionado o ECM ao PATH simplesmente funciona. A única dependência que preciso é o Qt6.

Quando a compilação terminar, posso só fazer um source no arquivo prefix. Se você estiver usando sh, bash ou zsh, você pode fazer um `source prefix.sh` ao invés de usar o `prefix.sh.fish`.

Depois de fazer o source, *você pode usar o mesmo shell em que você fez source para compilar seus projetos em Kirigami*. Testa esse programinha de exemplo para você ver:

`CMakeLists.txt`:

```cmake
cmake_minimum_required(VERSION 3.25)

project(kirigami6test)

find_package(Qt6 REQUIRED COMPONENTS Widgets Quick)
find_package(KF6Kirigami2 REQUIRED)

qt_policy(SET QTP0001 OLD) # Pode ser que você precise disto

qt_standard_project_setup()

qt_add_executable(kirigami6test)

target_sources(kirigami6test PRIVATE
    main.cpp
)

qt_add_qml_module(kirigami6test
    URI         example
    QML_FILES   main.qml
)

target_link_libraries(kirigami6test PRIVATE
    Qt6::Widgets
    Qt6::Quick
    Qt6::DBus
    KF6::Kirigami2
)
```

`main.cpp`:

```cpp
#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.load("qrc:/example/main.qml");
    app.exec();
}
```

`main.qml`:

```qml
import QtQml
import QtQuick.Layouts
import QtQuick.Controls as Controls

import org.kde.kirigami 2.20 as Kirigami

Kirigami.ApplicationWindow {
    width: 400
    height: 300
    pageStack.initialPage: Kirigami.Page {
        title: "Olá Mun6o!"
        Kirigami.FormLayout {
            anchors.fill: parent
            Kirigami.Separator {
                Kirigami.FormData.isSection: true
                Kirigami.FormData.label: "Para estudar Qt e Kirigami"
            }
            ColumnLayout {
                Kirigami.FormData.label: "Estou aprendendo:"
                Controls.Button {
                    text: "QtQuick"
                    onClicked: pageStack.push(qtquicklearn)
                }
                Controls.Button {
                    text: "Kirigami"
                    onClicked: pageStack.push(kirigamilearn)
                }
            }
        }
    }
    Component {
        id: qtquicklearn
        Kirigami.Page {
            title: "Alguns recursos legais de QtQuick"
            ColumnLayout {
                anchors.fill: parent
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "QML Book"
                    onClicked: Qt.openUrlExternally("https://www.qt.io/product/qt6/qml-book")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Curso de QML da KDAB no YouTube"
                    onClicked: Qt.openUrlExternally("https://www.youtube.com/playlist?list=PL6CJYn40gN6hdNC1IGQZfVI707dh9DPRc")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Exemplos mínimos em QML6"
                    onClicked: Qt.openUrlExternally("https://github.com/herzenschein/minimal-qml6-examples")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "QML Online"
                    onClicked: Qt.openUrlExternally("https://qmlonline.kde.org/")
                }
            }
        }
    }
    Component {
        id: kirigamilearn
        Kirigami.Page {
            title: "Alguns recursos legais de Kirigami"
            ColumnLayout {
                anchors.fill: parent
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Tutorial de Kirigami"
                    onCLicked: Qt.openUrlExternally("https://develop.kde.org/docs/getting-started/kirigami/")
                }
                Controls.Button {
                    Layout.alignment: Qt.AlignHCenter
                    text: "Exemplos do Kirigami"
                    onCLicked: Qt.openUrlExternally("https://invent.kde.org/frameworks/kirigami/-/tree/master/examples")
                }
            }
        }
    }
}
```

Como mencionei antes, use o mesmo shell em que você fez source do arquivo prefix para compilar. Roda o seguinte:

```bash
cmake -B build/
cmake --build build/
./build/kirigami6test
```

Seu projeto deverá estar usando o Kirigami compilado do master usando Qt6. Legal né?

Não esqueça de dar uma checada nos links acima.

Divirta-se!
