---
title: "Precisamos de uma pasta config limpa!"
date: 2023-07-10
url: kconfig
tags:
  - Planet KDE PTBR
---

E é simples de conseguir isso, na real.

Mas qual o lance disso afinal?

## O problema

Já há tempos existe a reclamação de que a pasta ~/.config/ em sistemas Linux pode ficar infestada de arquivos de configuração. Esse é o caso com software da KDE também.

A ideia que proponho é de colocar esses arquivos em subpastas dentro de ~/.config/.

A especificação XDG Base Directory da Freedesktop apenas menciona de modo geral que arquivos de configuração tradicionais precisam ir dentro de XDG_CONFIG_DIRS. É só tacar lá que funciona. Certamente não há nada de *errado* em simplesmente enchar a pasta ~/.config/ com eles.

Mas a especificação também não é tão específica com isso, ela se preocupa mais com o papel desses arquivos e os caminhos usados para procurar por eles. Contanto que esteja dentro do XDG_CONFIG_DIRS, funciona. Em outras palavras, os aplicativos podem armazenar seus arquivos de configuração de usuário em ~/.config/ e em ~/.config/appdir/.

Isso fica bem claro perto do fim da especificação:

> Specifications may reference this specification by specifying the location of a configuration file as $XDG_CONFIG_DIRS/subdir/filename. This implies that:
> * Default configuration files should be installed to $sysconfdir/xdg/subdir/filename with $sysconfdir defaulting to /etc.
> * A user-specific version of the configuration file may be created in $XDG_CONFIG_HOME/subdir/filename, taking into account the default value for $XDG_CONFIG_HOME if $XDG_CONFIG_HOME is not set.
> * Lookups of the configuration file should search for ./subdir/filename relative to all base directories indicated by $XDG_CONFIG_HOME and $XDG_CONFIG_DIRS . If an environment variable is either not set or empty, its default value as defined by this specification should be used instead.

O spec impõe a procura desse tipo de subpasta se existir.

Isso é inclusive o [padrão do QSettings do Qt](https://doc.qt.io/qt-6/qsettings.html#platform-specific-notes) se você definir um nome de organização:

`QSettings settings("MySoft", "Star Runner");`

em que "MySoft" é o nome da organização e "Star Runner" é o nome do aplicativo, isso resulta no arquivo:

`~/.config/MySoft/Star Runner.conf`

Então não tem efetivamente uma razão técnica para evitar armazenar arquivos de configuração dentro de ~/.config/subdir/. E mesmo assim guardamos a maioria deles direto em ~/.config/.

## O processo de organização

Se você, que nem eu, realmente desgosta de uma pasta ~/.config/ entupida, acho que você vai gostar deste artigo: você pode mudar o status quo. Só acompanhe o texto por enquanto.

Levando as coisas acima em consideração, você precisa cogitar certas coisas:

* Programas podem não ter só um, e sim múltiplos arquivos de configuração

O Kate por exemplo gera até quatro (talvez mais?) arquivos de configuração de usuário: katerc, kateschemarc, katevirc, katemetainfos.

Disso vem a conclusão natural: é só colocar os arquivos em ~/.config/kate/. Todos os quatro arquivos do mesmo app ficam juntos.

Isso funciona bem para quem quer, por exemplo, escolher quais arquivos de configuração vão pro seu backup. Ao trocar de sistema, você pode fazer um backup da pasta do Kate e só.

* Nem todo programa é um aplicativo completo por si só

Alguns programas são componentes essenciais do sistema. O KWIn, KIO, Plasma, KScreen, KRunner, Plasma Addons, Plasma Integration, Plasma Thunderbolt, essas coisas nunca são publicadas separadamente como aplicativos estilo o Kate.

Algumas coisas como o Spectacle, por exemplo, são ambíguas: o Spectacle é um app completo por si só, mas (atualmente) só pode ser usado no Plasma.

Poderíamos então armazenar todos esses arquivos no mesmo grupo, ~/.config/plasma/? Ou manter cada coisa em sua própria pasta ~/.config/appdir/?

* Existem vários grupos de arquivos possíveis

O Plasma por exemplo não é "só" o Plasma, mas também o Plasma Desktop, o Plasma Mobile, o Plasma Bigscreen, o Plasma Nano, etc. Precisaríamos então de algo como ~/.config/plasma-desktop, ~/.config/plasma-mobile/, and por aí vai.

Dentro do ~/.config/plasma-desktop/, por exemplo, se veria arquivos como o plasmashellrc ou o plasma-org.kde.plasma.desktop-appletsrc.

* Todo programa da KDE é feito pela comunidade KDE

...Obviamente. A conclusão natural seria de ter todos os programas da KDE dentro de ~/.config/kde/appdir/, não? Seria tão conveniente para os usuários pode simplesmente copiar a pasta ~/.config/kde/ ao trocar de sistema, certo?

Bem...

* Alguns programas da KDE são agnośticos

Eu acho perfeitamente aceitável que apps completos por si só sejam independentes do sistema ao nível dos arquivos de configuração também. O que quero dizer é que *impor* ou mesmo *sugerir* que aplicativos assim que estão sob o nome da KDE armazenem seus arquivos de configuração dentro de ~/.config/kde/appdir/ não daria certo.

Os apps devem ser livres para afirmar sem sombra de dúvidas que eles funcionam em qualquer ambiente e que eles tratam esses ambientes como primeira classe.

Ademais, o ponto anterior só funcionaria se todo programa da KDE ficasse dentro de uma pasta ~/.config/kde/. Afinal, se você tivesse aplicativos individuais com sua própria ~/.config/appdir/, por quê você iria procurar por outros aplicativos inidividuais dentro do ~/.config/kde/? Isso seria inconsistente.

* Alguns programas que parecem ser da KDE podem não ser da KDE

Este é o caso com KCMs.

A ideia dos KCMs é que eles podem ser embutidos dentro das Configurações do Sistema *independente de quem os criou*. Eles são feitos para serem úteis para terceiros também. Terceiros neste caso significa qualquer um que não esteja usando a infraestrutura da KDE para a distribuição direta de seu software, como distros do Linux, desenvolvedores não afiliados à KDE, empresas fornecendo seus próprios módulos.

Faria sentido então ter ~/.config/kcm/? Ou faria mais sentido que cada KCM tenha seu ~/.config/appdir/? Ou deveríamos então juntar esses arquivos todos dentro de ~/.config/plasma-{desktop,mobile}/appdir/, já que as Configurações do Sistema são tão essenciais para o Plasma?

## A proposta

O seguinte são minhas opiniões pessoais sobre o que fazer com arquivos de configuração, baseadas nas questões acima. Embora a seção anterior sirva para reflexão, a proposta em si é bem simples:

* Aplicativos individuais: ~/.config/appdir/
* Programas essenciais: ~/.config/kwin/, ~/.config/kio/, ~/.config/krunner/, ~/.config/spectacle, ~/.config/plasma-desktop/, ~/.config/plasma-mobile/
* KCMs: ~/.config/kcm/ OU ~/.config/systemsettings/

Não creio que seja necessário complicar as coisas com mais que uma subpasta dentro de ~/.config/. Além disso, a especificação do XDG Base Directory só impõe explicitamente a procura de um nível de subpastas dentro do XDG_CONFIG_DIRS.

Que tal deixar aplicativos individuais serem individuais e completos por si só. Apps essenciais que podem ser considerados individuais e completos por si só podem ser simplesmente isso mesmo.

Requisitos essenciais para produtos do Plasma podem ser postos num só lugar.

Considere talvez unir os KCMs em uma pasta separada sem relação direta com o Plasma para encorajar desenvolvedores terceiros.

## A implementação

A razão de eu ter escrito este artigo *agora* e não antes é porque agora entendo *bem* como implementar isso.

Na verdade é bem simples. O arquivo de configuração padrão para a KDE segue o formato INI. Por exemplo, tendo um arquivo `myappconfigrc`:

```ini
[Nome do Grupo]
Entrada1=Valor1
Entrada2=Valor2

[Outro Grupo]
RodouUmaVez=true
cor=#ffffff
```

Para gerenciar esses arquivos de configuração, a KDE usa a biblioteca KConfig. Ela precisa de quatro passos básicos:

1. Criar um objeto que representa um arquivo de configuração
2. Criar um objeto que representa um grupo num arquivo de configuração
3. Ler/escrever para o objeto representando o grupo
4. Se tiver escrito algo no objeto representando o grupo, sincronizar as mudanças para o disco

Se você souber um mínimo de C++, você vai entender rápido. Vamos falar só do passo 1, por enquanto.

Vamos dar uma olhada na assinatura da função do KConfig que nos interessa, um dos construtores:

```
KConfig (const QString &file=QString(), OpenFlags mode=FullConfig, QStandardPaths::StandardLocation type=QStandardPaths::GenericConfigLocation)
```

O primeiro parâmetro é `file`, ou arquivo, bastante auto-explicativo, é o arquivo de configuração que queremos mexer.

O segundo parâmetro é `mode`, o modo como o arquivo de confiração será gerenciado. Na maioria das vezes, o modo vai ser `KConfig::SimpleConfig` (apenas um arquivo) ou `KConfig::FullConfig` (todos os arquivos de config relevantes). Se um código de verdade que você encontrar tiver algo diferente, não mude.

O terceiro parâmetro é o que mais nos interessa, `type`, ou tipo, isto é, que tipo de arquivo é e onde ele deve ser salvo. Por padrão é `QStandardPaths::GenericConfigLocation`, correspondendo a `~/.config/`.

Vamos checar um exemplo prático rapidamente.

```
KConfig configurationFile("myappconfigrc", KConfig::SimpleConfig, QStandardPaths::AppConfigLocation);
```

O arquivo é `myappconfigrc`.

`SimpleConfig` foi escolhido aqui por razões didáticas.

A parte interessante é o `AppConfigLocation`. Ele corresponde a ~/.config/appname/.

Ao simplesmente mudar o padrão de `GenericConfigLocation`, isto é, ~/.config/, para `AppConfigLocation`, isto é, ~/.config/appname/, já terminamos a implementação.

Sim, você leu certo. É uma mudança de uma linha só.

Você talvez se depare com o KSharedConfig ao invés do KConfig. Sua assinatura de função relevante é:

```
KSharedConfigPtr KSharedConfig::openConfig (const QString &  fileName = QString(), OpenFlags  mode = FullConfig, QStandardPaths::StandardLocation  type = QStandardPaths::GenericConfigLocation)
```

`fileName`, aí `mode`, aí `type` com padrão em ~/.config/, mesma coisa que o KConfig.

Num código de verdade, você poderia ver algo similar a:

```
KSharedConfigPtr configurationFile = KSharedConfig::openConfig("myappconfigrc", KConfig::SimpleConfig, QStandardPaths::AppConfigLocation);
```

Bem simples. É quase a mesma coisa, você simplesmente atribui o resultado de `openConfig()` a um simples ponteiro.

O KSharedConfig é preferível ao KConfig no geral, na verdade. Então você provavelmente vai encontrar o KSharedConfig mais comumente em código de verdade.

Alguns outros exemplos de código de verdade que você pode encontrar seriam similares a:

```
KSharedConfigPtr configurationFile = KSharedConfig::openConfig("myappconfigrc", KConfig::SimpleConfig);
KSharedConfigPtr configurationFile = KSharedConfig::openConfig("myappconfigrc");
KSharedConfigPtr configurationFile = KSharedConfig::openConfig();
```

No primeiro caso, o último parâmetro, `type`, não existe, então seu padrão (o que vem após o = na assinatura da função) é o `GenericConfigLocation`.

No segundo caso, o parâmetro `mode` está faltando também, então o padrão é `FullConfig` e `GenericConfigLocation`.

No terceiro caso não há nenhum parâmetro, nem mesmo `fileName`, então o padrão se torna `appname`, `FullConfig` e `GenericConfigLocation`. `appname`, no caso, é o nome do aplicativo como foi definido com QCoreApplication ou KAboutData.

Você agora está preparado para alterar o código da KDE e solicitar que seu aplicativo favorito comece a usar ~/.config/appdir/! Vai lá! \òwó/

Bem, pelo menos para aplicativos que usem ~/.config/appdir/. Usar nomes de pastas específicas fica para outro dia. Mas já faz diferença.

Só para satisfazer a curiosidade, vamos dar uma olhada nos passos 2, 3 e 4, usando como base o arquivo de configuração INI mencionado alguns parágrafos atrás, e usando o KSharedConfig ao invés do KConfig. Só para relembrar, o arquivo INI é assim:

```ini
[Nome do Grupo]
Entrada1=Valor1
Entrada2=Valor2

[Outro Grupo]
RodouUmaVez=true
cor=#ffffff
```

Começamos com o passo 2:

```
KConfigGroup configurationGroup = configurationFile->group("Outro Grupo");
```

Literalmente não poderia ser mais simples. Você captura um grupo chamado "Outro Grupo" do arquivo de configuração e atribui ele a um objeto KConfigGroup.

```
QString cor = configurationGroup.readEntry("cor", QString());
```

Então lemos uma entrada que fica dentro do grupo "Outro Grupo", neste caso "cor". Se ele estivesse vazio, ele retornaria apenas um QString(), uma string vazia; daria para usar QColor no lugar de QString também se quisesse.

De todo modo, a entrada não está vazia, então o código retorna "#ffffff".

```
configurationGroup.writeEntry("RodouUmaVez", false);
```

Bem simples: escrevemos o valor `false` na entrada "RodouUmaVez". Embora tenhamos escrito para o *objeto* de configuração, ainda precisamos escrever para o *arquivo* de configuração, então nos falta o último passo:

```
configurationGroup.sync();
```

E pá, salvo.

Outra possível ocorrência que você pode encontrar em código real pode ser:

```
KSharedConfig::openConfig()->sync();
```

Que faz literalmente a mesma coisa.

E é isso. Você agora sabe o básico de como gerenciar arquivos de configuração com o KConfig. Bem simples, não? Eu adoro o quão fácil ele é de usar, só precisava de mais documentação, que está sendo feita.

## Os recursos

Ficou interessado e quer ver mais sobre? Estou trabalhando com a [documentação da API do KConfig](https://api.kde.org/frameworks/kconfig/html/) para deixar bastante fácil de ler e usar, fique no aguarde. Você pode clicar em Class List ou Alphabetical List para ver as classes e funções disponíveis.

Enquanto as melhorias não saem, a [Introdução ao KConfig](https://develop.kde.org/docs/features/configuration/introduction/) serve bem. É em inglês. Ela menciona mais ou menos o mesmo do que eu disse aqui.

Também vale a pena dar uma olhada na documentação de API do [QSettings](https://doc.qt.io/qt-6/qsettings.html) e do [QStandardPaths](https://doc.qt.io/qt-6/qstandardpaths.html).

Eu nem cheguei a falar sobre o [KConfigXT](https://develop.kde.org/docs/features/configuration/kconfig_xt/) porque, bem, ainda não estudei ele a fundo. Você só precisa saber que para ver mais você vai precisar procurar por arquivos .kcfg e .kcfgc. Não sou tão fã do uso de XML, mas o negócio tem de fato uma [API super conveniente](https://develop.kde.org/docs/features/configuration/kconfig_xt/#reading-and-setting-values).
