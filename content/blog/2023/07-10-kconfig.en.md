---
title: "I want a clean config directory!"
date: 2023-07-10
url: kconfig
tags:
  - Planet KDE
---

And it can be done easily, ackshually.

But what is that all about?

## The problem

It has been a longstanding complaint that the ~/.config/ directory on Linux systems can get riddled with configuration files. This is the case with KDE software as well.

My idea is that we should be putting those into subdirectories inside ~/.config/.

The Freedesktop XDG Base Directory specification generally only states that standard configuration files should go under XDG_CONFIG_DIRS. Dump them there and you're gold. So it's not *wrong* to just fill the ~/.config/ directory with them.

But the specification is not particularly picky either, as it concerns itself more with the types of files and the paths that are looked up to find them. As long as it's in XDG_CONFIG_DIRS, it works. What this means is that applications are allowed to store user configuration files in ~/.config/ AND in ~/.config/appdir/.

This is made obvious by the end of the specification:

> Specifications may reference this specification by specifying the location of a configuration file as $XDG_CONFIG_DIRS/subdir/filename. This implies that:
> * Default configuration files should be installed to $sysconfdir/xdg/subdir/filename with $sysconfdir defaulting to /etc.
> * A user-specific version of the configuration file may be created in $XDG_CONFIG_HOME/subdir/filename, taking into account the default value for $XDG_CONFIG_HOME if $XDG_CONFIG_HOME is not set.
> * Lookups of the configuration file should search for ./subdir/filename relative to all base directories indicated by $XDG_CONFIG_HOME and $XDG_CONFIG_DIRS . If an environment variable is either not set or empty, its default value as defined by this specification should be used instead.

The specification tries to enforce the lookup of such subdirectories if they exist.

And this is actually Qt's [QSettings default](https://doc.qt.io/qt-6/qsettings.html#platform-specific-notes) if you set an organization name:

`QSettings settings("MySoft", "Star Runner");`

where "MySoft" is the organization name and "Star Runner" is the application name, this results in a file:

`~/.config/MySoft/Star Runner.conf`

So there's no technical reason not to store configuration files inside a ~/.config/subdir/. Yet we store most of those directly in ~/.config/.

## The organization

If you, like me, really dislike a dirty ~/.config/ directory, then you'll like this article: you can change the status quo. Stay with me for now.

At this point, you have to think of a few things:

* Software might have not just one, but multiple configuration files

Kate for example may generate up to four (or more?) user configuration files: katerc, kateschemarc, katevirc, katemetainfos.

This leads to the natural conclusion: have Kate store configuration files in ~/.config/kate/. All four files are bundled together for the same app.

This is great, because then you can nitpick which configuration files to use for your backup. You can choose to only make a backup of the Kate folder and nothing else when moving systems.

* Not every software is a standalone app

Some software is a core system component. KWin, KIO, Plasma, KScreen, KRunner, Plasma Addons, Plasma Integration, Plasma Thunderbolt, these things are never shipped as standalone apps like Kate.

Things like Spectacle, for instance, are ambiguous: it's a standalone app, but it's something that you (currently) can only use in Plasma.

Could we then store all of these under the same bundle, namely ~/.config/plasma/? Should each single thing be in their own ~/.config/appdir/ directory?

* There are multiple possible config bundles

Plasma for example is actually not "just" Plasma, but also Plasma Desktop, Plasma Mobile, Plasma Bigscreen, Plasma Nano, etc. So we'd need something like ~/.config/plasma-desktop/, ~/.config/plasma-mobile/, and so on.

Inside ~/.config/plasma-desktop/, for example, we'd see files like plasmashellrc or plasma-org.kde.plasma.desktop-appletsrc.

* Every KDE software is made by the KDE community

This is obvious. The natural conclusion would be to have all KDE software under ~/.config/kde/appdir/, right? It would be soooo handy for users to simply copy a ~/.config/kde/ folder when they're switching systems, right?

Well...

* Some KDE software is agnostic

Because some apps are standalone apps, I think it is fine for them to be independent and system-agnostic at the config file level too. That is to say, to *enforce* or even *suggest* standalone apps under the KDE name to store their configuration files under ~/.config/kde/appdir/ would be silly.

Standalone apps are free to state that they work on any environment and that they treat such environments as first class.

Besides, this only works if all KDE software goes under such a ~/.config/kde/ directory. Otherwise, if you already have standalone apps with their own ~/.config/appdir/, why would you search for other standalone apps in ~/.config/kde/? It's inconsistent.

* Some software that seems to be from KDE might not be from KDE

This is the case with KCMs.

The idea behind KCMs is that they can be embedded into System Settings *no matter who creates them*. It is designed to be useful for third parties too. And by third parties I mean anybody who is not using KDE infrastructure for their direct software distribution, like Linux distros, third party developers, companies shipping their own modules.

Would it make sense to have ~/.config/kcm/? Or would it make more sense to have each KCM have their ~/.config/appdir/? Or should we bundle those under ~/.config/plasma-{desktop,mobile}/appdir/, since System Settings is so core to Plasma?

## The proposal

These are my personal opinions on what should be done with configuration files, based on the questions above. While the previous section provided food for thought, the actual proposal is actually quite simple:

* Standalone apps: ~/.config/appdir/
* Core software: ~/.config/kwin/, ~/.config/kio/, ~/.config/krunner/, ~/.config/spectacle, ~/.config/plasma-desktop/, ~/.config/plasma-mobile/
* KCMs: ~/.config/kcm/ OR ~/.config/systemsettings/

I don't think we ever need more than one subdirectory under ~/.config/. Moreover, the XDG Base Directory specification only explicitly ensures lookup of one level of subdirectories under XDG_CONFIG_DIRS.

Let's make standalone apps be standalone apps. Core apps that can be considered standalone can just be standalone.

Essentials for Plasma products can just be bundled together.

Consider maybe bundling KCMs in a separate folder that has no direct relation to Plasma, to encourage third parties.

## The implementation

So the reason why I wrote this article *now* is because only now do I understand *well* how to implement it.

It is actually pretty easy. The default configuration file for KDE follows the INI format. For example, you could have a file `myappconfigrc`:

```ini
[Group Name]
Entry1=Value1
Entry2=Value2

[Another Group]
RunOnce=true
color=#ffffff
```

To manage these configuration files, KDE uses a library called KConfig to manage them. It has four expected steps:

1. Create an object that represents a configuration file
2. Create an object that represents a group in the configuration file
3. Read/write to the group object
4. If you wrote something to the group object, sync the new changes to disk

If you know some minimal C++, you'll understand it promptly. We'll talk only about Step 1, for now.

Let's take a look at the KConfig function signature that interests us, one of the constructors:

```
KConfig (const QString &file=QString(), OpenFlags mode=FullConfig, QStandardPaths::StandardLocation type=QStandardPaths::GenericConfigLocation)
```

The first parameter is `file`, pretty self explanatory, it's the config file that we want to manage.

The second parameter is `mode`, how the configuration file will be managed. For most purposes, you'll want `KConfig::SimpleConfig` (a single file) or `KConfig::FullConfig` (all pertinent config files). If the real code you encounter has something different, don't change it.

The third parameter is what interests us, `type`, namely the type of file and where the configuration file is saved. By default it's in `QStandardPaths::GenericConfigLocation`, which corresponds to `~/.config/`.

Let's look at a practical example now.

```
KConfig configurationFile("myappconfigrc", KConfig::SimpleConfig, QStandardPaths::AppConfigLocation);
```

The file is `myappconfigrc`.

We chose `SimpleConfig` for illustrative reasons.

The interesting bit is `AppConfigLocation`. This corresponds to ~/.config/appname/.

By simply changing the default from `GenericConfigLocation` a.k.a. ~/.config/, to `AppConfigLocation` a.k.a. ~/.config/appname/, we've already done the implementation.

Yes, you read it right. It's a one line change.

Instead of KConfig, however, you might encounter KSharedConfig. Its relevant function signature is:

```
KSharedConfigPtr KSharedConfig::openConfig (const QString &  fileName = QString(), OpenFlags  mode = FullConfig, QStandardPaths::StandardLocation  type = QStandardPaths::GenericConfigLocation)
```

`fileName`, then `mode`, then `type` defaulting to ~/.config/, just like KConfig.

In real code, it could look like this:

```
KSharedConfigPtr configurationFile = KSharedConfig::openConfig("myappconfigrc", KConfig::SimpleConfig, QStandardPaths::AppConfigLocation);
```

That's it. It's almost the same thing, you just assign the result of `openConfig()` to a simple pointer.

KSharedConfig is generally preferred over KConfig, in fact. So you're more likely to encounter it when looking at real code.

A few other examples of real code you could encounter could be:

```
KSharedConfigPtr configurationFile = KSharedConfig::openConfig("myappconfigrc", KConfig::SimpleConfig);
KSharedConfigPtr configurationFile = KSharedConfig::openConfig("myappconfigrc");
KSharedConfigPtr configurationFile = KSharedConfig::openConfig();
```

In the first case, the last parameter, `type`, does not exist, so it defaults (uses what comes after = in the function signature) to `GenericConfigLocation`.

In the second case, the parameter `mode` is also missing, so it defaults to `FullConfig` and `GenericConfigLocation`.

In the third case there are no parameters at all, not even `fileName`, so it just defaults to `appname`, `FullConfig` and `GenericConfigLocation`. `appname`, in this case, is the application name you define with QCoreApplication or KAboutData.

You're now prepared to change KDE code and request your favorite app to use the ~/.config/appdir/! Go do it! \òwó/

At least for standalone apps that use ~/.config/appdir/. We'll leave custom folder names for another day. But that already makes a huge difference.

Out of curiosity, let's just take a look at steps 2, 3 and 4, based on the INI configuration file shown a few paragraphs above, and using KSharedConfig instead of KConfig. Brief reminder of how the INI configuration file looks like:

```ini
[Group Name]
Entry1=Value1
Entry2=Value2

[Another Group]
RunOnce=true
color=#ffffff
```

We start with step 2:

```
KConfigGroup configurationGroup = configurationFile->group("Another Group");
```

It literally could not be simpler. You grab a group called "Another Group" from the configuration file and assign it to a KConfigGroup object.

```
QString color = configurationGroup.readEntry("color", QString());
```

Here we read an entry that is under the "Another Group", namely "color". If it were empty, it would return QString(), an empty string; you could use a QColor instead too if you specified it.

In any case, it isn't empty, so it returns "#ffffff".

```
configurationGroup.writeEntry("RunOnce", false);
```

Fairly straightforward: we write the value `false` to the entry "RunOnce". While we wrote to the configuration *object*, we still have to write to the configuration *file*, so we do the last step:

```
configurationGroup.sync();
```

Bam, saved.

One other possibility you might find in code would be:

```
KSharedConfig::openConfig()->sync();
```

Which literally does the same thing.

That's it. You now know the basics of managing configuration files with KConfig. Pretty simple, right? I love how the thing is easy to use, it just needed some documentation, which is on the way.

## The resources

You want to learn more? I'm working on the [KConfig API documentation](https://api.kde.org/frameworks/kconfig/html/) so it gets exceedingly easy for you to read and use it. Just you wait. You can click on Class List or Alphabetical List to see its available classes and functions.

In the meanwhile, the [Introduction to KConfig](https://develop.kde.org/docs/features/configuration/introduction/) is pretty good. It mentions mostly the same as what I've said here.

You should take a look at the API docs for [QSettings](https://doc.qt.io/qt-6/qsettings.html) and [QStandardPaths](https://doc.qt.io/qt-6/qstandardpaths.html).

I haven't touched [KConfigXT](https://develop.kde.org/docs/features/configuration/kconfig_xt/) yet because, well, I didn't study it in depth just yet. Just know that you'll want to look at the .kcfg and .kcfgc files. I'm not really a fan of the XML usage, but the thing does have a [handy API](https://develop.kde.org/docs/features/configuration/kconfig_xt/#reading-and-setting-values).
