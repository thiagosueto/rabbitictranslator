---
title: "Portando pro Qt6 na prática"
date: 2023-04-21
url: port-to-qt6
toc: true
tags:
  - Planet KDE PTBR
---

Recentemente comecei a usar o KTimeTracker para gravar o tempo que passo trabalhando, e gostei bastante dele. Até agora foi o único app de produtividade que me atendeu, então eu decidi tentar portar ele para o Qt6.

Algoritmos são meu ponto fraco, mas eu manjo de compilar programas. Até escrevi um [longo post sobre compilação]({{< ref path="12-12-long-kfluff-compiling-with-stuff.md" lang="en" >}}). Então estava confiante de que faria ao menos *algum* progresso.

Neste post falo sobre como foi minha experiência.

Pra resumir logo de cara: tarefas bem simples de portar já te levam longe.

## Comparando diferenças

A KDE começou a fazer branching dos seus projetos este ano, então agora temos um branch para tudo que ainda está usando Qt5 (`kf5`) e um branch para as coisas que viriam a ser feitas em Qt6 (`master`).

O próximo passo após fazer branching naturalmente é ver como os apps da KDE estavam procurando e linkando bibliotecas do Qt e da KDE. Quando comecei a trabalhar no KTimeTracker, já haviam alguns projetos usando KDE Frameworks 6, então dei uma checada neles.

O port que eu viria a fazer claramente teria que começar com arquivos `CMakeLists.txt`, já que é neles que se procura por dependências e afins.

Quer dizer, então, que eu sabia que devia comparar os branches `kf5` e `master`, olhando como encontrar e linkar bibliotecas nos arquivos `CMakeLists.txt`. Ter ao menos um rumo já bastou para começar.

A mudança mais perceptível era de que algumas coisas de CMake que tinham um 5 na frente agora passavam a não ter nada ou a ter um 6. Eu sabia disso porque eu já estava familiarizado com a API de CMake do Qt, como o [qt_standard_project_setup()](https://doc.qt.io/qt-6/qt-standard-project-setup.html), que tem uma nota:

> Se o versionless commands (comandos sem versão) estiver desativado, use qt6_standard_project_setup() no lugar disto.


Essa nota se refere ao https://doc.qt.io/qt-6/cmake-qt5-and-qt6-compatibility.html#versionless-commands. Quer dizer então que versionless = sem 6, e com version = com 6. Okay. Fui de versionless porque sim. Não estava certo, afinal, se a KDE iria na direção de versionless ou não.

Tenho um exemplo aqui de "sem 6" em uma mudança que eu faria mais tarde no KTimeTracker:

```diff
- qt5_add_resources(icons_SRCS icons.qrc)
+ qt_add_resources(icons_SRCS icons.qrc)
```

O `qt5_add_resources()` só funciona com Qt5, mas o `qt_add_resources()` funciona com ambos Qt5 e Qt6.

Já as coisas que mudaram do 5 pro 6 costumavam ser vistas em funções do CMake ao invés de nas próprias funções. For exemplo, na pesquisa de bibliotecas Qt6/KF6:

```diff
- find_package(Qt5Test CONFIG REQUIRED)
+ find_package(Qt6Test CONFIG REQUIRED)
```

E na linkagem dessas bibliotecas Qt6/KF6 ao aplicativo:

```diff
- target_link_libraries(helpers libktimetracker Qt5::Test)
+ target_link_libraries(helpers libktimetracker Qt6::Test)
```

Nada muito surpreendente. A única diferença notável para mim foi isto daqui, que vi em todo projeto que usava KF6:

```diff
- set(KF5_MIN_VERSION "5.93.0")
+ set(KF6_MIN_VERSION "5.240.0")
```

Isso é fornecido pelo [extra-cmake-modules](https://api.kde.org/ecm/), que tem extensões do CMake feitas pela KDE para compilar projetos da KDE mais facilmente. O número era sempre "5.240.0". Isso estava claramente sendo usado como um substituto temporário ou alvo intermediário para coisas que tinham o Qt6 como alvo mas não podiam criar lançamentos oficiais ainda. É razoável assumir que uma vez que o porte para o Qt6 esteja completo e saia um lançamento, a nova versão mínima seria "6.0.0".

Só de ter visto essas mudanças superficiais nos arquivos `CMakeLists.txt` já era algo significativo. No geral, o CMake tem três passos de build: configuração do projeto, compilação e instalação. Só com essas mudanças já era possível completar o primeiro passo de build inteiro, a configuração do projeto, que lida com encontrar e linkar bibliotecas.

A partir daí, é hora de compilar o projeto.

## Finalmente compilando

Para compilar o KTimeTracker com Qt6, eu precisava compilar o KDE Frameworks 6. Felizmente a KDE tem uma ferramenta especial para compilar seus projetos, e ele tem uma [página da wiki detalhando como compilar com Qt6 usando kdesrc-build](https://community.kde.org/Get_Involved/development/More#kdesrc-build,_Qt6_and_KDE_Frameworks_6).

Eu já tinha preparado isso antes, mas o que fiz após foi diferente do que se experaria.

Depois de compilar as bibliotecas necessárias, rodei:

```bash
source ~/kde6/build/frameworks/kxmlgui/prefix.sh
cd to/my/cloned/repository/of/KTimeTracker
cmake -B build -G Ninja
```

E pá, todas as bibliotecas do KDE Frameworks 6 foram encontradas assim.

Isso é porque o conteúdo do `prefix.sh` tem isto daqui:

```bash
export PATH=/home/blumen/kde6/usr/bin:$PATH
export XDG_DATA_DIRS=/home/blumen/kde6/usr/share:${XDG_DATA_DIRS:-/usr/local/share/:/usr/share/}
export XDG_CONFIG_DIRS=/home/blumen/kde6/usr/etc/xdg:${XDG_CONFIG_DIRS:-/etc/xdg}
export QT_PLUGIN_PATH=/home/blumen/kde6/usr/lib64/plugins:$QT_PLUGIN_PATH
export QML2_IMPORT_PATH=/home/blumen/kde6/usr/lib64/qml:$QML2_IMPORT_PATH
export QT_QUICK_CONTROLS_STYLE_PATH=/home/blumen/kde6/usr/lib64/qml/QtQuick/Controls.2/:$QT_QUICK_CONTROLS_STYLE_PATH
```

Que adiciona os projetos compilados com kdesrc-build aos meus PATHs. Super útil, melhor coisa.

Mas por que raios eu faria isso quando eu podia simplesmente mandar o kdesrc-build compilar o KTimeTracker com um simples `kdesrc-build ktimetracker` ao invés de manualmente ir a outro repositório clonado e fazer tudo manualmente? De fato eu poderia simplesmente ter feito isso.

Bem, isso é porque eu já estava fazendo este mesmo processo com outro projeto, Kirigami. Estava brincando com meus próprios apps em Kirigami 6 já fazia um tempo: compilo o Kirigami com `kdesrc-build kirigami`, source no prefix.sh, compilo meus apps com a biblioteca. Tudo isso dentro de um distrobox, inclusive. Então pensei, "por quê não"? E não é que deu tudo ok e foi divertido?

As assunções que fiz sobre as mudanças que eu teria de fazer aos arquivos `CMakeLists.txt` estavam certas. Passei do passo de configuração rapidinho.

Este é o resultado que você veria após terminar este passo:

```
-- Configuring done (0.3s)
-- Generating done (0.0s)
-- Build files have been written to: /home/blumen/gitstuff/ktimetracker/build
```

E este é o [commit](https://invent.kde.org/pim/ktimetracker/-/commit/dd84c74eb057c57f76dd689536bffc599bbd8854?merge_request_iid=16) relevante com minhas alterações iniciais de CMake.

## O passo de compilação

Depois de terminar o passo de configuração rodando `cmake -B build -G Ninja`, fiz o passo de compilação com `cmake --build build`.

A seguir mostro os problemas que encontrei e as soluções que usei para o port inicial do projeto.

### KCalendarCore

A primeira saída que eu veria ao compilar era algo assim:

```bash
[26/86] Building CXX object src/CMakeFiles/libktimetracker.dir/file/icalformatkio.cpp.o
FAILED: src/CMakeFiles/libktimetracker.dir/file/icalformatkio.cpp.o 
<huge line>
In file included from /home/blumen/gitstuff/ktimetracker/src/file/icalformatkio.cpp:21:
/home/blumen/gitstuff/ktimetracker/src/file/icalformatkio.h:24:10: fatal error: KCalCore/ICalFormat: No such file or directory
   24 | #include <KCalCore/ICalFormat>
      |          ^~~~~~~~~~~~~~~~~~~~~
compilation terminated.
```

Por sinal, se você vir algo como `[número/total]`, então você definintivamente está no passo de compilação.

O erro acima estava ocorrendo com todo include do `KCalCore`.

Dei uma checada na minha pasta de build do KCalendarCore no kdesrc-build e verifiquei se tinha compilado tudo direitinho, e de fato `~/kde6/build/frameworks/kcalendarcore/` e `~/kde6/usr/lib64/libKF6CalendarCore.so` existiam.

Eu dei uma olhada na [API do KCalendarCore::ICalFormat](https://api.kde.org/frameworks/kcalendarcore/html/classKCalendarCore_1_1ICalFormat.html) e curiosamente tinha isto: `#include <icalformat.h>`.

Esquisito. Não devia ser `KCalCore/ICalFormat`? Tentei algumas técnicas diferentes de experiência prévia que me vieram à cabeça, mas sem sucesso.

Mas peraí. A seção "Use with CMake" na barra lateral estava mostrando `find_package(KF5CalendarCore)`. O site da API ainda mostrava coisa de KF5 pro KCalendarCore então, quer dizer que não era a melhor fonte para o include do KF6 então.

Demorou um pouco para me cair a ficha de que eu devia fuçar o repositório do KCalendarCore para encontrar a resposta.

Procurei por MRs com a palavra chave "kcalendarcore" e enquanto estava lendo alterações feitas em outros arquivos não relacionados, me deparei com algo diferente:

```cpp
#include <KCalendarCore/CalendarPlugin>
```

A assinatura do include tinha mudado em determinado ponto. Não era mais KCalCore, e sim KCalendarCore. Óbvio vendo em retrospecto.

Eu procuro por MRs contendo "include" e não acho nada, mas pesquisar por commits revelou [Rename the KCalCore Namespace to KCalendarCore](https://invent.kde.org/frameworks/kcalendarcore/-/commit/3890f8e9d48a9edb87739689e15fbc925b8699f2) e [Rename the header install locations to KCalendarCore](https://invent.kde.org/frameworks/kcalendarcore/-/commit/b9439ef68ef2efcfcbbc2022cc087b051991dd62).

Está confirmado então. Eu faço as mudanças necessárias no código, tanto no include quanto na função. Funciona!

Os commits relevantes são [este](https://invent.kde.org/pim/ktimetracker/-/commit/7974a0cd7be558f67d8fac7daf42332d53df5b28?merge_request_iid=16) e [este](https://invent.kde.org/pim/ktimetracker/-/commit/bec1539eefce08690154d0051d355038702ba37c?merge_request_iid=16).

### KWindowSystem

O próximo problema de compilação que vejo é este daqui:

```bash
/home/blumen/gitstuff/ktimetracker/src/dialogs/taskpropertiesdialog.cpp:82:44: error: ‘numberOfDesktops’ is not a member of ‘KWindowSystem’
   82 |     const int numDesktops = KWindowSystem::numberOfDesktops();
      |                                            ^~~~~~~~~~~~~~~~
/home/blumen/gitstuff/ktimetracker/src/dialogs/taskpropertiesdialog.cpp:92:55: error: ‘desktopName’ is not a member of ‘KWindowSystem’
   92 |         auto *checkbox = new QCheckBox(KWindowSystem::desktopName(i + 1));
      |                                                       ^~~~~~~~~~~
```

Isso ocorria com mais de uma função, então minha suspeita caiu no KWindowSystem.

Dou uma olhada na [página de API do KWindowSystem](https://api.kde.org/frameworks/kwindowsystem/html/classKWindowSystem.html): imediatamente vejo que o [KWindowSystem::desktopName()](https://api.kde.org/frameworks/kwindowsystem/html/classKWindowSystem.html#ab8b7b5a354bfa9673ad3ed74546f424a) tem uma nota de API obsoleta dizendo "Deprecado: desde o 5.101, use KX11Extras::desktopName() no lugar disto". Mesma coisa com o [numberOfDesktops()](https://api.kde.org/frameworks/kwindowsystem/html/classKWindowSystem.html#aed809b7c1d2beeae7bb6ff3e63563ef5).

Bem, era simples. Os commits relevantes são [este](https://invent.kde.org/pim/ktimetracker/-/commit/ff6733261517db3a8664655b8eb74b4008acde02?merge_request_iid=16) e [este](https://invent.kde.org/pim/ktimetracker/-/commit/84546143155ce446117b687e08c153653588bac9?merge_request_iid=16).

### QChar and QString

O próximo erro foi:

```bash
/home/blumen/gitstuff/ktimetracker/src/dialogs/exportdialog.cpp: In constructor ‘ExportDialog::ExportDialog(QWidget*, TaskView*)’:
/home/blumen/gitstuff/ktimetracker/src/dialogs/exportdialog.cpp:48:37: error: conversion from ‘QString’ to non-scalar type ‘QChar’ requested
   48 |     QChar d = QLocale().decimalPoint();
      |               ~~~~~~~~~~~~~~~~~~~~~~^~
```

No lugar mencionado, o código relevante era este:

```cpp
// Se o símbolo decimal for vírgula, então separador de campo padrão é ponto e vírgula.
// Na França e Alemanha, um e meio se escreve como 1,5 não 1.5
QChar d = QLocale().decimalPoint();
if (QChar::fromLatin1(',') == d) {
    ui.radioSemicolon->setChecked(true);
} else {
    ui.radioComma->setChecked(true);
}
```

…Não parecia ter nada errado? decimalPoint() retorna um QString, nas linhas seguintes mostra que se trata de um caractere só, então teria imaginado que as coisas seriam implicitamente convertidas normal.

Fiquei confuso, então perguntei no grupo do KDE Brasil Desenvolvimento para ver se alguém tinha alguma ideia sobre o que podia ser.

O pessoal de lá apontou corretamente que a documentação do QLocale tinha esta nota:

> Nota: Esta função retornará um QString ao invés de QChar no Qt6. Quem chamar esta função deve tirar proveito do construtor QString(QChar) para fazer esta conversão antes em preparação para esta mudança.

Coisa que não tinha visto porque eu estava olhando na documentação do QLocale *para Qt6, não para Qt5*. Por alguma razão não me passou pela cabeça que só a documentação do Qt5 iria apontar quais funções tinham mudado ou seriam deprecadas no 6.

No final das contas, é uma mudança simples:

```diff
- QChar d = QLocale().decimalPoint();
+ QString d = QLocale().decimalPoint();
```

O commit relevante é [este](https://invent.kde.org/pim/ktimetracker/-/commit/aea8a3f1d03d053d6a8df74aa233c54ec3be7b99?merge_request_iid=16).

### QXmlDefaultHandler

O próximo problema foi:

```bash
In file included from /home/blumen/gitstuff/ktimetracker/src/model/projectmodel.cpp:25:
/home/blumen/gitstuff/ktimetracker/src/import/plannerparser.h:26:10: fatal error: QXmlDefaultHandler: No such file or directory
   26 | #include <QXmlDefaultHandler>
      |          ^~~~~~~~~~~~~~~~~~~~
```

Naturalmente checo a [API do Qt5 dele](https://doc.qt.io/qt-5/qxmldefaulthandler.html). Tem uma nota dizendo:

> Esta classe está obsoleta. Ela só está sendo fornecida ainda para manter código fonte antigo funcionando. Recomendamos fortemente que não use mais isto em código novo.

Checo a [API do Qt6 dele](https://doc.qt.io/qt-6/qxmldefaulthandler.html), e pelo visto está no componente Qt5 Core Compatibility. Não quero portar código ainda, então eu simplesmente incluo o Qt5CoreCompat no `CMakeLists.txt` para pelo menos compilar. O port de código de verdade pode ser feito depois, por mim ou alguém que manje mais que eu.

O commit relevante é [este](https://invent.kde.org/pim/ktimetracker/-/commit/0878a6431e9b84ea14e8baad262cf4601c1ef641?merge_request_iid=16).

### Compiler settings

Eu me toquei de que posso facilitar minha vida sumindo com alguns erros, ao menos temporariamente.


Veja bem, o [módulo KDECompilerSettings do extra-cmake-modules](https://api.kde.org/ecm/kde-module/KDECompilerSettings.html), dentre outras coisas, impede o uso de várias conversões implícitas. No geral isso é uma boa prática, mas para a finalidade de portar isso só aumenta o ruído quando posso resolver coisas mais importantes no momento. Eu removi algumas definitions dessas configurações de compilador como medida temporária. Assim que eu finalmente conseguir compilar o projeto, *aí sim* posso pensar em boas práticas.

E como pensei, o número de erros que estava tendo diminuiu bastante.

### fromRawString

O próximo erro foi interessante:

```bash
/home/blumen/kde6/usr/include/KF6/KCalendarCore/kcalendarcore/icalformat.h:116:28: note: candidate: ‘virtual bool KCalendarCore::ICalFormat::fromRawString(const KCalendarCore::Calendar::Ptr&, const QByteArray&, const QString&)’
  116 |     Q_REQUIRED_RESULT bool fromRawString(const Calendar::Ptr &calendar, const QByteArray &string, const QString &notebook = QString()) override;
      |                            ^~~~~~~~~~~~~
/home/blumen/kde6/usr/include/KF6/KCalendarCore/kcalendarcore/icalformat.h:116:28: note:   candidate expects 3 arguments, 4 provided
/home/blumen/gitstuff/ktimetracker/src/file/icalformatkio.cpp:75:33: error: no matching function for call to ‘ICalFormatKIO::fromRawString(const KCalendarCore::Calendar::Ptr&, const QByteArray&, bool, const QString&)’
   75 |             return fromRawString(calendar, text, false, urlString);
      |                    ~~~~~~~~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

Se eu der uma olhada no [icalformat.h versão kf5](https://invent.kde.org/frameworks/kcalendarcore/-/blob/kf5/src/icalformat.cpp?ref_type=heads#L184) e na [versão master](https://invent.kde.org/frameworks/kcalendarcore/-/blob/master/src/icalformat.cpp#L174), vejo duas assinaturas diferentes:

```cpp
// kf5
bool ICalFormat::fromRawString(const Calendar::Ptr &cal, const QByteArray &string, bool deleted, const QString &notebook)
```

```cpp
// master
bool ICalFormat::fromRawString(const Calendar::Ptr &cal, const QByteArray &string)
```

Tem dois argumentos a menos. Quando removo eles, o erro de compilação some:

```diff
- return fromRawString(calendar, text, false, urlString);
+ return fromRawString(calendar, text);
```

Isso provavelmente requer algumas mudanças extras no código, mas por enquanto, funciona.

O commit relevante foi [este](https://invent.kde.org/pim/ktimetracker/-/commit/8a5ed59413d3f85f2303a51a78678d2d24dc2d6b?merge_request_iid=16).

### setMargin

Este erro parecia claro:

```bash
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp: In constructor ‘TimeTrackerWidget::TimeTrackerWidget(QWidget*)’:
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp:62:13: error: ‘class QLayout’ has no member named ‘setMargin’
   62 |     layout->setMargin(0);
      |             ^~~~~~~~~
```

Mas nem a API do Qt5 nem a do Qt6 mencionam essa função *diretamente*. Demorei para perceber o link para "Obsolete Members" que só tem na documentação do Qt5 dele, e na verdade só encontrei essa página procurando um mecanismo de pesquisa.

Ainda assim, a página não menciona exatamente o que deve ser usado no lugar. De acordo com um [post do StackOverflow](https://stackoverflow.com/questions/60416290/setmargin-method-not-worked-for-qvboxlayout-in-qt-python), esta função é substituível por 
[setContentsMargins(0,0,0,0)](https://doc.qt.io/qt-6/qlayout.html#setContentsMargins).

A alteração funcionou:

```diff
- layout->setMargin(0);
+ layout->setContentsMargins(0,0,0,0);
```

O commit relevante é [este](https://invent.kde.org/pim/ktimetracker/-/commit/b8ba60a9ef024503454c2d9d1b5ef216b203b7bd?merge_request_iid=16).

### | e +

O próximo erro é simples também:

```bash
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp: In member function ‘void TimeTrackerWidget::setupActions(KActionCollection*)’:
/home/blumen/gitstuff/ktimetracker/src/timetrackerwidget.cpp:253:82: warning: ‘constexpr QKeyCombination operator+(Qt::Modifier, Qt::Key)’ is deprecated: Use operator| instead [-Wdeprecated-declarations]
  253 |         actionCollection->setDefaultShortcut(action, QKeySequence(Qt::CTRL + Qt::Key_T));
      |                                                                                  ^~~~~
```

Ele ocorreu em várias instâncias de QKeySequence. Como o aviso (warning) diz, mudar do `+` para `|` funciona.

O commit relevante é [este](https://invent.kde.org/pim/ktimetracker/-/commit/968b564464d8a057fddd0034d640e49ca51dd4be?merge_request_iid=16).

### YesNo

Este aqui eu já tinha visto antes:

```bash
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp: In member function ‘void IdleTimeDetector::timeoutReached(int, int)’:
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp:87:22: error: ‘questionYesNo’ is not a member of ‘KMessageBox’
   87 |         KMessageBox::questionYesNo(nullptr,
      |                      ^~~~~~~~~~~~~
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp:93:23: error: ‘Yes’ is not a member of ‘KMessageBox’
   93 |     case KMessageBox::Yes:
      |                       ^~~
/home/blumen/gitstuff/ktimetracker/src/idletimedetector.cpp:99:23: error: ‘No’ is not a member of ‘KMessageBox’
   99 |     case KMessageBox::No:
      |                       ^~
```

Porque foi mencionado no ["No Yes / No, Yes?"](https://frinring.wordpress.com/2022/09/13/no-yes-no-yes/), um post feito por outro contribuidor da KDE. Isso saiu mais ou menos na mesma época em que eu estava [atualizando uma das páginas de tutorial do KXmlGui](https://develop.kde.org/docs/getting-started/kxmlgui/hello_world/).

Basicamente, a mudança de API consiste em substituir o KMessageBox::questionYesNo por KMessageBox::questionTwoActions. O último requer valores de enum, então era um pouco mais elaborado que alguns exemplos anteriores:

```diff
- KMessageBox::questionYesNo(nullptr,
+ KMessageBox::questionTwoActions(nullptr,
```

```diff
- case KMessageBox::Yes:
+ case KMessageBox::ButtonCode::Ok:
```

## Conclusão

Essas tarefas de portar não eram especialmente difíceis. Consegui chegar bem perto de terminar a compilação do KTimeTracker usando Qt6 só lidando com erros simples.

No momento parecem haver apenas dois problemas faltando, do QDesktopWidget e QRegExp, que se tornaram obsoletos sem ter substitutos simples e sem estar no Qt5CoreCompat. Esses dois são um pouco além do que estou disposto a corrigir no momento.

A despeito da natureza simples deste post, espero que você agora tenha alguma ideia das mudanças básicas necessárias e do raciocínio usado para portar seu aplicativo favorito para o Qt6.
