---
title: Contributing to KDE is easier than you think – Porting websites to Markdown
author: Blumen Herzenschein
date: 2019-12-08
url: port-markdown
aliases: /wordpress/index.php/2019/12/08/contributing-to-kde-porting-websites-to-markdown/
tags:
  - Planet KDE
---
Hello all!

This will be a new series of blog posts explaining different ways to contribute to KDE in an easy-to-digest manner. I plan for this series to be parallel to my [keyboard shortcuts analysis][1] so that there can be content being published (hopefully) every week. I was also feeling a bit bad about the fact that this blog is available over [planet.kde.org][2] (a feed for blog posts made by KDE contributors that also shows a bit of their personal lives and projects), but my other series was focusing more on other DEs, despite also being a project to improve KDE.

The purpose of this series originated from how I feel about asking users to contribute back to KDE. I firmly believe that showing users how contributing is easier than they think is more effective than simply calling them out and directing them to the correct resources; especially if, like me, said user suffers from anxiety or does not believe they are up to the task, in spite of their desire to help back.

It is true that I had the initiative to contact Nate Graham and Carl Schwan through Reddit, but it is also true that, had they not shown me how contributing back can be done in several small, feasible ways too, I would likely not have started contributing back.

Out of respect and due to the [current need for help with updating the KDE websites][3], my first post on this subject will document how to help Carl Schwan port older websites to Markdown, despite there being easier tasks than that. Currently, and as to my knowledge, Carl Schwan and Adrián Chaves Fernandez are the only two main KDE websites contributors, with help and mentorship from other KDE contributors such as Jonathan Riddell and, of course, the whole Promo team, who handles websites as well. This is quite the low number of contributors for such a huge amount of websites to be updated, you see; that's why your help would be much appreciated!

If you'd like to skip the introduction and go straight to the documentation, [click here][4]. This task is not difficult, but you should know what HTML/PHP tags are beforehand, as well as basic Markdown knowledge, although this documentation is comprehensive enough that you should be able to learn on-the-go.

## The new website structure

Observe the difference between the older and newer layouts for two KDE Frameworks announcements, for instance:

{{< figure src="/2019/12/AnnouncementsOld.png" caption="Figure 1 – Old website layout, KDE Frameworks 5.52." >}}

{{< figure src="/2019/12/AnnouncementsNew.png" caption="Figure 2 – New website layout, KDE Frameworks 5.53." >}}

It is easy to see the reason for KDE to port their websites: most older websites date from the KDE3 or KDE4 age. Nostalgia aside, anyone who has been browsing the web lately can see that Figure 1 shows an old-fashioned design.

In addition, several other pages have outdated information that could/should be updated.

The new website layout is based on Ken Vermette's Jekyll theme, Aether. You can see more information about this on [Carl's blog][5]. In addition, since I use a global dark theme for my system, namely Breeze Dark, the [prefers-color-scheme that Carl implemented recently][6] was automatically set to dark mode, which is really neat. If for any reason you want to see how it looks like with the opposite theme to what you're currently using, this can be overriden: on Firefox you can go to about:config and create a new integer modifier named ui.systemUsesDarkTheme and set its values as 1 or 0, and on Chrome you can press F12 to open the developer console, click the hamburger menu on the lower section, select the Rendering tab and change "Emulate CSS media feature prefers-color-scheme" to what you wish.

The current procedure for porting websites involves ruby, Jekyll, HTML, PHP and Markdown, as detailed below.

## The procedure – Preparing your environment {#procedure}

The project I recently helped port to Markdown was KMyMoney, whose code and commits [can be seen here][7]. It hasn't been fully ported yet because of complicated code I cannot even grasp properly which Carl will change himself. I am not that knowledgeable about HTML, PHP and whatnot, you see; I've learned the bare minimum years ago. This does not hinder me from contributing, however.

Initially I had no idea that webpages could be made with Markdown, and so I asked Carl how the webpage would end up looking like once it gets ported, and so he pointed me to an already ported website, namely [konsole.kde.org][8], where you can see a clean example [right here][9].

It's nice and proper, ain't that right?

For setting the actual environment, on Ubuntu-based systems such as mine (KDE Neon Unstable), you only need to `sudo apt install ruby ruby2.5-dev git g++`.

The packages `ruby`, `ruby2.5-dev` and `g++` will be needed for compiling the website (yeah, I know, I had no idea compiling websites was a thing!) and checking how it looks with Markdown as well as for installing Jekyll, whereas `git` serves to download the webpage itself.

The next step is then to simply download the webpage with git. In this case, Carl created a new branch called jekyll to include the new layout theme without affecting the current website, so you only need to do `git clone git://anongit.kde.org/websites/kmymoney-org.git --branch jekyll`. For those who are not very acquainted with git, in this case, git is the program that downloads repositories, clone is the download command itself, git://anongit.kde.org/websites/kmymoney-org.git is the repository itself and \-\-branch jekyll asks for the jekyll branch Carl made.

The next step is this one: `gem install bundler jekyll --user-install`. Gem is a ruby command much akin to package repositories, and works parallel to cpan, pip and others. With this command, the ruby-bundler package will attempt to install jekyll, the theming engine for website creation, locally on your machine.

You should then move to the new kmymoney-org folder that should now exist on your home folder, so `cd kmymoney-org`.

The next command, `bundle install --path vendor/bundle`, will check if a file named Gemfile exists in your current folder, and if so, it will install all proper dependencies for the website to run properly, including KDE's Aether Jekyll theme configuration.

The last command is `bundle exec jekyll serve`. This effectively compiles the website, making it available locally on your browser. After running it, you should get a message like this:

```
Server address: http://127.0.0.1:4000/
Server running… press ctrl-c to stop.
```

Which means the website should now be reachable by opening your browser and going to the server address mentioned, 127.0.0.1:4000 (or localhost:4000, which is easier to type).

## Work itself — Porting PHP to Markdown

The first file I managed to port was download.md, however, the first to be pushed by Carl was appimage.md, which would replace appimage.php. First, open it and "Save as…" appimage.md so that you have a Markdown copy. Let's take a look on how it is:

```html
<?php
    $page_title = "the BEST Personal Finance Manager for FREE Users, full stop.";
    include ( "header.inc" );
?>

<h1>How to install and use the AppImage version?</h1>

<p>
For Linux users, who want to use a newer version than is available in their distro repository, we offer an AppImage version which reflects the current stable version including the latest fixes. It is build on a daily basis straight from the source.
</p>
<p>
Here's how you install and run it:
</p>

<ul style="width:60%;">
  <li>Download the file from <a href="https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/">https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/</a> to any location you like.</li>
  <li>Change into the directory where you downloaded the file.</li>
  <li>Make the file executable with <b>chmod +x <i>name-of-downloaded-file</i></b>.</li>
  <li>Execute the file with <b>./<i>name-of-downloaded-file</i></b>.</li>
</ul>

<p>
For the very adventurous Linux user, there is an AppImage with the latest development version at <a href="https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/">https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/</a>.
</p>

  <?php
  include("footer.inc");
  ?>
```

Since the commands for preparation might not work with the current repository (which is up-to-date), you can simply copy all of this code into a file and name it appimage.php for training if you wish.

Let's go through each step individually, and it should be easier for us to port it. In the header section:

```html
<?php
    $page_title = "the BEST Personal Finance Manager for FREE Users, full stop.";
    include ( "header.inc" );
?>
```

The only thing we need to keep is "the BEST Personal Finance Manager for FREE Users, full stop.", so we can ignore most of it and replace it like so:

```markdown
---
title: "the BEST Personal Finance Manager for FREE Users, full stop."
layout: page
---
```

This will allow the Jekyll theme to be applied on the header of the website automatically, so there's no need to include an extra file for the header like the PHP code does (`include ( "header.inc" )`).

If you save your file and go to `localhost:4000/appimage`, you should now be able to see how your Markdown page looks like! The command we ran before, `bundle exec jekyll serve`, updates the webpage automatically as you change its content. It's pretty handy to verify if you did something wrong or not.

The next part is very simple:

```html
<h1>How to install and use the AppImage version?</h1>
```

Since `<h1>` and `</h1>` are the tags used for Heading/Headers, that is, the text style used for titles, then we search for the Markdown equivalent. A quick Google or DDG search shows that `<h1>` can be done with `#` and a space before a title, so:

```markdown
# How to install and use the AppImage version?
```

The next step is even easier:

```html
<p>
For Linux users, who want to use a newer version than is available in their distro repository, we offer an AppImage version which reflects the current stable version including the latest fixes. It is build on a daily basis straight from the source.
</p>
```

Here, `<p>` stands for Paragraph, so just normal text. In Markdown, there's no marker for that, you just need to remove the `<p>` and `</p>` tags.

(You are effectively free to improve the text as you port it, in fact. I just noticed while writing this post that `build` is a typo, it should be `built`. I reported this to Carl just now.)

```markdown
For Linux users, who want to use a newer version than is available in their distro repository, we offer an AppImage version which reflects the current stable version including the latest fixes. It is built on a daily basis straight from the source.
```

The next part should be scary at first, but it's really not.

```html
<ul style="width:60%;">
  <li>Download the file from <a href="https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/">https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/</a> to any location you like.</li>
  <li>Change into the directory where you downloaded the file.</li>
  <li>Make the file executable with <b>chmod +x <i>name-of-downloaded-file</i></b>.</li>
  <li>Execute the file with <b>./<i>name-of-downloaded-file</i></b>.</li>
</ul>
```

The tag <ul> stands for Unordered List, that is, it is a bulleted list, so anything that goes inside it will be automatically bulleted. The tag <li> stands for List Item, and it is where the content for each item in the list should be. See how it looks:

{{< figure src="/2019/12/UnorderedListKMyMoney.png" caption="Figure 3 – Unordered List." >}}

The equivalent to that in Markdown is done by adding a simple `*` followed by a space and the content of each list item. We don't need to care about `style="width:60%;"` since Markdown will format it automatically in the desired way.

The tag `<a href="link here">Sample Text</a>` creates hypertext, that is, a link to another website, which in Figure 3 is rendered in blue. There are several Markdown equivalents for this, but the one I prefer is that one with reverse order, that is, `[Sample Text](link here)`. It is just a matter of preference, I like how it looks.

This line is a bit more complicated:

```html
Make the file executable with <b>chmod +x <i>name-of-downloaded-file</i></b>.
```

Any text enclosed within `<b>` and `</b>` will become bold, and any text enclosed within `<i>` and `</i>` will be in italics in HTML/PHP. So, in this line, `chmod +x` is bold because it is inside `<b></b>`, and `name-of-download-file` is both bold and in italics since it's inside `<i></i>`, which itself is inside `<b></b>`.

In Markdown, enclosing text between `*` will render it in italics, `**` in bold, and \*** in italics and bold. Since in this line I wanted to give emphasis to `chmod +x` and `name-of-downloaded-file` so as to make both different from the previous text, I decided to make `chmod +x` both bold and italics and `name-of-download-file` only in italics. Therefore, I enclosed the first in `***` and the latter in `*`, like so:

```bash
**chmod +x** **_name-of-downloaded-file_**
```

So the result is this:

```markdown
* Download the file from [https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/](https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/) to any location you like.
* Change into the directory where you downloaded the file.
* Make the file executable with **chmod +x** **_name-of-downloaded-file_**.
* Execute the file with name-of-downloaded-file.
```

The last part of the file should be pretty simple.

```php
</p>
  <?php
  include("footer.inc");
  ?>
```

You can simply remove it; it will not be needed for Markdown because the Jekyll theme already takes care of the footer. You know, the part that includes adorable icons of Kiki and Konqi, and the rest of the page below them!

Alas, the result is this:

```markdown
---
title: "the BEST Personal Finance Manager for FREE Users, full stop."
layout: page
---

# How to install and use the AppImage version?
For Linux users, who want to use a newer version than is available in their distro repository, we offer an AppImage version which reflects the current stable version including the latest fixes. It is built on a daily basis straight from the source.

Here's how you install and run it:

* Download the file from [https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/](https://binary-factory.kde.org/job/KMyMoney_Stable_Appimage_Build/) to any location you like.
* Change into the directory where you downloaded the file.
* Make the file executable with ***chmod +x*** **_name-of-downloaded-file_**.
* Execute the file with ***name-of-downloaded-file***.

For the very adventurous Linux user, there is an AppImage with the latest development version at [https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/](https://binary-factory.kde.org/job/KMyMoney_Nightly_Appimage_Build/).
```

Don't forget to check if your file was saved with the .md extension, of course!

And that's it! It's not that complicated, is it? It was just fairly extensive since I wanted to explain each step properly for those who are not very acquainted with HTML/PHP and Markdown.

When your new Markdown webpage is ready, send it to Carl so he can push it to the repository.

The process of porting websites like this one is fairly easy I'd say. There will be several HTML and PHP files that are too complicated for newcomers, but that is not an issue: more experienced contributors like Carl and Adrián will be able to fix them instead. It really helps them if other contributors can do these small tasks, since there's A LOT of content to be ported.

I will likely create another blog post about porting websites to Markdown in the future, targeting some more complex cases and explaining how to edit the header files directly through _config.yml! Nothing too complicated, of course. But that will have to wait for now.

If you're interested in helping now that you know it's relatively easy, you can take a look on the [Promo team wiki page][10] and the [Get Involved wiki page][11], contact us initially through the [KDE Welcome Matrix room][12] or directly through [Telegram][13], [Matrix][14] or [IRC][15]! There's now also [a Telegram group dedicated to KDE Websites][16]. You can also join our [Mailing List][17].

 [1]: https://rabbitictranslator.com/wordpress/index.php/category/keyboard-shortcuts-analysis/
 [2]: https://planet.kde.org/
 [3]: https://phabricator.kde.org/T10827
 [4]: #procedure
 [5]: https://carlschwan.eu/2019/08/29/more-sites-update.html
 [6]: https://carlschwan.eu/2019/10/31/happy-halloween.html
 [7]: https://cgit.kde.org/websites/kmymoney-org.git/
 [8]: https://konsole.kde.org
 [9]: https://cgit.kde.org/websites/konsole-kde-org.git/tree/get-involved.md?id=82157762eaa5ba5cf5caa481862301a613e318ee
 [10]: https://community.kde.org/Promo#How_you_can_join_our_team
 [11]: https://community.kde.org/KDE.org
 [12]: https://webchat.kde.org/#/room/#kde-welcome:kde.org
 [13]: https://t.me/joinchat/AEyx-0O8HKlHV7Cg7ZoSyA
 [14]: https://community.kde.org/Matrix
 [15]: http://webchat.freenode.net/?channels=kde-promo
 [16]: tg://join?invite=GNmHLhM4__drDjjVIRfDew
 [17]: https://mail.kde.org/mailman/listinfo/kde-promo
