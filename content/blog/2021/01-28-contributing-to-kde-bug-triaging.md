---
title: Contributing to KDE is easier than you think – Bug triaging
author: Blumen Herzenschein
date: 2021-01-28
url: bug-triaging
aliases: /wordpress/index.php/2021/01/28/contributing-to-kde-bug-triaging/
tags:
  - Planet KDE
---
Today, 2021-01-28, is the [Plasma Beta Review Day][1] for Plasma 5.21, that is to say, Plasma 5.20.90. Right now it's a bit after 2 a.m., so after this I'm going to bed so I can be present later.

This month I've mostly been enjoying my post-job vacation as last year I was bordering burnout. As such I didn't help much.

Before bed I'll be providing a few things I've learned about triaging, though. While this blog post isn't specifically about the Beta Review Day, this should make the general bug triaging process clearer for you, making it quite timely.

## Goals

The goals of bug triaging are simple:

  * Making things easier for developers
  * Making things faster for developers
  * Making things clearer for developers

Ultimately, a bug triager assists developers in solving bugs. As such, knowing how to code is not a requirement, as a bug triager isn't necessarily a developer. This also means that decreasing the number of bug reports shouldn't be your goal, and you shouldn't close reports for the sake having less reports. You need to assess their validity.

Another consequence of the goals is that, since they are for the developers' sake, there's a bit less value in statuses whenever they don't serve for organization. What I mean by this specifically is that **you should not obsess** whether a bug report should be marked as CONFIRMED every time for the sake of marking; REPORTED bugs can be fixed directly by the devs just as much as CONFIRMED bugs, what changes is whether someone has tested and verified that the issue actually exists. Sometimes the developers may find the issue by themselves after reading the report and apply the fix directly.

## Knowledge

While programming skills are not a requirement, they are a bonus and, together with other skills, allow triagers to do much more.

  * Knowing how to code allows you to fix the bug yourself
  * Knowing how programs are written allows to detect which part of the code is faulty in backtraces
  * Knowing how to send commits allows to make simple edits despite not knowing how to code
  * Knowing how to use debug tools allows to instruct users to provide better information
  * Knowing how to use a program allows to determine its expected behavior
  * Knowing many programs allows for a better understanding of feature requests and to ask users to provide more information
  * Knowing more than one language allows you to assist non-English speaking bug reporters
  * Knowing English well allows you to better understand what the bug reporter is trying to convey

## What do?

Knowing the goals of bug triaging makes choosing what to do simple. You just need to know what you _can_ do.

Let's see what can be done for each goal.

  * Making things easier for developers

While this goal is also the end result of bug triaging, it can refer to very specific activities.

Bug testing is a major step in this. **You can verify the conditions to reproduce a specific bug.** By identifying the proper means to reproduce of a bug, you allow the developer to apply less energy on testing and more on bug fixing. Clearly, this ties in with **testing whether a bug is reproducible**, in which case you'd inform its reproducibility status in the comments.

Bug fixing is rarer, but much effective, regardless of level of difficulty. **You can fix a small bug or make a simple string change.** This way, the main developers of that application will be able to focus on the larger things: critical bugfixes or reworking the code. **You can also fix major bugs that personally affect you, if you're able to, or attempt a patch.** At that point, you're not just a triager, you'll be a developer too. Both things are great to motivate you to contribute to the project at large! And sharing the workload between more devs makes things easier for everyone.

If you know the difference between what each component does, or if you know how to gather info about the bug, **you can determine which component is faulty, and whether it's upstream or downstream.** This is often a hypothesis that you can learn by doing and by being curious; often you'll be mistaken; but by sharing your findings, developers will have an easier time fixing things, as they will be more certain of where to seek for answers or where to refer the bug reporter. Even better, **you can move the bug report to where it should be** so only the correct developers will see it.

  * Making things faster for developers

**You can summarize what the reporter is trying to say.** I'm personally the type to be over-completionist when reporting bugs and to need validation before reporting due to my history with anxiety; a lot of times in the past my over-zealousness paired with self-imposed pressure over failure made me write overly complicated bug reports, so I know how it is. It's more common than it looks like. At the end I notice my own mistakes and have a need to fix them, which further complicates things. Summarizing helps people like me to recollect and see simpler ways of being useful, and it helps developers know what the issue is faster.

The opposite personality type is also pretty common, namely that of being under-informative and/or initially uncooperative. In such cases, **you can ask for more information.** Most people you'll find do not report information at first because they either do not know what to report or because they expect to be instructed on what to do. In that case, **you can instruct them on what commands to run**, of course. **If you mark a bug as NEEDSINFO**, developers can postpone bugs that are not actionable, so they can fix other bugs that are.

If you can reproduce the same issue on your machine, **you can provide the missing information yourself**, in which case you'll be making the whole procedure even faster.

In the case of KDE, we have a really high number of bug reports, many of which are old, really old: over 5 years. It is an extremely time-consuming process for developers to verify whether an ancient bug is valid or not, and most often it's not that worth it, because software has changed so much that most bugs will be irrelevant. Thus, **you can test the validity of old bug reports, assess their possibility of being currently irrelevant, and ask whether it is still reproducible.** This is a particularly easy task for new bug triagers and it doesn't require the most up-to-date KDE software to do so. Moreover, **if you mark it as NEEDSINFO WAITINGFORINFO**, our bugzilla bot, the Bugzilla Janitor, will automatically send a message in 15 and 30 days to remind the user to provide required info. If nothing is provided, the bug is autoclosed as RESOLVED FIXED. For old bugs, the majority of time you'll be marking it like so, but mind their validity. If you're sure it's reproducible, change it to CONFIRMED and mention its reproducibility; if you're unsure, keep its original status and mention its reproducibility.

Another major task in bug triaging, but which I'm not as experienced with, is that **you can find duplicates.** This is easiest with crashes: by checking the first lines of the main thread in a backtrace (introduced by the line stating KCrash Handler if it was reported via DrKonqi), copying it and pasting in the bugzilla search. Non-crashes are harder because they require extensive reading of bug reports and determining whether both reports are referring to the same issue, or whether a given issue will be solved by fixing another.

  * Making things clearer for developers

While summarizing is great for letting developers know what's the issue faster, **by translating what the user says into simpler words**, it will be easier for developers to understand what the actual issue is. Those are two sides of the same coin.

This also applies to confusing bug report names. If you can understand what the actual issue is about, **you can change the bug report name to be more precise.** Done in a clear manner, developers will immediately know what needs to be fixed.

If you know the non-English language the user is speaking, **you can help translate it to the general language most developers will know, namely English.** This is really rare, as it's unlikely the reporter will use their mother language for this, but it does happen. Less rare is when the user doesn't fare well with English, in which case, if you're able to identify the user's language or the language pair's common issues, **you can ask whether the user means this or that.** More often than not, language speakers will be better at reading and listening than writing and speaking, and by seeing well expressed thoughts in English they will know whether you're right or not.

If the user is being highly uncooperative or even hostile, you should have tough skin: **you can ask to focus on the issue at hand**, instead of engaging at the same level of hostility. Of course, it doesn't mean literally just asking, but actually conducing the conversation towards it. Emotional and ill-intended discussions only serve to obscure the issue; thus, focusing on technical issues clarifies the issue. Even hostile users may be reporting legitimate issues. Although, if they cease to cooperate further with the report and their bug had low validity, **you can mark it as WORKSFORME**, and a future user who really feels that bug had high validity should either reopen it or create a new one.

## How to?

Let's summarize our list of things you can do. You can:

  * **verify the conditions to reproduce a specific bug**
  * **test whether a bug is reproducible**
  * **fix a small bug or make a simple string change**
  * **fix major bugs that personally affect you or attempt a patch**
  * **determine which component is faulty**
  * **determine whether the issue is upstream or downstream**
  * **move the bug report to where it should be**
  * **summarize what the reporter is trying to say**
  * **ask for more information**
  * **instruct users on what commands to run**
  * **mark a bug as NEEDSINFO**
  * **provide the missing information yourself**
  * **test the validity of old bug reports**
  * **assess the possibility of old reports being currently irrelevant**
  * **ask whether an old report is still reproducible**
  * **mark it as NEEDSINFO WAITINGFORINFO**
  * **translate what the user says into simpler words**
  * **change the bug report name to be more precise**
  * **help translate it to English**
  * **ask whether the user means this or that**
  * **ask to focus on the issue at hand**
  * **mark it as WORKSFORME**

Let's see. Out of these, which are good for people who want to get into bug triaging for the first time without having specific skills?

  * **verify the conditions to reproduce a specific bug**
  * **test whether a bug is reproducible**
  * **summarize what the reporter is trying to say**
  * **ask for more information**
  * **provide the missing information yourself**
  * **test the validity of old bug reports**
  * **ask whether an old report is still reproducible and mark it as NEEDSINFO WAITINGFORINFO**
  * **find duplicates**
  * **translate what the user says into simpler words**
  * **change the bug report name to be more precise**
  * **ask whether the user means this or that**
  * **ask to focus on the issue at hand**

See? Quite a bit to do. Let's synthesize this even further.

Which of these are the simplest, most common tasks you can start working on?

  * **verify the conditions to reproduce a specific bug**
  * **test whether a bug is reproducible**
  * **ask for more information**
  * **test the validity of old bug reports**
  * **ask whether an old report is still reproducible and mark it as NEEDSINFO WAITINGFORINFO**
  * **find duplicates**
  * **ask whether the user means this or that**

What is required for each of these tasks?

To verify the conditions to reproduce a specific bug, the bug reporter must have provided their means to reproduce, which you'll use to test. When testing, you should be wary of possible ways to trigger the same issue under different conditions so as to determine the underlying cause. For instance, if a bug is caused by interacting with a specific widget, is it possible that only certain types of interaction cause the issue? Is it possible that some local change is inadvertently causing the issue?

To test whether a bug is reproducible does not necessarily mean verifying the conditions to reproduce. It merely means that you followed the means to reproduce provided by the user and your findings indicate whether it is reproducible or not; this will most likely be your first introduction to triaging. If you were able to reproduce the issue but are unsure whether it's always reproducible or if it's caused by a local change, simply write a comment stating this. If you're pretty sure the issue is generally reproducible and you have gathered a bit of experience, you may mark the report as CONFIRMED. If your testing results in you being unable to reproduce this issue, simply comment your testing and how you did it without changing the report status.

Asking for more information is quite simple and straightforward, but whether you should change a report's status can be a bit confusing. In doubt, do not change its status. If you're sure the bug report was lacking critical information for its resolution, you should mark it as NEEDSINFO WAITINGFORINFO for the bot to be called. If info is provided, change it to its original status, which is usually REPORTED. If info is not provided, the bot will autoclose the issue. This won't be a problem if the issue was not actionable in the first place. Feature requests should most likely be kept with their original status.

Testing the validity of old bug reports is also a fairly approachable task for beginners. Use the advanced search to seek oldest bugs and set the desired range of years in **Search By Change History between**. You can use dates like 5y, which means 5 years. If you're sure the bug report is definitely invalid nowadays, you may write a comment mentioning this and mark it as RESOLVED FIXED. If you're unsure, just leave a comment and wait for another triage to confirm the same results as you. Your comment will update its modified date anyway.

Asking whether an old report is reproducible also goes hand in hand with testing whether an old report is valid, but it's much easier. Simply use the provided means to reproduce to test it first; if it's reproducible, you've found an old gem that needs fixing ASAP and is actionable, so you can safely mark it as CONFIRMED to get high priority over REPORTED. If it's not reproducible, mention this and ask the user whether it's still reproducible (just in case), then mark it as NEEDSINFO WAITINGFORINFO. It's highly unlikely the user will respond, but then again, it's also highly unlikely that an issue that's unreproducible on your current machine will be valid. Of course, this highly depends on the number of years gone by: if it's higher than three years, it's likely invalid, if it's lower than three years, you should be wary of its possible validity.

As aforementioned, finding duplicates of crashes are easier than general reports, but the actual actions taken by you are the same. If you're unsure whether a crash is a duplicate, you can simply mention this in the comments and wait for someone to confirm your suspicions; if you're absolutely sure, mark it as DUPLICATE and wait. In cases of certainty it's more practical to mark it and make questions later. It's still possible that your certainty of that being a duplicate is incorrect, and that will be stated either by another more experienced triager, by the developers, or by users themselves. Have tough skin and admit whenever you notice you were in fact incorrect about the duplicate status.

Asking what the user means doesn't require much energy nor is it complicated. Formulate a hypothesis on what the user means, and mention whether what you said was correct, and if incorrect, ask the user to explain the issue once more. If after the issue is explained it is reproducible or considered of high validity, you may mark it as CONFIRMED, otherwise you should keep its status as REPORTED.

## Join the Plasma Beta Review Day today…;

While this blog post was more about bug triaging in general (and so it included things like triaging old bug reports), this should make the general process of triaging clear, which is helpful for Beta Review Days.

The Plasma Beta Review Day serves specifically to detect regressions in Plasma, more specifically Plasmashell, KWin, Plasma Mobile, Discover and System Monitor. It is an important part in providing a better experience to users before official releases.

Bug testers, triagers and Plasma developers will be in the [official BigBlueButton room][2] to assist each other to achieve this goal.

You can help us by simply downloading a [live image][3] and using that for bug testing and triaging. More daring adventurers would install those systems directly on their computers, but that's not required.

More technical users might want to take a look at the new instructions on [debugging Plasmashell][4] and [debugging KWin][5].

## …;and join the KDE Bugsquad!

You can continue to help with bug testing and triaging after the Beta Review Day ends. Just join us at the [KDE Bugs Matrix channel][6] after having taken a look at the [general guidelines][7]. There we can assist you however we can in your training—as long as you interact with us, of course! Do not refrain from mentioning your doubts, and please be patient when waiting for responses.

 [1]: https://community.kde.org/Schedules/Plasma_5_BetaReviewDay
 [2]: https://meet.kde.org/b/dav-gmr-qzx
 [3]: https://community.kde.org/Plasma/Live_Images
 [4]: https://community.kde.org/Plasma/Debugging
 [5]: https://community.kde.org/KWin/Debugging
 [6]: https://webchat.kde.org/#/room/#kde-bugs:kde.org
 [7]: https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging
