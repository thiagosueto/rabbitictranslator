---
title: Short Fluff – Manually installing plugins for the Ardour user flatpak
author: Blumen Herzenschein
date: 2021-04-02
url: ardour-flatpak
aliases: /wordpress/index.php/2021/04/02/short-fluff-plugins-ardour-user-flatpak/
tags:
  - Fluff
---
These days I was trying to learn Ardour because I want to make a video for Subtitle Composer using Kdenlive. I'm more comfortable with voice-over rather than improvising with my speech skills, so OBS Studio, while a great tool for recording which has noise reduction by default, wouldn't suit me that well for the task.

I didn't like the interface of Audacity much, even with the [Clean Dark][1] theme which seemingly integrates better with Breeze Dark, and found Ardour more appealing. I installed it as a user flatpak (as I prefer to install all my flatpaks without root privileges) and was testing it, but it was lacking a noise reduction plugin. I quickly encountered [noise-repellent][2], which seemed to work nicely from what I saw in [a video by unfa][3]. Compiling it from source and setting the path in Ardour, however, wouldn't work, as it installs to a **_root_** folder: `/usr/lib64/lv2`. My flatpak didn't have permissions for that, so the plugin couldn't be found.

First thing I did was search flathub for LinuxAudio.Plugin, trying to find another plugin for noise reduction (given that one of the Ardour plugin tags showing up in the available list was noise-reduction). Installed several, Ardour would detect them fine, but I found no noise reduction plugin. At a given point I had countless plugins which I wouldn't make use of anyway, as I'm still learning and my needs are probably minimal.

Then I tried to point Ardour to the compiled files in the git cloned folder. It didn't work for some reason. I don't really get why: Ardour did have access to my home folder.

Alright then, let's take a look at where those plugins were being installed. They were considered part of runtimes, so they were stored in `~/.local/share/flatpak/runtime`. Inside, noticing the folder structure: `~/.local/share/flatpak/runtime/org.freedesktop.LinuxAudio.Plugins.PluginName/x86_64/versionNo/appID/files{lib,lv2,lxvst}` , it's a pretty simple way to handle plugins: inside the correct folder (such as lv2) there's a folder containing a `manifest.ttl`, a `pluginname.so` and a `pluginname.ttl` for each plugin. Pretty much the three files that are created when compiling noise-repellent. Alright, let's install it there.

Out of laziness (or efficiency, if you will), I compiled and installed noise-repellent to the very git cloned folder (`meson build --buildtype release --prefix /home/myusername/gitstuff/noise-repellent/nrepelbuild`), and simply copied the files to the folder of some plugin I downloaded from flathub. Way faster to do that clicking in Dolphin than typing a long-ass path in the terminal. It worked! Ardour would find the plugin correctly.

But if I were to uninstall that random flathub plugin eventually, noise-repellent would be removed as well. That's a no-go, I wanted something more like: if I ever want to uninstall Ardour, then that plugin goes with it.

I took a look at the default path that Ardour uses to search for plugins, namely in `Edit > Preferences > Plugins > Linux VST2 Path`, and as expected from a flatpak application, it pointed to its own build folder, `/app/extensions/Plugins/lxvst`. The folder `extensions/Plugins` is what I wanted, then.

Taking a look at the user flatpak folder structure for Ardour, the `/app` folder as perceived by the application was actually set to `~/.local/share/flatpak/app/org.ardour.Ardour/x86_64/stable/ardourID/files`. Inside there was a folder `extensions/Plugins`, where I created an lv2 folder and added the noise-repellent plugin. It worked! And now my plugin list was pretty clean, containing just the default Ardour plugins and noise-repellent.

It worked somewhat nicer than Audacity's noise reduction plugin in fact, but you gotta praise the fact that Audacity at least *comes* with this plugin by default. From what I've seen, noise reduction is one of the most searched queries when it comes to recording audio, which I believe denotes its importance well. I'd like to see noise-repellent being easier to install, like becoming available as a LinuxAudio plugin on flathub. It would be much more convenient.

Pretty fun to learn how to do this though, even if just because I was being stubborn on the matter of having only user flatpaks. I figured I'd write a short post about it so it would be publicly documented and anyone else interested in doing the same—however unlikely that would be—would find it via search.

 [1]: https://www.deviantart.com/podel1/art/Audacity-Clean-Dark-Theme-1-3-by-Podel-715604676
 [2]: https://github.com/lucianodato/noise-repellent
 [3]: https://youtu.be/LeKyGoAmbFE
