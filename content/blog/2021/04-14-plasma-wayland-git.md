---
title: My current Plasma Wayland from git
author: Blumen Herzenschein
date: 2021-04-14
url: plasma-wayland-git
aliases: /wordpress/index.php/2021/04/14/plasma-wayland-git/
tags:
  - Planet KDE
  - Translation
  - Fluff
---

However much distress the current scenario and some personal stuff might cause, I've really been having fun with my laptop. Running everything on bleeding edge is exciting: kernel 5.12.rc6, openSUSE Tumbleweed with Plasma built daily from master (so openSUSE Krypton), using only the Wayland session, switching entirely to pipewire and so on. I figured I might share what I have set up for those interested, while providing some workarounds and sharing some experiences.

## Krypton

My general distro of choice (openSUSE) offers quite a few conveniences. YaST, the main "hub" software for managing the system, is a power beast. I learned quite a bit with its sysconfig manager, being able to edit kernel and boot settings, snapshots, journal, network, and update my system is handy.

The maintainers add a ton of patches (pretty much like Fedora) to ensure things run fine. For instance, [plasma5-workspace][1] is [patched to make GTK decorations are shown correctly on Wayland][2]. You don't really need to setup `MOZ_ENABLE_WAYLAND=1` for Firefox to work on Wayland either.

For bug testers YaST Software has a neat feature that its command line equivalent Zypper doesn't: being able to install all the debug symbol packages respective to the **_current_** packages you already have on your system. It lies on the **_Extras_** menu, and if you've ever tried to create backtraces and don't care about having a huge number of packages installed, you'll probably appreciate it. As far as I know, this is the only distro and software that does that, and I'd have liked to see it on every distribution, to be honest.

{{< figure src="/2021/04/debuginfo.png" caption="Where that feature is located." >}}

A mildly infuriating issue that only occurs on openSUSE is that Discover doesn't work well with zypper. Super annoying. There's apparently some long history of the libzypp backend for PackageKit being broken.

I also disliked the (prior) defaults to evdev/synaptics. Yes, I actually like libinput as synaptics was buggy and libinput just felt right even on default settings. It was one of my main motivations to try the Wayland session before I learned I could force its priority by changing the version number of the conf files under `/usr/share/X11/xorg.conf.d/` (higher number = higher priority). As a bonus, libinput works nicely for touchpad gestures. It seems that it is no longer the case now, and openSUSE defaults to libinput, but I might be wrong.

I am fine with the current two Plasma Wayland sessions (Wayland and Full Wayland), but in my opinion openSUSE could rely entirely on having just a default full Wayland session once Electron apps stop requiring `GDK_BACKEND=x11` with [Electron 12][3]. So probably by the time Plasma 5.22 is well established, or to be on the safer side, maybe Plasma 5.23.

As for my specific distro of choice ([openSUSE Krypton][4]), the cool thing about it is **_how safe it is_** to run Plasma from git.

OpenSUSE Krypton is essentially openSUSE Tumbleweed with KDE Unstable repositories. That means it includes `repo-oss`, `repo-non-oss` and `repo-update`, the three main repositories from Tumbleweed, as well as KDE Apps, Extra, Frameworks and Qt which are from KDE Unstable. If a package version is higher in Krypton (which is pretty much always the case in relation to the openSUSE repos), then a `zypper dup` (which installs the latest functional snapshot) upgrades all packages to KDE Unstable.

If you take a look at `zypper repos` you'll see the installed repositories. They have both numbers and aliases, so they can be called easily if needed, and with `zypper repos --priority` you can see which repos have priority (lower number = higher priority) over each other (so they are preferred even if a low priority repo has more up-to-date versions).

The nice thing about zypper that makes Krypton safer is how easy it is to just fallback to Tumbleweed. You can just `zypper dup --from repo-oss --from repo-non-oss --from repo-update`.  
Assuming that the Tumbleweed repos are assigned the numbers 1, 2, 3, 4, you can simply `zypper dup --from 1 --from 2 --from 3 --from 4` as well.

To return to Krypton, assuming that the KDE Unstable repos have been aliased to something like `kdeu-apps`, `kdeu-extra`, `kdeu-frmwrks` and `kdeu-qt`, and that they are numbered 5, 6, 7, 8 respectively, you can do something as simple as `zypper dup --from kdeu-apps --from kdeu-extra --from 7 --from 8`.

{{< figure src="/2021/04/switchpackages-1.png" caption="Switch system packages is the equivalent to zypper's \-\-from." >}}

You can also fallback specific software instead of switching all your installed packages. At the beginning, Elisa was a bit unstable as a git package, so I'd occasionally `zypper install --from repo-oss elisa`. It would then work perfectly. I also had a specific command for the entire Akonadi/Kontact stack just in case, but since mid December I saw no more need for it, as KMail, my most used KDE PIM app, hasn't broken in a long while even on git, and after [bug 397825][5] it worked perfectly on Wayland for me.

## Devices - Mouse

My preferred mouse is a [Logitech Trackman Marble][6], so a trackball. It's well supported on Linux and it's ambidextrous, which is nice as using the non-dominant hand is a great way to balance the damage inflicted on your hands by continuous hours of mouse usage. It's well known that I'm an ergonomics aficionado who's very concerned with RSI. If I had the money I'd buy an [Ergodox EZ][7] or something of the sort too.

On X11, regardless of whether you're using evdev or libinput, you'll need a Xorg conf file for the Trackman Marble to use scrolling. Annoying as it should ideally be handled by GUI, but the Arch wiki has easy templates for both: here's one for [evdev][8] and one for [libinput][9].

I added a file `/usr/share/X11/xorg.conf.d/90-marble.conf` containing the following contents:

```xorg.conf
Section "InputClass"
 Identifier   "Marble Mouse"
 MatchProduct "Logitech USB Trackball"
 Driver       "libinput"
 Option       "ScrollMethod"    "button"
 Option       "ScrollButton"    "8"
 Option       "MiddleEmulation" "true"
EndSection

Section "InputClass"
       Identifier  "Marble Mouse Evdev"
       MatchProduct "Logitech USB Trackball"
       Option "EmulateWheel" "true"
       Option "EmulateWheelButton" "8"
       Option "XAxisMapping" "6 7"
       Option "Emulate3Buttons" "true"
EndSection
```

For Plasma Wayland, you'll need to use qdbus (in openSUSE it's qdbus-qt5) to setup scrolling instead of a conf file. It's a libinput thing, but since input is handled by KWin, it needs to be setup at the KWin level. The instructions are mentioned [in the Arch wiki][10], but it was probably traced from [when I asked Martin about it][11], as I didn't find any other resource on the matter that was older than that.

If you ever want to switch the side button to the other side, you'll need to know what its number is. In my case, it's 275 for the left one, 276 for the right one. How do I know? I open KRunner and type KWin. I'll get the option Open KWin Debug Console. There you'll find not only a way to easily determine whether windows are running on XWayland or Wayland, but you'll find a [xev][12]-like tool where you just need to press each key to see their Qt keycode.

{{< figure src="/2021/04/kwinwindows.png" caption="The xev-like utility." >}}

## Devices - Headphone

As for other pieces of hardware, I have a [JBL T450BT][14]. It's a bluetooth headset with three main profiles: A2DP (SBC), HSP/HFP, and A2DP (SBC-XQ). With pulseaudio-bluetooth I was affected by an issue where my HSP/HFP would [simply vanish][15]. As you can see from the link, it wasn't just me. In addition, the SBC-XQ profile wasn't available at all.

With pipewire-pulseaudio it simply worked out of the box and reliably. Moreover, editing `/etc/pipewire/media-session.d/bluez-monitor.conf` allowed me to use A2DP SBC-XQ.

{{< figure src="/2021/04/jblcodecs.png" >}}

Here SBC is enabled [with pipewire][16], but not yet SBC-XQ. Note the new interface to switch profiles.

One problem it had that was consistent with both pulseaudio and pipewire was the awful quality of its HSP/HFP profile, which would even go so far as cut certain parts of my speech when recording. That is, I've heard, an issue with bluetooth on Linux concerning the lack of support of a hackish workaround done on Windows, OSX and Android to switch between high quality audio A2DP and low quality HSP/HFP, or something. I have no technical knowledge to further discuss this or even to ensure this is not FUD, so let's leave it there.

## Devices - Microphone

I bought myself a [BM-800 Condenser Microphone][17]. Unlike the linked product, the one I got wasn't branded, it came only with the microphone, the jack cable, the windscreen and a shock mount, which is fine as it was particularly cheap.

It works nicely, in fact. Way better than I expected for its price. However with it I noticed a major caveat of pipewire: its lack of an echo cancel module. It's in the [TODO list][18], which for me is unfortunate because this is **_one of the most important features I could ever wish for audio recording_**. You can save a huge amount of time editing audio by simply having cleaner audio in real time, and no other plugin managed to do the same on pipewire: neither [Cadmus][19], nor [Pulseeffects][20] (even with [this method][21]), nor [Noisetorch][22] worked. For audio editing, weirdly enough, the noise filter available in OBS Studio and based on [rnnoise][23] worked quite well, yet rnnoise itself didn't work quite as good, [noise-repellent][24] would come third place and next would be [speech-denoiser][25]. Audacity's built-in noise filter plugin was sufficient, but not good enough.

As you can guess, my search for denoise plugins was what resulted in [this blog post][26].

So while pipewire is effectively for practically everything, if you need realtime denoise, it's better for you to stay on pulseaudio for a while, at least before the pipewire devs implement it.

For comparison, take a look at how my voice sounds on pipewire and pulseaudio:

Pipewire without echo cancel
{{< audio src="/2021/04/pipewire.mp3" >}}

Pulseaudio with echo cancel
{{< audio src="/2021/04/pulseaudio.mp3" >}}


Night and day.

## Devices - Controller

I recently bought a [Dualsense][27], the PS5 controller, simply because [Sony decided to implement the drivers in the Linux kernel][28]. Thus I'm now using kernel 5.12.rc6. Kernel 5.12.rc5 would make SDDM not start for some reason. I was mostly ideologically motivated to buy it, and I personally do not regret it: it's great and works out of the box.

To pair it the first time it's a bit tedious: you have to very precisely press the Playstation button together with the Create button (the one on the upper left). To be sure you succeeded, you'll get six pulses with two spaces in between; that is to say: 11 11 11. If you get six in a row (111111), then you've accidentally tapped the Playstation button first, and it attempts to pair with an existing device.

Needless to say, after the initial pairing, to reconnect to my machine I simply needed to press the Playstation logo.

It's recognized perfectly on both pulseaudio-bluetooth and pipewire-pulse. It has an embedded touchpad in the middle section and it works well, but clicking is a bit of a chore since, you know, you have to actually press it down instead of tap. I haven't tested the microphone, but it doesn't seem to show up as a device when connected via bluetooth.

For Steam, some games wouldn't recognize it immediatelly, but going to Manage Game > Controller Options and selecting Force Off for Steam Input Per-Game Settings fixed this for some reason.

Here's an example of how well I managed to play with this controller on Plasma Wayland: Hollow Knight, my addiction. The details are in each video's description.

{{< youtube dxzXfIblzAg >}}

As for my general skill in Hollow Knight (for you to actually see how good that controller is in comparison), you can see these two older videos, one of which is private:

{{< youtube t_cOhew00Hk >}}

The real dodging battle starts at 1:10.

{{< youtube wKxyIyO-gYQ >}}

This is private because it’s pretty much fail. But it’s Wayland!

One issue I experienced with it was the fact that my WiFi would get slower or borderline unusable when connecting my Dualsense. This is due to [bluetooth driver coexistence][29], in my case with the ath9k drivers. My fix was [simple and needed][30], but it wasn't a permanent fix, as sometimes my WiFi still gets slow, just not all the time.

## OBS Studio

When attempting to use OBS Studio on Wayland, you need to be aware of three things: first, that you need [xdg-desktop-portal-kde][31]; that you need to have both pipewire and pipewire-media-session running; and that your OBS includes [feaneron's patches][32] ([obs-desktop-portal][33]).

The easy way to use OBS with the required patches is via [flathub-beta][34]. Mind this [PSA of mine][35] mentioning how to use different commits, and be absolutely sure that the required services are running on your distro. OpenSUSE recently had a bug [hindering pipewire-media-session from starting][36].

I noticed that purely recording the desktop (by this I mean no running program) would work fine on my laptop screen, but on my external monitor (LG 24MK430H 1920x1080 IPS Full HD Freesync), mouse movement would be laggy. This laggyness is completely erased when at least some bit of the OBS window is present in the external monitor. I have yet to report this, but it's really weird.

For desktop apps everything generally works fine with proper FPS.

The way I used to record my playthrough with Steam games on X11 was to use [SimpleScreenRecorder][37] and apply its [lib injection][38] to the game I wanted, and I'd be able to record it with less latency and higher quality than by simply capturing the screen or window. This is also possible on wlroots compositors with [SimpleScreenRecorder-wlroots][39]. On OBS Studio however this is not possible by default. In theory it should be possible with [obs-glcapture][40] or [obs-vkcapture][41], but on my machine the first would not compile and the latter would simply not work. Thus my latest gaming video (The Collector) is low quality, especially since my laptop is potato-ish.

One thing that must be noted is that the screen recording function records audio by default. This means you'll be able to add an audio filter without needing to add an audio input source, pretty handy. As mentioned before, the noise filter plugin based on rnnoise works great. I am, however, not yet comfortable with improv, I'd much prefer doing voice-over.

Adding multiple Wayland sources crashes OBS Studio, be wary.

From what I gather, the current state of Wayland screencasting is sore on Plasma. It seems third-party developers aren't targeting Plasma at all: GNOME has [Blue Recorder][42], [RecApp][43], [Peek][44] and [Kooha][45] despite already having a full-blown screen recording implementation, while Sway has [wlrobs][46], [wf-recorder][47] and [ssr-wlroots][39]. Plasma only has OBS Studio, and a first-party implementation is far from sight. 🙁

## What about work?

I work as a translator of academic papers first, an IT enthusiast second. I'm no longer employed and am now a freelancer taking care of my parents, but I've been using Wayland for over an year by now. And yes, Wayland has been usable for my workflow for more than a year already. It could have been for you too.

The main things you need to know when running a Wayland session is: ensure the programs you always need for work run; runs apps on XWayland when necessary.

wxWidgets apps like Audacity, Java apps like JOSM and SDL apps like Steam games default to XWayland, so you don't really need to do anything about those.

While perhaps with bugs, KDE and GTK applications should run natively on Wayland just fine.

[Firefox](https://wiki.archlinux.org/index.php/Firefox#Wayland) and [Chrome/ium](https://wiki.archlinux.org/index.php/chromium_#Native_Wayland_support) use their own toolkits, so they need specific configurations to run properly. Firefox [absolutely needs GDK_BACKEND to be set][48] in its environment, regardless of whether it's x11 or wayland, otherwise it borks itself. It also needs MOZ\_ENABLE\_WAYLAND if GDK_BACKEND is not set to wayland. In my experience, Chrome/ium works significantly worse on Wayland, and Firefox is in a much better shape.

Some Qt apps might need QT\_QPA\_PLATFORM set to xcb if they're too buggy. But the real issue are Electron apps. They never default to XWayland unless you explicitly set GDK_BACKEND=x11, and they won't run natively on Wayland if they're not using Electron 12 with the correct flags.

It's particularly common to use Electron apps that are packaged as flatpak. For a very long time I'd install flathub as -user just so when I right click each app's respective launcher on the menu I'd be able to edit its command to include GDK_BACKEND=x11. However, recently over the KDE Brazil telegram group someone mentioned it's possible to override this variable for apps installed from root flathub: `sudo flatpak override --env=GDK_BACKEND=x11 your.app.here`.

At the company I used to work for, an Electron app I'd use on a daily basis was [Slack][49]. It works perfectly fine on XWayland. Needless to say, it was my main work communications app. I'd much rather have used selfhosted [Mattermost][50] or [Rocket.Chat][51], and even recommended such, but you know how it is with small companies. Just fetching the freemium version of everything and enduring their limitations.

[LibreOffice][52] has had a rather rough history with its Wayland implementation in my experience, but nowadays it's pretty stable and reliable. I used it for proofreading/revising/editing papers that were already written in English, often by Brazilians but also Turkish, Chinese and Greek people. I used LibreOffice out of ideological motivation since it lacks baloons and the ability to create keyboard shortcuts for certain Unicode symbols, so it's not exactly ideal.

Alternatively I'd use [WPSOffice][53] because it has track changes in baloons—in fact, a better implementation than that of Microsoft Word, as it includes scrolling—despite its functional limitations and security controversies. I used it out of pragmatic motivation. It worked well on Wayland, but would occasionally render some weird freezes related to the file dialog. Those happened on X11 as well, although less. LibreOffice should have track changes in baloons in the exact same way, but they're somewhat resistant to it. Can't blame them, it sounds like a nightmare to code and maintain. But it has been one of the top 20 most requested features of LibreOffice for over a decade, with good reason, and only recently some work on that has started.

[SoftmakerOffice][54] 2018 is a suite I paid for, because first, they are not [Linux-hostile][55]; second, their PDF editor is sold as an actual product rather than just SaaS, unlike [Iceni][56]'s monopolistic, anti-consumer practices; the interface is worth it while also having plenty of functionality; and its PDF editor, which works well on WINE, allows to export as XLIFF for translation and future reimport. I really [wish this was available in Okular][57] or other PDF readers/editors. Moreover, it has been a longstanding competitor to Microsoft's office suite. It works perfectly on Wayland, but it's not as compatible with .docx as I'd wish, especially when rendering landscape pages and formulae. So I used it mostly out of ideological motivation.

[OnlyOffice][58] is unusable for me because of [this issue regarding word count][59]. So I didn't even care to evaluate its usability on Wayland. It indeed has very high compatibility with .docx, though.

[Calligra Words][60] does not have any track changes functionality, which is understandable since it's basically maintained by one person now, but this means it's impossible for me to use for work. It is commendable however that it's the fastest document editor of all, starting almost instantly, and if you do not mind its `t h i c c` sidebar, it's actually usable for basic writing.

[o20.Word][61] is way too new to use for work. I haven't used it much aside from some brief testing, but it looks promising.

For email, I used KMail and Thunderbird. For KMail, I requested that a [default keyboard shortcut be added to quickly add attachments][62]; Thunderbird theoretically has this, but it's broken. I always used KMail first and I do like the entire KDE PIM suite, and KMail in particular is way too powerful and handy (a post about its sheer coolness is on the writing). However, when using it on Wayland, [this QtWebEngine bug][5] was particularly disruptive. Nothing that setting QT\_QPA\_PLATFORM=xcb wouldn't fix, though. [Since December I don't experience it anymore][63]. For some time, [this Gmail bug][64] bothered me immensely, so at that time I used Thunderbird (and noticed its shortcomings, but those are for another time). After those two bugs were fixed, now it would run perfectly on Wayland for me.

For actual translation, I was glad to find out that [Memsource][65], my first paid professional [CAT tool][66], is a Qt app that runs perfectly fine and natively on Wayland. It is fast and has powerful keyboard shortcuts in addition to the ability to reposition its panes easily, as expected from a good professional Qt app. Its primary machine translation (which was firstly based on Microsoft Translator with some bits of Google Translate) was quite poor, however. It also doesn't mix well with Breeze Dark since colors in the actual segments aren't hardcoded, so it follows your system theme when it shouldn't.

{{< figure src="/2021/04/memsourcethemeissue.png" caption="That theming issue with Breeze Dark is atrocious." >}}

{{< figure src="/2021/04/memsourcedefault.png" caption="The actual interface is pretty good though. Is there a way to automatically run it in Breeze? Please tell me." >}}

[Matecat][67] had superior machine translation out of the box and it integrates well with [MyMemory][68], a public translation memory. It's browser based, which means it's easily usable on Linux, but it requires Chrome to use it, to the point that you actually get a block front page. You can bypass this by using Chromium or changing the user agent in Firefox, but it's slow. It depends on google components to run with the best performance: certainly not ideal. But hey, [the actual server is open source][69] and you can [host your own instance of it][70]! Or at least [it was like that before][71]…

[Smartcat][72] is also browser-based, but it's way faster than Matecat and it's not Chrome dependent. Its interface actually enforces a proper translation workflow: you need to create and manage your own glossaries to work with it. The machine translation is on a middle ground between Memsource and Matecat.

I subscribed to [ProZ][73] a few months ago and got myself a license for [Cafetran Espresso][74]. It has quirky defaults (like Alt+9 for adding tags for seemingly no reason) and it's Java, but it's fast and more powerful than Memsource. It has quickly become my favorite. It's well integrated with ProZ itself, which should be great for freelance work and managing invoices. Some time ago pinning it to the task manager on the panel would [render this][75]. While the [security message has been fixed][76], the lack of icon and inability to open from the task manager remain. I'll report this later.

{{< figure src="/2021/04/cafetran.png" caption="Cafetran Espresso, my main CAT tool." >}}

An issue I personally experienced when attempting to run Cafetran Espresso on openSUSE was that neither openJRE or Java's JRE would execute it properly. I needed to install [icedtea-web][77] to be able to run it. While an exceptionally trivial fix, I spent over a week searching for a way to run it…

Java apps are actually rather common for professional CAT tools (as much as I'd have preferred Qt apps). [OmegaT][78] is the example that comes to mind when one thinks of translating on Linux. It also required icedtea-web, but it otherwise is perfectly supported on Linux. I have a free Yandex API key from some time ago that I manually connect to it in order to enable machine translation. It sucks, but it's sufficient. The point of machine translation in CAT tools is not for it to do your job, it's for it to spare you from unnecessary typing, so as long as it's doing that, it's usable.

The OmegaT interface is severely dated, but with some customization it can look nice and provide a proper keyboard-driven workflow. It can also handle LaTeX files well, unlike all the previous contenders, which is a massive plus. [I wrote a blog post about how to easily proofread and translate such files][79], but it's simply because having more options is always good, as is OmegaT.

For transcription and subtitling, I started using [Subtitle Composer][80]. I liked it a lot, translated most of its interface into Brazilian Portuguese, started the work on its website (which still needs polish), wrote its flatpak manifest together with the maintainer, and I even contributed to [TranslateOnLinux][81] to add it to the list. A major motivation for me to learn C++/Qt is for me to contribute to it eventually, and the reason I now have a microphone is to promote it via videos and tutorials, both public and on [ProZ videos][82].

{{< figure src="/2021/04/subcomp.png" caption="Great show. Still relevant today." >}}

Subtitle Composer underwent some major changes: most notably, its video player was rewritten from scratch and now works on Wayland (even as a flatpak). The next version will be its prime time, so stay tuned.

Lastly, for LaTeX proofreading, I'd actually use [Kile][83]. Its highlighting (probably a Kate component) just works really well for searches where you need to change specific words, and its scrollbar preview is handy for finding myself around the text. If you've ever had to edit an over-2000 words LaTeX document, you'll know quite well that a mere scrolling can make you lose several seconds or even minutes trying to find where you were before. Moreover, double clicking on the PDF to scroll back to the correct section in the text, although not so precise, is invaluable. Whenever I didn't want to find the correct LaTeX packages to install or whenever the author used some weird selection of packages that would simply not compile, I'd use [Overleaf][84] instead. Sometimes, when the files were small, I'd just use Kate.

Now, why wouldn't I use [Lyx][85] instead since it has track changes functionality built-in? Well, simply because it's tied to its .lyx file format. I couldn't really receive a .tex file and return a .lyx to the client, now could I? It's more comfortable for the translator, the company and the client to just use a simple diff tool to see the changes I've made to the original text.

A rather recent endeavor of mine was attempting to use [Winapps][86] to run Microsoft Word on Linux. It's more of a pain to setup than I wanted, but it's pretty worth it if you have the resources for such. I have removed it already, but not before testing it on Wayland: it runs fine, but it chugs somewhat with regard to window management, sometimes not complying to the position I want it to stay.

## What about leisure?

The first thing you think about when talking about leisure is games. And I must say: it's pretty much the same on Wayland as it is on X11. I haven't actually noticed any particular issues on Wayland that weren't already present on X11.

I did notice however that `SDL_VIDEODRIVER=wayland` simply will not work on 98% of games. With `SDL_VIDEO_MINIMIZE_ON_FOCUS_LOSS=0` games will (usually) not automatically minimize when you switch focus to another window. Unity games integrate fairly well on Wayland, to the point they're able to detect the correct Wayland screens where you want your games to run. This is true with Adventure Capitalist and 7 Days to Die.

If you take a look at the Unity Standalone Player command line arguments, you'll find some interesting settings for Unity games, like `-force-wayland` , `-force-vulkan` , and `-screen-fullscreen` .

The first two will likely degrade your experience somewhat since both are experimental on Unity, but the third, the flag to set fullscreen, is useful if your Unity games ever get black screens with sound, as was the case with Hollow Knight. I've experienced this twice, and it's not a Wayland issue, but my guess is one of three things: switching the window Fullscreen status as opposed to the game's built in controls; changing the resolution; or updates. Regardless, by unsetting fullscreen status, the game will always render its graphics instead of displaying a black screen.

Game audio from Proton games runs perfectly fine even if your setup is pure pipewire and no alsa/pulse/jack. Alsa (32 bit) is usually a Steam dependency, but my Steam is a flatpak, so…

Flash games like Epic Battle Fantasy 4 run alright with the [Adobe Flash Player Projector on Flathub][87], which can be sandboxed, nice.

Games from [itch.io][88] work pretty much the same as on Steam: if it's broken, it's broken on both X11 and Wayland. No difference there. Expectedly, gaming platforms shouldn't make any difference as to whether games run well.

DOSBOX games are fine, as is [GZDoom][89]. I play it with the [Doom Wolf Mod][90], which is badass, woof. [Fox Doom][91] looks adorable, but I haven't tested it yet. Cool stuff though: you can actually play it natively on Wayland with SDL_VIDEODRIVER=wayland, and I didn't experience [this bug][92].

RetroArch… well, I'm not sure. I've noticed slowdowns whenever I start using Fast Forward too much, but I haven't been able to pinpoint if it's a Wayland issue or an emulation issue. Together with RetroArch I use [tsukuyomi][93] to patch games with ups and [floating][94] to patch with ips.

For streaming, I've been using [ElectronPlayer][95] for Netflix and Prime Video. It actually runs decently despite being Electron, very little difference from Firefox in terms of CPU usage. This one requires GDK_BACKEND=x11, as expected.

Another Electron application I've been using is [Cumulonimbus, or CPod][96] as it's called now. Surprisingly enough, it does NOT require GDK_BACKEND=x11! Somehow it does default to XWayland. And now I actually have some incentive to listen to [Furry.FM][97] (a German furry podcast) and some language learning podcasts. Its only caveat is [the lack of a sort option][98]. If Elisa ever gets podcast functionality, I'm ditching CPod immediately, though. 😛

I use yet another Electron app named [OpenTodoList][99]. It syncs my notes with Nextcloud on the laptop and on my phone with a pleasant Material interface with checkboxes, which is pretty much what I need: a Google Keep clone, but simpler.

## What about hobbies?

I don't consider hobbies fully leisure. Hobbies have a more developmental nature than leisure activities, but it's less of a must do than work (so a sort of Bildung). My hobbies are typically things that involve learning and applying skills.

Recently Plasma became usable for drawing with graphics tablets on Wayland. There is no KCM and no color management yet, but I never needed either to begin with. It's still not perfect to [use the actual desktop][100] with my tablet, but drawing I can. The important thing is that my tablet buttons, my stylus buttons and combinations with Ctrl and Shift work on [Krita][101], which defaults to XWayland and will show you an error message if you attempt to run it on Wayland with `-platform wayland`. Ah, I should note that my tablet is a [Wacom Intuos Pen Small (CTL480)][102]. This is probably relevant since Wacom products are well supported on Linux since [xsetwacom][103] was started. Now on Wayland the backend used is [libinput][104].

{{< youtube jGafW3Jzmks >}}

Private video because this sketch absolutely sucks. But it's drawn on Wayland!

[Ardour][105] is a bit crashy as a flatpak on Wayland, but I haven't gotten to check if it's a Wayland issue. I still prefer its interface over [Audacity][106]. The latter only integrates well with Breeze Dark with the [Clean Dark][107] theme. Both default to XWayland. I'm learning a bit of video editing, more specifically [Kdenlive][108]. This one is native, as one would expect from a KDE application.

Contribution wise, the only software that gives me problems still is [kdesvn][109]. If you've read my blog before, you probably know that I like how simple it is. But it [still freezes on Wayland][110]. I'm still able to commit since those freezes are temporary, but it's still a rather scary experience.

As for languages, [duolingo-desktop][111] is a good Electron wrapper over Duolingo if you don't care about using [nativefier][112] to create your own wrapper. As most Electron apps, it runs only with GDK_BACKEND=x11.

[Anki][113], contrary to what is mentioned in [this thread][114], actually works for me natively. It's a Qt app (although it looks real bad on Breeze Dark), so it's governed by QT\_QPA\_PLATFORM; if you experience issues, set it to xcb. On my machine, it displays this message in the terminal:

```bash
mpv not found, reverting to mplayer
qt: Running on wayland. Qt WebEngine will disable usage of the GPU.
Note: you can set the QT_WEBENGINE_DISABLE_WAYLAND_WORKAROUND
environment variable before running this application, but this is &nbsp;
not recommended since this usually causes applications to crash.
qt: Wayland does not support QWindow::requestActivate()
```

## And the general state of Plasma?

In 5.21 some people have reported being unable to use [Spectacle][115]'s [rectangular capture][116]; in the 5.22 alpha, most of it works, including rectangular capture (as you can see in this very post), as well as all keyboard shortcuts except Capture Current Monitor.

Clipboard is still finicky, although generally functional. If you copy something from a GTK application it will stay on the clipboard and pasting elsewhere works, [but if you close the original application pasting no longer works][117].

The commendable KWin devs have introduced a wrapper called kwin\_wayland\_wrapper that reattaches the current session to its original sockets if kwin\_wayland crashes (I hope I got this right). This essentially means: kwin\_wayland crashes no longer [take you back to the login screen][117]. This is major and it's being implemented in steps. I recall that just after the release of 5.21 (when this first started being worked on) it would crash everything and render dozens of DrKonqi windows but keep the session standing. Later on it would crash everything but no longer display error messages. The current state is: everything crashes but plasmashell recovers incredibly fast, and there are no error messages.

The next cool thing I'm waiting for is for the [KWin Debugging][118] process to get easier. Currently a major caveat is how kwin_wayland [does not provide usable core dumps][119]. With this being handled, we should be able to foster a better environment for bug reporters interested in Plasma Wayland.

With [XWayland support for NVIDIA drivers][120] merged, [Electron 12 allowing for native Wayland execution][3], KWin salvaging the session, Fedora 34 defaulting to Plasma Wayland, screen recording and sharing made possible on Wayland with pipewire, and graphics tablet support in at least two major DEs, it finally seems like it's just a matter of time before Plasma Wayland goes prime.

 [1]: https://build.opensuse.org/package/show/openSUSE:Factory/plasma5-workspace
 [2]: https://build.opensuse.org/package/view_file/openSUSE:Factory/plasma5-workspace/0001-Set-GTK_BACKEND-x11-in-a-wayland-session.patch?expand=1
 [3]: https://www.reddit.com/r/linux/comments/lw7cvk/electron_12_has_just_been_released_with_wayland/
 [4]: https://en.opensuse.org/SDB:Argon_and_Krypton
 [5]: https://bugs.kde.org/show_bug.cgi?id=397825
 [6]: https://www.logitechstore.com.br/mouse-com-fio-logitech-trackball-cinza/
 [7]: https://ergodox-ez.com/
 [8]: https://wiki.archlinux.org/index.php/Logitech_Marble_Mouse#Using_evdev
 [9]: https://wiki.archlinux.org/index.php/Logitech_Marble_Mouse#libinput
 [10]: https://wiki.archlinux.org/index.php/Logitech_Marble_Mouse#Plasma_and_Wayland
 [11]: https://www.reddit.com/r/kde/comments/cihn8b/is_there_a_way_to_enable_scroll_emulation_for_the/
 [12]: https://wiki.archlinux.org/index.php/Keyboard_input#Identifying_keycodes_in_Xorg
 [13]: https://gitlab.freedesktop.org/xorg/app/xeyes
 [14]: https://www.jbl.com.br/over-ear-headphones/JBL+T450BT.html
 [15]: https://www.reddit.com/r/kde/comments/mdfa2u/plasmapa_pipewirepulse_sbc_by_default_and_a_neat/gs9l533?utm_source=share&utm_medium=web2x&context=3
 [16]: https://www.reddit.com/r/kde/comments/mdfa2u/plasmapa_pipewirepulse_sbc_by_default_and_a_neat/
 [17]: https://www.amazon.com/ZINGYOU-Microphone-BM-800-Adjustable-Double-layer/dp/B01MZCS8MY
 [18]: https://gitlab.freedesktop.org/pipewire/pipewire/-/wikis/TODO
 [19]: https://github.com/josh-richardson/cadmus
 [20]: https://github.com/wwmm/pulseeffects
 [21]: https://www.reddit.com/r/linux_gaming/comments/m9fibg/rnnoise_live_noise_canceling_with_pipewire/
 [22]: https://github.com/lawl/NoiseTorch
 [23]: https://github.com/xiph/rnnoise
 [24]: https://github.com/lucianodato/noise-repellent
 [25]: https://github.com/lucianodato/speech-denoiser
 [26]: https://rabbitictranslator.com/wordpress/index.php/2021/04/02/short-fluff-plugins-ardour-user-flatpak/
 [27]: https://www.playstation.com/en-us/accessories/dualsense-wireless-controller/
 [28]: https://www.phoronix.com/scan.php?page=news_item&px=Sony-HID-PlayStation-PS5
 [29]: https://wireless.wiki.kernel.org/en/users/documentation/bluetooth-coexistence
 [30]: https://ask.fedoraproject.org/t/bluetooth-vs-wifi-interference-coexistence-driver-ath9k/5030/2
 [31]: https://invent.kde.org/plasma/xdg-desktop-portal-kde
 [32]: https://feaneron.com/2021/03/30/obs-studio-on-wayland/
 [33]: https://gitlab.gnome.org/feaneron/obs-xdg-portal
 [34]: https://github.com/flathub/flathub#using-the-flathub-repository
 [35]: https://www.reddit.com/r/kde/comments/krfrl9/psa_if_youre_planning_to_test_the_obs_studio/
 [36]: https://www.reddit.com/r/openSUSE/comments/m6zrp7/psa_for_latest_pipewire_update_on_tumbleweed/
 [37]: https://www.maartenbaert.be/simplescreenrecorder/
 [38]: https://www.maartenbaert.be/simplescreenrecorder/recording-steam-games/
 [39]: https://github.com/foxcpp/ssr-wlroots
 [40]: https://github.com/2xsaiko/obs-glcapture
 [41]: https://github.com/nowrep/obs-vkcapture
 [42]: https://github.com/xlmnxp/blue-recorder
 [43]: https://github.com/amikha1lov/RecApp
 [44]: https://github.com/phw/peek
 [45]: https://github.com/SeaDve/Kooha
 [46]: https://hg.sr.ht/~scoopta/wlrobs
 [47]: https://github.com/ammen99/wf-recorder
 [48]: https://www.reddit.com/r/linux/comments/mo9d2v/this_week_in_kde_activities_on_wayland/gu87w8m?utm_source=share&utm_medium=web2x&context=3
 [49]: https://slack.com/
 [50]: https://mattermost.com/
 [51]: https://rocket.chat/
 [52]: https://www.libreoffice.org/
 [53]: https://www.wps.com/linux
 [54]: https://www.softmaker.com/en/softmaker-office
 [55]: https://forum.softmaker.com/viewtopic.php?f=330&t=18828&p=62179&hilit=linux#p62173
 [56]: https://www.iceni.com/
 [57]: https://bugs.kde.org/show_bug.cgi?id=419774
 [58]: https://www.onlyoffice.com/
 [59]: https://github.com/ONLYOFFICE/DesktopEditors/issues/166
 [60]: https://calligra.org/words/
 [61]: https://flathub.org/apps/details/io.gitlab.o20.word
 [62]: https://bugs.kde.org/show_bug.cgi?id=425161
 [63]: https://bugs.kde.org/show_bug.cgi?id=397825#c22
 [64]: https://bugs.kde.org/show_bug.cgi?id=429406
 [65]: https://www.memsource.com
 [66]: https://translateonlinux.org/#cat-tools
 [67]: https://www.matecat.com/
 [68]: https://mymemory.translated.net/
 [69]: https://github.com/matecat/MateCat
 [70]: https://site.matecat.com/advanced-manual-setup/
 [71]: https://site.matecat.com/support/404.php#getstart
 [72]: https://www.smartcat.com/
 [73]: https://www.proz.com/
 [74]: https://www.cafetran.com/
 [75]: https://www.reddit.com/r/kde/comments/llqm1u/for_security_reasons_launching_executables_is_not/gns9q5j?utm_source=share&utm_medium=web2x&context=3
 [76]: https://bugs.kde.org/show_bug.cgi?id=433148
 [77]: https://software.opensuse.org/package/icedtea-web
 [78]: https://omegat.org/
 [79]: https://rabbitictranslator.com/wordpress/index.php/2020/04/05/translate-latex-by-conversion/
 [80]: https://subtitlecomposer.kde.org/
 [81]: https://translateonlinux.org/#subtitling
 [82]: https://videos.proz.com/
 [83]: https://apps.kde.org/kile/
 [84]: https://www.overleaf.com/
 [85]: https://www.lyx.org/
 [86]: https://github.com/Fmstrat/winapps
 [87]: https://flathub.org/apps/details/com.adobe.Flash-Player-Projector
 [88]: https://itch.io/
 [89]: https://doom.fandom.com/wiki/GZDoom
 [90]: https://aminoapps.com/c/furry-amino/page/item/doom-wolf-mod/pXWl_r1LtpIJNwXPg83D3r6mq5qo53DDDP
 [91]: https://www.moddb.com/mods/fox-doom
 [92]: https://bugs.kde.org/show_bug.cgi?id=389298
 [93]: https://www.romhacking.net/utilities/519/
 [94]: https://www.romhacking.net/utilities/1040/
 [95]: https://github.com/oscartbeaumont/ElectronPlayer
 [96]: https://github.com/z-------------/CPod
 [97]: https://furry.fm/de-de/
 [98]: https://github.com/z-------------/CPod/issues/220
 [99]: https://opentodolist.rpdev.net/
 [100]: https://bugs.kde.org/show_bug.cgi?id=426581
 [101]: https://krita.org/en/
 [102]: https://www.amazon.com/Wacom-Intuos-Tablet-CTL480-Version/dp/B00EN27TCI/
 [103]: https://github.com/linuxwacom/xf86-input-wacom
 [104]: https://wayland.freedesktop.org/libinput/doc/latest/tablet-support.html
 [105]: https://ardour.org/
 [106]: https://www.audacityteam.org/
 [107]: https://www.deviantart.com/podel1/art/Audacity-Clean-Dark-Theme-1-3-by-Podel-715604676
 [108]: https://kdenlive.org/
 [109]: https://apps.kde.org/kdesvn/
 [110]: https://bugs.kde.org/show_bug.cgi?id=427564
 [111]: https://github.com/hmlendea/duolingo-desktop
 [112]: https://github.com/nativefier/nativefier
 [113]: https://apps.ankiweb.net/
 [114]: https://www.reddit.com/r/openSUSE/comments/mmn1mr/do_you_use_waylandfull_wayland_in_kde/gtsql0m?utm_source=share&utm_medium=web2x&context=3
 [115]: https://apps.kde.org/spectacle/
 [116]: https://bugs.kde.org/show_bug.cgi?id=432260
 [117]: https://bugs.kde.org/show_bug.cgi?id=424649
 [118]: https://community.kde.org/KWin/Debugging
 [119]: https://invent.kde.org/plasma/kwin/-/issues/33
 [120]: https://gitlab.freedesktop.org/xorg/xserver/-/merge_requests/587
