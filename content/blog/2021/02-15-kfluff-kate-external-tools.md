---
title: KFluff — Kate’s External Tools
author: Blumen Herzenschein
date: 2021-02-15
url: kate-external-tools
aliases: /wordpress/index.php/2021/02/15/kfluff-kate-external-tools/
tags:
  - Planet KDE
  - Fluff
---
I've been learning C++ lately. About two months ago I finished [Codecademy's C++ course][1] (honestly really good for the basics), a month ago I managed to fetch the [C++ Fundamentals book from PacktPub for free][2], and now I'm mostly following [this amazing YouTube online course by The Cherno][3] and taking a look at [C++ Weekly][4]. Since I did occasionally dabble with HTML, CSS, JS, C# and Perl in the past and know some Bash basics, learning C++ is progressing nicely, and it's probably my most well-understood language so far. I took a short look at [Qt for Beginners][5] these days, which might warrant another blog post later.

Because of this, I was taking a look at KDE/Qt-centric programs for development purposes; namely Kate, KDevelop and Qt Creator.

Qt Creator has been great for learning QtWidgets, but it's way more than is needed for basic C++ development/learning. KDevelop is more focused in this regard, but it's likewise project-based and it utilizes CMake for building. C++ is already quite the challenge to learn, I was not in the mood to learn a new syntax just for the sake of building my simple C++ tutorials—at least not yet. For now some basic g++ commands suffice.

Kate has the basics and it works well; its backend is used in KWrite, Kile and KDevelop, so it's solid for text editing, that's for sure. When [I was venturing through KDE Web stuff][6], I used Kate, Nano and Atom, but mostly Kate.

## External Tools

In 2019 there was a [resurrection of Kate's External Tools plugin][7]. It allows you to do some pretty rad stuff as long as you know your way with terminal commands, similar to [KDE Connect's custom commands][8]. Recalling this, I added two buttons to my toolbar in Kate: **Compile** and **Compile and Run**.

It's really easy too: you go to `Settings > Configure Kate... > External Tools > Add > Add Tool...`, and then you get the following popup:

{{< figure src="/2021/02/emptyexternaltools.png" >}}

I then filled it like so in order to create a **Compile** tool:

{{< figure src="/2021/02/filledexternaltools-1.png" >}}

Basically:

```
Executable: g++
Arguments: %{Document:FileName}
Save: Current Document
```

Where `%{Document:FileName}` means current filename without path and with extension.

It's really simple to use. I loved it.

When I tried the same to additionally execute the program, it failed. I attempted something like this:

```
Executable: g++
Arguments: %{Document:FileName} -o %{Document:FileBaseName} && ./%{DocumentFileBaseName}
```

Where `%{Document:FileBaseName}` means the name of the file without path or extension.

I was quite confused about this, but then I noticed: `&&` and `./` are shell stuff. Therefore the command requires a shell to run; and the default example for this, **Run Shell Script** tool uses `konsole -e sh -c 'command here'`, which opens a new Konsole window running sh running your command.

That works, but there's a much cleaner way to showcase the output of commands: the **External Tools** pane. For that I found out that I just need to redirect the output to a shell, sh or bash; so out of the previous command we saw, we only need `sh -c 'command here'`. I personally prefer using bash when sh is not strictly required since most major distributions default to bash.

The resulting command is:

```
Executable: bash
Arguments: -c 'g++ %{Document:FilePath} -o %{Document:FileBaseName} && ./%{Document:FileBaseName}'
Save: Current Document
Output: Display in Pane
```

It ends up displayed like this:

{{< figure src="/2021/02/compilerun-1.png" >}}

{{< figure src="/2021/02/compilerun1.png" >}}

{{< figure src="/2021/02/compilerun2.png" >}}

Nice.

I then went on a spree and looked at how to achieve some interesting commands to implement as external tools in Kate. The commands I'll provide next are basically distinguished between file manipulation and string manipulation.

## Useful commands: file manipulation

File manipulation is simple: you simply redirect `%{Document:FileBaseName}`, `%{Document:FileName}` or `%{Document:Text}` to a command.

### You can simply open the same file in a GUI program…

Open in Kile:

```
Executable: kile
Arguments: %{Document:FileName}
```

### Send the entire file content to a GUI program…

Send content as email (KMail):

```
Executable: kmail
Arguments: --composer --body %{Document:Text}
```

### Send the file to a terminal application…

View Docbook as HTML:

```
Executable: meinproc5
Arguments: %{Document:FileName}
```

### Validate the current file/content…

Validate .desktop file:

```
Executable: desktop-file-validate
Arguments: %{Document:FileName}
Output: Display in Pane
```

Validate AppStream file:

```
Executable: appstreamcli
Arguments: validate %{Document:FileName}
Output: Display in Pane
```

Check PO file for errors:

```
Executable: bash
Arguments: -c 'msgfmt --verbose --check %{Document:FileName}'
Output: Display in Pane
```

Proof your shellscript:

```
Executable: shellcheck
Arguments: %{Document:FileName}
Output: Display in Pane
```

Check for bashisms:

```
Executable: checkbashisms
Arguments: %{Document:FileName}
Output: Display in Pane
```

## Useful commands: string manipulation

This is where things get interesting. The variable `%{Document:Selection:Text}` allows us to handle specific strings that we select, which is more flexible in that we can add strings to anything in command arguments.

Seeing as I was the one who added [this section of the KDE Connect Useful Commands][9], you can imagine this was the first thing I thought of when I found you can manipulate strings! I reused the same command structure, in fact.

### Redirect via terminal command…

Send to KDEConnect device as notification:

```
Executable: bash
Arguments: -c 'kdeconnect-cli -d $(kdeconnect-cli -a --id-only) --ping-msg $(echo %{Document:Selection:Text})'
```

Sort text alphabetically:

```
Executable: sort
Arguments: %{Document:Selection:Text}
Output: Replace Selected Text
```

### Redirect to GUI application for further manipulation…

Send to KRunner:

```
Executable: krunner
Arguments: %{Document:Selection:Text}
```

### Search for string within website…

Search the KDE API:

```
Executable: xdg-open
Arguments: "https://api.kde.org/search.html?query=%{Document:Selection:Text}"
```

Search the Qt API:

```
Executable: xdg-open
Arguments: "https://doc.qt.io/qt-5/search-results.html?q=%{Document:Selection:Text}"
```

Search on Linguee:

```
Executable: xdg-open
Arguments: "https://www.linguee.com/search?source=auto&query=%{Document:Selection:Text}"
```

Search for duplicate crashes on KDE Bugzilla:

```
Executable: xdg-open
Arguments: "https://bugs.kde.org/buglist.cgi?bug_severity=crash&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&bug_status=RESOLVED&bug_status=NEEDSINFO&bug_status=CLOSED&list_id=1839914&query_format=advanced&short_desc=%{Document:Selection:Text}&short_desc_type=allwordssubstr"
```

Confirm cron dates on Crontab.Guru:

```
Executable: xdg-open
Arguments: "https://crontab.guru/#%{Document:Selection:Text}"
```

## Kate is really powerful

And I hope you may find usefulness in these examples.

I can easily imagine a regex, sed, awk, Perl or Python master to make better use of Kate's external tools for manipulating strings and file content, but I'm none of those things. Nonetheless I presented a few practical cases that I believe could be used in production, if anything just to make your life easier or to make interesting use of Kate.

So far it has proven convenient. I can see myself using this functionality together with [Translate Toolkit][10] and similar tools in the future, but for the time being I'll simply be learning C++.

 [1]: https://www.codecademy.com/learn/learn-c-plus-plus
 [2]: https://www.reddit.com/r/kde/comments/l0ada3/packtpub_c_fundamentals_is_available_for_free_for/
 [3]: https://www.youtube.com/playlist?list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb
 [4]: https://www.youtube.com/playlist?list=PLs3KjaCtOwSZ2tbuV1hx8Xz-rFZTan2J1
 [5]: https://wiki.qt.io/Qt_for_Beginners
 [6]: https://rabbitictranslator.com/wordpress/index.php/2020/05/26/contributing-to-kde-websites-from-scratch/
 [7]: https://kate-editor.org/post/2019/2019-09-21-external-tools-plugin/
 [8]: https://userbase.kde.org/KDE_Connect/Tutorials/Useful_commands
 [9]: https://userbase.kde.org/KDE_Connect/Tutorials/Useful_commands#Send_output_of_any_command_to_your_phone
 [10]: http://docs.translatehouse.org/projects/translate-toolkit/en/latest/index.html
