---
title: Long KFluff – compiling C++/Qt with g++/clang++, QMake, CMake, Kate, KDevelop, QtCreator
author: Blumen Herzenschein
date: 2021-12-12
url: kfluff-compiling
aliases: /wordpress/index.php/2021/12/12/long-kfluff-compiling-with-stuff/
tags:
  - Planet KDE
  - Fluff
---
I've been learning how to code recently, but it takes a lot of my time to achieve even the basics since I have no IT background, which makes me unproductive for other stuff. And I was having some family health issues to take care of. I've decided to do what I like best instead; documenting, translating, subtitling.

Let me casually introduce you to compilation of C++ and Qt software. I'm sure you'll learn at least something. You will need some basic C++ knowledge, though. I plan on making a learning trajectory for those interested in learning Qt/KF, but that day is not today.

To start off, we can make a simple C++ program just so we have something to compile.

This guide requires you to have installed your distro's build tools, the Qt libraries, git, Make, CMake, QMake, KDevelop and QtCreator (which usually comes with the package qt5-tools), g++, clang++, ninja and qt5-doc-html. Needless to say, no automagic is possible without all the tools being installed on your system!

## Compiling the manual way

First create a dedicated folder for our little project, then initialize a git repository inside. Believe me, it will make things easier later.

```bash
git init
```

Then:

```c++
#include <iostream>

int main (int argCount, char* argVector[])
{
std::cout << "Sup World!";
return 0;
}
```

Add this to a file named main.cpp, you can do it with Kate.

It should be easy to understand with minimum knowledge of C++.

To compile it, we can open a terminal (like Kate's embedded one which shows up with F4), go to its folder and run:

```bash
g++ main.cpp -o myapp
```

Where the first argument is the source file, main.cpp, and -o represents the output, the resulting binary.

To run it, we just run:

```bash
./myapp
```

Now let's try using another library, OpenGL, just to compile it; we are not even going to use any of its variables, functions or classes.

```c++
#include <iostream>
#include <GL/gl.h>
int main (int argCount, char* argVector[])
{
std::cout << "Sup World";
return 0;
}
```

Since we're using an external library, we will need to use a slightly different command to compile.

```bash
g++ main.cpp -o myapp -lGL
```

Where -l means library, followed by the library's name, GL.

That is simple enough. But what if we add a Qt library? It is a tad more complex to include.

```c++
#include <iostream>
#include <QCoreApplication>

int main (int argCount, char* argVector[])
{
QCoreApplication myappname(argCount, argVector);
std::cout << "Sup World";
}
```

The line containing `myappname` merely creates an object of type `QCoreApplication` which will represent our app. There's no event loop, so as soon as we run `std::cout`, the program returns 0 (success) and terminates.

To compile it manually, we'll need to do a few things.

```bash
pkg-config --list-all | grep --ignore-case qt
```

`pkg-config` is lovey dovey, your best friend, that one who's got your back, yes, that one you can rely on. It will list all the Qt libraries on your system with their correct names for use when compiling. In our case, what we are using is `Qt5Core`.

```bash
g++ main.cpp -o myapp $(pkg-config --cflags --libs Qt5Core)
```

This is the same as running:

```bash
g++ main.cpp -o myapp $(pkg-config --cflags Qt5Core) $(pkg-config --libs Qt5Core)
```

This will apply the required Cflags and libraries. If we take a look at what these pkg-config commands show:

```bash
$pkg-config --cflags Qt5Core
  -DQT_CORE_LIB -I/usr/include/qt5/QtCore -I/usr/include/qt5 
$pkg-config --libs Qt5Core
  -lQt5Core
```

You will notice that `-lQt5Core` is pretty much the same thing as `-lGL`: the flag `-l` plus the library name.

Now the cflags thing is what is used to add the necessary variables (-DQT\_CORE\_LIB) and includes (-I, upper case letter i lol) for compilation.

In practice, this is the same as running:

```bash
g++ main.cpp -o myapp -DQT_CORE_LIB -I/usr/include/qt5/QtCore -I/usr/include/qt5 -lQt5Core
```

And what if we dislike gcc/g++? Well, we can use the same command, but with clang/clang++:

```bash
clang++ main.cpp -o myapp -DQT_CORE_LIB -I/usr/include/qt5/QtCore -I/usr/include/qt5 -lQt5Core
```

Okay, so manual compilation isn't that hard, it's just a lot to type. It is particularly annoying that this method requires you to manually check and add each library and include to the compile command. But Qt can be compiled with QMake and CMake too, right? Do those make our life easier?

## Compiling with QMake

QMake isn't particularly hard either. Create a file named myapp.pro (where .pro stands for project):

```qmake
QT += core
SOURCES += main.cpp
```

That's it. Pretty simple, right? We are using the `QCoreApplication` class of the `Qt5Core` library, whose QMake name is `core`. Among our sources files, we have main.cpp.

In a terminal, compilation would be like so:

```bash
qmake -o Makefile myapp.pro
make
```

This generates an output file Makefile that is used to compile via `make`.

It is not without reason that TheQtCompany made QMake. It's simple enough, and doesn't get majorly complex like CMake. Let's make our code a bit more complex though. Let's add a header and another source file. Let's also use pure Qt now.

main.cpp:

```c++
#include <QCoreApplication>
#include "thatother.hpp"

int main (int argCount, char* argVector[])
{
QCoreApplication myappname(argCount, argVector);
QTextStream qout(stdout);
qout << QString::number(twelve);
}
```

thatother.hpp:

```c++
extern int twelve;
```

thatother.cpp:

```c++
#include "thatother.hpp"
int twelve = 12;
```

Basically, we create a header thatother.hpp where we declare the variable twelve and make it accessible to other source files, namely main.cpp, by using the keyword `extern`. We define the variable in thatother.cpp. Then, in main.cpp, we turn the int twelve into a [QString][1], then pipe it into our own Qt implementation of `std::cout`. A bit mindboggling for first starters, but simple enough to understand with basic C++, complex enough to showcase what to do with multiple files.

To compile this with gcc, you'd use:

```bash
g++ main.cpp thatother.cpp -o myapp $(pkg-config --cflags Qt5Core) $(pkg-config --libs Qt5Core)
```

You'd have to explicitly specify all source files. This can even accept globbing, like:

```bash
g++ *.cpp -o myapp $(pkg-config --cflags Qt5Core) $(pkg-config --libs Qt5Core)
```

But on QMake, it's as simple as creating a file myapp.pro like this:

```qmake
QT += core
SOURCES += main.cpp thatother.cpp
HEADERS += thatother.hpp
```

And to compile, we just run:

```bash
qmake -o Makefile myapp.pro
make
```

Way easier, isn't it? You could have a hundred source and header files in your project, the command would remain the same. What about CMake though?

## Compiling with CMake

CMake is a tad more complex, but it's also a tad more powerful. Nevertheless, let's focus on simple.

CMakeLists.txt:

```cmake
cmake_minimum_required(VERSION 3.16)
project(myapp)
find_package(Qt5 COMPONENTS Core REQUIRED)
add_executable(myapp main.cpp thatother.cpp thatother.hpp)
target_link_libraries(myapp PRIVATE Qt5::Core)
```

The first line simply determines the minimum version of CMake to be used (it is not obligatory in our case, it will just render a warning). The second line simply names the project. The third line simply tells the computer to import the libraries we will use, namely Qt5's Core, and all its variables. The fourth line determines that the executable name should be `myapp` and it should use the following list of source and header files. The last line tells the linker what libraries should be used for linking.

After that, we just need to run:

```bash
cmake .
make
```

Where the dot after cmake is the current directory, namely _where to find the root CMakeLists.txt_.

Like QMake, CMake may have a fuckton of files being compiled via one command, but unlike QMake, it allows for code complexity even within its CMake files. With those, one can set variables and determine which files will be compiled for which cases, making for a very flexible build system, although one with a certain learning curve. I won't elaborate further than that because I'd write way too much for a KFluff and with way too little technical accuracy, as I do not know much CMake either. Wanna see what CMake is capable of with KDE projects? [Take a brief look][2]. Want a quick learn of some things involving Qt? [Here, enjoy][3]. You will probably want to want to look at the [official documentation][4], though. There's a ton to learn. Personally, I found it rather overwhelming and wanted to focus on learning to code instead.

Let's see just a bit more of CMake, though. There are two cool things we can do with it.

The first is the ability to specify a build folder. This is good practice and it's often referred to as "out of source build", because the project is built in a folder other than its source code.

```bash
cmake -B build/
```

This runs CMake on the current directory, but all the build files are stored in a folder called build. I added the backslash just to clarify you're specifying a folder to the command, but it's not really needed. This is, in practice, the same as doing:

```bash
mkdir build
cd build
cmake ..
```

Which is commonly seen in projects' readmes if you've ever compiled a cmake project before.

Another way of doing the same here is

```bash
cmake -B build -S .
```

Where, as before, -B refers to the build folder called `build`, and -S refers to the current folder, `.`, in our example it's where we stored the source code. If you want a different build folder or a different source folder, just specify those.

Another cool thing we can do with CMake is specify a generator.

You see, CMake just sets up the configuration for the project, like finding libraries or specifying the target binary. It generates the required files to compile with a build system like make or ninja; that is to say, if your generator is "Unix Makefiles", CMake will generate a `Makefile` which allows you to build with `make`, and if your generator is "Ninja", CMake will generate a `build.ninja` which allows you to build with `ninja`. If you type:

```bash
cmake -G
```

It will error out and mention what generator options you can use to build. Typically on a Linux system, that's Make, and it is the default, but Ninja is nifty and generally faster than Make. For small projects that speed improvement likely won't matter, but as you recompile projects over and over, you'll soon notice what a difference it makes. You can specify Ninja in a couple of ways. To specify it for one run:

```bash
cmake -G "Ninja" -B build -S .
```

or as a cmake variable:

```bash
cmake -D CMAKE_GENERATOR="Ninja" -B build -S .
```

or for CMake to always use ninja, you can set this as your user or global environment variable:

```bash
CMAKE_GENERATOR="Ninja"
```

For more basic information about this stuff you can see `cmake --help` or `man cmake`.

## Compiling with Kate

Before anything else, we will want to make use of Kate's built-in project manager plugin. For that, we need to initialize a git project in the dedicated folder where we are saving our little project, which is why I suggested to do so before. If you haven't done this yet, just press F4 and run:

```bash
git init
```

Then you can simply close and reopen Kate, and the Projects pane which is usually on the left side of the editor will show your project files. This should be way easier to manage your files than the default filesystem browser, but we will see a better way of managing our programming projects later on.

As you have probably noticed, so far this guide has been going through the manual and more painful way of doing things and iterating further towards more automated tools which make our job easier; this will be the case with Kate as well. There are three ways you can compile your programs with Kate: the painful way, manually setting an external tool; the okay way without LSP and automation; and the recommended, automated way with a Kate project.

### With an external tool

So we want to compile our programs using a GUI tool. Kate works well for this, and you can do this using two different methods: an external tool or the built-in build plugin. The third way I mentioned above also concerns the built-in build plugin, but it improves the experience significantly.

I already added an external tool for compiling C++ code [here][5]. In fact, this should work if we use our first code example at the top of this post which consists of pure C++; but we're trying to compile multiple files now, and with Qt instead.

Under Settings > Configure Kate… > External Tools, click Add… > Add tool… and let's name it "Compile QtCore". Under executable, we add `sh`, and under arguments, we add the command we already defined above:

```bash
-c 'g++ *.cpp -o myapp $(pkg-config --cflags Qt5Core) $(pkg-config --libs Qt5Core)'
```

Note the globbing which I mentioned before.

In case you don't get why we use `sh -c ''` here, you may want to read my [previous blog post about external tools][6].

We make it so that the current document is always saved when this external tool is run, and we make the output show up in the Kate pane. Interestingly enough, using qDebug() or qInfo() doesn't make the output show up in Kate; Kate does not accept stderr in its pane, hence why I made a simple Qt version of cout.

{{< figure src="/2021/11/KateCompileQtCore-2.png" >}}

Now, see that toolbar containing buttons like New, Save, Undo in the first page of Kate? If it doesn't show up on your Kate, simply select Settings > Show Toolbar. Right click on it and select Configure Toolbar…, then select the toolbar containing <externaltools> in its name, and add your Compile QtCore to the toolbar by dragging it from the left and into the right. Another way to access this window is via Settings > Configure Toolbar…

You should now have a practical button on your toolbar which you can click anytime to compile your program. Because our g++ command is piped into a shell, we may simply add `&& ./myapp` to it, and the output of the application should show up in our Kate output pane.

If you so wish, you could go to Settings > Configure Keyboard Shortcuts… and add a keybinding for your newly created button. I suggest Ctrl+F5.

### With the build plugin

Kate has a plugin for compilation too. It has to be enabled in its settings first, under Settings > Configure Kate… > Plugins > Build Plugin.

You will then find a compile option in your menu and a pane on the bottom called Build Output. Let's open that pane first, and click the button to add a new target set on the upper right side of the pane.

Click twice on the "T: Target Set" field and call it "My Build". We can ignore the "Dir:" field for now, as it defaults to the current file folder.

Let's click twice on the first target, by default called Build, and rename it to g++, then apply the same command as before:

```bash
g++ *.cpp -o myapp $(pkg-config --cflags Qt5Core) $(pkg-config --libs Qt5Core)
```

Then, let's click twice on the second target, by default called Clean, and rename it to qmake, then apply the same command as before, but together with the make command:

```bash
qmake -o Makefile myapp.pro && make
```

Then, let's click twice on the second target, by default called Config, and rename it to cmake, then apply the same command as before, but together with the make command:

```bash
cmake . && make
```

Let's then delete the fourth target by clicking on it once and then clicking the trash icon in the upper right corner of the pane. We don't need it.

{{< figure src="/2021/11/KateBuildPlugin-1.png" >}}

Now, if you select the command and press the Build Selected Target command right above, your build command will run. If your code has no errors or warnings, it shouldn't output anything, and your build was successful.

If you tick that tickbox to the left of the command, you will set it as the default target. You can set a keyboard shortcut for running the default target too, again I suggest Ctrl+F5.

Try each one. To run the executable, it is probably easier to open the built in terminal with F4 and run it with `./myapp`, or make an external tool to run the executable.

But wait, if it doesn't show the output, why should I use this?

Well, first off, it's easy to edit at any moment, and to define the default target. Second, it is extremely flexible for later on; one could create a target set for desktop, another for android, another for windows, another for debugging, another for releases, another for packaging, etc. Each target set can be defined for a specific folder, that Dir: thingie, so your command will never unintentionally run in the wrong folder and fail with silly errors. You could be working in a file many subdirectories inside your project, your build commands will still only happen in the folder you want them to run.

Now, why does it not show application output, then? Well, it's a tad [hard to implement this][7]. For now it should be simple enough to envelop the running command inside a `konsole -e ''` or `konsole --noclose -e ''` instead, which would have been necessary for external tools anyway if we were to use any sort of interactivity like `std::cin`, `std::getline` or `QTextStream qin(stdin)`, like so:

```bash
g++ *.cpp -o myapp $(pkg-config --cflags Qt5Core) $(pkg-config --libs Qt5Core) && konsole --noclose -e './myapp'
```

(The `--noclose` flag is necessary only to ensure that Konsole doesn't close automatically upon being done.)

In any case, it works with GUI applications. Let's change our code so it is more visible, and use Qt5Widgets instead of Qt5Core. We won't be creating anything complex, just a QLabel that uses our already created twelve variable. We also make it a fixed size so the window containing it isn't atrociously small.

main.cpp:

```c++
#include <QApplication>
#include <QLabel>
#include "thatother.hpp"

int main (int argCount, char* argVector[])
{
QApplication myappname(argCount, argVector);
QLabel label;
label.setFixedSize(250,100);
label.setText(QString::number(twelve));
label.show();
myappname.exec();
}
```

g++ command:

```bash
g++ *.cpp -o myapp $(pkg-config --cflags Qt5Widgets) $(pkg-config --libs Qt5Widgets) && ./myapp
```

myapp.pro:

```qmake
QT += widgets
SOURCES += main.cpp thatother.cpp
HEADERS += thatother.hpp
```

CMakeLists.txt:

```cmake
cmake_minimum_required(VERSION 3.20)
project(myapp)
find_package(Qt5 COMPONENTS Widgets REQUIRED)
add_executable(myapp main.cpp thatother.cpp thatother.hpp)
target_link_libraries(myapp PRIVATE Qt5::Widgets)
```

Then after compilation, you will see the following:

{{< figure src="/2021/11/KateQtWidgets.png" >}}

And if you remove a semicolon anywhere on your code, you will see the following:

{{< figure src="/2021/11/KateError-1.png" >}}

I think now with a bit of testing you understand why this build plugin rocks.

### With a Kate project file

This is the recommended way of using the build plugin. So far, we have been using the plugin without any setup for learning purposes because understanding those will make the following steps clearer.

As you will recall, I told you to delete the default target set provided by the plugin; let's take a look at one of the options made available by default. If your version of Kate isn't that old, you will see the following command as "config":

```bash
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ../
```

Most importantly, we want to take a look at `-DCMAKE_EXPORT_COMPILE_COMMANDS=1 ../`. The first, the variable `CMAKE_EXPORT_COMPILE_COMMANDS` that is enabled (`=1`), makes CMake create a file `compile_commands.json`, namely a _compile database_. It contains exactly what it's called: it is a list of compile commands. This list is used by Kate (and any other editor/IDE, really) to enable a C++ language server, in our case clangd. You can read more about it [on the clang website][8]. An LSP, or Language Server Protocol, provides really useful functionality, like those popups you get when hovering a class, or suggestions while typing code. This improves the coding experience tremendously.

The second part, `../`, is telling us that cmake will use the source provided one folder above; it is no different than passing `-S ../` to the command. Now why is this interesting to us? Because this implies the command is to be run from a build folder, which is typically seen inside the project's folder (at least when best practices apply), and because it is the default. We will come back to that in a minute, just bear with me for now.

Now for actually doing what the title of this subsection suggests: how do you create a Kate project file?

It's pretty simple in fact; you just pass "Kate - Unix Makefiles" or "Kate - Ninja" as the generator. This will only work with the `-G` flag though; Kate is one of multiple _extra generators_, meaning those will be created _in addition_ to the default generator, and being extra means you can't pass it as the default `CMAKE_GENERATOR`either as a cmake variable or as an environment variable. Thus the command will look like this:

```bash
cmake -S . -B build/ -G "Kate - Unix Makefiles" -D CMAKE_EXPORT_COMPILE_COMMANDS=ON
```

This will tell cmake to use the source in the same folder to build the project in the build folder with the default generator AND with the Kate Make generator while creating the `compile_commands.json` mentioned earlier.

This in turn will create two Kate-specific files in the build folder: a hidden `.kateproject` and a `myapp@build.kateproject`. Open the latter with Kate and bam, the build plugin will instantly show a new target set: Project Target.

{{< figure src="/2021/12/KateProject.png" >}}

I'm using Ninja here, but you will see something similar regardless, and it will be automatic. Now, with this setup, you can build your program easily without having to care about typing the right command; and with the default target set, after opening the Kate project file, one can run that aforementioned cmake command to run CMake in case you occasionally change your CMakeLists file, and with the correct generation of the compilation database.

At this point you've seen the cool stuff Kate can do, and I only have two suggestions now: create a "run" configuration that suits your liking (so with a `konsole -e ''` if you need a terminal, or run the binary directly if it's just a GUI), and set the Dir: of the default target set to your build folder.

Now that we know the proper way of configuring Kate for development, let's summarize for a bit how simple the general workflow is.

  * Download the sources (if it's not your project) or write down the CMakeLists
  * Run cmake with the Kate generator and creating the compilation database
  * Open the Kate project file
  * Create a run configuration
  * Code

This workflow is particularly practical when combined with KDE's main tool for compilation, [kdesrc-build][9], but that's a story for another time.

However powerful Kate is, sometimes a text editor does not fit the user's tastes or needs. Now let's try an IDE instead.

## Compiling with KDevelop

Ok, so I've seen many people struggling with KDevelop, mostly because they're used to IDEs and text editors that abstract what I've just mentioned in this blog post. In practice, if you got the general idea of the build plugin in Kate, you will get the general idea of compiling with KDevelop.

There are a few actionable things that might cause this:

  * No files are shown as available upon opening/importing a project. [Reported][10].
  * No default launch settings. [Reported][11].

With that out of the way, let's open KDevelop and import our current project. Just click on Open Project and select either our myapp.pro or CMakeLists.txt. Let's select the latter.

{{< figure src="/2021/11/KDevelopImport-1.png" >}}

So there are a few things to consider for our use of KDE's IDE. So far we have been building our project in the same folder as the project itself, but I'll have to reiterate that this is not good practice. With CMake, typically you'd create a build folder, and run the CMake command so that the build files are located in it. KDevelop follows this good practice for CMake projects.

Our build type influences whether our compiled project will allow for debugging or not, among other things. This [StackOverflow post][12] should clarify things well, but for our newbie purposes, it won't effectively make a difference, so you can leave it as is. It would be, however, if we were to try to build big projects like KDE software tends to be, or if we need to debug our application.

After confirming this dialog, you'll be brought back to the main KDevelop window and your project will be configured via a CMake command on the bottom pane. To access your project, click on the Projects pane.

{{< figure src="/2021/12/KDevelopProjects-2.png" >}}

Because our project is configured properly with a build system and we ensured it works in this guide, it should be mostly ready to go; we just need to ensure it runs. Let's click the Execute button on the toolbar. It should open a new window, Launch Configurations, which can also be accessed via Run > Configure Launches.

You should see Global and the folder containing your project; in my case, it's called "blogpost". Select the folder, click Add and select the fourth option, the one with the name of your project (in this case, `myapp`). You should see something like this:

{{< figure src="/2021/12/KDevelopLaunchConfigurations-1.png" >}}

You don't really need to care much about these options for now; the important thing is that the Project Target is selected. Target here is the same first thing we set in our CMakeLists with `add_executable` and `target_link_libraries`, namely `myapp`.

While we do already have an executable which we can point to that we can add to the Executable field, this is often not the case for new projects, and without an executable it's not possible to run it, so bear this in mind. We would need to build it first before we get an executable we can link to.

After you click Ok, your project should automatically build and execute now.

So what's some cool stuff we can do with KDevelop that makes it worth it over Kate?

Well, first, the theme is sexy as hell.

Second, documentation built in! This is awesome, but sorta hidden. You need to right click the empty space on any of the pane areas, like the one containing Projects to the left, the one containing Run at the bottom or the one containing External scripts on the right. Then you can add the documentation toolview. You can remove the toolviews you don't care about, too.

The documentation toolview lets you see the documentation for CMake and Qt, and add some extra documentation like Python and cppreference. This only applies to system-installed documentation though; those are installable via packages from your distribution and usually stored as `html` and `qch` under `/usr/share/doc` or `/usr/share/doc/packages`, KDevelop will use the qch ones (which are [Qt documentation files][13]). If the Qt documentation doesn't appear for you, you can go to Settings > Configure KDevelop… > Qt Help and add the correct directory for the documentation you have. On my openSUSE Tumbleweed system I typically use Qt6, mine is located under `/usr/share/doc/qt6`. In this guide I used Qt5, which is stored in `/usr/share/doc/packages/qt5`. If your distro doesn't organize your docs neatly, just look for the Qt documentation for `qtcore` and `qtwidgets`, those should probably suffice for you.

Unfortunately there is no KF5 documentation for it yet, but at least now your documentation toolview should be populated; it should display local webpages that are identical to those of Qt's website. I also strongly recommend you to install the cppreference documentation.

Another cool thing about KDevelop is the Scratchpad. With it you can execute bits of code quickly and easily. This is useful for new developers who need to test classes, keep notes of how certain syntax is done or experiment with some algorithm they think might be tricky. On the bottom of the scratchpad pane, you can configure how to build the scratchpad example (if needed) with g++, clang++ and whatnot, quite like what we learned in this post.

The first scratchpad example will always run just fine if you just use pure C++ or include iostream. But that's no fun now that we learned how to compile even Qt projects manually, right?

{{< figure src="/2021/12/KDevelopScratchpad-1.png" >}}

So in this example I use `g++ $f -o /tmp/qtextstream.out $(pkg-config --cflags --libs Qt5Core) && /tmp/qtextstream.out`, where `$f` is the scratchpad filename. The target binary gets built inside `/tmp` because why would you want to save a scratchpad binary locally if it's just for reference's sake?

If KDevelop is going to be your main IDE, it's nice to get various examples set up for the things you can get confused about easily and use them for reference later.

One such cool thing I like to keep stored is this, adapted from the [Wayland book][14], just to convince myself that Wayland shouldn't be too scary to code with:

{{< figure src="/2021/12/KDevelopWayland.png" >}}

Another cool thing KDevelop can do is execute "external scripts", as mentioned before, on the right pane. And yes, that's right, it's like Kate's external tools! This existing in KDevelop was the main reason why I added a few default external tools to Kate some time ago, in fact. I like it a bit more than Kate's for this purpose though. Since it's not general purpose like Kate, it can be more development focused.

{{< figure src="/2021/12/KDevelopExternalScripts.png" >}}

It includes just what you'll probably ever need for handling code, simple enough, and lets you set keyboard shortcuts right away. To learn what those % variables are, just hover the mouse over the Command text field.

There is another thing I should mention that might trip a newbie with using KDevelop and that is related to external scripts. KDevelop is geared towards _projects_, so stuff that _includes project managers like CMake or QMake_. If you just start writing a single, plain C++ file without any CMake or QMake, you'll get plenty confused that the build and execute buttons won't be able to build and execute your file. What you should use instead is the Quick Compile external script.

This is, of course, a user issue rather than a KDevelop issue, but an issue that can easily lead to frustration. Perhaps a warning mentioning this if a user tries to open a standalone C++ file without any project attached? Bam. [One more bug report][15].

Continuing on, a brief comment on cool KDevelop functionality: Concentration Mode. If you just type `Meta + C`, you hide the menubar and all three panes. This is practical in different arrangements: if you close all toolview panes, only the build and run ones will show up upon execution; if you open all three side panes and they're all you need, you'll probably not need to leave Concentration Mode so soon; if you leave the configuring and writing code at different times, alternating between Concentration Mode and normal mode should be trivial and simple.

(Sidenote: KDevelop should probably move away from Meta keyboard shortcuts.)

The last thing I want to tackle about KDevelop is templates. It has quite a bunch. The file templates include: GNOME's GLib templates for C, classes, tests and a few nifty Qt examples for C++, a CMake module template, a barely useful one for Docker, a complete one for Flatpak manifests, one for PHP and a few simple Python ones.

The project templates however outnumber the file templates by far: KDevelop plugins, CMake C/C++, QtQuick, QtWidgets, Kirigami, KParts, Plasmoids, Plasma Wallpaper modules, KRunner plugins, KIOSlaves/Workers, KTextEditor plugin. And more can be installed with Get Hot New Stuff!

Good templates are mighty timesavers. And in our case, having CMake templates is extremely handy.

It's amazing to be honest, although many need updates and some more could be added, like KXmlGui. For those feeling more adventurous, these should be good junior jobs to start contributing to KDE.

## Compiling with QtCreator

We've gone through quite the long list of compilation stuff and some extras about KDevelop. Knowing about the previous stuff should make QtCreator a piece of cake by now.

Conversely, you could ask: if Kate is that effective and KDevelop is that powerful, what am I to gain with QtCreator?

Well, it's cleaner. That's a really important thing to consider. It doesn't show too much of its horsepower and most UI elements are easily discoverable. As much as KDevelop is powerful and almighty, it can be overwhelming, and that's okay for what it is, actually, but the overwhelmingness should be taken into account.

Secondly, it does more stuff automagically. You'll see what I mean in a moment.

Thirdly, the documentation system is more easily integrated and it plays a bigger role in the interface. Namely: Pressing F1 when the text cursor is on top of a word will attempt to search that word in the Qt documentation and open a small sidebar on the right; the Welcome screen includes quick links to examples and tutorials; a direct link to the Get Started Now guide; and a permanent Help button on the sidebar. This is highly appreciated for beginners.

It should be noted that like KDevelop, QtCreator is geared towards projects rather than single files. Unlike KDevelop however, QtCreator does NOT provide a means to run single files without a project! So don't even try. Use a project.

There's one issue I see with how QtCreator is provided by distros, though. From what I can tell, at least Debian, Ubuntu and openSUSE don't make the Qt API documentation package a mandatory dependency of QtCreator. Dunno about Fedora and Arch. In openSUSE, from what I understand it's called `libqt5-qtdoc-html` and on Debian/Ubuntu it's called `qt5-doc-html`. Without these packages, it's not possible to view examples and tutorials through QtCreator. You can read more about it [here][16].

Anyway, onwards to compilation.

Once we open QtCreator, we are greeted with the Welcome screen. Click Open to import our current project, then select the CMakeLists.txt. You should then get something like this:

{{< figure src="/2021/12/QtCreatorConfigureProject.png" >}}

It proposes to use a kit for our project. The hell is a kit?

It's a preset. It includes, among other things, the name of the target environment, whether it's for desktop, mobile or embedded, the C and C++ compilers, the debugger, the Qt version to be built against, the CMake tool, the CMake generator, its configuration…

If you're on Linux (and if you're still reading this extremely Linux-oriented post you probably are), then the default Desktop kit could as well be called Linux Desktop. You could create a kit for Embedded with special CMake configuration, or another to cross compile to Windows with the Mingw compiler, or another for Android targetting mobile, or another kit just for static compilation. You get the idea. Knowing what you now know about compilation, you can learn on your own how to make such kits.

This is one of the examples of the automagical configuration QtCreator does; if you have installed Qt from your distro or the official installer, G++ or Clang++ or Mingw, GDB, and CMake, the Desktop kit should just work.

Click the Configure project to proceed, this will be similar to running CMake on your project. You'll get to the Edit button of the sidebar and will be shown your Project structure by default.

The part we will immediately be concerned with will be the bottom left area. There you can see a computer icon which, when clicked, will show your kit. If you have more than one functional kit, you'll see the option to switch between kits here.

{{< figure src="/2021/12/QtCreatorBuild.png" >}}

Below that icon, the first one is the one you'll use to run your program. Its keyboard shortcut is Ctrl+R, and it first builds then runs your program. The second button won't matter to us at least for now. The last button just builds the project without running.

It should be noted that none of those two will work if the execution of CMake has failed. This is the case for both CMakeLists errors and for when you clean your CMake environment via Build > Clear Cmake Configuration.

Let's take a closer look at what is happening behind the scenes with this automagic though.<figure class="wp-block-image size-large">

{{< figure src="/2021/12/QtCreatorBuildSettings.png" >}}

Quite a bunch to unpack.

QtCreator makes a few things differently than KDevelop. The first one is that the out of tree build folder is located on the same folder that the project folder resides. My blogpost folder is located in my home, therefore the build folder is located in my home too. In contrast, my CMake explanation with the -B flag has the build folder situated inside my blogpost folder, and KDevelop does the same as my CMake example.

Another thing it does differently is autoconfigure a lot of CMake settings, stuff like -DCMAKE\_PROJECT\_INCLUDE\_BEFORE and QT\_QMAKE_EXECUTABLE which are set just to ensure everything works. As far as I understand, KDevelop doesn't do _that much_ autoconfiguration.

It can be a fun endeavor to go through the different build configurations (Debug, Minimum Size Release etc) while reading that StackOverflow post I mentioned and researching a bit more. It's easier to search for things you have seen before, you know.

{{< figure src="/2021/12/QtCreatorBuildSettings2-1.png" >}}

The rest of the build settings is interesting as well: the build and clean steps are simple CMake commands, and if we were to want anything more elaborate for our project, we can.

The Build Environment can be useful sometimes too. If you are, for instance, learning to play with [QEnvironmentVariable][17], you can set your own variable here and attempt to grab it and modify it via code. Or attempt to check some environment variable on the user's system and let your code act differently depending on it being set or not.

{{< figure src="/2021/12/QtCreatorRunSettings.png" >}}

Under the Run Settings, the cool stuff is that QtCreator catches the information provided in the CMakeLists and sets up the Run configuration so it works by default. Hence we see "myapp" here—our target.

The Command Line Arguments, Working Directory and Run In Terminal options are probably the most interesting choices out of this page. With Run In Terminal we get our app to run from a terminal window rather than directly from the binary; this lets us play with `std::cout`, `QDebug` and `QTextStream`, which will be handy tools for debugging and learning. If you are unsure if a rather elaborate [Qt connect][18] will work, you can make your slot run a message on the terminal instead to test, for instance.

Working Directory is useful if you want your application to be run in a specific folder instead of the default. For instance, you're playing with [QJsonDocument][19] and [QJsonObject][20] and want your Json files to be kept inside your source code folder for organization.

Command Line Arguments is particularly fun if you're using [QCommandLineParser][21] and [QCommandLineOption][22]. Also, did you notice my examples use `int argCount, char* argVector[]` instead of the typical `int argc, char* argv[]` or `int argc, char** argv`? I prefer it my way because it makes the actual intent clearer, despite all three doing the same. The first parameter of the main function, `int argCount`, is the number of arguments passed to the command line of the application, including the binary itself. So, in our current example project, if you run `myapp firstargument secondargument`, and do a `label.setText(QString::number(argCount))`, a `3` shows up on the window. The second parameter of the main function, `char* argVector[]`, is a vector containing C-style strings, namely the very words passed as arguments. If you run your app with an argument called `blep` and do a `label.setText(argVector[1])`, you'll get `blep`.

Let's just take a look at a few more nifty things regarding compilation and we'll call a day. Under Tools > Options > Build and Run:

{{< figure src="/2021/12/QtCreatorBuildAndRun.png" >}}

You can see we have quite a lot of settings here too, but you'll probably not care about most since it's sensible stuff, but I highly recommend you to use a separate folder for your Qt projects. You will probably want to untick the "Always ask before stopping applications" option too, otherwise you'll get annoying popups everytime you run your app while your previous instance is already running.

{{< figure src="/2021/12/QtCreatorBuildAndRun2.png" >}}

On the Application Output, you'll want to ponder whether to merge stderr and stdout or not. This can be useful to ensure both will be seen by you when using both QDebug (which outputs to stderr) and QTextStream + stdout (which obviously outputs to stdout). Likewise, it can be annoying if you see all text at once.

{{< figure src="/2021/12/QtCreatorBuildAndRun3-1.png" >}}

This one is fun. By default, once you add new files to your project via the File > New File or Project… option, you'll need to add their names to your CMakeLists.txt. While QtCreator is not able to add those automatically to your CMakeLists (oh, I wish it could), it can automatically copy their names and paths to your clipboard, and in turn all you need to do is open the CMakeLists and paste. Just a brief timesaver I like. If you select the option "Copy file paths" above, it will be put on the clipboard automatically.

The Package manager auto setup option is referring to [automatic management of Qt dependencies with Conan][23] which was introduced recently. Cool stuff, but I don't use Conan to teach you anything about it. 😛

I don't think any build setting for QMake will be useful for beginners here.

And that's it! Hopefully this was an interesting read for you, and hopefully this should make compilation not look like some cryptic underworld of meaningless commands.

 [1]: https://doc.qt.io/qt-6/qstring.html
 [2]: https://develop.kde.org/docs/getting-started/hello_world/#cmakeliststxt
 [3]: https://doc.qt.io/qt-6/cmake-get-started.html
 [4]: https://cmake.org/cmake/help/latest/guide/tutorial/A%20Basic%20Starting%20Point.html
 [5]: https://invent.kde.org/utilities/kate/-/merge_requests/337/diffs#5498c725ae74ca442ac8d1fc1c5893a2412d0edd_194_196
 [6]: https://rabbitictranslator.com/wordpress/index.php/2021/02/15/kfluff-kate-external-tools/
 [7]: https://bugs.kde.org/show_bug.cgi?id=445103
 [8]: https://clangd.llvm.org/installation.html
 [9]: https://community.kde.org/Get_Involved/development#Building_software_with_kdesrc-build
 [10]: https://bugs.kde.org/show_bug.cgi?id=445143
 [11]: https://bugs.kde.org/show_bug.cgi?id=445144
 [12]: https://stackoverflow.com/questions/48754619/what-are-cmake-build-type-debug-release-relwithdebinfo-and-minsizerel
 [13]: https://doc.qt.io/qt-6/qthelp-framework.html
 [14]: https://wayland-book.com/wayland-display/creation.html
 [15]: https://bugs.kde.org/show_bug.cgi?id=446866
 [16]: https://stackoverflow.com/questions/18178796/in-qt-creator-examples-are-missed
 [17]: https://doc.qt.io/qt-5/qtglobal.html#qEnvironmentVariable
 [18]: https://doc.qt.io/qt-6/signalsandslots.html
 [19]: https://doc.qt.io/qt-6/qjsondocument.html
 [20]: https://doc.qt.io/qt-6/qjsonobject.html
 [21]: https://doc.qt.io/qt-6/qcommandlineparser.html
 [22]: https://doc.qt.io/qt-6/qcommandlineoption.html
 [23]: https://www.qt.io/blog/installing-qt-via-conan-package-manager
