---
title: Fantastic workspace dimensions and their limits
author: Blumen Herzenschein
date: 2019-10-13
url: workspace-dimensions
aliases: /wordpress/index.php/2019/10/13/fantastic-workspace-dimensions-and-their-limits/
categories:
  - Keyboard Shortcuts Analysis
---

[After I decided to improve the state of keyboard-driven workflows on Plasma][1], I started using the DE that has been known as the prime example of keyboard navigation: GNOME. This is the beginning of a series evaluating keyboard shortcuts in most Linux DEs and, most importantly, trying to improve this experience in Plasma.

CORRECTION: Roman Gilg pointed out to me that my understanding of dimensions in math was flawed. What was originally 1D, 2D, 3D and 4D will now be represented by 0D, 1D, 2D and 3D.

I used it for three weeks on a true translation workflow, and ended up finding a few interesting things about GNOME and about DEs in general. I reached the conclusion that it does not have a _powerful_ default keyboard-driven workflow by any means, and in fact with the exception of only two functionalities, Plasma can reproduce them perfectly; however, I believe most of its keyboard shortcuts are exceptionally well thought out, and that GNOME does so _contextually_ well.

In this post I’ll initially elaborate on the concept of dimensions: 0D, 1D, 2D and 3D. Most notably, this will focus on how GNOME and Plasma do things, and less so other DEs. This first post shall be a simple introduction, and as such will not focus much on keyboard shortcuts. Next post will include an extensive analysis of GNOME specifically.

Let's start with 0D, which is usually the first paradigm users face when using a computer, and develop it upwards gradually until 3D.

With 0D, you have only one workspace and one screen, as was the case when computers first got an UI. Window movement/navigation only needs to focus on the ability to switch between windows, and how to organize windows on a given screen. Everything is contained within this single box, which you're not allowed to leave. The absence of movement between workspaces makes it so that a zero number of axes exist, hence it's 0D.

{{< figure src="/2019/10/screenshot_20191013_022406.png" caption="Figure 1 – 0D model" >}}

Later on, you have only one workspace, but are allowed to have more than one screen. In this case, in addition to requiring the ability to switch between windows and how to organize them on a given screen, it's also necessary to switch between screens. For didactic purposes, this will be considered 0D as well.

{{< figure src="/2019/10/screenshot_20191013_022543.png" caption="Figure 2 – Still 0D model, but with screens" >}}

This is the most basic GUI interaction the user can face with their computer. I'd argue that the reason Windows only got native workspaces after 2015 is because the common user does not typically require more than a 0D model.

Current GNOME, on the other hand, tries to implement a sort of 1D model, that is, it allows you to move between workspaces on a single direction, a single axis. **If I’m not mistaken**, this is a previous OSX paradigm turned vertical instead of horizontal. The following image exemplifies a typical workspace layout in GNOME.

{{< figure src="/2019/10/screenshot_20191013_000011.png" caption="Figure 3 – Typical GNOME layout, 1D model" >}}

Under a configuration with a main screen in the middle and two monitors on the sides, vertically you're able to move between workspaces and horizontally between screens, and this sort of thought is present in its keyboard shortcuts (as I'll explain on a later post). It must be noted, however, that screen layout is not horizontal, but rather contained within a single workspace.

Since the side screens are contained in the main workspace and we are evaluating the dimensionality of workspaces, I'll change the previous figure to look like so:

{{< figure src="/2019/10/screenshot_20191013_000031.png" caption="Figure 4 – Containing screens within workspaces" >}}

Whereas something like OSX would be:

{{< figure src="/2019/10/screenshot_20191013_022927.png" caption="Figure 5 – Typical OSX layout, 2D model" >}}

In addition to this, GNOME seems to use a workspace style that I call reverse hospital elevator, namely the opposite behavior of a hospital elevator (which usually contains two doors, one in each side), in a fantastic manner: every time you go down or go up, the elevator itself changes and the floor always stays the same. That is, if you switch workspaces, your primary monitor will change, but your remaining monitors will not.

This is so because your main work activity might change with some frequency, but peripheral appliances and documents (like music and references) might not. Usually people do keep peripherals open for more time, although hidden, and constantly change their main activity, which makes this workspace setting genius.

If users do want to keep peripherals hidden, GNOME has two mechanisms to do so. The first is Hide, activated by Meta+H, which, unlike minimize, only allows for its restoration through the Overview; the second is sending the program to the main screen in another workspace, preferably one that is not to be used often, like the last workspace. This works particularly well if the user typically works on a 0D workspace environment and barely ever works in a 1D environment, that is, if the user almost never uses more than one workspace.

Another thing of note is that, in an elevator, your intent makes you want to press the button, which in turn will lead to the floor you want. This is analogous to the keyboard-shortcut paradigm. Sort of obvious, I know, but it serves to showcase the interface input:intent, which will be important in a later post.

To exemplify: at work, I have only two screens, the main monitor to the left and the laptop screen to the right. When I'm proofreading in GNOME, I have LibreOffice or another text editor on my main monitor and some media player on the laptop screen, in my tests it was Elisa playing Foxes and Peppers.

If I switch to the second workspace, immediately below, my main monitor will now display a Google Chrome{{< super 1 >}} window with reference dictionaries, while Elisa continues at the laptop screen. When switching workspaces, the only movement I perceive is on the main monitor, and as such I ignore the laptop screen, which simply preserves status quo. This fits GNOME's design concerns very well, I believe.

If I once again switch to the next workspace immediately below, I'll see my beloved KMail there, ready for any communication between me and my translation manager at any given time. Again, nothing changed on the laptop screen: Elisa keeps playing and showing that gleeful blurry goodness to me.

The workspace immediately below could contain Dolphin for getting files to proofread or upload them to the server, for instance.

It's worth noting that with dynamic workspaces, GNOME can create an infinite number of workspaces downwards in a very fluid manner.

Windows, OSX, tiling window managers and Pantheon also use a 1D model, but it’s set up so that workspaces are only created horizontally. It is true, however, that this allows for window and workspace movement to go in _only_ one axis, which has its practical uses, which, again, will be analyzed on a later post.

So after the two dimensions, there is the 2D model proposed by most DEs. Almost every other desktop environment that allows for multiple workspaces also allows for the 2D model, since 2D models can fully reproduce 0D, the most important configuration, in addition to partially reproducing 1D models, depending on the DE. Plasma on KDE Neon—and if I'm not mistaken, Kubuntu—, ship with a 2D model, but only one workspace by default. This is so that the workspace pager can hide automatically if the user is content with the defaults, while also automatically appearing once the user adds workspaces. This is the most evident example of Plasma's motto, “Simple by default, powerful when needed”.

With a recent update (5.16), Plasma also acquired the ability to set workspaces according to lines, and this automatically sets their columns as well. This can be set manually too. It allows for the almost full reproduction{{< super 2 >}} of the 1D model: if one wants to use vertical lines like GNOME or horizontal lines like Pantheon, they can. I’d argue that this 2D setting is inherently superior to any other 2D setting, and I don’t really know why most other DEs{{< super 3 >}} don’t already have that. The only thing it cannot reproduce is GNOME’s reverse elevator behavior, but maybe that could be done in the future. Or perhaps not, given that Plasma has no obligation whatsoever to copy other DE functionalities.

Plasma however also ships with the 3D model, _Activities_. How does this work?

Activities in Plasma are better understood if we switch the analogy from dimensions to physical reality, that is, multiverse theory.

First of all, if the user has a 2D model containing, say, 4 workspaces, and those workspaces are arranged like a square/rectangle, with one in each corner, all of that is contained in the Default Activity.

{{< figure src="/2019/10/screenshot_20191013_021315.png" caption="Figure 6 – Example 2D model" >}}

If that’s a tiny universe we created ourselves, adding another Activity would be similar to accessing a parallel universe, where the same set of workspaces exist in their exact original place, just their configuration (widgets and programs) differ.

{{< figure src="/2019/10/screenshot_20191013_021255.png" caption="Figure 7 – Two activities, parallel universes" >}}

To make full use of this, Plasma also has what’s called Window Rules. These rules allow to, for instance, move a specific program to a specific screen, a specific workspace, or a specific Activity. This way, it is easy to keep different tasks separate from one another according to their purpose within that particular context. Indeed, if multiple parallel universes do exist, I’d imagine there would be creatures that only exist in one universe or that exist in only a few, as they would make sense only within certain ecosystems.

I’d also imagine that there would be travelers able to transit between Activities, and as such, they require distinct equipment to do so. That is the purpose of keyboard shortcuts. For those who prefer the mouse, they might also like the devices set up in their titlebars (that little button typically at the left side) or the gadgets presented by right-clicking the taskbar on their panel.

The only limit for those travelers **that I know of** is that they cannot travel to _other universes_, that is, an Activity that has a workspace setting different from their original one. So, for instance, if you come from a universe with 3 workspaces, you cannot travel to a universe with 5 workspaces.

That's it for the concept of dimensions. I hope it is clear enough so that the concepts shown here are understood regardless of terminology, since this is a complicated matter. This might be an oversimplification and not perfectly analogous, but despite its flaws I believe this should make the following posts clearer and straightforward.

{{< super 1 >}} I have to use Chrome for work because of Matecat, a cloud-based CAT tool. The tool is great, but it's sad that I am forced to use Chrome. Firefox is my favorite. It's a red panda after all!

{{< super 2 >}} The addition of dynamic workspaces in a vertical setting like GNOME is a bit tricky. While the KWin script for dynamic workspaces exists, the addition of new workspaces would be bound by the number of existing lines. For this to work, one has to include a number of lines higher than the possible number of workspaces they might use.

{{< super 3 >}} That is an assumption based on personal experience, and as such this part of the text will be changed once this statement can be verified.

 [1]: https://phabricator.kde.org/T11520
