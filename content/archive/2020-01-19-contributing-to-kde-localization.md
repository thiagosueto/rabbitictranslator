---
title: Contributing to KDE is easier than you think – Localization and SVN
author: Blumen Herzenschein
date: 2020-01-19
url: kde-localization
aliases: /wordpress/index.php/2020/01/19/contributing-to-kde-localization/
tags:
  - Planet KDE
---

Hello all!

This is a series of blog posts explaining different ways to contribute to KDE in an easy-to-digest manner. This series is supposed to run parallel to my [keyboard shortcuts analysis][1] so that there can be content being published (hopefully) every week.

The purpose of this series originated from how I feel about asking users to contribute back to KDE. I firmly believe that showing users how contributing is easier than they think is more effective than simply calling them out and directing them to the correct resources; especially if, like me, said user suffers from anxiety or does not believe they are up to the task, in spite of their desire to help back.

This time I'll be explaining how the localization workflow looks like for contributing to KDE; this should also immediately enable you to translate your favorite third-party Plasma widgets (if the project supports it), and generally allow you to translate any PO file with your preferred localization software. I will also explain a bit about CAT tools in general and how professional translation is done since it's my field of expertise, but that will serve only as optional reading for those interested.

Don't get scared with how lengthy this blog post is: by the end of this text, you should be perfectly fine to start working with localization, that's the point. The localization process is quite straightforward, I simply put a lot of explanations in-between so you don't have many (or better yet, any!) doubts about how stuff works.

This article should be timely in that a new Plasma version, 5.18, will be released in about [two weeks][2]. **Contributions to the stable branch would be quite appreciated in the following days!**

If you're already acquainted with git, svn, Phabricator and KDE Identity or if you would simply like to skip the explanations, understand the CAT tool and go to the how-tos, [click here][3].

If you think you already know enough of translation software interfaces and you don't need to get acquainted with the default KDE localization tool, Lokalize, you can skip the explanation about its interface and work immediately by [clicking here][4].

## The KDE localization infrastructure

Currently, KDE's infrastructure is divided between several places, which is something I particularly like.

We have bugs.kde.org for reporting bugs, phabricator.kde.org for discussion and patch reviewing, identity.kde.org for centralized account management, cgit.kde.org and invent.kde.org for active git repositories, github.com/KDE for a git mirror, etc.

To understand the KDE infrastructure, first we must understand a tiny bit about version control software.

Git is the most popular version control software, or so I've heard. Subversion, or SVN, is also version control software, and both serve as a means to store code and data in such a way that we end up having multiple versions of the data stored, meaning three things: one, if a specific version is problematic, it can be reverted, two, there's always a backup/record of what was done, and three, multiple people can suggest changes for a new version of the software.

These suggestions are called commits and, if you're a developer, such commits would go through a review as patches to the code. For translators, however, it works a bit differently: if you do not have the rights to send translation commits directly, it must be sent to either the mailing list or to a translator with commit rights through any means, such as Telegram, IRC or Matrix. If you do have such rights, you are an experienced enough translator who is also trusted by your more experienced peers. With this, your submissions will be available for everyone to see, and your task will be easier and faster.

Some websites provide a frontend for version control software, such as Github or Gitlab, whose names clearly indicate they handle git. KDE intends to migrate code handling to Gitlab, but SVN will still be used for translation and some other stuff.

Both repositories can be seen in Phabricator, which means any translation commit uploaded to git or SVN will show up there. Even if you don't submit the commit yourself, the translator who submits it for you will be able to assign (and should assign) your name as the author of that commit.

You can see the SVN repository on Phabricator here, it's just one of many repositories hosted by KDE: <https://phabricator.kde.org/source/svn/>.

In addition to Phabricator, another place that is relevant to translators is Bugzilla, which serves to report bugs. There is a specific section for managing translation issues, namely the i18n product: <https://bugs.kde.org/describecomponents.cgi?product=i18n>.

And yes, translation issues are also reportable bugs, as weird as that might sound for someone who is not acquainted with bug reporting. If a specific string of text (a segment, sentence or any amount of text in a sentence) is not in the target language of a given application or if it is incorrectly translated, you can report it to the localization team responsible for working on the translations into your mother language.

The reason the Bugzilla product is called i18n is because, long time ago, there was some confusion pertaining to how internationalization (i18n) and localization (l10n) were supposed to be handled. It is a complicated matter, one which I won't adventure within in fact, but to simplify things I prefer to use [the definition provided by Qt][5]:

 > The internationalization and localization of an application are the processes of adapting the application to different languages, regional differences and technical requirements of a target market. Internationalization means designing a software application so that it can be adapted to various languages and regions without engineering changes. Localization means adapting internationalized software for a specific region or language by adding locale-specific components (such as date, time, and number formats) and translating text.

The important part here is that i18n is generally handled by software developers and l10n is generally handled by translators. These definitions may vary a lot if you search for them, though, especially considering internationalization and localization can easily be mixed together.

However, the i18n product in the KDE Bugzilla is a general place to report any issues pertaining to translation, including internationalization and localization issues.

The last important place we need to talk about (briefly) is identity.kde.org.

A KDE Identity is a common login which can be used on Phabricator, Bugzilla, the Wikis, Invent and some other places. There, you can apply for different kinds of account, among them a developer account, which is required for translators to commit directly to SVN.

Since a developer account provides commit rights, this is only provided by sysadmins to trustworthy contributors who have several public contributions and are referred by one of the main contributors in their specific field, which in this case would be a member of the localization team which you contacted and are collaborating with.

This may sound scary but it really is not. It is a mere formality, as long as you fill the two main requirements of referral and enough public contributions.

## The first contact

The first thing you must do in order to start translating is contact the localization team responsible for the language you want to translate KDE software into.

The main, formal way of doing this is by subscribing to the respective mailing list and sending an email showing your interest in contributing, whereas the main informal way of contacting your team should be by IRC, such as the [#kde-i18n channel][6].

A few teams, like mine ([Brazilian Portuguese][7]), have other means of contact, such as Telegram and Matrix, two different Instant Messaging software similar to Whatsapp and Discord, respectively. So if you're unacquainted with IRC or mailing lists, you may contact your team this way. The KDE Community has a [wiki for Telegram][8] and a [wiki for Matrix][9], which I'll try to update later with Telegram/Matrix rooms I find.

Your localization team should instruct you both on how to start translating and on commonly-agreed terminology.

The first thing you'll likely have to do is install three very useful utilities called Lokalize, subversion and kdesvn. Lokalize is the main Computer Assisted Translation (CAT) tool we'll be using to translate KDE software; subversion allows us to download the files for translation, which are hosted through SVN and accessible through a web interface which you can see here: <https://websvn.kde.org/>; and kdesvn is a tool used by translators with a KDE developer account to easily submit any translated files.

The three main places we as translators should care about are as follows:

  * <https://websvn.kde.org/trunk/l10n-support/>
  * <https://websvn.kde.org/trunk/l10n-kf5/>
  * <https://websvn.kde.org/branches/stable/l10n-kf5/>

The folder l10n-support contains folders respective to each language available to translate, and within each folder there are scripts, glossaries and other such things that are used by each team to ease the translation process.

The respective trunk/l10n-kf5 folder for your translation team contains two main folders of interest: docmessages and messages. This is also true for stable/l10n-kf5.

The folder docmessages stores the documentation for a given application, a.k.a. its respective usage manual, whereas the folder messages stores translation files pertaining to the interface of a given application; that is, text strings appearing in the application itself.

Docmessages have a few differences compared to software translation and the translator should at least know a bit about the documentation team and its workflow, so I'd recommend you start with the messages folder instead. It's not that complicated either, though.

Both the stable and trunk folders may also contain some pertinent scripts or glossaries for quick access.

Trunk includes versions of applications that are still in development between major/point releases (such as 5.17.0), in addition to the www file, which pertains to KDE website strings; stable includes a specific version of applications as frozen in time (more specifically, a major release). Once trunk reaches a major release, its current state will be the new stable, and so it is of primary importance to translators; however, stable should also be a priority so that distro releases may benefit from translations. At this exact moment, the current stable will be used for the new Plasma 5.18, which makes this article quite convenient, as I might catch your interest in translating KDE software for this Long Term Support release! 🙂

## The barebones {#barebones}

After that, the one thing you need to do is get the files needed for translation.

For that, you may first check on websvn for the correct folder you want. For instance, trunk/l10n-kf5/pt_BR/messages, the folder containing only interface-related files for Brazilian Portuguese, should show an SVN link: [svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/pt_BR/messages](). To download the entire folder, you may simply type the following in a terminal:

```bash
svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/pt_BR/messages
```

And it should start downloading files into your home folder.

You can also just download the specific file you want from websvn and work on that if you want.

Likewise, you may also create a simple bash script for downloading your desired folders automatically, if you're able to do so.

The files you just downloaded, as you will see, are PO files.

They follow a standard widely used in localization named [GetText][10]. Typically, translators would receive PO template files (those with a .pot extension), translate them and rename them as .po files. Many CAT tools also provide automatic conversion for ease of use. However, since we will translate incomplete files, we'll likely not need to do such a conversion at all.

Let's take a look on how a PO file looks. Here's [kontact.\_desktop\_.po][11] just as an example, since it's quite short.

```gettext
# Translation of desktop_kdepim.po to Brazilian Portuguese
# Copyright (C) 2003-2016 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Antonio Sergio de Mello e Souza <asergioz@bol.com.br>, 2003.
# Lisiane Sztoltz <lisiane@conectiva.com.br>, 2003, 2004.
# Lisiane Sztoltz Teixeira <lisiane@conectiva.com.br>, 2004.
# Lisiane Sztoltz Teixeira <lisiane@kdemail.net>, 2004.
# Henrique Pinto <henrique.pinto@kdemail.net>, 2005.
# Eduardo Habkost <ehabkost@conectiva.com.br>, 2005.
# Eduardo Habkost <ehabkost@raisama.net>, 2007.
# André Marcelo Alvarenga <alvarenga@kde.org>, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016.
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2008, 2009, 2010, 2011, 2012.
# Marcus Gama <marcus.gama@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: desktop files"
"Report-Msgid-Bugs-To: https://bugs.kde.org"
"POT-Creation-Date: 2019-07-22 01:21+0000"
"PO-Revision-Date: 2016-04-06 08:00-0300"
"Last-Translator: André Marcelo Alvarenga <alvarenga@kde.org>"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_br@kde.org>"
"Language: pt_BR"
"MIME-Version: 1.0"
"Content-Type: text/plain; charset=UTF-8"
"Content-Transfer-Encoding: 8bit"
"Plural-Forms: nplurals=2; plural=(n > 1);"
"X-Generator: Lokalize 2.0"
```

This is the header section of PO files and it can be safely ignored: that section will be filled automatically for you later by Lokalize, as long as you fill your information properly.

```gettext
#: src/data/kontactconfig.desktop:14
msgctxt "Name"
msgid "Kontact Configuration"
msgstr "Configuração do Kontact"

#: src/data/kontactconfig.desktop:61
msgctxt "Comment"
msgid "Default KDE Kontact Component"
msgstr "Componente padrão do KDE Kontact"

#: src/data/kontactconfig.desktop:108
msgctxt "X-KDE-Keywords"
msgid "kontact"
msgstr "kontact"

#: src/data/org.kde.kontact.desktop:2
msgctxt "Name"
msgid "Kontact"
msgstr "Kontact"

#: src/data/org.kde.kontact.desktop:73
msgctxt "GenericName"
msgid "Personal Information Manager"
msgstr "Gerenciador de Informações Pessoais"
```

The body section contains strings to be translated.

The first line shows which file contains the string we are going to translate, that is, the context of the string, which is useful to determine which component of the software contains that string; the second contains a generic name for that specific string.

The third and fourth lines are the ones that matter most to translators. The corresponding text for msgid, contained between quotation marks is the source text to be translated, and msgstr is the translated string that the translator enters.

Let's see how it looks from within Lokalize. But first, let's get acquainted with its interface.

## The Lokalize interface {#lokalize}

If you recall, we originally downloaded the required trunk files with `svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/pt_BR/messages`. They will be found on your home folder, so in `~/messages`, that is, `/home/yourusername/messages`. You can then go to whichever file you want and open it directly through Lokalize. But that's not practical: there are, literally, hundreds of files to navigate to, and you don't even know which of these is incomplete! That's where Lokalize comes in. Here's how it looks like when you open it directly for the first time:


{{<figure src="/2020/01/Screenshot_20200119_210508.png" caption="Figure 1 – The Lokalize welcome screen." >}}

If we click on "Configure Lokalize", we'll see the Settings dialog. Lokalize attempts to fill your Identity data based on the default language set up for your user, but it doesn't always get it right, so it's best if we fix it. Mine, for instance, came with kde-i18n-doc@kde.org as mailing list when it should be kde-i18n-pt_br@kde.org, as stated [here][12]. A simple peek on Google, DuckDuckGo, Searx, Ecosia or whatever search engine you prefer should show you the proper mailing list.


{{<figure src="/2020/01/Screenshot_20200119_211246.png" caption="Figure 2 – The Identity dialog." >}}

Alright, now closing the dialog, you may click the "Translate Software" button. It's quite straightforward, and it works the same as going in the menubar and selecting Project > Create software translation project…

A new dialog asking for the location to create an index.lokalize file will appear. There, you can select the messages folder we downloaded before and press Save. This file serves to store your project info and will be opened automatically every time you start Lokalize, which is quite convenient.

On the next dialog, you just need to fill the Target Language field and the mailing list accordingly, then press Ok.

What you will see afterwards is something like this:


{{<figure src="/2020/01/Screenshot_20200119_212246.png" caption="Figure 3 – List of files to translate without titlebar in order to fit into one screenshot and with the kdewebdev folder open." >}}

Now we have all files perfectly sorted in alphabetical order and displaying the level of completeness of each folder and file.

If you have a keen eye, you may have noticed I focused the "Hide completed items" up above. Its very useful functionality allows the translator to pick which file to translate easily.


{{<figure src="/2020/01/Screenshot_20200119_213349.png" caption="Figure 4 – List of incomplete files to translate." >}}

I deliberately didn't open the extragear and playground folders to mention a little something.

The extragear, playground and www folders are special. Extragear corresponds to any KDE software that do not follow the KDE Frameworks and KDE Applications release schedules; playground contains only KDE software that's [incubating][13], that is, software that has recently been added as part of KDE applications and is still being integrated. That's the case with Subtitle Composer, for instance; the www folder isn't related to software text, but rather contains all translatable text found in [kde.org][14] within the www_www.po file, such as the [kde.org/announcements][15] page, and as such it includes content which must be handled and reviewed with more attention, as it's essentially the face of KDE. The latter is an ever-growing behemoth of over 10,000 strings—yeah, strings, not words or characters.

Core KDE software and www should receive special attention, especially when a new release is coming, but you shouldn't dismiss other applications either. As a matter of fact, when contributing to something you like, you should be having fun. And I do mean having fun, choosing what looks the most interesting to you and working with what you would like to translate, at the very least initially. If you have seen a little mistake in the interface or somewhere in your favorite software, maybe fixing the translation should render that petite joy you might want on the specific day you started translating something.

Now, let's finally take a look on how the interface for translating looks like. Click on any .po file you'd like; I'll use subtitlecomposer.po as an example.


{{<figure src="/2020/01/Screenshot_20200119_222823.png" caption="Figure 5 – The translation interface." >}}

If you are already acquainted with Computer-Assisted Translation (CAT) tools, you might recognize most components on this screen almost immediately.

On the upper left section, Translation Units, you'll see a list of strings available to translate containing lots of data over each string. The most blatant are the text and its corresponding translation state.

A green check mark icon indicates the string has already been translated; a purple question mark indicates a string that has some text translated but hasn't been approved yet for some reason (fuzzy), and the gray X shows the string hasn't been translated or edited yet.

A string marked as fuzzy is generally quite important. It may contain a note explaining any doubts you might have so other translators may contact you later or take into consideration when translating. It may simply be a string that has been moved somewhere in the application code and thus might be slightly different. It may also have been detected automagically by Lokalize because of another branch; say you're translating a file on the stable branch and the string looks exactly the same as one that has already been translated in trunk, you'd get a dialog prompting you to check the corresponding trunk string, showing up as fuzzy for you to either change or accept. This is also true for similar strings.

Notice in Figure 5 the bottom left pane, Alternate Translations. The referenced translated string is "Check for errors in the current subtitle", and the current string is "Clear detected errors and marks in the current subtitle". Thus, the previous translation, "Verificar erros na legenda atual", shows up automatically, so even if it is not correct, this at least means you don't need to type the entire sentence for translation, only specific sections.

A better example of this can be seen in www_www.po:

```
Today KDE releases a bugfix update to Plasma 5, versioned 5.3.1.
Plasma 5.3
was released in January with many feature refinements and new modules to
complete the desktop experience.
```

```
Today KDE releases a bugfix update to Plasma 5, versioned 5.3.2.
Plasma 5.3
was released in April with many feature refinements and new modules to
complete the desktop experience.
```

Notice how the only thing that needs to be changed here is the months (January and April) and the version (5.3.1 and 5.3.2) . This feature is incredibly useful for automating the translation of www_www.po.

As for the other panes, Unit Metadata contains notes and the previously mentioned context of the string, that is, in which part of the code it is located. With the context, you can sometimes sort of guess where in the interface the string is, and so you can more easily verify whether your translation is appropriate or not.

If you click the "Add a note…" button, you can add a small note that should appear in your PO file as # Some note. See how it looks like if I edit this string, keep it fuzzy and add a note:

```gettext
#This is a note.
#: src/application.cpp:673
#, fuzzy, kde-format
# | msgid "Check for errors in the current subtitle"
msgid "Clear detected errors and marks in the current subtitle"
msgstr "Limpar erros e marcas detectadas na legenda atuals"
```

The first line is the note itself. The third line includes the word fuzzy in it, indicating its fuzzy status. The fourth line starts with a |, which indicates the string referenced in a previous translation.

You can see a tooltip explaining the role of Alternate Translations by hovering your mouse over the pane's titlebar.

The Translation Memory and Glossary panes are common to all CAT tools. Translation Memory works similarly to Alternate Translations, but only with strings that were translated by you ever since you started translating; it grows together with the number of translations you do. There's even a tab in the interface dedicated to looking into translation memories.

The Glossary, also known as the Term Base, serves to store any terms you'd like to keep for future reference, with additional notes and whatnot. Depending on your localization team, you might have a shared glossary which you can add to Lokalize in order to guarantee a certain level of consistency between translations.

In the professional translation world, glossaries are quite useful for beginners and, well, forever, if well kept. Translation memories are nevertheless very well-regarded by highly-experienced translators (by this I mean several years, possibly decades), as all of the data they contain facilitate the translator's job immensely. Another use for the Translation Memory is to contain only strings pertaining to specific subjects or to specific clients, which is quite desirable. You may have a dedicated translation memory that accounts for all documents pertaining to the internal management of a company, but it might contain specific terminology that only that client requests and that is not canon within the field; using such a translation memory could potentially be detrimental if you're translating for another company insofar as you utilize non-appropriate terminology for that company's documents.

Last, but not least, there's the unnamed translation pane on the upper right. It's no surprise at all: its upper section contains the source text, that is, the text to be translated, and the bottom section contains the target text, the translation _per se_. That's where you'll be working the most.

## The translation workflow {#workflow}

Now that we've gotten ourselves familiarized with the interface of Lokalize, I'll show you the default workflow for translating and a few tips by me.

For those who skipped this wall of text, you can download the files for translation using the method described in the beginning of [this section][16].

After you've opened your desired file, you can select a string that hasn't been translated yet and translate it. It is recommended that you keep the line size on the translated string similar to the source text, but it is generally a cosmetic change for better readability of PO files. Going forward with the explanation: if the cursor is on the last line of the translation, you can simply press Down to jump to the next string. Likewise, pressing up when you're on the first line will send you to the previous string. The same can be achieved by pressing PgDown and PgUp, respectively.

The next string may already be translated, so other useful keyboard shortcuts are Ctrl+Shift+PgUp/PgDown, which go to the previous and next non-ready strings. Non-ready here means both untranslated and fuzzy, of course.

If you find a fuzzy string that doesn't need to be changed, you may simply approve it; similarly, if you don't feel like you want or is currently able to translate a specific term, you might want to translate most of the sentence and set it as fuzzy. In the toolbar just below the menubar and high above the Lokalize interface, you should see the Approved button. Clicking its arrow should allow you to choose which status to set as true for that specific string; clicking directly on Approved will approve the string. You can also toggle this state by pressing Ctrl+U. This only works when there's text in the text target field, of course.

If you're a minimalist and want to clean your interface and arrange it to make it as comfortable as possible, you can do several things:

One is right-clicking on the bar containing Source, Target, Notes, Context etc. on the Translation Units pane and deselecting whatever field you don't want to display. Another is click-and-holding the corresponding titlebar for each pane and moving it. You can arrange them however you'd like, even relative to each pane; it's an advantage of Qt applications, you see. This is the same with the proprietary CAT tool Memsource, also developed using Qt. You can also change the size of each pane like how you can resize windows in any application: by grabbing its borders.

If you have a multi-monitor setup, one thing you can do is unpin a pane from the interface by clicking the diamond icon to the left of the pane's close button and put it on your secondary monitor. Removing panes you don't find useful is also quite handy; in my experience, the Translation Memory pane hasn't been very useful when localizing software even though I've used it periodically, so I often remove it.

If you think a specific string is worth future consideration, say, if you think a dubious translation should be reviewed after you translated the rest of the file or after terminology research, you may also press Ctrl+B to bookmark it. You can then later check the Go > Bookmarks section on the menubar.

Unlike other CAT tools (and similarly to Memsource), Lokalize doesn't abstract tags by transforming them into icons; tags are kept as is, and in the case of PO files, they are usually html tags, such as in `<b>Some string that is bold.</b>`. For those used to traditional translation with professional CAT tools (as opposed to localization software), tags usually correspond to formatting (italics or bold, for instance) in .doc, .docx and .odt document files, and they surround the text that is modified by said formatting. Software doesn't usually contain this kind of formatting though, which is why GetText is a standard that utilizes plain text.

Since Lokalize keeps tags as is, including multi-line links, it is kind of a pain to add them by copy-pasting. You may have the idea of copying the source text to the target text field with Ctrl+Space and then working upon that, but that's not really convenient at all, now is it? Instead, you can use keyboard shortcuts such as Ctrl+T to add tags from a list, or Ctrl+M to add tags sequentially, that is, in a sequence based on which tag has already been added.

I strongly recommend that you explore the Settings > Configure Keyboard Shortcuts… dialog if you're already used to some CAT tool, though. Setting the most used keyboard shortcuts to something you prefer or are used to is incredibly more productive than trying to learn a completely different set of shortcuts which are highly customizable. I tend to set Ctrl+Enter for jumping to the next non-ready string and Ctrl+Shift+Enter for the opposite direction, Ctrl+i for copying source to target and Ctrl+Up/Down for previous/next string. The three first ones come from Memsource, whereas the latter two ones come from Matecat.

One of the last things I should mention and that you absolutely must pay attention to are plurals. Let's take a look on Figure 6 for a bit:


{{<figure src="/2020/01/Screenshot_20200120_001341.png" caption="Figure 6 – Plural Form." >}}

This was a mistake I made on one of my first translations.

Note that the sentence to be translated contains:

```
An error occurred while renaming %1 image.
```

Originally, I had translated it like so:

```
Ocorreu um erro ao renomear a imagem %1.
```

I know most people reading this post won't know any Portuguese, so I'll translate what I assumed the source text meant:

```
An error occurred while renaming image ImageName.
```

I hadn't noticed the upper tabs mentioning Plural Form 1 and 2 on my first time. Additionally, I didn't notice the lack of an article between renaming and %1, and it's generally a silly error. For comparison, see how Plural Form 2 looks like for the source text:

Plural Form 1: `An error occurred while renaming %1 image.`

Plural Form 2: `An error occurred while renaming %1 images.`

This essentially means that, in the first case, the variable %1 stands for the number 1, and in the second case it corresponds to any number higher than 1, that is, anything that requires "image" to be instead plural "images".

Variables are something unique to localization compared to other translation variants: they stand for text whose position may vary according to the context and language that it is being translated into. Translators should be quite wary of them. They are usually represented by some symbol (%, &, #, @) and a number.

Thus, in Brazilian Portuguese, the proper translations would be:

Plural Form 1: `Ocorreu um erro ao renomear %1 imagem.`

Plural Form 2: `Ocorreu um erro ao renomear %1 imagens.`

When one of the experienced translators of my localization team tried to upload my translation, he met with an error provided by a script that checks all PO files before proper upload. By verifying the error with the command `msgfmt -c`, he found out what the error was. The script noticed that Plural Form 2 was missing, and the command specified what string in what file had that error.

Out of curiosity, let's check how it looks like on the PO file:

```gettext
msgid ""
"An error occurred while renaming %1 image."
"Do you want to rename this image again or rename this image by overwriting?"
msgid_plural ""
"An error occurred while renaming %1 images."
"Do you want to rename these images again or rename these images by "
"overwriting?"
msgstr[0] ""
"Ocorreu um erro ao renomear %1 imagem."
"Você prefere renomear essa imagem novamente ou sobrescrevê-la?"
msgstr[1] ""
"Ocorreu um erro ao renomear %1 imagens."
"Você prefere renomear essas imagens novamente ou sobrescrevê-las?"
```

There are two things of note here: msgid_plural now exists and corresponds to Plural Form 2, and msgstr now contains [0] and [1], which are array notation used to correspond strings: [0], the first entry in a typical array, corresponds to Plural Form 1, the first form, whereas [1], the second entry in a typical array, corresponds to Plural Form 2, the second form.

This string also allows us to verify another particularity of PO files: multi-line text is shown below msgid/msgstr and after a "" in front of msgid/msgstr.

One last thing to mention is that, if you've followed this article and you installed kdesvn, you should now have a right-click entry on the KDE file manager Dolphin that is named "Update (Kdesvn)". You don't need to run svn co or svn up whenever you want to re-download your translation files anymore, you can simply go to the folder where you translated them, right-click an empty space and select the option "Update (Kdesvn)". It should show a dialog mentioning it's complete almost immediately, depending on your internet connection.

It is important that you always update your translation files before working on them, since during the time interval in which you've been away or not translating someone might have already changed the file you intended to translate. It's no use working on an old file, you see.

Now, if you take a look on the kdesvn tool, you'll notice how streamlined the interface is.


{{<figure src="/2020/01/Screenshot_20200120_005501.png" caption="Figure 7 – The kdesvn interface." >}}

It is incredibly useful for translators with proper permission to submit translations themselves: kdesvn detects the repository whose folder you open, organizes all files in tree view, highlights the folders and files containing changes, allows to update the repository quickly, and if you already have configured your environment, uploading only the files changed are a breeze. I know svn is quite old and most people acquainted with code will prefer git, but this fine tool that is integrated in Dolphin is precisely what made me like svn in the first place, and I am quite satisfied with it. If KDE ends up migrating the localization infrastructure to git in the (not near) future, I'd strongly appreciate a similar tool that is simple and easy to use even for people not that acquainted with repositories, as well as integrated to Dolphin.

Today will not be the day I explain how translators with permission can upload translated files, though. I still don't know that much about the process, either, to be honest. So if you've finished translating a file, you may send it either directly to a translator with upload permissions or to the mailing list. Initially I sent my translated files to the Brazilian Portuguese Telegram group, and now that I have a developer account I upload files directly to SVN, though I've slacked with that in recent times.

## To summarize

This was a fairly lengthy article, but it should clarify most things necessary and not-so-necessary for potential contributors to make their first step into localization involving KDE software. I wrote it so that it was quite comprehensive, but in practice the workflow isn't complicated at all. On the first time, it boils down to:

  * Download the files to be translated
  * Create a project to list them easily
  * Translate them
  * Send them for upload

And after the first time:

  * Right-click the file manager and update your existing files
  * Open Lokalize (it opens the previous project automatically)
  * Translate
  * Send them for upload

And that's not hard at all. In fact, from the second try onwards and aside from translation itself, the rest of the procedure is so easy and straightforward it ends up becoming part of muscle memory!

For the last bit of this post, I'll add a few links for those willing to learn more or contribute.

Interesting links include the [Get Involved][17] wiki page for general contributing and the [Get Involved Translation][18] wiki page for a brief explanation on where to get more information; you may find the mailing list for your language in the [links available here][19] under kde-i18n or kde-l10n; you may also contact the [general i18n team on IRC][6]; if you're Brazilian or speak Brazilian Portuguese, we have a [Telegram group][7] for you; if you'd like to translate widgets instead, you might wanna have a look at [store.kde.org][20] and search for the github page for the widget; most things related to KDE localization can be found on [the official page][21], for instance the [Translation HOWTO][22], which is an even more comprehensive guide than this blog post; and [this online tool][23] allows you to search already translated strings in KDE software, serving as an interesting translation memory that should even help in other fields. Some localization teams might be more approachable if they have a dedicated website for your country, such as [fr.kde.org][24] for France or [kde.ru][25] for Russia. If you'd like more information about translation on Linux, you may also be interested in checking [TranslateOnLinux][26] or a specialized distribution focused on translation called [tuxtrans][27], made by an Austrian professor at the University of Innsbrück. If you'd like to know about other professional CAT tools, you may want to check the [Proz comparison tool][28]; some of the tools displayed there also run on Linux, either natively, using Java (like [OmegaT][29], an open-source CAT tool) or through the browser.

 [1]: https://rabbitictranslator.com/wordpress/index.php/category/keyboard-shortcuts-analysis/
 [2]: https://community.kde.org/Schedules/Plasma_5
 [3]: #lokalize
 [4]: #workflow
 [5]: https://doc.qt.io/qt-5/internationalization.html
 [6]: irc://irc.kde.org/kde-i18n
 [7]: https://t.me/kdel10nptbr
 [8]: https://community.kde.org/Telegram
 [9]: https://community.kde.org/Matrix#Rooms
 [10]: https://www.gnu.org/software/gettext/
 [11]: https://websvn.kde.org/trunk/l10n-kf5/pt_BR/messages/pim/kontact._desktop_.po?view=markup
 [12]: http://kde-i18n-pt_br@kde.orghttps://mail.kde.org/mailman/listinfo/kde-i18n-pt_br
 [13]: https://community.kde.org/Incubator
 [14]: http://kde.org
 [15]: http://kde.org/announcements
 [16]: #barebones
 [17]: https://community.kde.org/Get_Involved
 [18]: https://community.kde.org/Get_Involved/translation
 [19]: https://mail.kde.org/mailman/listinfo/
 [20]: http://store.kde.org
 [21]: https://l10n.kde.org/
 [22]: https://l10n.kde.org/docs/translation-howto/
 [23]: https://l10n.kde.org/dictionary/search-translations.php
 [24]: http://kde.fr
 [25]: http://kde.ru
 [26]: https://translateonlinux.org/
 [27]: https://www.uibk.ac.at/tuxtrans/
 [28]: https://www.proz.com/software-comparison-tool/cat/cat_tools/2
 [29]: https://omegat.org/
