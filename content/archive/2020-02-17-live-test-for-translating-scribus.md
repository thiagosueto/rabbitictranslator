---
title: How to create a live-test environment for translating Scribus
author: Blumen Herzenschein
date: 2020-02-17
url: translating-scribus
aliases: /wordpress/index.php/2020/02/17/live-test-for-translating-scribus/
tags:
  - Translation
---

Sup everyone!

I've been busy lately with house maintenance, contributions of other sorts, buying and installing furniture, and becoming exhausted from work. I haven't left the house for leisure in three months! That's why I took so long to write a new post.

And technically this post shouldn't be new either. It's a [call for translators][1] I wrote around 7 months ago on the r/scribus subreddit. Just so you have something interesting to read in the meantime.

I chose this one mostly because I finally managed to translate 97% of Scribus into Brazilian Portuguese, yay! I started working on it when it was around 52 or 58%, I can't recall precisely. It was a lot of work, considering it has around 50k unique words / almost 13k unique segments in total!

I should note that as of now the Scribus package hasn't been updated to include the current translation. Also, Scribus [might become a KDE application][2], you know? Let's wait and see.

With the new translation, Brazilian editors should have a better experience using Scribus with up-to-date translations that follow proper desktop publishing terms. Generally speaking, you'll likely not encounter an untranslated section in the GUI unless you're messing with scripts. Hopefully I'll manage to reach 100% and start revising everything from scratch so everything gets consistent! Feel free to send feedback and contribute back with correction of terms. I am not a desktop publishing expert, so I imagine there will be mistakes somewhere.

Some of the build dependencies mentioned in this post are also available on my [github repository][3]. In addition, whatever is presented in [ ] should be regarded as a note.

Hopefully next week I'll be back to posting here!

## Live-testing

Hello folks!

I came here to tell you that Scribus has had quite the development between versions 1.4.x and 1.5.x.

Hopefully by the end of the year we should have the newest 1.6 stable release. But meanwhile…

[The year was 2019 and the Scribus team didn't manage to release 1.6 yet. There have been several technical challenges in that, including the full port to Python 3, so it might take a while still.]

I've been contributing to the Brazilian Portuguese translation of Scribus and would like to share a bit on how to translate and "live-test" your translations locally.

Scribus is an awesome piece of software that I see as having much potential. Unfortunately, while it currently has 62 localization projects, almost none are fully translated, only a few are close and the majority is still far from being done. This means there's a lot of demand for translators! And devs would really appreciate to see more people contributing.

Since several prospective translators may end up wanting to test and see how their translations look like on the Scribus interface, but wouldn't like to depend on devs updating it or are not acquainted with how to build an application with new translations, I made this small tutorial to both be useful and explanatory!

Well, to be clear, there are two ways you can run Scribus with your up-to-date translations: by building the application itself or by changing the AppImage files.

But I'll tell you how to build the application on linux, since that's what I tried and since the AppImage version is currently a little behind (1.5.4).

Since the code for Scribus is hosted on a Subversion instance, we can use svn co instead of the usual git clone. So open the terminal and run the following:

```bash
svn co svn://scribus.net/trunk/Scribus
```

Alternatively, if you prefer, you can clone the repository on gitlab, which is a mirror of subversion.

```bash
git clone https://gitlab.com/scribus/scribus.git
```

It will take quite a while to download the whole source code.

After the download is done, you should have a Scribus folder inside your home folder. Inside, let's create a new folder called `build`, just to keep things nice and clean.

```bash
mkdir ~/Scribus/build
```

That and the folder `~/Scribus/resources/translation` should be the ones we'll use the most and the ones we will care about.

Let's first change directories into the build folder we just created.

```bash
cd ~/Scribus/build
```

The first step in building applications from source is installing dependencies. Fortunately I already researched those for you. 🙂

It's not particularly hard to find out which applications are needed, but that can be explained another time! In any case…

You can install the following dependencies:

[I updated the openSUSE dependencies according to what I tested recently and is recorded in my [github repo][4]. Since the dependencies changed a few months ago, I might need to update the rest of the dependencies for the other distributions in the future.]

**On Ubuntu like so:**

```bash
sudo apt install subversion g++ cmake extra-cmake-modules libpoppler-dev libpoppler-cpp-dev libpoppler-private-dev qtbase5-dev qttools5-dev libopenscenegraph-dev libgraphicsmagick-dev libcairo2-dev librevenge-dev python-all-dev libhunspell-dev libcups2-dev libboost-python-dev libpodofo-dev libcdr-dev libfreehand-dev libpagemaker-dev libmspub-dev libqxp-dev libvisio-dev libzmf-dev libgraphicsmagick++1-dev
```

**On openSUSE like so:**

```bash
sudo zypper install subversion cmake extra-cmake-modules libqt5-qttools-devel GraphicsMagick-devel libfreehand-devel librevenge-devel libvisio-devel libqxp-devel libmspub-devel libcdr-devel libpagemaker-devel cups-devel libtiff-devel libzmf-devel libpoppler-qt5-devel libqt5-qtbase-devel libOpenSceneGraph-devel python-devel libjpeg62-devel liblcms2-devel harfbuzz-devel libopenssl-devel hunspell-devel python3-devel libpodofo-devel boost-devel
```

**On Arch like so:**

```bash
sudo pacman -S subversion gcc make cmake extra-cmake-modules qt5-base qt5-tools openscenegraph python2 pkgconfig hunspell podofo boost graphicsmagick poppler librevenge harfbuzz-icu libfreehand libpagemaker libcdr libmspub libqxp libvisio libzmf
```

**On Solus like so:**

```bash
sudo eopkg install -c system.devel

sudo eopkg install subversion qt5-tools-devel graphicsmagick-devel openscenegraph-devel poppler-qt5-devel qt5-base-devel librevenge-devel libfreehand-devel libvisio-devel libqxp-devel libhunspell-devel libmspub-devel libcdr-devel libpagemaker-devel podofo-devel cups-devel libjpeg-turbo-devel libtiff-devel libzmf-devel libboost-devel
```

_(yes, for Solus it's two commands; the first adds the repo containing build dependencies, the second contains dependencies themselves)_

After that, you can run this single command:

```bash
cmake -DCMAKE_INSTALL_PREFIX:PATH=~/bin/scribus ..
```

Which will generate build instructions inside the build folder using the content from the upper directory (`..`), that is, the Scribus folder, and set it to install directly to a new `~/bin/scribus` folder.

It should finish just fine. If not, do tell me so I can update this post. 😀

[There are other two interesting options for us to add to cmake: `-DWANT_GRAPHICSMAGICK=1`, `-DWANT_GUI_LANG="en_GB;de;fr;it;en;pt_BR"` and `-DWANT_DEBUG=1`. The first includes graphicsmagick functionality; the second lets you select which languages will be the only ones available for Scribus, wherein the less, the quicker it compiles; the third builds Scribus with debug symbols if you'd like to contribute back to the team with [bug reports][5] should you experience any crash.]

Once finished, you no longer have to run this command ever again (for our localization purposes). You can now build and install it:

```bash
make install
```

Note that the output mentions several localization files, the kind we will be using for translating, such as `scribus.pt_BR.ts`

Shouldn't take too long.

Now, the localized files that were previously `.ts` now have a `.qm` extension and should be installed in `~/bin/scribus/resources/translations/`. Sadly, we can't edit/translate these kinds of files.

Anyway, now we have the latest version of Scribus 1.5.x installed! Good job.

Just one last command is needed.

```bash
sudo ln -s ~/bin/scribus/bin/scribus /usr/local/bin/scribus-trans
```

This command symlinks the scribus binary you just installed to the usual path where local executables are found, and names it `scribus-trans`. This means you can run it from the terminal by writing `scribus-trans` or through your menu. You can name it however you want, but if you name it differently you can have multiple versions on your system! Namely, you can install `scribus`, `scribus-ng`, `scribus-trunk` and your built `scribus-trans`. If it's going to be the only version in your system, you can simply call it scribus, or if you want to keep two versions, you can call it scribus-trunk.

This is useful in case you want to compare features or translations between the 1.4.x and 1.5.x versions.

Now to translate: you have two options, either you join the localization team in the very popular, easy to use and absolutely proprietary™ platform for localization, Transifex, or you can translate the localization files themselves.

On Transifex, after changing any string, you should be able to go to your Dashboard, visit the section for your language pair and click on Resources. Download the file for use, rename it according to the scheme in `~/Scribus/resources/translation` and replace it!

Similarly, you can translate the files locally by simply editing the aforementioned localization files (the ones with `.ts` extension under `scribus/resources/translations`) on a text editor or opening them in something like QtLinguist (which should be provided by the package `qt5tools` that you already have installed by now).

I'd suggest checking how your translation looks in Scribus every 1000 or 2000 words. This way, you can always keep in mind more or less where each part you translated is and whether the language you used is consistent, while also not interrupting your workflow much. But it's just a suggestion!

After that, simply come back to your build folder and type the command make install again!

You don't need to run any of the other previous commands again; if you need to update your translation, simply replace the `.ts` file, go to your build folder and make install! Simple!

This will change your current local Scribus to include the new translation. It should give you a better idea on whether your translation fits well with the interface or not, if it's consistent, if there were typos or mistakes regarding variables, or if the interface reads fluently like one would expect from a professional desktop publishing application.

That's it. There isn't that much to it aside from the initial setup. Now this should be a suitable setting for you to live-test your translations easily! I hope this little tutorial has been helpful!

For more information, please refer to the [current scribus wiki](https://wiki.scribus.net/canvas/Official:Translation_Howto) or contact the devs over at #scribus on IRC, they're very friendly. 🙂

 [1]: https://www.reddit.com/r/scribus/comments/c7mn19/call_for_translators_heres_how_to_create_a/
 [2]: https://phabricator.kde.org/T10034
 [3]: https://github.com/herzenschein/build-deps
 [4]: https://github.com/herzenschein/build-deps/blob/master/Scribus_1-5-x_openSUSE.txt
 [5]: https://bugs.scribus.net/
