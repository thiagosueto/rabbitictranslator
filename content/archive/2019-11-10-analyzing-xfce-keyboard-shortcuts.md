---
title: Analyzing XFCE keyboard shortcuts
author: Blumen Herzenschein
date: 2019-11-10
url: xfce-shortcuts
aliases: /wordpress/index.php/2019/11/10/analyzing-xfce-keyboard-shortcuts/
categories:
  - Keyboard Shortcuts Analysis
---

Hello one more time! Last week I analyzed [GNOME keyboard shortcuts][1] because I’m in an endeavor to analyze keyboard shortcuts in most major DEs just so that I can provide insight into what the best defaults would be for KDE Plasma.

I must say, this one brings me memories since XFCE was my second Desktop Environment, and it also rendered quite some nice insight since a significant basis for comparisons has already been established—my blog should target primarily Plasma users, after all, and GNOME, a keyboard-enabling environment, was already verified.

With that said, let's start our analysis of XFCE keyboard shortcuts.

## Preparations

For testing XFCE, I installed full xubuntu-desktop on my work machine which includes Kubuntu 19.04 (with backports).

I also used a Xubuntu 19.10 live USB on both my home and work machines. Oh, how I missed that squeaky mascot placeholding the interactive area of whisker menu!

I checked MX Linux, Sparky Linux, Arco Linux, EndeavourOS and Void musl XFCE on VMs for comparison. Generally speaking, Xubuntu, MX Linux and Arco Linux had very different keyboard shortcuts, and I must say beforehand: the distro which comes closest to that seems to be Void, which does not even include a panel by default (similarly to Openbox) and does not seem to change anything about XFCE keyboard shortcuts, whereas the most complete experience was Xubuntu. As it has the most polish and is arguably the most popular one, I'll talk first about Xubuntu and comment on the others using it as parameter.

For sources, since XFCE themselves do not provide lists with keyboard shortcuts, I initially used [this random but minimally comprehensive page][2] to acquire the typical modifier combos so that I could experiment; however, afterwards I would learn that XFCE, unlike Plasma and GNOME, stores its keyboard shortcuts in a single file, which made my life easier. It is stored in `/etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml`, and it is the default configuration for XFCE; distros however instead prepare a user config file stored in `~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml`, even if no significant changes were made.

I must mention two things: first, it would be useful for readers to pay particular attention to when I mention "XFCE" and when I mention "Xubuntu", as those are not used interchangeably. In addition, reading of [my previous post on workspace dimensions][3] should make things more clear for those who never read my blog or this series before.

## Xubuntu's immediate affairs

The first contact the user might have with XFCE after logging in is the menu, and in the case of people with keyboard-driven workflows, its associated key, Meta.

In XFCE, that is not the case. XFCE's defaults include no keybinding for the menu, so Xubuntu adds its own. The whisker menu can be opened with Ctrl+Esc, which is an interesting choice because it was clearly made to compensate for an issue related to binding the Meta key to the menu.

What happens is that XFCE is unable to discern modifier behavior from typical keyboard shortcut behavior, that is, any keyboard shortcut you set, no matter the keys used, will behave the same. If you bind Meta to open the menu, when using any other keyboard shortcut that involves Meta, the menu will open before the keycombo is finished. Period.

If setting Meta to the Menu will open the menu every time someone wants to, for instance, open the Terminal with Meta+T, this can quickly become annoying. By default, the whisker menu steals focus when it is open, which means the terminal you tried to open will not accept input unless you close the whisker menu.

The terminal is also the first contact for several tech-savvy or experienced Linux users out there. Its keyboard shortcuts are interesting as well: both Ctrl+Alt+T and Meta+T are set by default. This was clearly in the devs minds as they designed the desktop: as I'll show soon, Xubuntu uses an alphabetical logic to set its application shortcuts, and sets them so with Meta; we can safely assume that Meta+T is the preferred keycombo, whereas Ctrl+Alt+T is present for two reasons: it's the default for XFCE, and it's standardized among desktop environments.

By default, Xubuntu only includes one workspace, which determines that its preference is a 0D model, as expected, since it's the most basic of desktop usage; however, it is easy to create more, as long as you don't exceed 32 workspaces. I created my usual 4 workspaces—Work, Folders, Browser and Media—and added a workspace switcher to my tray. By default, workspaces are displayed in a horizontal manner, very typical of 1D models, but a 2D model can be achieved by configuring the workspace switcher to have more than one row.

As I use a two-monitor setup both at home and at work, I set up layout first by how I knew best, xfce4-display-settings, XFCE's own tool for display settings that looks a lot better than arandr. A few days later, when searching for keyboard shortcuts, I found out that the XFCE default Meta+P activates a popup for screen configuration much similar to Plasma's kscreen! It was a very nice surprise. Even nicer is that both share the same keycombo.

## Xubuntu &#8211; Respect!

As stated before, Xubuntu by default has only one workspace. This is likely a preferred setting, given the fact the remaining distributions displayed the ability of setting the workspace switcher by default in the panel with multiple workspaces preset. In addition, XFCE includes absolutely no GUI-discoverable way to determine that navigation shortcuts even exist.

There is also currently [no means to move a window to another monitor in the window manager][4]. Consequently, [there is no keyboard shortcut for that][5]. What this means is that the only way to move a window to another screen—at least currently— is by pulling the window with the mouse either on the titlebar or the menubar, requiring the help of a third-party tool like wmctrl+xdotool in order to execute such functionality with a keycombo.

Aside from that, controlling windows within one workspace in Xubuntu is rather impressive. By default, Alt+F5 will maximize a window horizontally, Alt+F6 vertically, Alt+F7 fully, while all three can be toggled. Alt+F8 shows a window in all workspaces, Alt+F9 minimizes the window, Alt+F11 turns the window fullscreen, **including the absence of the titlebar for any application regardless of being CSD or SSD**, functionality I had never even thought about before. I found this awesome for laptop usage. Alt+F12 pins a window to always stay above.

Xubuntu makes some sense in its use of the F# keys, though it has issues. Window sizing is neatly set with very close keys, first horizontally (as we read), then vertically (the parallel equivalent), then maximized (both horizontal and vertical), which is nice. F11 is traditional; many a software have used it before. What I dislike is the fact that minimize, maximize and fullscreen are not paired together when they could, as they make sense semantically, from minimal screen space usage to maximal screen space usage. I also don't think it is practical to have (Alt+)F5 available compared to XFCE's defaults (which don't have it) simply because it is easy to accidentally press F4, a potentially destructive behavior.

It is worth mentioning that XFCE's defaults on that are very different. In XFCE, F6 sticks a window in all workspaces, F7 moves a window with the mouse, F8 resizes the window with the mouse, F9 minimizes, F10 maximizes, F11 is fullscreen and F12 pins a window to always stay above. Notice how F9 to F11 are neatly stacked, similarly to Xubuntu's F5 to F7.

Alt+Shift+PgDown is a unique treasure for this 0D environment also. What it does is set the current window to the bottom layer. For instance: say you have three Thunar windows, 1, 2 and 3, where 2 is stacked over 3, and 1 is stacked over 2. That is, 1 is on the upper-most layer (selected) and 3 the bottom-most layer. Pressing this keybinding makes it so that 1 gets below 3, and 2 assumes its upper-most position. Pressing it again transfers 2 below 1 which is below 3; pressing it again transfers 3 below 2 below 1, restoring the original layout and making it a perfect cycle. This behavior can be read in another way too, if it is easier to understand; for instance, it could mean "switch focus to a window one layer below". This is not entirely representative because if true it would mean that this command would never reach window 3, similarly to dynamic/smart application switching seen in both Plasma and Windows, but it is easier to interpret, I assume.

Alt+Tab and Alt+Shift+Tab are the preferred way to switch between applications within the same workspace with the keyboard however, given that they are primarily based on a two-keys keycombo that is well known in any environment; there is no such thing as Meta+number to switch between applications on the panel's task manager; generally speaking, everything can be done with the mouse, not everything with the keyboard, which is fine. It is not a keyboard-oriented environment, after all.

One thing I found clunky and very similar to Plasma was the presence of Up and Down snapping, namely Meta+Up/Down/Left/Right would snap the window to each respective part of the screen. The majority of screens still being shipped is 1366x768, and so this kind of snapping to the upper-/bottom-most part is essentially useless. Even in my 1920x1020 screen I cannot find any use for that, though I presume it is possible and it therefore exists. I see, however, no particular solution to that other than what GNOME did.

Anyway, all of this makes XFCE very suitable as a 0D environment. It also shows signs of a good 1D model preference, given the following 1-axis-oriented default XFCE keycombos:

Ctrl+Alt+Home is set to "Move window to previous workspace", and Ctrl+Alt+End to "Move window to next workspace". Ctrl+Alt+Left/Right move between workspaces horizontally. If PgUp and PgDown semantically imply Up/Down, Home/End can imply Left/Right by analogy. If previous is associated with Left and next is associated with Right, then it forms one horizontal axis.

Ctrl+Alt+Shift+Left/Right is a mysterious thing. Supposedly it should allow to move a window from one workspace to another, but I could not do that in any of the distributions tested. I can only think this is a bug in XFCE. Ctrl+Alt+Home/End work, so in practice their absence is not an issue, but as a parallel to Ctrl+Alt+Left/Right, they would work very well, despite the fact they are 4-keys keycombos.

Meta+Insert and Meta+Delete add/remove new workspaces, respectively. This is an XFCE default.

In addition, Ctrl+F# switches to a given # workspace, namely Ctrl+F4 would switch to the 4th workspace. This is typical of 1D environments in that the F# keys serve as a row much akin to the standard layout.

If we switch to a 2D model, however, things still work out just fine. In addition to previous/next, Ctrl+Alt+Up/Down allow for vertical movement within workspaces. The F# keys do lose their row semantics, but aside from that, it shows no major issues.

As can be seen, all dimensions shown are respected by XFCE, thus being perfectly compliant with any workspace workflow (with the exception of 3D, which is a Plasma-exclusive feature), despite some shortcut issues.

Changing the subject from navigation to applications, default application keyboard shortcuts in Xubuntu **generally** use the first-letter rule assigned to functionality semantics, regardless of application name. That is: Meta+T = terminal, Meta+F = file manager, Meta+M = mail, Meta+E = editor, Meta+W = web browser, Meta+L = lock. Lock also makes sense since each bit of XFCE is an independent application, and named like so, xflock4.

This is possible because of the presence of exo-utils and libexo, a small program which works very similarly to xdg-open, kde-open5 and gnome-open. It sets default applications for default functionality, and so keybindings are not set to specific applications, providing certain flexibility with keyboard shortcuts.

The exceptions to this rule seem to be an afterthought that makes sense. I found no discernible way to explain why Meta+P = display and Meta+F1 = show mouse position. Presumably, Meta+P could refer to popup, but it seems unlikely. Meta+1 opens parole, the music player, 2 opens pidgin, the IM, 3 opens LibreOffice Writer and 4 opens LibreOffice Calc, which shows they are additional, common applications set to easy-to-type keycombos. They make sense, but the reason for that clearly is the fact the aforementioned rule is very limiting in that many default programs will typically have the same initial letter, in addition to the fact that variants of the same application break this rule: L cannot be set to both Writer and Calc, for instance, and so the first letter of the second word would be expected. Using Meta+1–4 is a simple solution for what's left while also not making use of Meta+5–0, which are simply awful to type with one hand. Technically speaking, however, Meta+4 is already bad for typing with one hand, so there's that.

Aside from Meta+first letter, XF86 keys are also available. For those who do not know this, the function keys, typically available by pressing a function modifier key plus an F# key or by pressing extra keyboard keys (such as Home, Volume+, Brightness-, Email, etc., not available in every keyboard) are canonically recognized in X11 as XF86. XF86WWW for instance is the equivalent to that key with a Browser icon. XF86AudioRaiseVolume, XF86AudioLowerVolume and XF86AudioMute should be more common and familiar to users, since if this sort of keys exists in your keyboard, then at the very least it likely includes these keys.

For the sake of completion, Alt+F1 opens the desktop's context menu, Alt+F2 opens the collapsed application finder (very similar to KRunner), Alt+F3 opens the application finder fully (very similar to the menu), Alt+Space opens the popup menu for the current window. Those are XFCE defaults.

Ctrl+Alt+Esc, similarly to Plasma, invokes xkill, which kills any window instantly with a mouse click.

Unlike Plasma and GNOME, XFCE and Xubuntu are not striving to standardize Meta as a system/shell/desktop shortcut; as a matter of fact, this would go against the way Xubuntu has used Meta at least since 2014, from what I could find.

All in all, I generally liked very much the amount of thought put into keyboard shortcuts in Xubuntu 19.04.

## Arco Linux &#8211; Too many options, too many heads

Like Xubuntu, Arco makes use of Ctrl+Esc in order to free Meta for applications, but the use of the first-letter rule is much more extensive than Xubuntu's, especially since it uses Meta, Alt, Ctrl+Alt and Meta+Shift as modifiers.

Meta+M = music player (pragha), Meta+H = htop task manager in urxvt (which is inconsistent with Ctrl+Shift+Escape), Meta+V = volume control with pavucontrol, Meta+Esc = xkill (instead of Ctrl+Alt+Esc), Meta+Enter = terminal (akin to i3), Meta+X = eXit, a.k.a. logout, Meta+L = lock screen with slimlock instead of xflock, Meta+E = editor, namely Geany, Meta+R = rofi theme selector, Meta+C = toggling conky on and off, Meta+Q = quit application, again akin to i3.

There there's the Meta+F# keys: Meta+F1 = browser, Meta+F2 = Atom, Meta+F3 = Inkscape, Meta+F4 = Gimp, Meta+F5 = Meld, Meta+F6 = VLC, Meta+F7 = VirtualBox, Meta+F8 = Thunar, Meta+F9 = Evolution, Meta+F10 = Spotify, Meta+F11 = fullscreen rofi, Meta+F12 = rofi. Those seem to be arbitrarily defined as third-party tools being bundled in the F# keys (with the exception of thunar), which, once again, feels like an afterthought.

There's also the Ctrl+Alt+letter keys: Ctrl+Alt+U = pavucontrol, which is inconsistent with Meta+V, Ctrl+Alt+M = settings **_M_**anager, Ctrl+Alt+A = app finder, Ctrl+Alt+V = Vivaldi, Ctrl+Alt+B = Thunar (I see no letter association here), Ctrl+Alt+W = Atom, Ctrl+Alt+S = Spotify, Ctrl+Alt+C = Catfish, Ctrl+Alt+F = Firefox, Ctrl+Alt+G = Chromium, Ctrl+Alt+Enter = Terminal (like Meta+Enter).

Other keyboard shortcuts include Alt+F2 to execute gmrun, an application launcher similar to application finder, Alt+N/P/T/F/Up/Down for variety, the wallpaper changer, Ctrl+Shift+Escape to run the XFCE task manager, Ctrl+Shift+Print = gnome-screenshot, Meta+Shift+D to run dmenu, Meta+Shift+Enter to run Thunar, Ctrl+Alt+PgUp/PgDown to transition between conky layouts, Ctrl+Alt+D shows the desktop, F4 (yes, by itself, not including Alt) opens the XFCE terminal as a dropdown, quite similarly to Yakuake.

All in all, I found Arco to be a mess. No logic is used throughout the whole keyboard-driven UX (it can be the first-letter of application name or functionality, it can be the first-letter of second name, it can be the corresponding letter to a CLI option, it can be a bundle based on nativity of software…), modifiers have no discernible logic either (what differs between Ctrl+Alt, Meta+Shift, Meta? Only Alt is more or less consistent), applications bound to F# keys are not memorizable at all and seem completely arbitrary, and the main issue with all of that: the huge majority of custom changes to Arco CANNOT be seen in XFCE's keyboard settings tool, and only partially through the default conky file, which makes their majority effectively undiscoverable unless the user checks the preset user config file, which is also messy. But then again, aside from XFCE's default config file, all distros had messy user config files.

Additionally, Arco Linux includes 4 workspaces by default.

## MX Linux &#8211; Not-so-custom keybindings for a very custom duck

One thing that it does differently from Xubuntu is usage of the menu, which is bound to Meta. Because of this, it uses no other Meta+key shortcut aside from Meta+P in order to avoid annoying the user with the aforementioned issue. Meta+P behaves differently, however: instead of a popup, it opens the display settings application directly.

I particularly found two keyboard shortcuts interesting in MX Linux.

Alt+F1, instead of opening the generally-not-so-useful desktop context menu, opens the mxmanual, basically the Help section for MX Linux, which is pretty friendly to new users, though the manual also lies on the desktop by default as an icon. It is also nice in that it uses F1, which is standardized as Help in many applications.

F4 (yes, by itself, not including Alt) opens the XFCE terminal as a dropdown, quite similarly to Yakuake. This is a particularity of MX Linux, which is nice in that it gives this heavily-customized distro a sense of identity. I have no idea why it would be F4, however.

Ctrl+Alt+M to find the mouse is nice in that it follows the first-letter rule, but aside from that, nothing else does this.

MX Linux patito feo did not have much in terms of keyboard shortcuts in comparison with its amount of user-friendly utilities.

Additionally, it includes 2 workspaces by default, and in a vertical manner, curiously enough. This is likely because the panel in MX Linux is set vertically on the left side of the screen.

## Sparky Linux, Void Linux, EndeavourOS

All three did not do much in terms of keyboard shortcuts. If anything, their differences are mainly aesthetic or low-level. EndeavourOS provides a much more glossy and modern design, with transparency in the panel and whatnot; Sparky seemed to strive for a more traditionalist aspect, similar to Linux Mint, but more bland; Void ships XFCE as pure as one can get, with its clearly dated interface.

Sparky included 2 workspaces, Void included 4 workspaces, and EndeavourOS included 1 workspace by default. XFCE is so fitting with all three dimensions that these three distributions, shipping essentially default XFCE, managed to ship with three different dimensions.

## Final Considerations

I am more and more inclined to believe that Ctrl+Alt+arrows are very suitable to navigate between workspaces. I also feel like the ability to switch between screens is becoming more relevant with the increasingly common multi-monitor setup.

The first-letter rule works with small environments, but can be overwhelming if the DE or distribution makes excessive use of preconfigured keybindings.

I wonder what was the semantic association which lead both XFCE and Plasma to use Meta+P for display settings.

Xubuntu worked around the challenges of 2D environments and its compatibility with 0D and 1D environments very well.

Verifying keyboard shortcuts in different distros was tiring as hell too.

 [1]: https://rabbitictranslator.com/wordpress/index.php/2019/10/14/analyzing-gnome-keyboard-shortcuts/
 [2]: https://defkey.com/xfce-shortcuts
 [3]: https://rabbitictranslator.com/wordpress/index.php/2019/10/13/fantastic-workspace-dimensions-and-their-limits/
 [4]: https://bugzilla.xfce.org/show_bug.cgi?id=12377
 [5]: https://bugzilla.xfce.org/show_bug.cgi?id=15114
