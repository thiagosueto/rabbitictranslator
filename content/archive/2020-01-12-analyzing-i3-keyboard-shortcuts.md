---
title: Analyzing i3 keyboard shortcuts
author: Blumen Herzenschein
date: 2020-01-12
url: i3-shortcuts
aliases: /wordpress/index.php/2020/01/12/analyzing-i3-keyboard-shortcuts/
categories:
  - Keyboard Shortcuts Analysis
---

Hello all! I am late with my blog posts, I know. One week I was too tired, the other I got sick, the other I drank and ate a little too much, the rest I was too busy. This however means I've had more weeks available at work to use the next environment in this series, i3, whose workflow is heavily-keyboard driven. Although it's not a Desktop Environment, it deserves an analysis due to the mere fact it's highly keyboard driven.

For those who aren't acquainted with this series yet, I am in [an endeavor to analyze keyboard shortcuts][1] in most major DEs so that we, the KDE community, can decide on the best defaults for KDE Plasma. I have already taken a look at [Cinnamon][2], [MATE][3], [XFCE][4] and [GNOME][5].

## Preparations

This time I've added i3wm to my work computer, which has Kubuntu 19.04 with backports, and also tried Manjaro i3 briefly. The first was done as the previous times, for convenience; the latter was chosen simply because Manjaro ships a community edition with i3 by default, which caters to the significant Arch/Manjaro userbase of i3, but as it is a highly customized community edition of Manjaro, and thus includes about five times more keyboard shortcuts than default i3, I did not include most of its keyboard shortcuts here.

Since i3 is single-config-file-based and is [hosted on github][6], it was easy to both check on how upstream suggests that keyboard shortcuts should be bound and keep track of keyboard shortcuts.

You may also see the default Manjaro i3 config file [here][7] if you're curious about it. Manjaro does include several interesting QoL utilities such as i3exit and blurlock.

Despite that, this post will be considerably lengthy because, although upstream i3 has few shortcuts, several of them introduce complex concepts.

In addition, due to the nature of i3's config file, this post is more complex and requires previous knowledge on Linux and perhaps coding before grasping how i3 works. It's not **that** complicated, but it's quite the paradigm shift compared to DEs.

## The Meta/Alt dillema

Right after booting into i3, a small utility called i3-config-wizard will run and ask us if we would like to have Meta or Alt as the main modifier.

After choosing any of the two, it will promptly read the keycodes of your keyboard layout so that it can generate a config file in ~/.config/i3/config which is keysym-based.

Now, I'm not that knowledgeable about how keyboards are managed at such a low level as X, but I think what this means is that i3 lets the keyboard be detected by X, which typically associates keypresses with keycodes, similarly to how XF86 keys work. Keycodes for your keys can be seen with xev, bound with xmodmap/xbindkeys, and shortcuts for them can be set with xdotool. The i3-config-wizard utility will then notice if something in your keyboard layout differs from default US QWERTY keyboards, and sets a keysym for that keyboard shortcut to work properly.

This is better explained with an example; my keyboard layout, ABNT2, has a left-to-right sequence "hjklç", which differs from standard US QWERTY, "hjkl;". Thus, as i3 checks the keycodes in my keyboard layout and notices that a ccedilla ç is situated where a semicolon ; is typically located, it writes the config file as to include "ccedilla" instead of "semicolon", the latter of which would be out of place in a typical Brazilian keyboard layout such as mine.

Hence, by not shipping a config file by default, i3 guarantees that each key is in its correct place, regardless of which key that is. I figure this should be particularly helpful with typical french AZERTY keyboards.

What I found most interesting the first time I used i3, long ago, was the extensive use of letter keys corresponding to the arrow keys. The default i3 comes with jkl;, which is vim-like, but not a clone to vim, which uses hjkl. This is so because jkl; is perfectly positioned on the home row, thus rendering it more natural for typing.

I am particularly fond of this stance since the i3wm developers decided for the assumedly better, most ergonomic choice instead of simply adhering to vim keybindings, whose defaults are kept to this day for historical reasons which no longer make sense.

For those who are unaware of vim keybindings, vim is a default editor which can be found in most linux systems—if not all of them—whose workflow attempts to keep the user's hands near the main letter section of the keyboard, and so it avoids the arrow keys, the Home/End/PageUp/PageDown keys and the Numpad area. For that, it binds h to left, j to down, k to up and l to right. In i3 this is brought one key to the right, so j is left, k is down, l is up and ; is right. Those can be changed for whoever would like to use vim keybindings or, if they are moved away from i3 simply due to this design choice, sway adopted vim keybindings by default and is quite similar to i3, as can be seen [here][8].

Anywho, back to the topic: i3-config-wizard serves to associate such keys dynamically for you, but it also serves to set the default modifier key for your keyboard shortcuts, namely Meta or Alt, as mentioned earlier.

This is a highly critical choice to me, at least insofar as it is problematic. In addition to the main modifier key, the Shift key is quite used as well. If one chooses Meta, then Meta would stay perhaps too close to the Shift key, potentially rendering awkward hand positions, such as Meta+Shift+Q; if one chooses Alt, then the user will see themselves encountering less awkward hand positions, but since i3 extensively uses the first-letter-for-functionality convention (such as Modifier+F, where F means fullscreen), such fairly common combinations (Alt+F) might conflict with in-application keyboard shortcuts, most notably accelerators. For instance, Alt+F is a common accelerator and keyboard shortcut for opening the File menu in applications that come with a menubar.

Despite my fair share of concerns with hand positioning, I tend to use Meta as the main modifier so as to avoid such conflicts since both my keyboards have an acceptable distance between Meta and Shift. Meta is also the default shown in the live Manjaro i3, by the way; it corresponds in the config file to Mod4, whose keycode is Super\_L. The system default config file for i3 has Mod1, whose keycode is Alt\_L, the left Alt key.

Let's henceforth call the modifier key MOD for easier reference, regardless of it being Meta or Alt.

## bindsymming all the way

Generally speaking, i3 doesn't have that many keyboard shortcuts, which makes it particularly easy to maintain in terms of key semantics. All keyboard shortcuts can be set with bindsym in the config file, and searching for the word bindsym in it will render 72 results. In practice, however, the number of keyboard shortcuts we should need to remember would lie between 20 and 30, quite the low number.

For keeping this blog post short, I will only talk about keyboard shortcuts available through bindsym, and not other functionality available from the config file.

There are around four different syntax for bindsym.

The first is based on executing a command-line application, typically on the background, and the first example in the default config file.

```bash
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status
```

If you have extra volume keys on your keyboard, run xev and press the key, your terminal should likely spout some info, among which resides something like `keycode 122 (keysym 0x1008ff11, XF86AudioLowerVolume)` for the lower volume key. The command bindsym will first read the keysym name shown.

If it is followed by the exec command, the next argument will run the functionality of the given CLI application, which in this case is pactl.

The other exec-based keybindings found on the default config file are as follows:

`bindsym $mod+Return exec i3-sensible-terminal`, which runs the terminal. I particularly liked the use of the Enter key together with the main modifier key. Enter has quite the semantic possibilities with other keys and is rather easy to type due to its size.

`bindsym $mod+d exec dmenu_run`, which runs the main application launcher, namely dmenu. This is fairly easy to type when MOD is Alt, but it can be a tad annoying when using Meta, and with such a keycombo you lose the speed and convenience of opening your menu with a single key, which is available on most other DEs.

`bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"`, which runs the small i3-nagbar prompt for logging out. I find this to be highly incoherent with the i3 ecosystem. Essentially everything in i3 is keyboard driven, but suddenly, to log out of your system, you have to click a small button on the upper right of your screen to confirm your exit. I understand this confirms user intent for a potentially destructive action, but I don't effectively see a reason for this user intent confirmation to be mouse-only. The GUI feedback is desirable, but maybe its confirmation could be done with the keyboard too. The i3exit utility (which Manjaro ships by default) is way more suited to i3 I'd say.

Aside from the destructive actions avoidance, this reminds me marginally of how GNOME includes the application launcher at the bottom part of the left-sided dock, which is also an intended design choice. This is so that the user may associate the first icon of the dock with 1, so pressing Meta+1 will run the respective application. This is a consistent confusing aspect of Unity, which has the application launcher at the upper part of the left-sided dock, and so the application opened with Meta+1 is the **second** in the dock. In addition to that, the application launcher at the bottom necessarily guarantees user intent, and is a design choice that privileges keyboard-driven workflows. I am not particularly a fan of this, but that's what it is, a design choice.

The next syntax for bindsym would be like these:

```bash
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right
```

Which is similar to exec, but instead of executing an application the argument immediately after the keys determines the operation desired, or function, to simplify things. In the case presented, MOD+arrow keys will switch the focus to another window in cardinal directions. This is similar to how Plasma has Meta+Alt+arrows.

You see, to explain why this is particularly useful, I'll have to explain how tiling window managers typically work, at least a bit.

Common DEs use a floating-mode for windows; windows can be placed above each other at any given position desirable, and they may follow the mouse, that is, they're "allowed to float". Tiling window managers use tiling for windows; creating new windows automatically places them adjusted on the screen, effectively dividing the screen with automatic window management. This is similar to how you may push a window with the mouse to a screen edge and fix it there.

The advantage of tiling arrangements is their neat organization. Since they are always positioned relative to each window and relative to the screen, their **re**positioning becomes easy as you don't need to handle imperfect directions such as those in floating-mode.

It can get quite complicated if you have more than 4 windows in the same screen in your i3 setup, though.

Continuing with the previous bindsyms, another thing of note is that the i3 config file allows for variables, which can be set with… set. Since

```bash
set $up l
set $down k
set $left j
set $right semicolon
```

was already defined (in my case ccedilla instead of semicolon), instructions like

```bash
bindsym $mod+$left focus left
```

will then refer to MOD+j, whereas

```bash
bindsym $mod+Left focus left
```

refers to MOD+left arrow key.

There are several other keyboard shortcuts that follow this syntax:

`bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right`, and their respective arrow key equivalents for moving a window to a given direction in your screen, including between screens.

`bindsym $mod+h split h`, which will make the creation of new windows always open horizontally, that is, one immediately to the side of the other, and `bindsym $mod+v split v` vertically, that is, each window is tiled one above/below the other.

`bindsym $mod+f fullscreen toggle`, which will toggle the fullscreen state of the window.

`bindsym $mod+w layout tabbed`, which will fuse the titlebar of all windows which follow this layout, effectively bringing all of them into a tabbed interface. It's particularly effective on laptops. It does not seem to follow the first-letter convention for the sake of being able to type it easily with one hand, it seems. It does make sense with regard to hand positioning considering that w and s fit well with the position of the middle and index fingers.

`bindsym $mod+s layout stacking`, which will essentially do the same as the tabbed layout, but keeping each titlebar layered one above/below each other. This uses quite some vertical space, but is convenient with applications whose file path names are displayed on each titlebar.

`bindsym $mod+e layout toggle split`, which changes how two windows are aligned; if it's aligned vertically, then it will become aligned horizontally, and vice-versa.

`bindsym $mod+Shift+space floating toggle`, which toggles whether a window will float or tile. This keyboard shortcut is particularly odd in that most keyboard shortcuts involving MOD and Shift can be typed with only the left hand, but this one absolutely requires the right hand, despite its main key being quite close to the modifier, unless MOD is Alt. In addition, MOD will be below Shift and the main key, which is unusual as it's the only instance where this happens. If the user then attempts to use only the left-most key, the hand positioning becomes quite awkward. This keyboard shortcut must then be executed with the right hand on the main key, Space.

`bindsym $mod+space focus mode_toggle`, which will switch focus between tiled windows and floating windows. You see, tiling and floating are effectively containerized modes, so if one window is floating and another is tiled, MOD+arrows is unable to switch between them. This keybinding serves to cover this limitation, and it's semantically fitting with the previous keyboard shortcut.

`bindsym $mod+a focus parent` and `bindsym $mod+d focus child` are quite peculiar. As windows can also be grouped by tiling them in specific positions, the first keyboard shortcut pertaining to parent will transfer focus to a group of windows, which can then be tiled and moved however the user wants, all at the same time, whereas child serves to restore focus to its original state, only giving focus to a single window.

`bindsym $mod+Shift+minus move scratchpad` and `bindsym $mod+minus scratchpad show` are apparently more recent additions to i3, and effectively create "minimize" functionality. The first keyboard shortcut sends the selected window to an unaccessible workspace, thus hiding the window from your computer, whereas the latter will bring it back.

There is also the following syntax:

`bindsym $mod+Shift+q kill`, which unlike the previous two syntax has specific i3 functionality immediately after the keybinding. In this case, it's simply the order "close immediately/kill this window". I am not entirely sure it sends a SIGTERM signal to the application so it is closed cleanly, though. It seems to be SIGKILL. This key combination isn't particularly fortunate if MOD is Meta.

`bindsym $mod+Shift+c reload`, which will simply reload the config file. This is useful, for instance, if you want to apply your newly-written keybindings immediately to your session. This key combination isn't as terrible as the previous hand in terms of hand position since a specific position of the left hand can be achieved with the pinky finger on Shift, thumb on MOD and index on C, far less bizarre than with Space, but awkward for exclusive-left-use nevertheless.

`bindsym $mod+Shift+r restart`, which will effectively restart your entire session without logging out and back in again. As stated in the config file itself, it can be used when upgrading i3 and applying changes immediately, but it may also serve to reload other display elements, such as the bar.

Lastly, `bindsym $mod+r mode "resize"`, which requires some explanation, as it is unique. It assumes the following code prior to execution:

```bash
mode "resize" {
bindsym j resize shrink width 10 px or 10 ppt
bindsym k resize grow height 10 px or 10 ppt
bindsym l resize shrink height 10 px or 10 ppt
bindsym semicolon resize grow width 10 px or 10 ppt
bindsym Left resize shrink width 10 px or 10 ppt
bindsym Down resize grow height 10 px or 10 ppt
bindsym Up resize shrink height 10 px or 10 ppt
bindsym Right resize grow width 10 px or 10 ppt
bindsym Return mode "default"
bindsym Escape mode "default"
bindsym $mod+r mode "default"}
```

This block uses the second type of syntax and, essentially, serves to resize windows. This way, if the user presses `$mod+r`, this mode activates, and jkl; as well as the arrow keys, Enter and Esc will do different things from what they usually do. Arrow/jkl; keys **by themselves** will resize both the width and height of the currently selected window in 10 pixel increments, whereas the remaining keys serve to escape this mode.

The last syntax we'll see now actually comes in another block of various keyboard shortcuts, and all of them pertain to workspace navigation. Namely:

```bash
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"
```

which sets $wsNumber as Number, but if you'd like to avoid naming each workspace as numbers, Unicode is available here in-between parenthesis. The keybinding still corresponds to each respective number, though. That can be done, for instance, with set $ws 1:WhateverNameYouWant.

```bash
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10
```

which switches to a specific workspace according to their number regardless of whether they were already created or not.

```bash
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10
```

which moves a given window to a specific workspace. These here for some reason use several words for its function, that is, "move container to workspace number". Not anything particularly special with this syntax, and could arguably be said to be the same as the second syntax presented here, but it's a bit different regardless.

Those are all very easy to understand. However, I must note a few things about workspaces in i3.

Workspaces are created dynamically, but not like DEs like GNOME. While GNOME is a 1D environment in which every new workspace gets added dynamically at the end of the axis (the bottom-most one), and each workspace is assumedly assigned a position according to the point of origin (the top-most one), i3 does not care about the position between workspaces themselves, but about their number and their respective screen.

Perhaps dynamic isn't the most suitable word. On-demand should be more fitting? By default, i3 comes with 10 workspaces already enabled, but your access to them changes depending on your input.

To make things more clear: let's say we have a computer with GNOME and the user currently has three workspaces: the first one being the top-most and has a window in it, the second comes immediately afterwards and also has a window in it, the third comes immediately afterwards and is empty, being the bottom-most workspace. Opening a new window in the third workspace should create a fourth one; a new window in the fourth workspace creates a fifth workspace, and so on. Every workspace is created in relation to the previous one, and can be assigned numbers according to which one came before which one. This works like so on the main monitor, whereas any other monitor won't change workspaces.

In i3, if you have two screens, the main monitor on the left and the secondary monitor on the right, by default the main monitor contains workspace 1 and the secondary monitor contains workspace 2. If focus is on the main monitor and we move directly to workspace 3, then workspace 3 will appear immediately to the side of 1 on the main monitor, so we have 1 · 3. Moving to workspace 1 without doing anything will then delete workspace 3, as it's empty. If we then transfer focus to the secondary monitor and move to workspace 8, then workspace 8 will appear immediately to the side of 2 on the secondary monitor. So we have 2 · 8.

It is perfectly possible to have completely random combinations such as 1 · 3 · 6 · 0 on the main monitor and 2 · 4 · 5 · 7 · 8 · 9, for instance. Each workspace number absolutely must be organized in ascending order, however, as it would be particularly cumbersome for users to keep track of workspaces otherwise.

I would call it a 1D environment, perhaps, in that each screen would have one horizontal axis, but that would have to be disregarding its numeration and displacement in relation to the keyboard **sequence** of numbers from 1 to 0, which is typically followed through in other 1D environments. Perhaps naming this category a **Non-sequential 1D** environment would be more fitting, and the previously seen 1D DEs would be **Sequential 1D** environments.

Another interesting thing is that, by not being sequential while at the same time using numbers for moving between workspaces, i3 manages to avoid use of cardinal directions, thus confining cardinal directions to switching focus and confining numbers to workspaces. This also forces workspace movement to be done with both hands, which differs significantly from i3's main left-handed focus.

## Conclusion

Well, generally speaking i3 is quite the unusual beast compared to the DEs analyzed in this series. Mostly because it's simply a Window Manager and not a full-blown Desktop Environment, but also due to the way it handles workspaces.

Like GNOME, i3 keyboard shortcuts should be considered contextually, that is, in accordance with how the environment is set up. It is a particularly interesting case study, so to speak, but as far as navigation keyboard shortcuts go, it seemingly cannot be applied to Plasma, a 2D and 3D environment which necessarily uses cardinal directions for navigation. There are however several interesting ideas that i3 provides and could be implemented in Plasma if deemed appropriate, but that is for another time.

That is to say, I see several issues with hand positioning in many instances, and i3 does not always follow the same semantic scheme for defining keyboard shortcuts, but it does so rather well, and I'd say it's a very well-made keyboard-driven environment.

It is also highly focused on using the left hand for them, with little use of the right hand, which is not approved in several available studies on RSI, which suggest using both hands so as to distribute strain—but then again, from what I researched, studies properly verifying the validity of using only one hand instead of two and vice-versa are either nonexistent or scarce. I didn't find any systematic review evaluating this subject in particular either, but I'm not an ergonomics specialist nor am I a Physical Therapist. So there's the benefit of the doubt there, I suppose. Regardless of using one or two hands, if you want to actually choose one, you should be consistent in doing so.

 [1]: https://phabricator.kde.org/T11520
 [2]: https://rabbitictranslator.com/wordpress/index.php/2019/12/15/analyzing-cinnamon-keyboard-shortcuts/
 [3]: https://rabbitictranslator.com/wordpress/index.php/2019/12/02/analyzing-mate-keyboard-shortcuts/
 [4]: https://rabbitictranslator.com/wordpress/index.php/2019/11/10/analyzing-xfce-keyboard-shortcuts/
 [5]: https://rabbitictranslator.com/wordpress/index.php/2019/10/14/analyzing-gnome-keyboard-shortcuts/
 [6]: https://github.com/i3/i3/blob/next/etc/config
 [7]: https://github.com/manjaro/desktop-settings/blob/master/community/i3/skel/.i3/config
 [8]: https://github.com/swaywm/sway/blob/master/config.in
