---
title: Analyzing MATE keyboard shortcuts
author: Blumen Herzenschein
date: 2019-12-02
url: mate-shortcuts
aliases: /wordpress/index.php/2019/12/02/analyzing-mate-keyboard-shortcuts/
categories:
  - Keyboard Shortcuts Analysis
---

Hello yet again! For those who are not acquainted with this series, I am in an endeavor to analyze keyboard shortcuts in most major DEs so that we, the KDE community, can decide on the best defaults for KDE Plasma. Last time I analyzed [XFCE][1] and before that, [GNOME][2].

This time we will also check on a non-keyboard-driven environment, MATE. I personally quite like MATE, mostly for two things: MATE Tweak's ability to change panel layouts, and the Software Boutique, which looks as fashionable as its name.

## Preparations

For testing MATE, I installed full ubuntu-mate-desktop on my work machine and used virtual machines containing Debian, Ubuntu MATE and Manjaro.

This time, one of the candidates for the virtual machine was chosen based on a project currently being held at the public university I graduated in, namely the University of São Paulo, in São Paulo, Brazil. I chose Debian MATE in honor to [the plans to migrate the computers at the computer section of FFLCH][3] to Linux, the humanities school. Pragmatically speaking, Debian is also a good choice for usually keeping defaults as is for each desktop environment.

For sources, I simply used MATE's keyboard shortcuts application and its corresponding manual.

## Navigating from home and sailing till the end

One way MATE made my life easier is that no distribution seems to change the defaults significantly, similarly to GNOME. Thus, I was able to refer to MATE keyboard shortcuts instead of each distro tested.

I used Debian as the base for this analysis because, while there were no significant differences between distributions, Debian kept most keyboard shortcuts active, whereas Ubuntu MATE and Manjaro MATE chose to hide most of them.

Having been concerned with the MATE implementation of the menu keyboard shortcut considering XFCE had some issues and both are GTK-based desktop environments, this was the first thing I verified. No, the menu is not affected by Meta-related keybindings, which is nice.

However, in what concerns the Meta key, MATE barely uses it. The accessibility keyboard shortcuts use it and follow the simple rule previously seen in XFCE, namely that of first-letter, so Meta+Alt+M, S and K correspond to **_M_**agnifier, **_S_**creen reader and on-screen **_K_**eyboard, so while maybe a concern for the visually impaired, it is at least consistent.

The only other use of Meta lies within snapping keyboard shortcuts, namely Meta+Ctrl+Right/Left/Home/End/PgUp/PgDown. Right and Left naturally refer to Right and Left snapping, but the other choices are more interesting.

Given that Up and Down snapping seem to be disregarded (they do not follow the convention shown with right and left, as they use Ctrl+Alt instead of Ctrl+Meta, in addition to conflicting with other keybindings), the only remaining snapping possibilities are the corners of the screen.

Given that Home assumes the upper left position in the typical 4-key set of traditional keyboard, it snaps the selected window to the upper left corner. The same logic is applied to End, PgUp and PgDown, which are bottom left, upper right and bottom right, respectively. I will name the Home/End/PgUp/PgDown keyset as Home-End for easy reference.

Funnily enough, those were not enabled by default in any system tested. Such keyboard shortcuts come with (keypad) written immediately after the keyboard shortcut description, and this (keypad) disappears when you set the keyboard shortcuts manually.

Similar to snapping capabilities, an interesting feature is the ability to move windows to all typical snapping directions, while also not effectively snapping windows. That is, the window is not resized so as to fit a perfectly-sized chunk of the screen, but it does move to that specific corner. The correspondent keyboard shortcuts are Ctrl+Alt+arrow keys or Home-End.

Note how, as previously mentioned, snapping Up and Down use Ctrl+Alt+Up/Down, which conflicts with Ctrl+Alt+Up/Down for moving Up/Down.

Immediately afterwards in the MATE keyboard shortcuts application, 8 keybindings noticeably have no keybindings assigned: namely moving windows to top, bottom, left and right monitors, and moving windows to the first four workspaces.

This was a curious choice. The first bothers me similarly to XFCE and Plasma since no keyboard shortcut is set by default for switching between left and right monitor, an essential feature when dealing with multi-monitor setups. Moving windows in a vertical axis is something more elaborate which requires extra gear and assembly to do, so it's rare, thus it makes sense to not have Up and Down by default.

The latter can be thought either as making sense or not. It theoretically would not make sense because the following four keyboard shortcuts move windows between workspaces in a 2D environment, that is, two axes of workspace movement, vertical and horizontal. Thus, moving windows to the first four workspaces seem, at least initially, necessary for consistency.

However, it does make sense after seeing the keys chosen for the keyboard shortcut, that is, the Ctrl+Alt+Shift+arrow keys. This is a 4-keys keycombo, although it could be argued to function as three, since a fourth finger to press Ctrl or Shift is optional. By analogy, MATE would have to use Ctrl+Alt+Shift+numbers or F# in order to be consistent with the remaining keyboard shortcuts.

I'd rather avoid three keystrokes or more for a keyboard shortcut, but my impression so far is that once keybindings are assigned to snapping, screen movement, workspace movement, and moving windows between screens, the last one in the hierarchy, moving windows between workspaces, ultimately end up using a 3- or 4-keys keycombo.

## I thought I had sailed afar, but it's the same sight

Aside from navigation and the fact Meta is barely used, there were several similarities between MATE, XFCE and GNOME.

Similarly to XFCE, MATE uses the XF86 keys extensively. However, unlike XFCE, the only keyboard shortcut it sets for actual application opening is the terminal, Ctrl+Alt+T. The lockscreen is evoked with our typical Ctrl+Alt+L, and Ctrl+Alt+Delete serves to shutdown.

Also similarly to XFCE, as well as Plasma, Alt+F1 opens the menu, Alt+F2 opens the Run Application dialog box. Alt+F3 is not set up, however.

Alt+F5 is Restore, Alt+Esc switches between windows immediately and Alt+F6 switches between windows of a same application, clearly a GNOME influence. As previously stated in my GNOME analysis, \` and F6 have no semantics associated with them, and so MATE inherits this specific GNOME issue here.

Alt+' for moving between windows of a same application also seems to be a GNOME influence.

Alt+F7, F8, F9 and F10 are Move, Resize, Minimize and Maximize Toggle. All of them are neatly grouped. I really like how Minimize and Maximize have this specific order and are set immediately before F11, the traditional key for Fullscreen. Move and Resize are also actions that fill the gaps nicely. Sadly, F5 restore is too far from the other similar titlebar actions set in F8-F10, but this is compensated by F10's maximize being a toggle. In theory, this invalidates the usefulness of F5, which could be an issue in terms of keyboard shortcut organization.

It should be noted that F12, while not having anything assigned to it, can be set to open Tilda, a drop-down terminal much similar to Yakuake.

Alt+' for moving between windows of a same application also seems to be a GNOME influence.

## Conclusion

That is it, essentially. MATE is not a keyboard-driven environment, but the devs clearly thought of making it consistent.

It sacrifices several things for this consistency, however: barely any use of Meta, which reduces its keyboard shortcut repertoire, leading to a 4-keys keycombo; barely any application shortcuts, with the exception of the terminal; accessibility keybindings that require reading instead of physical placement or layout of keys in the keyboard; loss in semantics for the choice of modifiers; and all defaults (since several keyboard shortcuts are unset by default).

The last point effectively proves MATE was not striving for a keyboard driven-environment, but they did include relatively sane defaults for those who wish to use it this way. MATE has been, by far, the most consistent out of the three DEs, but also rather limited due to its sacrifices.

Its defaults work well within its context and it comparatively has less issues than GNOME or XFCE, but I do not think it is a keyboard shortcut model Plasma should strive for, despite its fitting 2D environment. I do not think such sacrifices fit the vision I have for Plasma.

MATE however is a great example of minimalism applied to a 2D environment, which itself is rather complex to implement. Some things could be improved, such as reconsidering the semantics of the modifier keys, removing unset (keypad) from certain particularly sane keyboard shortcuts that the devs think are the most sensible for an out-of-the-box keyboard-driven experience, and including a more friendly multi-monitor set of keybindings by default.

 [1]: https://rabbitictranslator.com/wordpress/index.php/2019/11/10/analyzing-xfce-keyboard-shortcuts/
 [2]: https://rabbitictranslator.com/wordpress/index.php/2019/10/14/analyzing-gnome-keyboard-shortcuts/
 [3]: http://proaluno.fflch.usp.br/comissao-linux-na-pro-aluno
