---
title: Contributing to KDE is easier than you think – Phabricator patches using the web interface
author: Blumen Herzenschein
date: 2020-05-03
url: phabricator
aliases: /wordpress/index.php/2020/05/03/contributing-to-kde-phabricator-web-interface/
tags:
  - Planet KDE
---

Heya! This post will be ridiculously brief and simple, albeit filled with screenshots.

As usual: This is a series of blog posts explaining different ways to contribute to KDE in an easy-to-digest manner.

The purpose of this series originated from how I feel about asking users to contribute back to KDE. I firmly believe that showing users how contributing is easier than they think is more effective than simply calling them out and directing them to the correct resources; especially if, like me, said user suffers from anxiety or does not believe they are up to the task, in spite of their desire to help back.

Last time I explained [how translators with a developer account have a really straightforward workflow][1] and [how the entire localization process for KDE works][2]. I’ve also posted [a little article I made some time ago on how to create a live-test environment to translate Scribus more easily][3], given that [Scribus might become a KDE application][4] in the future.

This post explains the process of sending your first patch to KDE. This tutorial, of course, is only useful for small patches, likely those which alter only one file, as the web interface is convenient for such cases but not when there is a ton of files from the same project.

I recently learned how to do this, so I really got excited to write a tutorial about it. However, it's not worth adding it to the wiki as of now since the KDE infrastructure is being migrated to [Gitlab][5]. So instead I'm writing this blog post. Seriously, it's so ridiculously easy that I honestly believe if this had been promoted before there would be many more patches coming from people who are still in the process of getting used to the terminal. Or those who simply prefer using a GUI, like me.

For more information about sending patches, please refer to <https://community.kde.org/Get_Involved/development>}} and <https://community.kde.org/Infrastructure/Phabricator>}}.

## Creating your patch

First of all, I decided what I wanted to change. If you check <https://phabricator.kde.org/T12810>}}, you'll see the only change I'll be doing to the selected projects is switching the contents of the `GenericName` field. That is, I'll be changing a single line of code in one file of each project. I'll start with [GCompris][6].

{{< figure src="/2020/05/Screenshots-0503_1329.png" caption="Figure 1 - Finding out what to change." >}}}}

GCompris is not yet on KDE Invent. So, for downloading the project, I'm going to git clone it from [cgit.kde.org][7]. While the interface is not the best, it serves its purpose and it's lightweight.

On cgit.kde.org, I press `Ctrl+F` to find the project's repository. It's available [here][8].

We'll need to copy the link portrayed on the screenshot, it's in the bottom left corner of the page. You can either select it or right-click and copy its address.

{{< figure src="/2020/05/Screenshots-0503_1405.png" caption="Figure 2 - Getting the repository address for cloning." >}}

Next we'll be using a terminal command. Don't worry, it's easy to follow and it's minimal too. For this I like using [Yakuake][9], sometimes I use [Konsole][10], but for this tutorial, I'll be using [Dolphin][11] to keep it more GUI-friendly.

Open a Dolphin window and press `F4` to open its terminal. It should open on your home directory, so you can download the repository there, but with Dolphin you can simply navigate to the folder you want and clone the repository, for instance, inside `~/Documents/Patches`.

Please install `git` if you don't yet have it on your system. This is the only thing you'll need to install.

After selecting the desired folder and pressing F4, download the repository using git using the previously copied link. It will look similar to the command `git clone git://anongit.kde.org/gcompris.git`:

{{< figure src="/2020/05/Patches-0503_1412.png" caption="Figure 3 - Cloning from Dolphin." >}}

The above screenshot is only for illustration purposes, I cloned mine inside my home (`~/`) folder.

After that, you should have something like the following screenshot.

{{< figure src="/2020/05/gcompris-0503_1334.png" caption="Figure 4 - Repository downloaded." >}}

Open the desired file with e.g. [Kate][12] and make the desired change. In my case, I need to change `org.kde.gcompris.desktop`:

{{< figure src="/2020/05/org.kde_.gcompris.desktop-0503_1332.png" caption="Figure 5 - Changing the desired files." >}}

Don't forget to save your changes. Now back to Dolphin, go back to its terminal and type `git diff`. Yes, the same tool we used for downloading the repository we use to create the patch! Git is really the only requirement for this tutorial. You should have something like this:

{{< figure src="/2020/05/gcompris-0503_1334-1.png" caption="Figure 6 - Checking the diff." >}}

{{< figure src="/2020/05/gcompris-0503_1335.png" caption="Figure 7 - Diff output." >}}

The original text which was removed is shown in red and prefixed with a minus, the text added in its stead is shown in green and prefixed with a plus. Just be sure the changes you did are correctly depicted in green.

For the patch, as we will see later, you can simply copy what shows up in your terminal. But another way to do that is by creating a file and sending its contents instead. If you want to create a file, use `git diff >}} filename.diff`. I used the extension `.diff`, but it doesn't really matter; you can name it `.txt` or `.patch`, for instance. For checking if everything is alright, you can then open it by right-clicking the file and opening it with Kate, or you can run `kate gcompris.diff` as shown below.

{{< figure src="/2020/05/gcompris-0503_1335-2.png" caption="Figure 8 - Creating a diff file to send as a patch." >}}

## Sending your patch

Let's now go to Phabricator. Before submitting a patch, you'll need a [KDE Identity account][13]. You don't need to have a developer account. With a KDE Identity, you can login to Phabricator which is located on [phabricator.kde.org][14].

After logging into Phabricator, click on Code Review on the upper left corner of the page.

{{< figure src="/2020/05/Screenshots-0503_1348.png" caption="Figure 9 - Code Review location." >}}

On the next window, click on Create Diff on the upper right corner.

{{< figure src="/2020/05/Screenshots-0503_1349.png" caption="Figure 10 - Create Diff location." >}}

On the next window, if you want to send the diff file, click on the `Browse...` button next to `Raw Diff From File` and select the file.

{{< figure src="/2020/05/Screenshots-0503_1349-1.png" caption="Figure 11 - Create Diff window with already selected file." >}}

If you don't want to send a file but instead you just want to paste the diff itself, you can do so on the Raw Diff section. Just remember: **_choose ONE of those two methods. Don't send the diff through both methods_**.

{{< figure src="/2020/05/Screenshots-0503_1350.png" caption="Figure 12 - Create Diff window with diff content." >}}

I'll repeat: **_choose ONE of those two methods. Don't send the diff through both methods_**.

Also important: **Don't forget to add the correct repository name**. In my case, it was this one:

{{< figure src="/2020/05/Screenshots-0503_1504.png" caption="Figure 13 - How it looks like after selecting the correct repository." >}}

On the next window, simply accept the default for creating a new revision, confirm and go to the next window. It will look similarly to the following:

{{< figure src="/2020/05/Screenshot_2020-05-03-normal.png" caption="Figure 14 - Create Revision window (for desktop readers)." >}}

For the users following this tutorial from their mobile, here's a better framed screenshot:

{{< figure src="/2020/05/Screenshot_2020-05-03-zoom.png" caption="Figure 15 - Create Revision window (for mobile readers)." >}}

Fill each field accordingly. Add a descriptive title and a short summary explaining what the patch does, and in the reviewers, repository, tags and subscribers sections, add the correct project. Just by typing the program name Phabricator should suggest the correct project for you, just click on the one you think is right. Again, in my case, it was GCompris.

After filling the fields correctly, click on Create New Revision.

And done! Your patch will look similar to this:

{{< figure src="/2020/05/Screenshot_2020-05-03-finished-normal.png" caption="Figure 16 - Patch sent!" >}}

That's it for sending your first patch through the web interface of Phabricator. If your patch has any issues, those should be pointed out during the review stage by the more experienced contributors responsible for the project.

The most important thing that should be done is getting in touch with the community. If you want to know more about contributing, please read [this very clear wiki page on Getting Involved][15]. There's [Matrix][16], [Telegram][17] and [IRC][18] for instant messaging.

 [1]: https://rabbitictranslator.com/wordpress/index.php/2020/02/24/contributing-to-kde-simple-localization/
 [2]: https://rabbitictranslator.com/wordpress/index.php/2020/01/19/contributing-to-kde-localization/
 [3]: https://rabbitictranslator.com/wordpress/index.php/2020/02/17/live-test-for-translating-scribus/
 [4]: https://phabricator.kde.org/T10034
 [5]: http://invent.kde.org
 [6]: https://kde.org/applications/education/org.kde.gcompris
 [7]: https://cgit.kde.org
 [8]: https://cgit.kde.org/gcompris.git/
 [9]: https://kde.org/applications/system/org.kde.yakuake
 [10]: https://kde.org/applications/system/org.kde.konsole
 [11]: https://kde.org/applications/system/org.kde.dolphin
 [12]: https://kde.org/applications/utilities/org.kde.kate
 [13]: http://identity.kde.org
 [14]: https://phabricator.kde.org
 [15]: https://community.kde.org/Get_Involved
 [16]: https://community.kde.org/Matrix
 [17]: https://community.kde.org/Telegram
 [18]: https://userbase.kde.org/IRC_Channels
