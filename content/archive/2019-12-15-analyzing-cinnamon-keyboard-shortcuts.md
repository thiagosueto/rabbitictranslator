---
title: Analyzing Cinnamon keyboard shortcuts
author: Blumen Herzenschein
date: 2019-12-15
url: cinnamon-shortcuts
aliases: /wordpress/index.php/2019/12/15/analyzing-cinnamon-keyboard-shortcuts/
categories:
  - Keyboard Shortcuts Analysis
---

Hello yet again, once again! For those who are not acquainted with this series, I am in [an endeavor to analyze keyboard shortcuts][1] in most major DEs so that we, the KDE community, can decide on the best defaults for KDE Plasma. Last time I analyzed [MATE][2], and before that, [XFCE][3].

This time we analyze Cinnamon, a non-keyboard-driven environment that quite surprised me. I didn't recall it was the case (I've used Cinnamon as a replacement for Windows 7 for some time in an old machine), but Cinnamon is actually quite similar to Plasma. It has quite surprised me, but this will stay for another day.

One thing of note I will do on my next post in the series will be breaking the order I've followed until now for which DE to analyze, which was the list I made on the Phabricator task which is being tracked in this blog series. This is so because we're close to Plasma 5.18, which is an important milestone to KDE—it will be an LTS version which should likely ship with LTS Kubuntu. Thus, I'll focus first on keyboard-driven environments and speed things up for quicker decision-making.

Oh, and I've had my birthday on the 12th of December! As a treat to myself, I tweaked the blog a bit. Weirdly enough, if I schedule my posts correctly, this post should be up three days after my birthday, the next should be three days prior to christmas and the next should be three days prior to New Year's eve!

## Preparations

For testing Cinnamon, I (obviously) installed Linux Mint Cinnamon on a virtual machine, followed by Void Cinnamon and Manjaro Cinnamon. Void and Manjaro seemed to be close enough to bleeding edge so that I could track any differences in Cinnamon that may have occurred since Mint's latest release. Linux Mint is a fairly conservative distribution, after all. Void seems to be like Debian, keeping the defaults provided by the DE, whereas Manjaro is a heavily-customized, so I expect things to be rather different.

At my work machine I installed cinnamon-desktop-environment on my Kubuntu 19.04 with backports. However, since Cinnamon has the largest dependency on GNOME libraries and programs from the desktop environments seen here so far, I manually removed applications which were duplicates of my Qt applications, so there shouldn't be any compromises to how keyboard shortcuts are handled on Cinnamon.

For sources, I used Cinnamon's keyboard shortcuts application, similarly to what I did with MATE.

After having tested the different DEs, I found out that none of them did anything differently, and so I assume the defaults for Cinnamon were kept as is, which makes this analysis way easier!

## Context always matters!

Cinnamon is blatantly a 1D environment—meaning it only includes one axis of movement between workspaces—and it has 4 workspaces by default that are lined horizontally. It does not make much use of them, however. Though there are multiple workspaces and keybindings for moving left and right between them, namely Ctrl+Alt+Left/Right, there are no keyboard shortcuts for switching to a specific workspace like XFCE, nor is there any way to go back to the first nor to the last workspace immediately like GNOME. Workspaces do not loop either, meaning if you're on the 4th workspace and press the correct keycombo for the right workspace, you won't go back to workspace 1. This can be desirable behavior, of course, so there's no issue there, and likewise, Cinnamon is not designed around keyboard shortcuts like GNOME is.

Moving a window to a different workspace was, unsurprisingly, a combination of Ctrl+Alt and Shift, namely Ctrl+Alt+Shift+Left/Right. It is a four-key keycombo, which as I stated before, is quite bad in terms of RSI, but it's one we can get away with since Ctrl and Shift are close enough so as to function as one key.

What was surprising, however, was the rest of the Ctrl+Alt key group, namely Ctrl+Alt+Up/Down. The latter would toggle a smart GNOME-like window overview, the Window Selection Screen, whereas the first would toggle the Workspace Selection Screen, a coordinate-based workspace overview. Plasma also includes both by default on the desktop—Present Windows and the Workspace Grid.

In the case of the first, Ctrl+Alt+Up, its alternative keycombo was Alt+F1, which is also an interesting choice considering that until now this was the canonical keyboard shortcut for menu or menu-like functionality.

Another typically highly missed functionality that is present in Cinnamon was the ability to switch between screens. It was an interesting choice too: Meta+Shift+Left/Right/Up/Down. This is clearly due to the semantics of Shift meaning transfer; if Ctrl+Alt was already used for workspace movement, it would only make sense to use another set of modifier keys (Meta) for screens as opposed to workspaces.

Its Up and Down variants are unusual, however. Mostly because it is more likely that a company might have a horizontal multi-monitor setup than it a vertical setup, which requires more money, preparation and a minimal "physical infrastructure" for setting monitors this way. This was likely done so simply to make the respective keyboard shortcuts consistent and more inclusive, however niche a vertical setup might be.

Now, what I really appreciated in Cinnamon was snapping, tiling, or whatever you might want to call it.

While Meta+arrows do the expected behavior—albeit not optimal—of tiling windows left, right, up and down, what Cinnamon calls "push snap" is effectively contextual tiling!

With tiling, windows aren't being transferred to a different digital site, Shift makes no sense here, and so Meta+Ctrl+arrows is assigned to push snapping.

This is a bit difficult to explain to whomever has never seen it in action, but I'll try exemplifying it. First I must say it is an inherent toggle-based kind of keyboard shortcut. That is, each state that is toggled determines what the keyboard shortcut will do next. This is different from traditional keyboard shortcuts in that traditional keyboard shortcuts are unaware or disregard the current state a window is in.

For instance, if I set maximize window in Cinnamon to Alt+F9, regardless of the state of the current focused window, pressing such a keyboard shortcut will make the window maximized, even if it is already maximized. Pressing it again will make it try to, once again, maximized, thus showing no effect. I'll call this an absolute keyboard shortcut, or a Level 1 (L1) keyboard shortcut. The default Alt+F10 keycombo, however, _toggles the maximize state of a window_. That is, if a window is in its normal state and Alt+F10 is pressed, it will become maximized; if it is pressed again, it will return to its original state; if pressed again, it becomes maximized once again. I'll call this a toggle keyboard shortcut, or Level 2 (L2).

Push snapping, or contextual snapping as I'd prefer calling it, is one level beyond that. If a window is in its normal state and **_i)_** Meta+Ctrl+Up is pressed, it will tile to the upper part of the screen similarly to Meta+Up; however, if **_ii)_** Meta+Ctrl+Right is pressed while the window is in this state, instead of tiling to the right portion of the screen, it will tile to the upper right corner. If **_iii)_** Meta+Ctrl+Left is pressed while the window is tiled to the left, it will return to its original state; pressing **_iv)_** Meta+Ctrl+Right proves this fact by once again tiling the window to the right; if **_v)_** Meta+Ctrl+Down is then pressed, the window will be tiled to the right portion of the screen. If, once again, **_vi)_** Meta+Ctrl+Down is pressed, it will be tiled to the bottom right part of the screen.

This is one level beyond because, aside from the behavior similar to L1 (so the example shown in i), it also displays behavior typical of L2, namely that shown in iii and iv; all of that while showing contextually-aware behavior, as seen in ii, iii, v and vi. Hence, I think calling it an L3 keyboard shortcut is fair. I would imagine that, as keyboard shortcut behavior evolves, window management becomes more complex and difficult to maintain for developers, though this is a mere assumption.

Another example to clarify: suppose you have a single window in its normal state (non-maximized), and want to snap it to the bottom left corner. That means that you have two paths available to achieve this: down left, and left down.

This may not be the most direct way to achieve this, as shown in my MATE analysis, however it is intuitive once you've used it at least once, as I see it. This is actually Windows-like behavior, and it is in my very tiny list of things Windows does right. I find it likely Cinnamon devs implemented this as an influence of Windows and not because of another DE, given their goals of being a user-friendly desktop environment that caters to Windows users.

What is noteworthy about L3 keyboard shortcuts is the fact they effectively solve the issue of window tiling requiring two different sets of keys to work, namely the arrow keys (Left, Right, Up, Down) and the Home-End keys (Home, End, PgUp, PgDown). With contextually-aware snapping, a DE does not require being limited to the directions provided by the arrow keys, and if you've been following this series for long enough, you might have noticed how DEs do different things to compensate for the lack of enough keys with specific coordinate semantics.

## Some oddities and similarities

There aren't many _unique_ keyboard shortcuts in Cinnamon: only 35. The rest are determined by XF86 keys (those you may have on your keyboard containing small icons depicting specific functionality). That's why it only took five hours total to write this blog post, and it only took two weeks of testing to guarantee the keyboard-driven workflow was correct.

There's little to be said now aside from the Alt+F# keycombos, accessibility keyboard shortcuts and some things Cinnamon does similar to Windows and Plasma.

We've already seen that Alt+F1 shows, weirdly enough, the workspace selection screen instead of the menu. Alt+F2 follows the tradition of being assigned to the run dialog; Alt+F4 closes windows, as expected; Alt+F5 unmaximizes windows, which is actually rather unexpected considering that the maximizing toggle already exists; Alt+F7 moves a window and Alt+F8 resizes it. Alt+F10, as stated before, toggles the maximized state, and that's it.

It must be said that F7 and F8 assigned to move and resize have functioned as standards in all environments seen here so far, that is, move and resize. Xubuntu does something quite elaborate and deviates from this, but default XFCE works like Cinnamon here, as well as GNOME and MATE.

A curious thing I've found is the use of Alt+Space for activating the window menu. This seems to be also true for XFCE and GNOME, though not for MATE. This might be a standard, however, as previously stated in my GNOME analysis, I think it is not so efficient to assign Alt+Space to rather niche functionality such as the popup concerning window options—especially since most of the functionality it makes available is redundant to other keyboard shortcuts—but this might simply be me acting as an opinionated rabbitic user whose workflow does not include the window menu.

An oddity I've found is the assignment of Meta+L to Looking Glass, which is supposed to be an accessibility feature, but which is not activated by it at all. The Cinnamon Debugger is activated by it instead, and I believe this might not be intended and that there is a keyboard shortcut conflict.

Lastly, there are a few keyboard shortcuts based on the first-letter rule.

As is usual in several DEs, Ctrl+Alt+T opens the terminal. Zoom in is Meta+= and zoom out is Meta+-, which, as stated before, makes sense in that zooming is based on the semantics of + and -. This is also what Plasma uses.

Show Desktop is Meta+D, the File Manager is opened with Meta+E and Meta+P for redetecting displays (coming from Projector/Presentation mode); all of those are also present in Windows, something which Plasma has followed partially as well.

Two keycombos are fairly unique to Cinnamon as far as I know, however: Meta+O locks the orientation of the screen, which is something I haven't tested but can only assume it means that if you have a vertically-positioned monitor, then your screen setup should be saved and carried out throughout sections, but I can only assume. The other is Meta+S, which is rather interesting as well: it allows to display the desklets (akin to Plasma widgets) currently available (and hidden by other windows) on your desktop by bringing them to the foreground, that is, in front of whatever windows you might have on your desktop. It's also a togglable keycombo in that pressing it once activates it and pressing it a second time deactivates it.

Speaking of unique functionalities, one that I found interesting (however badly implemented) was that, when snapping a window manually with the mouse and waiting a little, a popup shows up displaying how to use a hidden feature of Cinnamon, namely snapping the window to a different workspace. The implementation is clunky, however; while keeping your hand on the mouse which stands on the place you want to snap, you must press Ctrl+left/right to switch between workspaces. This is highly uneffective with this specific keyboard shortcut, since it's quite hard to type an awkward dual-key combination while keeping your right hand on the mouse. This would be interesting, however, if it did not require Ctrl.

## Conclusion

It is not a complex desktop in terms of keyboard shortcuts by any means (being similar to MATE in this regard), and it clearly tries to be similar to Windows, succeeding in this endeavor. As a conservative desktop environment, it restricts itself to few keyboard shortcuts, which is a sure method of working well if the devs effectively care about keyboard driven workflows. They seemingly do, considering that navigation keyboard shortcuts in Cinnamon differed from Windows, so a copycat Cinnamon is not.

Considering that Cinnamon does not use a 2D environment nor does it have enough keyboard shortcuts, I don't think it is viable as a general reference to be used for current Plasma, but I do believe its use of contextual keyboard shortcuts, likely inherited from Windows, is something that must be taken into consideration for complex keyboard-driven 2D environments.

 [1]: https://phabricator.kde.org/T11520
 [2]: https://rabbitictranslator.com/wordpress/index.php/2019/12/02/analyzing-mate-keyboard-shortcuts/
 [3]: https://rabbitictranslator.com/wordpress/index.php/2019/11/10/analyzing-xfce-keyboard-shortcuts/
