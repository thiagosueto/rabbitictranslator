---
title: "About me"
author: Blumen Herzenschein
---

{{< image src="/2023/01/flowers_heartshine_no_bg-240x240-1.png" style="width:20%" position="center" >}}

Hello, and welcome to my website. It was made using [Hugo](https://gohugo.io/) and the awesome [hello-friend-ng theme](https://themes.gohugo.io/themes/hugo-theme-hello-friend-ng/). This is mostly just a blog for my personal musings and findings.

I'm a furry from Brazil, stern but kind hearted.

I studied Brazilian Portuguese and German for five years at the University of São Paulo and worked as a translator and proofreader of academic papers since 2018. My main language pair is Brazilian Portuguese <-> US English.

I currently work with technical documentation, which is fitting since I like to help others.

My interests involve:

* Software: I don't care much about hardware. I've taken a liking to the open source philosophy and I'm generally more fond of copyleft licenses. I dabbled with C, C#, JavaScript and Perl, but C++ is what sticked to me; GUI development is awesome, and Qt has made C++ more enjoyable to me. I don't find algorithms as fun as creating a program I can actually visualize. KDE is my software community of choice.

* Fandoms: I like reading articles about social psychology, especially group dynamics, and I quite love the academic work done by the [International Anthropomorphic Research Project](https://furscience.com/) studying the furry fandom and breaking most misconceptions about it.

* Health: I enjoy learning about and doing things that can help me prevent potential health issues down the line, like exercise techniques, stretches, using ergonomic hardware, and getting fit so I look good in my programming socks. I follow YouTube channels like Institute of Human Anatomy.

* Games: Not as much a fan of AAA as much as a fan of games with interesting dynamics, lore, and design. I prefer RPGs and Metroidvanias, a few FPS. Videogame music is my main taste for music, so much so that I know very little stuff to sing at the karaoke. I also follow a few speedrunners and would do this myself if I had better hardware. I follow YouTube channels like Nitro Rad, Design Doc, Daryl Talks Games and 8-bit Music Theory.

* Narratology: I'm interested in the narrative techniques used in several media, like books, series, movies and games. This applies also to cinematography and how camera angles, soundtrack and other such techniques change the narrative aspect of a piece. I follow YouTube channels like Savage Books and Hello Future Me.
