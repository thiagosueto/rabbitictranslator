# rabbitictranslator

This repo has an initial port of my own blog to Hugo.

Install `go` and `hugo`, then run `hugo server`.

The website can then be accessed in your browser with http://localhost:1313.
